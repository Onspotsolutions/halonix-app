package com.halonix.onspot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private ConnectionLostCallback listener;

    public MyBroadcastReceiver() {
        super();
    }

    public MyBroadcastReceiver(ConnectionLostCallback listener) {
        this.listener = listener;    //<-- Initialze it
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (isOnline(context)) {
                listener.connectionLost(true);
            } else {
                listener.connectionLost(false);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }


    }
}
