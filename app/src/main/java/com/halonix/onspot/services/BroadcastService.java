package com.halonix.onspot.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;

import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.utils.Constants;


public class BroadcastService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        Log.d(Constants.TAG, "onTaskRemoved: " + provider);
        if (provider.contains("gps")) { //if gps is enabled
            GetCurrentLocation lListener = new GetCurrentLocation(this);
            lListener.stopGettingLocation();
        }
        stopSelf();

    }

    @Override
    public void onDestroy() {
        Log.d(Constants.TAG, "onDestroy: ");
        super.onDestroy();
    }

}
