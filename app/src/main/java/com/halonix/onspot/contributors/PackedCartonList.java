package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackedCartonList {
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("packing_ratio")
    @Expose
    private Integer packingRatio;
    @SerializedName("product_name")
    @Expose
    private String productName;

    @SerializedName("sender_id")
    @Expose
    private String sender_id;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPackingRatio() {
        return packingRatio;
    }

    public void setPackingRatio(Integer packingRatio) {
        this.packingRatio = packingRatio;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }
}
