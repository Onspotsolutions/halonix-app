package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class sale_history_list {


    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("sale_date")
    @Expose
    private String saleDate;
    @SerializedName("mfg_date")
    @Expose
    private String mfgDate;
    @SerializedName("sale_type")
    @Expose
    private String saleType;

    public sale_history_list(String code, String model, String saleDate, String mfgDate) {
        this.code = code;
        this.model = model;
        this.saleDate = saleDate;
        this.mfgDate = mfgDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(String mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }
}
