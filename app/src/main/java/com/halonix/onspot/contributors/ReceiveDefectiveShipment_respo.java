package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.halonix.onspot.contributors.ReceiveDefectiveShipmentList_respo;

import java.util.ArrayList;

public class ReceiveDefectiveShipment_respo {
    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("data")
    @Expose
    private ArrayList<ReceiveDefectiveShipmentList_respo> data = null;

    @SerializedName("code_list")
    @Expose
    private ArrayList<ReceiveDefectiveShipmentList_respo> code_list = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<ReceiveDefectiveShipmentList_respo> getData() {
        return data;
    }

    public void setData(ArrayList<ReceiveDefectiveShipmentList_respo> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ReceiveDefectiveShipmentList_respo> getCode_list() {
        return code_list;
    }

    public void setCode_list(ArrayList<ReceiveDefectiveShipmentList_respo> code_list) {
        this.code_list = code_list;
    }
}
