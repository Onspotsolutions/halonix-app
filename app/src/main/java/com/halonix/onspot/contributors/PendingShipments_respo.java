package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PendingShipments_respo {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("list")
    @Expose
    private ArrayList<pending_shipmentsLIst> list = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<pending_shipmentsLIst> getList() {
        return list;
    }

    public void setList(ArrayList<pending_shipmentsLIst> list) {
        this.list = list;
    }
}
