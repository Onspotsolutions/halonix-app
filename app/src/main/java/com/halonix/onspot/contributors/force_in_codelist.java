package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class force_in_codelist {
    @SerializedName("is_master")
    @Expose
    private Boolean isMaster;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("packing_ratio")
    @Expose
    private Integer packingRatio;
    @SerializedName("defectiv")
    @Expose
    private Boolean defectiv;

    @SerializedName("allow_to_defect")
    @Expose
    private Boolean allow_to_defect;

    public Boolean getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Boolean isMaster) {
        this.isMaster = isMaster;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getPackingRatio() {
        return packingRatio;
    }

    public void setPackingRatio(Integer packingRatio) {
        this.packingRatio = packingRatio;
    }

    public Boolean getDefectiv() {
        return defectiv;
    }

    public void setDefectiv(Boolean defectiv) {
        this.defectiv = defectiv;
    }

    public Boolean getAllow_to_defect() {
        return allow_to_defect;
    }

    public void setAllow_to_defect(Boolean allow_to_defect) {
        this.allow_to_defect = allow_to_defect;
    }
}
