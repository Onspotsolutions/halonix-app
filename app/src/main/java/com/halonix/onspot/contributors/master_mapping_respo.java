package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class master_mapping_respo {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("list")
    @Expose
    private ArrayList<SearchHistoryModel> list = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SearchHistoryModel> getList() {
        return list;
    }

    public void setList(ArrayList<SearchHistoryModel> list) {
        this.list = list;
    }
}
