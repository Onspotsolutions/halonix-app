package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class shipment_history_list {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("rec_quantity")
    @Expose
    private Integer recQuantity;
    @SerializedName("completed")
    @Expose
    private Boolean completed;
    @SerializedName("Invoice_number")
    @Expose
    private String invoiceNumber;
    @SerializedName("Invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("PO_number")
    @Expose
    private String pONumber;
    @SerializedName("PO_date")
    @Expose
    private String pODate;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("client")
    @Expose
    private String client;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getRecQuantity() {
        return recQuantity;
    }

    public void setRecQuantity(Integer recQuantity) {
        this.recQuantity = recQuantity;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPONumber() {
        return pONumber;
    }

    public void setPONumber(String pONumber) {
        this.pONumber = pONumber;
    }

    public String getPODate() {
        return pODate;
    }

    public void setPODate(String pODate) {
        this.pODate = pODate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
