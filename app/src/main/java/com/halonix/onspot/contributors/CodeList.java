package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CodeList {
    @SerializedName("item_code")
    @Expose
    private String itemCode;
    @SerializedName("packing_ratio")
    @Expose
    private Integer packingRatio;
    @SerializedName("MasterCartonCode")
    @Expose
    private ArrayList<String> masterCartonCode = null;
    @SerializedName("Qrcode")
    @Expose
    private ArrayList<String> qrcode = null;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Integer getPackingRatio() {
        return packingRatio;
    }

    public void setPackingRatio(Integer packingRatio) {
        this.packingRatio = packingRatio;
    }

    public ArrayList<String> getMasterCartonCode() {
        return masterCartonCode;
    }

    public void setMasterCartonCode(ArrayList<String> masterCartonCode) {
        this.masterCartonCode = masterCartonCode;
    }

    public ArrayList<String> getQrcode() {
        return qrcode;
    }

    public void setQrcode(ArrayList<String> qrcode) {
        this.qrcode = qrcode;
    }

}
