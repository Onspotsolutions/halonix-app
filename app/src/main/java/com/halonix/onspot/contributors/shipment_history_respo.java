package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class shipment_history_respo {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("list")
    @Expose
    private ArrayList<shipment_history_list> list = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<shipment_history_list> getList() {
        return list;
    }

    public void setList(ArrayList<shipment_history_list> list) {
        this.list = list;
    }
}
