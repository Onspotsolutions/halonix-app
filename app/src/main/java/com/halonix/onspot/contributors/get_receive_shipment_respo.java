package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class get_receive_shipment_respo {


    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("invoice_codes")
    @Expose
    private ArrayList<String> invoiceCodes = null;
    @SerializedName("list")
    @Expose
    private ArrayList<get_receive_shipmentList> list = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<String> getInvoiceCodes() {
        return invoiceCodes;
    }

    public void setInvoiceCodes(ArrayList<String> invoiceCodes) {
        this.invoiceCodes = invoiceCodes;
    }

    public ArrayList<get_receive_shipmentList> getList() {
        return list;
    }

    public void setList(ArrayList<get_receive_shipmentList> list) {
        this.list = list;
    }
}
