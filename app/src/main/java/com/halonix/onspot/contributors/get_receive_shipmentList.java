package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class get_receive_shipmentList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("packing_ratio")
    @Expose
    private Integer packingRatio;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("sweep")
    @Expose
    private String sweep;
    @SerializedName("color")
    @Expose
    private String color;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPackingRatio() {
        return packingRatio;
    }

    public void setPackingRatio(Integer packingRatio) {
        this.packingRatio = packingRatio;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSweep() {
        return sweep;
    }

    public void setSweep(String sweep) {
        this.sweep = sweep;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
