package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiveDefectiveShipmentList_respo {


    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("model")
    @Expose
    private String model;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    @SerializedName("packing_ratio")
    @Expose
    private int packing_ratio;


    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("return_invoice_no")
    @Expose
    private String return_invoice_no;

    @SerializedName("return_invoice_date")
    @Expose
    private String return_invoice_date;

    @SerializedName("client")
    @Expose
    private String client;

    @SerializedName("receive_quantity")
    @Expose
    private String receive_quantity;

    @SerializedName("rec_quantity")
    @Expose
    private int rec_quantity;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("rv_no")
    @Expose
    private String rv_no;

    public String getCode() {
        return code;
    }

    public String getModel() {
        return model;
    }

    public String getProduct_name() {
        return product_name;
    }

    public int getPacking_ratio() {
        return packing_ratio;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getReturn_invoice_no() {
        return return_invoice_no;
    }

    public String getReturn_invoice_date() {
        return return_invoice_date;
    }

    public String getClient() {
        return client;
    }

    public String getReceive_quantity() {
        return receive_quantity;
    }

    public int getRec_quantity() {
        return rec_quantity;
    }

    public String getDate() {
        return date;
    }

    public String getRv_no() {
        return rv_no;
    }
}
