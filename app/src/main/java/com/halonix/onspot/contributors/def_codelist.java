package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class def_codelist {
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("sender_id")
    @Expose
    private String senderId;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
}
