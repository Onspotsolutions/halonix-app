package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class repacking_respo {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("code_list")
    @Expose
    private ArrayList<repacking_list_respo> code_list = null;

    @SerializedName("blade_codes")
    @Expose
    private ArrayList<repacking_list_respo> blade_codes = null;

    @SerializedName("master_codes")
    @Expose
    private ArrayList<String> master_codes = null;

    public Boolean getSuccess() {
        return success;
    }


    public ArrayList<repacking_list_respo> getCode_list() {
        return code_list;
    }

    public ArrayList<repacking_list_respo> getBlade_codes() {
        return blade_codes;
    }

    public ArrayList<String> getMaster_codes() {
        return master_codes;
    }
}
