package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class code_list_respo {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("list")
    @Expose
    private CodeList list;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public CodeList getList() {
        return list;
    }

    public void setList(CodeList list) {
        this.list = list;
    }

}
