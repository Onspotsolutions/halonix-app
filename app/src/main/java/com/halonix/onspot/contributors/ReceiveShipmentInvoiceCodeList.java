package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiveShipmentInvoiceCodeList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("packing_ratio")
    @Expose
    private Integer packingRatio;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPackingRatio() {
        return packingRatio;
    }

    public void setPackingRatio(Integer packingRatio) {
        this.packingRatio = packingRatio;
    }
}
