package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class packed_carton_respo {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("master_carton_codes")
    @Expose
    private ArrayList<PackedCartonList> masterCartonCodes = null;

    @SerializedName("msg")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<PackedCartonList> getMasterCartonCodes() {
        return masterCartonCodes;
    }

    public void setMasterCartonCodes(ArrayList<PackedCartonList> masterCartonCodes) {
        this.masterCartonCodes = masterCartonCodes;
    }
}
