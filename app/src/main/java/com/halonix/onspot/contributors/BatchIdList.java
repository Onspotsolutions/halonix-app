package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BatchIdList {

    @SerializedName("item_code")
    @Expose
    private String itemCode;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("seprate_blade")
    @Expose
    private Boolean seprateBlade;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean getSeprateBlade() {
        return seprateBlade;
    }

    public void setSeprateBlade(Boolean seprateBlade) {
        this.seprateBlade = seprateBlade;
    }


}
