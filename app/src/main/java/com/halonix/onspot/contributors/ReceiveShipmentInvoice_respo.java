package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReceiveShipmentInvoice_respo {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("list")
    @Expose
    private ArrayList<ReceiveShipmentInvoiceList> list = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<ReceiveShipmentInvoiceList> getList() {
        return list;
    }

    public void setList(ArrayList<ReceiveShipmentInvoiceList> list) {
        this.list = list;
    }
}
