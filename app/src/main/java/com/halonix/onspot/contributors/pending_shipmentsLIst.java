package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class pending_shipmentsLIst {
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Invoice_number")
    @Expose
    private String invoiceNumber;
    @SerializedName("Invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("pending")
    @Expose
    private Integer pending;
    @SerializedName("shipment_id")
    @Expose
    private String shipmentId;

    @SerializedName("uuid")
    @Expose
    private String uuid;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getUuid() {
        return uuid;
    }
}
