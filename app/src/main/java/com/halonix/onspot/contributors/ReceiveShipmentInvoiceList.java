package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReceiveShipmentInvoiceList {

    @SerializedName("model")
    @Expose
    private String model;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    @SerializedName("codes")
    @Expose
    private ArrayList<ReceiveShipmentInvoiceCodeList> codes = null;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }


    public ArrayList<ReceiveShipmentInvoiceCodeList> getCodes() {
        return codes;
    }

    public void setCodes(ArrayList<ReceiveShipmentInvoiceCodeList> codes) {
        this.codes = codes;
    }
}
