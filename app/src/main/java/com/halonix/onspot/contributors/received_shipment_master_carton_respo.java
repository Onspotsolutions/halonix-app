package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class received_shipment_master_carton_respo {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
