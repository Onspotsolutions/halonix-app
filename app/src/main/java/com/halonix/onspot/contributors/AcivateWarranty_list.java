package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcivateWarranty_list {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("sweep")
    @Expose
    private String sweep;
    @SerializedName("voltage")
    @Expose
    private String voltage;
    @SerializedName("warranty")
    @Expose
    private String warranty;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSweep() {
        return sweep;
    }

    public void setSweep(String sweep) {
        this.sweep = sweep;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }
}
