package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class unpacked_batch_respo {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("batch_id_list")
    @Expose
    private ArrayList<BatchIdList> batchIdList = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<BatchIdList> getBatchIdList() {
        return batchIdList;
    }

    public void setBatchIdList(ArrayList<BatchIdList> batchIdList) {
        this.batchIdList = batchIdList;
    }
}
