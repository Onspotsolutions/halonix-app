package com.halonix.onspot.contributors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class repacking_list_respo {

    @SerializedName("separate_blades")
    @Expose
    private Integer separate_blades;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("product_name")
    @Expose
    private String product_name;

    public Integer getSeparate_blades() {
        return separate_blades;
    }

    public void setSeparate_blades(Integer separate_blades) {
        this.separate_blades = separate_blades;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
