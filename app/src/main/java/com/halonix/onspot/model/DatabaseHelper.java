package com.halonix.onspot.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.halonix.onspot.contributors.BatchIdList;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static com.halonix.onspot.utils.Constants.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {

    public final static String DATABASE_NAME = "halonix.db";
    public final static int DB_VERSION = 29;

    // ------ Agent TABLE
    private final static String TABLE_NAME_AGENT = "agent_list_data";
    private final static String AGENT_ID = "id";
    private final static String AGENT_NAME = "name";
    private final static String AGENT_CODE = "code";
    private final static String AGENT_EMAIL = "email";
    private final static String AGENT_ADDRESS = "address";
    private final static String AGENT_MOBILE = "mobile";h
    private final static String AGENT_CP_PERSON = "cp_person";
    private final static String AGENT_TYPE = "type";
    private final static String AGENT_LOCATION = "location";


    private static final String CREATE_TABLE_AGENT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_AGENT
            + "("
            + AGENT_ID
            + " INTEGER,"
            + AGENT_NAME
            + " TEXT, "
            + AGENT_CODE
            + " TEXT, "
            + AGENT_EMAIL
            + " TEXT, "
            + AGENT_ADDRESS
            + " TEXT, "
            + AGENT_MOBILE
            + " TEXT, "
            + AGENT_CP_PERSON
            + " TEXT, "
            + AGENT_TYPE
            + " TEXT, "
            + AGENT_LOCATION
            + " TEXT"
            + ")";

    // ------ USER TABLE
    private final static String TABLE_NAME_USER_INFO = "registerd_user_table";
    private final static String USER_COL_EMAIL = "user_email";
    private final static String USER_COL_NAME = "user_name";
    private final static String USER_COL_NUMBER = "user_number";
    private final static String USER_COL_AUTH_TOKEN = "user_auth_token";
    private final static String USER_COL_ID = "user_id";

    private static final String CREATE_TABLE_USERINFO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_USER_INFO
            + " ("
            + USER_COL_EMAIL
            + " TEXT"
            + ","
            + USER_COL_NUMBER
            + " TEXT"
            + ","
            + USER_COL_NAME
            + " TEXT"
            + ","
            + USER_COL_AUTH_TOKEN
            + " TEXT"
            + ","
            + USER_COL_ID
            + " TEXT"
            + ")";


    // ------ Batches TABLE
    private final static String TABLE_NAME_BATCH_LIST = "batch_list";
    private final static String ITEM_CODE = "item_code";
    private final static String MODEL = "model";
    private final static String SEPARATE_BLADE = "seprate_blade";


    private static final String CREATE_TABLE_BATCH_LIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_BATCH_LIST
            + " ("
            + ITEM_CODE
            + " TEXT"
            + ","
            + MODEL
            + " TEXT"
            + ","
            + SEPARATE_BLADE
            + " TEXT"
            + ")";

    // QR Code List
    private static final String TABLE_QR_CODE = "qr_code";
    private final static String SYSTEM_QR_LIST_ID = "qr_id";
    private final static String QR_CODELIST = "qr_codelist";
    private final static String QR_CODELIST_ITEM_CODE = "qr_codelist_item_code";

    private static final String CREATE_TABLE_QRCODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_QR_CODE
            + "("
            + SYSTEM_QR_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + QR_CODELIST_ITEM_CODE
            + " TEXT,"
            + QR_CODELIST
            + " TEXT"
            + ")";


    // Blade Code List
    private static final String TABLE_BLADE_CODE = "blade_code";
    private final static String SYSTEM_BLADE_LIST_ID = "blade_id";
    private final static String BLADE_CODELIST = "blade_codelist";
    private final static String BLADE_CODELIST_ITEM_CODE = "blade_codelist_item_code";

    private static final String CREATE_TABLE_BLADECODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_BLADE_CODE
            + "("
            + SYSTEM_BLADE_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + BLADE_CODELIST_ITEM_CODE
            + " TEXT,"
            + BLADE_CODELIST
            + " TEXT"
            + ")";


    // MASTER_QR Code List
    private static final String TABLE_MASTER_QR_CODE = "master_qr_code";
    private final static String SYSTEM_MASTER_QR_LIST_ID = "master_qr_id";
    private final static String MASTER_QR_CODELIST = "master_qr_codelist";
    private final static String MASTER_QR_CODELIS_ITEM_CODE = "master_qr_codelist_item_code";

    private static final String CREATE_TABLE_MASTER_QRCODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_MASTER_QR_CODE
            + "("
            + SYSTEM_MASTER_QR_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + MASTER_QR_CODELIS_ITEM_CODE
            + " TEXT,"
            + MASTER_QR_CODELIST
            + " TEXT"
            + ")";

    //Store scanned item codes
    private static final String TABLE_ITEMCODE = "table_item_code";
    private final static String ITEMCODE_ID = "itemcode_id";
    private final static String ITEMCODE_LIST = "itemcode_list";
    private final static String ITEMCODE_FLAG = "itemcode_flag";

    private static final String CREATE_TABLE_ITEMCODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ITEMCODE
            + "("
            + ITEMCODE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ITEMCODE_LIST
            + " TEXT,"
            + ITEMCODE_FLAG
            + " TEXT"
            + ")";


    // Scanned product QR code list
    private static final String TABLE_SCANNED_PRODUCT_QRLIST = "scanned_product_qrlist";
    private final static String SYSTEM_SCANNED_PRODUCT_ID = "sp_id";
    private final static String SCANNED_PRODUCT_QR_CODE = "scanned_product_qr_code";
    private final static String SCANNED_PRODUCT_QR_CODE_ITEMID = "scanned_product_qr_code_itemid";
    private final static String SCANNED_PRODUCT_QR_CODE_FLAG = "scanned_product_qr_code_flag";


    private static final String CREATE_TABLE_SCANNED_PRODUCT_QRCODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_PRODUCT_QRLIST
            + "("
            + SYSTEM_SCANNED_PRODUCT_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_PRODUCT_QR_CODE
            + " TEXT,"
            + SCANNED_PRODUCT_QR_CODE_ITEMID
            + " TEXT,"
            + SCANNED_PRODUCT_QR_CODE_FLAG
            + " TEXT"
            + ")";


    //Temp Scanned product QR code list
    private static final String TABLE_TEMP_SCANNED_PRODUCT_QRLIST = "temp_scanned_product_qrlist";
    private final static String SYSTEM_TEMP_SCANNED_PRODUCT_ID = "temp_sp_id";
    private final static String TEMP_SCANNED_PRODUCT_QR_CODE = "temp_scanned_product_qr_code";
    private final static String TEMP_SCANNED_PRODUCT_QR_CODE_ITEMID = "temp_scanned_product_qr_code_itemid";
    private final static String TEMP_SCANNED_PRODUCT_QR_CODE_FLAG = "temp_scanned_product_qr_code_flag";


    private static final String CREATE_TABLE_TEMP_SCANNED_PRODUCT_QRCODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TEMP_SCANNED_PRODUCT_QRLIST
            + "("
            + SYSTEM_TEMP_SCANNED_PRODUCT_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TEMP_SCANNED_PRODUCT_QR_CODE
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_QR_CODE_ITEMID
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_QR_CODE_FLAG
            + " TEXT"
            + ")";


    // Scanned product blade code list
    private static final String TABLE_SCANNED_PRODUCT_BLADELIST = "scanned_product_bladelist";
    private final static String SYSTEM_SCANNED_PRODUCT_BLADEID = "b_id";
    private final static String SCANNED_PRODUCT_BLADE_CODE = "scanned_product_blade_code";
    private final static String SCANNED_PRODUCT_BLADE_CODE_ITEMID = "scanned_product_blade_code_itemid";
    private final static String SCANNED_PRODUCT_BLADE_CODE_FLAG = "scanned_product_blade_code_flag";

    private static final String CREATE_TABLE_SCANNED_PRODUCT_BLADECODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_PRODUCT_BLADELIST
            + "("
            + SYSTEM_SCANNED_PRODUCT_BLADEID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_PRODUCT_BLADE_CODE
            + " TEXT,"
            + SCANNED_PRODUCT_BLADE_CODE_ITEMID
            + " TEXT,"
            + SCANNED_PRODUCT_BLADE_CODE_FLAG
            + " TEXT"
            + ")";

    // Temp Scanned product blade code list
    private static final String TABLE_TEMP_SCANNED_PRODUCT_BLADELIST = "temp_scanned_product_bladelist";
    private final static String SYSTEM_TEMP_SCANNED_PRODUCT_BLADEID = "temp_b_id";
    private final static String TEMP_SCANNED_PRODUCT_BLADE_CODE = "temp_scanned_product_blade_code";
    private final static String TEMP_SCANNED_PRODUCT_BLADE_CODE_ITEMID = "temp_scanned_product_blade_code_itemid";
    private final static String TEMP_SCANNED_PRODUCT_BLADE_CODE_FLAG = "temp_scanned_product_blade_code_flag";

    private static final String CREATE_TABLE_TEMP_SCANNED_PRODUCT_BLADECODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST
            + "("
            + SYSTEM_TEMP_SCANNED_PRODUCT_BLADEID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TEMP_SCANNED_PRODUCT_BLADE_CODE
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_BLADE_CODE_ITEMID
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_BLADE_CODE_FLAG
            + " TEXT"
            + ")";


    // Scanned product master code list
    private static final String TABLE_SCANNED_PRODUCT_MASTERQRLIST = "scanned_product_masterqrlist";
    private final static String SYSTEM_SCANNED_PRODUCT_MASTERID = "masterqr_id";
    private final static String SCANNED_PRODUCT_MASTERQR_CODE = "scanned_product_masterqr_code";
    private final static String SCANNED_PRODUCT_MASTERQR_CODE_ITEMID = "scanned_product_masterqr_code_itemid";
    private final static String SCANNED_PRODUCT_MASTERQR_CODE_FLAG = "scanned_product_masterqr_code_flag";
    private final static String SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO = "scanned_product_masterqr_code_pkg_ratio";

    private static final String CREATE_TABLE_SCANNED_PRODUCT_MASTERQRCODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_PRODUCT_MASTERQRLIST
            + "("
            + SYSTEM_SCANNED_PRODUCT_MASTERID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_PRODUCT_MASTERQR_CODE
            + " TEXT,"
            + SCANNED_PRODUCT_MASTERQR_CODE_ITEMID
            + " TEXT,"
            + SCANNED_PRODUCT_MASTERQR_CODE_FLAG
            + " TEXT,"
            + SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO
            + " TEXT"
            + ")";


    // Scanned product master code list
    private static final String TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST = "temp_scanned_product_masterqrlist";
    private final static String SYSTEM_TEMP_SCANNED_PRODUCT_MASTERID = "temp_masterqr_id";
    private final static String TEMP_SCANNED_PRODUCT_MASTERQR_CODE = "temp_scanned_product_masterqr_code";
    private final static String TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID = "temp_scanned_product_masterqr_code_itemid";
    private final static String TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG = "temp_scanned_product_masterqr_code_flag";
    private final static String TEMP_SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO = "temp_scanned_product_masterqr_code_pkg_ratio";


    private static final String CREATE_TABLE_TEMP_SCANNED_PRODUCT_MASTERQRCODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST
            + "("
            + SYSTEM_TEMP_SCANNED_PRODUCT_MASTERID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TEMP_SCANNED_PRODUCT_MASTERQR_CODE
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG
            + " TEXT,"
            + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO
            + " TEXT"
            + ")";


    // Packing Ratio
    private static final String TABLE_PACKING_RATIO = "packing_ratio";
    private final static String PACKING_RATIO_ID = "packing_ratio_id";
    private final static String PACKING_RATIO_ITEM_CODE = "packing_ratio_item_code";
    private final static String PACKING_RATIO_CNT = "packing_ratio_cnt";
    private final static String PACKING_RATIO_FLAG = "packing_ratio_flag";

    private static final String CREATE_TABLE_PACKING_RATIO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_PACKING_RATIO
            + "("
            + PACKING_RATIO_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            PACKING_RATIO_ITEM_CODE
            + " TEXT," +
            PACKING_RATIO_CNT
            + " TEXT,"
            + PACKING_RATIO_FLAG
            + " TEXT"
            + ")";


    // Invoice Code List
    private static final String TABLE_INVOICE_CODE = "invoice_code";
    private final static String SYSTEM_INVOICE_LIST_ID = "id";
    private final static String INVOICE_CODELIST = "invoice_codelist";
    private final static String INVOICE_CODELIST_FLAG = "invoice_codelist_flag";

    private static final String CREATE_TABLE_INVOICE_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_INVOICE_CODE
            + "("
            + SYSTEM_INVOICE_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + INVOICE_CODELIST
            + " TEXT,"
            + INVOICE_CODELIST_FLAG
            + " TEXT"
            + ")";


    //Scanned Invoice Code List
    private static final String TABLE_SCANNED_INVOICE_CODE = "scanned_invoice_code";
    private final static String SYSTEM_SCANNED_INVOICE_LIST_ID = "scanned_ic_id";
    private final static String SCANNED_INVOICE_CODELIST = "scanned_invoice_codelist";
    private final static String SCANNED_INVOICE_CODELIST_FLAG = "scanned_invoice_codelist_flg";

    private static final String CREATE_TABLE_SCANNED_INVOICE_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_INVOICE_CODE
            + "("
            + SYSTEM_SCANNED_INVOICE_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_INVOICE_CODELIST
            + " TEXT,"
            + SCANNED_INVOICE_CODELIST_FLAG
            + " TEXT"
            + ")";


    // ------ New Shipment
    private static final String TABLE_NAME_SHIPMENT = "newshipment";
    private final static String SHIPMENT_ID = "shipment_id";
    private final static String SHIPMENT_PACKEDRATIO = "packed_ratio";
    private final static String SHIPMENT_CODE = "shipment_code";
    private final static String SHIPMENT_PRODUCTNAME = "product_name";
    private final static String SHIPMENT_UPLOAD_TO_SERVER = "shipment_upload_to_server";
    private final static String SHIPMENT_OFFLINE_FLAG = "shipment_offline_flag";
    private final static String SHIPMENT_SENDER_ID = "shipment_sender_id";

    private static final String CREATE_TABLE_NEWSIPMENT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_SHIPMENT
            + "("
            + SHIPMENT_ID
            + " TEXT,"
            + SHIPMENT_PACKEDRATIO
            + " TEXT,"
            + SHIPMENT_CODE
            + " TEXT,"
            + SHIPMENT_PRODUCTNAME
            + " TEXT,"
            + SHIPMENT_UPLOAD_TO_SERVER
            + " TEXT,"
            + SHIPMENT_OFFLINE_FLAG
            + " TEXT,"
            + SHIPMENT_SENDER_ID
            + " TEXT"
            + ")";


    //Scanned Master Carton Code List-New Shipment
    private static final String TABLE_SCANNED_BREAKAGE_CODE = "scanned_breakage_code";
    private final static String SCANNED_BREAKAGE_ID = "scanned_breakage_id";
    private final static String SCANNED_BREAKAGE_CODELIST = "scanned_breakage_codelist";
    private final static String SCANNED_BREAKAGE_FLAG = "scanned_breakage_flag";


    private static final String CREATE_TABLE_SCANNED_BREAKAGE_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_BREAKAGE_CODE
            + "("
            + SCANNED_BREAKAGE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_BREAKAGE_CODELIST
            + " TEXT,"
            + SCANNED_BREAKAGE_FLAG
            + " TEXT"
            + ")";


    //Scanned Master Carton Code List-New Shipment
    private static final String TABLE_SCANNED_MCC_CODE = "scanned_mcc_code";
    private final static String SCANNED_MCC_ID = "scanned_mcc_id";
    private final static String SCANNED_MCC_CODELIST = "scanned_mcc_codelist";
    private final static String SCANNED_MCC_AGENCY = "scanned_mcc_agency";
    private final static String SCANNED_MCC_AGENCY_TYPE = "scanned_mcc_agency_type";
    private final static String SCANNED_MCC_INVOICE_NO = "scanned_mcc_invoice_no";
    private final static String SCANNED_MCC_FLAG = "scanned_mcc_flag";
    private final static String SCANNED_MCC_OFFLINE_FLAG = "scanned_mcc_offline_flag";

    private static final String CREATE_TABLE_SCANNED_MCC_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_MCC_CODE
            + "("
            + SCANNED_MCC_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_MCC_CODELIST
            + " TEXT,"
            + SCANNED_MCC_AGENCY
            + " TEXT,"
            + SCANNED_MCC_AGENCY_TYPE
            + " TEXT,"
            + SCANNED_MCC_INVOICE_NO
            + " TEXT,"
            + SCANNED_MCC_FLAG
            + " TEXT,"
            + SCANNED_MCC_OFFLINE_FLAG
            + " TEXT"
            + ")";


    //Scanned Master Carton Code List-Unpack
    private static final String TABLE_SCANNED_UNPACK_CODE = "scanned_unpack_code";
    private final static String SCANNED_UNPACK_ID = "scanned_unpack_id";
    private final static String SCANNED_UNPACK_CODELIST = "scanned_unpack_codelist";
    private final static String SCANNED_UNPACK_FLAG = "scanned_unpack_flag";
    private final static String SCANNED_UNPACK_OFFLINE_FLAG = "scanned_unpack_offline_flag";

    private static final String CREATE_TABLE_SCANNED_UNPACK_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_UNPACK_CODE
            + "("
            + SCANNED_UNPACK_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SCANNED_UNPACK_CODELIST
            + " TEXT,"
            + SCANNED_MCC_FLAG
            + " TEXT,"
            + SCANNED_MCC_OFFLINE_FLAG
            + " TEXT"
            + ")";


    // ------ NewShipment Offline
    private static final String TABLE_NAME_NEWSHIPMENT = "newshipmentoffline";
    private final static String NEWSHIPMENTID = "newshipmentid";
    private final static String NAUTH_TOKEN = "Auth_token";
    private final static String QUANTITY = "quantity";
    private final static String NAGENCY_ID = "Agency_id";
    private final static String NINVOICE_NUMBER = "Invoice_number";
    private final static String NINVOICE_DATE = "Invoice_date";
    private final static String NPO_NUMBER = "Po_number";
    private final static String NPO_DATE = "Po_date";
    private final static String SHIPMENT_TYPE = "shipment_type";
    private final static String MASTER_CODE = "MasterCartonCode";
    private final static String SHIPMENT_UPLOAD_SERVER = "shipment_upload_to_server";

    private static final String CREATE_TABLE_NEWSHIPMENT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_NEWSHIPMENT
            + "("
            + NEWSHIPMENTID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAUTH_TOKEN
            + " TEXT,"
            + QUANTITY
            + " TEXT,"
            + NAGENCY_ID
            + " TEXT,"
            + NINVOICE_NUMBER
            + " TEXT,"
            + NINVOICE_DATE
            + " TEXT,"
            + NPO_NUMBER
            + " TEXT,"
            + NPO_DATE
            + " TEXT,"
            + MASTER_CODE
            + " TEXT,"
            + SHIPMENT_TYPE
            + " TEXT,"
            + SHIPMENT_UPLOAD_SERVER
            + " TEXT"
            + ")";


    // Receive New Shipment
    private static final String TABLE_NAME_RECEIVENEWSHIPMENT = "receive_new_shipment";
    private final static String RNS_ID = "rns_id";
    private final static String RNS_CODE = "rns_code";
    private final static String RNS_PACKEDRATIO = "rns_packed_ratio";
    private final static String RNS_PRODUCTNAME = "rns_product_name";
    private final static String RNS_SWEEP = "rns_sweep";
    private final static String RNS_COLOR = "rns_color";
    private final static String RNS_SHIPMENT_UPLOAD_TO_SERVER = "rns_shipment_upload_to_server";

    private static final String CREATE_TABLE_RECEIVENEWSHIPMENT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_RECEIVENEWSHIPMENT
            + "("
            + RNS_ID
            + " TEXT,"
            + RNS_CODE
            + " TEXT,"
            + RNS_PACKEDRATIO
            + " TEXT,"
            + RNS_PRODUCTNAME
            + " TEXT,"
            + RNS_SWEEP
            + " TEXT,"
            + RNS_COLOR
            + " TEXT,"
            + RNS_SHIPMENT_UPLOAD_TO_SERVER
            + " TEXT"
            + ")";

    private static final String TABLE_SYSTEM_BARCODE = "master_carton_code";
    private final static String SYSTEM_BARCODE_LIST_ID = "mc_id";
    private final static String SYSTEM_BARCODE_LIST = "mc_list";

    private static final String CREATE_TABLE_SYSTEM_BARCODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SYSTEM_BARCODE
            + "("
            + SYSTEM_BARCODE_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SYSTEM_BARCODE_LIST
            + " TEXT"
            + ")";

    private static final String TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST = "rs_ic_list_table";
    private final static String RSI_CL_ID = "rsi_c_id";
    private final static String RSI_CL_LIST = "rsi_c_list";

    private static final String CREATE__RECEIVE_SHIPMENT_INVOICE_CODE_LIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST
            + "("
            + RSI_CL_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + RSI_CL_LIST
            + " TEXT"
            + ")";


    // Receive New Shipment By Invoice
    private static final String RECEIVE_INVOICE_SHIPMENT = "receiveinvoiceshipment";
    private final static String RECEIVE_INVOICE__ID = "receive_invoice_shipment_id";
    private final static String RECEIVE_INVOICE_MODEL = "receive_invoice_model";
    private final static String RECEIVE_INVOICE_SWEEP = "receive_invoice_sweep";
    private final static String RECEIVE_INVOICE_COLOR = "receive_invoice_color";
    private final static String RECEIVE_INVOICE_QTY = "receive_invoice_qty";


    private static final String CREATE_TABLE_RECEIVE_INVOICE_SHIPMENT = "CREATE TABLE IF NOT EXISTS "
            + RECEIVE_INVOICE_SHIPMENT
            + "("
            + RECEIVE_INVOICE__ID
            + " TEXT,"
            + RECEIVE_INVOICE_MODEL
            + " TEXT,"
            + RECEIVE_INVOICE_SWEEP
            + " TEXT,"
            + RECEIVE_INVOICE_COLOR
            + " TEXT,"
            + RECEIVE_INVOICE_QTY
            + " TEXT"
            + ")";


    //Receive New Shipment Scanned Invoice Code List
    private static final String TABLE_RS_SCANNED_INVOICE_CODE = "receive_shipment_scanned_invoice_code";
    private final static String RS_SCANNED_INVOICE_LIST_ID = "receive_shipment_scanned_ic_id";
    private final static String RS_SCANNED_INVOICE_CODELIST = "receive_shipment_scanned_invoice_codelist";

    private static final String CREATE_TABLE_RS_SCANNED_INVOICE_CODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_RS_SCANNED_INVOICE_CODE
            + "("
            + RS_SCANNED_INVOICE_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + RS_SCANNED_INVOICE_CODELIST
            + " TEXT"
            + ")";

    //Scanned Master Carton Code List
    private static final String TABLE_SCANNED_MASTER_CARTON_BARCODE = "scanned_master_carton_code";
    private final static String SYSTEM_SCANNED_MASTER_CARTON_LIST_ID = "smc_id";
    private final static String SYSTEM_SCANNED_MASTER_CARTON_LIST = "smc_list";
    private final static String SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG = "smc_list_flag";

    private static final String CREATE_TABLE_SCANNED_MASTER_CARTON_BARCODE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_MASTER_CARTON_BARCODE
            + "("
            + SYSTEM_SCANNED_MASTER_CARTON_LIST_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SYSTEM_SCANNED_MASTER_CARTON_LIST
            + " TEXT,"
            + SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG
            + " TEXT"
            + ")";


    //  --- LOGS-Master Carton Scan
    private static final String TABLE_NAME_LOGS = "logs";
    private final static String LOG_ID = "id";
    private final static String LOG_NUMBER_OF_CARTONS = "no_of_cartons";
    private final static String LOG_COMPLETED = "log_completed";
    private final static String LOG_UPLOAD_TO_SERVER = "log_upload_to_server";

    private static final String CREATE_TABLE_LOGS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME_LOGS
            + "("
            + LOG_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + LOG_NUMBER_OF_CARTONS
            + " TEXT DEFAULT 0,"
            + LOG_COMPLETED
            + " TEXT DEFAULT 0,"
            + LOG_UPLOAD_TO_SERVER
            + " TEXT DEFAULT 0"
            + ")";


    //Store scanned item codes
    private static final String TABLE_SCANNED_IS_MASTER_CARTON = "table_scanned_is_master_carton";
    private final static String SCANNED_IS_MASTER_CARTON_PRODUCT_NAME = "scanned_is_master_carton_product_name";
    private final static String SCANNED_IS_MASTER_CARTON_CODE_ = "scanned_is_master_carton_code";
    private final static String SCANNED_IS_MASTER_CARTON_TYPE_ = "scanned_is_master_carton_type";
    private final static String SCANNED_IS_MASTER_CARTON_PKG_RATIO_ = "scanned_is_master_carton_pkg_ratio";

    private static final String CREATE_TABLE_SCANNED_IS_MASTER_CARTON = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_IS_MASTER_CARTON
            + "("
            + SCANNED_IS_MASTER_CARTON_PRODUCT_NAME
            + " TEXT,"
            + SCANNED_IS_MASTER_CARTON_CODE_
            + " TEXT,"
            + SCANNED_IS_MASTER_CARTON_TYPE_
            + " TEXT,"
            + SCANNED_IS_MASTER_CARTON_PKG_RATIO_
            + " TEXT"
            + ")";

    //Store master carton code by invoice scan
    private static final String TABLE_IS_MasterCarton = "table_is_mastercarton";
    private final static String product_name_MasterCarton = "productname_mastercarton_name";
    private final static String invoice_number_MasterCarton = "productname_mastercarton_id";
    private final static String Mastercode_MasterCarton = "invoicecode_mastercarton_list";
    private final static String Packing_ratio_MasterCarton = "packing_ratio_mastercarton_list";

    private static final String CREATE_TABLE_IS_MasterCarton = "CREATE TABLE IF NOT EXISTS "
            + TABLE_IS_MasterCarton
            + "("
            + product_name_MasterCarton
            + " TEXT,"
            + invoice_number_MasterCarton
            + " TEXT,"
            + Mastercode_MasterCarton
            + " TEXT,"
            + Packing_ratio_MasterCarton
            + " TEXT"
            + ")";

    //Store individual product codes
    private static final String TABLE_IP_CODES = "table_ip_codes";
    private final static String IP_CODE_ID = "ip_code_id";
    private final static String IP_CODE_LIST = "ip_code_list";

    private static final String CREATE_TABLE_IP_CODES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_IP_CODES
            + "("
            + IP_CODE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + IP_CODE_LIST
            + " TEXT"
            + ")";


    //Store individual product codes
    private static final String TABLE_IP_CODELIST = "table_ip_code_list";
    private final static String IP_SHIPMENT_ID = "ip_shipment_id";
    private final static String IP_INVOICE_NO = "ip_invoice_code";
    private final static String IP_INVOICE_DATE = "ip_invoice_date";
    private final static String IP_QTY = "ip_qty";
    private final static String IP_PENDING = "ip_pending";

    private static final String CREATE_TABLE_IP_CODELIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_IP_CODELIST
            + "("
            + IP_SHIPMENT_ID
            + " TEXT,"
            + IP_INVOICE_NO
            + " TEXT,"
            + IP_INVOICE_DATE
            + " TEXT,"
            + IP_QTY
            + " TEXT,"
            + IP_PENDING
            + " TEXT"
            + ")";

    //Store scanned individual product codes
    private static final String TABLE_SCANNED_IP_CODES = "table_scanned_ip_codes";
    private final static String IP_SCANNED_CODE_ID = "ip_scanned_code_id";
    private final static String IP_SCANNED_CODE_LIST = "ip_scanned_code_list";
    private final static String IP_SCANNED_CODE_TYPE = "ip_scanned_code_type";

    private static final String CREATE_TABLE_SCANNED_IP_CODES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SCANNED_IP_CODES
            + "("
            + IP_SCANNED_CODE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + IP_SCANNED_CODE_LIST
            + " TEXT,"
            + IP_SCANNED_CODE_TYPE
            + " TEXT"
            + ")";


    //Store force in code details
    private static final String TABLE_FORCE_CODE_DETAILS = "table_force_code_details";
    private final static String FORCE_CODE_ID = "force_code_id";
    private final static String FORCE_IS_MASTER = "force_is_master";
    private final static String FORCE_PRODUCT_NAME = "force_product_name";
    private final static String FORCE_PRODUCT_CODE = "force_product_code";
    private final static String FORCE_PKG_RATIO = "force_pkg_ratio";
    private final static String FORCE_DEFECTIVE = "force_defective";
    private final static String FORCE_DEFECTIVE_FLAG = "force_defective_flag";
    private final static String TOKEN = "token";


    private static final String CREATE_TABLE_FORCE_CODE_DETAILS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_FORCE_CODE_DETAILS
            + "("
            + FORCE_CODE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FORCE_IS_MASTER
            + " TEXT,"
            + FORCE_PRODUCT_NAME
            + " TEXT,"
            + FORCE_PRODUCT_CODE
            + " TEXT,"
            + FORCE_PKG_RATIO
            + " TEXT,"
            + FORCE_DEFECTIVE
            + " TEXT,"
            + FORCE_DEFECTIVE_FLAG
            + " TEXT,"
            + TOKEN
            + " TEXT"
            + ")";

    //Scanned Defective Return Send Code List
    private static final String TABLE_DEF_CODE_LIST = "scanned_def_code_list";
    private final static String DEF_CODE_ID = "def_code_id";
    private final static String DEF_CODE = "def_code";
    private final static String DEF_CODE_NAME = "def_code_name";
    private final static String DEF_CODE_PKG_RATIO = "def_code_pkg_ratio";
    private final static String DEF_CODE_SENDER_ID = "def_code_sender_id";
    private final static String DEF_CODE_RIN = "def_code_rin";
    private final static String DEF_CODE_FLAG = "def_code_flag";
    private final static String DEF_CODE_OFFLINE_FLAG = "def_code_offline_flag";

    private static final String CREATE_TABLE_DEF_CODE_LIST = "CREATE TABLE IF NOT EXISTS "
            + TABLE_DEF_CODE_LIST
            + "("
            + DEF_CODE_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DEF_CODE
            + " TEXT,"
            + DEF_CODE_NAME
            + " TEXT,"
            + DEF_CODE_PKG_RATIO
            + " TEXT,"
            + DEF_CODE_SENDER_ID
            + " TEXT,"
            + DEF_CODE_RIN
            + " TEXT,"
            + DEF_CODE_OFFLINE_FLAG
            + " TEXT,"
            + DEF_CODE_FLAG
            + " TEXT"
            + ")";
    //Scanned Defective Return Send Code List
    private static final String TABLE_OFFLINE_DEF_RET_SEND = "offline_def_ret_send";
    private final static String DEF_DRS_ID = "def_drs_id";
    private final static String DEF_DRS_SENDER = "def_drs_sender";
    private final static String DEF_DRS_RIN = "def_drs_rin";
    private final static String DEF_DRS_RID = "def_drs_rid";
    private final static String DEF_DRS_REASON = "def_drs_reason";
    private final static String DEF_DRS_OFFLINE_FLAG = "def_drs_offline_flag";
    private final static String DEF_DRS_SHIPMENT_UPLOAD_TO_SERVER = "def_drs_shipment_upload_to_server";

    private static final String CREATE_TABLE_OFFLINE_DEF_RET_SEND = "CREATE TABLE IF NOT EXISTS "
            + TABLE_OFFLINE_DEF_RET_SEND
            + "("
            + DEF_DRS_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DEF_DRS_SENDER
            + " TEXT,"
            + DEF_DRS_RIN
            + " TEXT,"
            + DEF_DRS_RID
            + " TEXT,"
            + DEF_DRS_REASON
            + " TEXT,"
            + DEF_DRS_SHIPMENT_UPLOAD_TO_SERVER
            + " TEXT,"
            + DEF_DRS_OFFLINE_FLAG
            + " TEXT"
            + ")";

    //Defective Return Receive Code List
    private static final String TABLE_DEF_RETURN_RECEIVE_CODES = "def_return_receive_codes";
    private final static String DEF_RRC_ID = "def_rrc_id";
    private final static String DEF_RRC_CODE = "def_rrc_code";
    private final static String DEF_RRC_MODEL = "def_rrc_model";
    private final static String DEF_RRC_PKG_RATIO = "def_rrc_pkg_ratio";
    private final static String DEF_RRC_FLAG = "def_rrc_flag";
    private final static String DEF_RRC_OFFLINE_FLAG = "def_rrc_offline_flag";

    private static final String CREATE_TABLE_DEF_RETURN_RECEIVE_CODES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_DEF_RETURN_RECEIVE_CODES
            + "("
            + DEF_RRC_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + DEF_RRC_CODE
            + " TEXT,"
            + DEF_RRC_MODEL
            + " TEXT,"
            + DEF_RRC_PKG_RATIO
            + " TEXT,"
            + DEF_RRC_FLAG
            + " TEXT,"
            + DEF_RRC_OFFLINE_FLAG
            + " TEXT"
            + ")";

    //SALE Return Receive Code List
    private static final String TABLE_SALE_RETURN_RECEIVE_CODES = "sale_return_receive_codes";
    private final static String SALE_RRC_ID = "sale_rrc_id";
    private final static String SALE_RRC_CODE = "sale_rrc_code";
    private final static String SALE_RRC_MODEL = "sale_rrc_model";
    private final static String SALE_RRC_PKG_RATIO = "sale_rrc_pkg_ratio";
    private final static String SALE_RRC_FLAG = "sale_rrc_flag";
    private final static String SALE_RRC_OFFLINE_FLAG = "sale_rrc_offline_flag";

    private static final String CREATE_TABLE_SALE_RETURN_RECEIVE_CODES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SALE_RETURN_RECEIVE_CODES
            + "("
            + SALE_RRC_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + SALE_RRC_CODE
            + " TEXT,"
            + SALE_RRC_MODEL
            + " TEXT,"
            + SALE_RRC_PKG_RATIO
            + " TEXT,"
            + SALE_RRC_FLAG
            + " TEXT,"
            + SALE_RRC_OFFLINE_FLAG
            + " TEXT"
            + ")";


    private static DatabaseHelper sInstance;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_AGENT);
        db.execSQL(CREATE_TABLE_USERINFO);
        db.execSQL(CREATE_TABLE_BATCH_LIST);
        db.execSQL(CREATE_TABLE_INVOICE_CODE);
        db.execSQL(CREATE_TABLE_SCANNED_INVOICE_CODE);
        db.execSQL(CREATE_TABLE_QRCODE);
        db.execSQL(CREATE_TABLE_BLADECODE);
        db.execSQL(CREATE_TABLE_MASTER_QRCODE);
        db.execSQL(CREATE_TABLE_NEWSIPMENT);
        db.execSQL(CREATE_TABLE_NEWSHIPMENT);
        db.execSQL(CREATE_TABLE_SCANNED_PRODUCT_QRCODELIST);
        db.execSQL(CREATE_TABLE_TEMP_SCANNED_PRODUCT_QRCODELIST);
        db.execSQL(CREATE_TABLE_SCANNED_PRODUCT_BLADECODELIST);
        db.execSQL(CREATE_TABLE_TEMP_SCANNED_PRODUCT_BLADECODELIST);
        db.execSQL(CREATE_TABLE_SCANNED_PRODUCT_MASTERQRCODELIST);
        db.execSQL(CREATE_TABLE_TEMP_SCANNED_PRODUCT_MASTERQRCODELIST);
        db.execSQL(CREATE_TABLE_PACKING_RATIO);


        db.execSQL(CREATE_TABLE_LOGS);
        db.execSQL(CREATE_TABLE_RECEIVENEWSHIPMENT);
        db.execSQL(CREATE_TABLE_SYSTEM_BARCODE);
        db.execSQL(CREATE_TABLE_SCANNED_MASTER_CARTON_BARCODE);

        db.execSQL(CREATE__RECEIVE_SHIPMENT_INVOICE_CODE_LIST);
        db.execSQL(CREATE_TABLE_RECEIVE_INVOICE_SHIPMENT);
        db.execSQL(CREATE_TABLE_RS_SCANNED_INVOICE_CODE);
        db.execSQL(CREATE_TABLE_ITEMCODE);
        db.execSQL(CREATE_TABLE_IS_MasterCarton);
        db.execSQL(CREATE_TABLE_SCANNED_IS_MASTER_CARTON);
        db.execSQL(CREATE_TABLE_IP_CODES);
        db.execSQL(CREATE_TABLE_IP_CODELIST);
        db.execSQL(CREATE_TABLE_SCANNED_IP_CODES);
        db.execSQL(CREATE_TABLE_SCANNED_MCC_CODE);

        db.execSQL(CREATE_TABLE_SCANNED_BREAKAGE_CODE);
        db.execSQL(CREATE_TABLE_FORCE_CODE_DETAILS);
        db.execSQL(CREATE_TABLE_DEF_CODE_LIST);
        db.execSQL(CREATE_TABLE_OFFLINE_DEF_RET_SEND);

        db.execSQL(CREATE_TABLE_DEF_RETURN_RECEIVE_CODES);
        db.execSQL(CREATE_TABLE_SALE_RETURN_RECEIVE_CODES);
        db.execSQL(CREATE_TABLE_SCANNED_UNPACK_CODE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USER_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_AGENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_BATCH_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_INVOICE_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_QR_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BLADE_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTER_QR_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SHIPMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_NEWSHIPMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_PRODUCT_MASTERQRLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKING_RATIO);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_LOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_RECEIVENEWSHIPMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYSTEM_BARCODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_MASTER_CARTON_BARCODE);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + RECEIVE_INVOICE_SHIPMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RS_SCANNED_INVOICE_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMCODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IS_MasterCarton);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_IS_MASTER_CARTON);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IP_CODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IP_CODELIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_IP_CODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_MCC_CODE);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_BREAKAGE_CODE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORCE_CODE_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEF_CODE_LIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE_DEF_RET_SEND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEF_RETURN_RECEIVE_CODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALE_RETURN_RECEIVE_CODES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCANNED_UNPACK_CODE);
        onCreate(db);
    }

    public void deleteDefReturn() {
        SQLiteDatabase sqldb = getWritableDatabase();
//        sqldb.execSQL("DELETE FROM " + TABLE_DEF_CODE_LIST);
        sqldb.execSQL("DELETE FROM " + TABLE_OFFLINE_DEF_RET_SEND);
    }


    // Fetch Scanned Master carton Code List boolean-New Shipment
    public boolean checkRINinDB(String rin) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_OFFLINE_DEF_RET_SEND + " WHERE " + DEF_DRS_RIN + " = '" + rin + "' AND " + DEF_DRS_OFFLINE_FLAG + " = '" + 1 + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }

    public ArrayList<NewShipment> getNotUploadedDefRetShipments() {
        ArrayList<NewShipment> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_OFFLINE_DEF_RET_SEND + " WHERE " + DatabaseHelper.DEF_DRS_OFFLINE_FLAG + " = " + 1 + " AND " + DEF_DRS_SHIPMENT_UPLOAD_TO_SERVER + " = " + 0, null);
        if (objCursor != null && objCursor.moveToFirst()) {
            if (objCursor != null && objCursor.getCount() > 0 && objCursor.moveToFirst()) {
                do {
                    arrLogDetailModels.add(new NewShipment(
                            objCursor.getString(objCursor.getColumnIndex(DEF_DRS_SENDER)),
                            objCursor.getString(objCursor.getColumnIndex(DEF_DRS_RIN)),
                            objCursor.getString(objCursor.getColumnIndex(DEF_DRS_RID)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_DRS_REASON)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_DRS_OFFLINE_FLAG))
                    ));
                } while (objCursor.moveToNext());
            }
        }

        objCursor.close();
        return arrLogDetailModels;
    }


    public long insertDefRetSendOffline(String sender_id, String rin, String rid, String reason,
                                        String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_DRS_SENDER, sender_id);
        objContentValues.put(DEF_DRS_RIN, rin);
        objContentValues.put(DEF_DRS_RID, rid);
        objContentValues.put(DEF_DRS_REASON, reason);
        objContentValues.put(DEF_DRS_OFFLINE_FLAG, flag);
        objContentValues.put(DEF_DRS_SHIPMENT_UPLOAD_TO_SERVER, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_OFFLINE_DEF_RET_SEND, null, objContentValues);
        return result;
    }

    // Store Def Code List -Defective return send code list
    public long insertDefCodeList(String product_code, String product_name, String pkg_ratio, String sender_id,
                                  String def_flag) {
        long result = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String sender = "";
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE + " = '" + product_code + "'", null);
        if (objCursor.getCount() == 1) {

        } else {

            ContentValues objContentValues = new ContentValues();
            objContentValues.put(DEF_CODE, product_code);
            objContentValues.put(DEF_CODE_NAME, product_name);
            objContentValues.put(DEF_CODE_PKG_RATIO, pkg_ratio);
            objContentValues.put(DEF_CODE_SENDER_ID, sender_id);
            objContentValues.put(DEF_CODE_RIN, "");
            objContentValues.put(DEF_CODE_FLAG, def_flag);
            SQLiteDatabase db1 = this.getWritableDatabase();
            result = db1.insert(TABLE_DEF_CODE_LIST, null, objContentValues);
        }
        return result;
    }


    // Fetch Def Code List -Defective return send code list
    public ArrayList<String> getDefCodeList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sender = "";
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_SENDER_ID + " = '" + sender + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<String> getDefCodeList(String sender) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_FLAG + " = '" + sender + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }


   /* public ArrayList<NewShipmentDetailModel> getDefShipmentSystemBarcode(String barcode) {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE + " = '" + barcode + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_FLAG)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_SENDER_ID)))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }*/

  /*  public ArrayList<NewShipmentDetailModel> getDefShipmentSystemBarcode() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_DEF_CODE_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_FLAG)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_SENDER_ID))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }*/


    public ArrayList<String> getScannedDefCodeListForUpload(String def_flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_FLAG + " = '" + def_flag + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }


    public ArrayList<String> getScannedDefCodeListForUpload(String def_flag, String vendor, String rin) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_FLAG + " = '" + def_flag + "' AND " + DEF_CODE_SENDER_ID + " = '" + vendor + "' AND " + DEF_CODE_RIN + " = '" + rin + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<NewShipmentDetailModel> getScannedDefCodeList(String flag) {
        String offline_flag = "0";
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + DEF_CODE_ID + "," + DEF_CODE_FLAG + "," + DEF_CODE + "," + DEF_CODE_NAME + ",SUM(" + DEF_CODE_PKG_RATIO + ") as " + DEF_CODE_PKG_RATIO + " FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_FLAG + " = '" + flag + "' AND " + DEF_CODE_OFFLINE_FLAG + " = '" + offline_flag + "' GROUP BY " + DEF_CODE_NAME, null);

        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_CODE_FLAG)), 0));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    // Fetch Scanned Def Code List -Defective return send code list
    public boolean getScannedDefCodeListBoolean(String code, String flag) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE + " = '" + code + "' AND " + DEF_CODE_FLAG + " = '" + flag + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }

    public void deleteScannedDefCodes() {
        SQLiteDatabase sqldb = getWritableDatabase();
        ArrayList<String> stringArrayList = getScannedDefCodeListForUpload("1");

        for (int i = 0; i < stringArrayList.size(); i++) {
            sqldb.execSQL("DELETE FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE + " = '" + stringArrayList.get(i) + "' ");
        }
    }

    public void deleteScannedDefCodes(String rin) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_DEF_CODE_LIST + " WHERE " + DEF_CODE_RIN + " = '" + rin + "' ");
    }


    // Update Scanned Def Code List -Defective return send code list- For Single delete
    public long updateDEFRETShipment(String rin) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_DRS_SHIPMENT_UPLOAD_TO_SERVER, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_DRS_RIN + "=?";
        String whereArgs[] = {rin};
        result = db.update(TABLE_OFFLINE_DEF_RET_SEND, objContentValues, whereClause, whereArgs);
        return result;
    }

    // Update Scanned Def Code List -Defective return send code list- For Single delete
    public long updateScannedDefCodeWithNo() {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_CODE_FLAG, "0");
        objContentValues.put(DEF_CODE_OFFLINE_FLAG, "0");
        objContentValues.put(DEF_CODE_SENDER_ID, "");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, null, null);
        return result;
    }

    // Update Scanned Def Code List -Defective return send code list- For Single delete
    public long updateScannedDefCode(String flag, String code, String offline_flag, String sender_id) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_CODE_FLAG, flag);
        objContentValues.put(DEF_CODE_OFFLINE_FLAG, offline_flag);
        objContentValues.put(DEF_CODE_SENDER_ID, sender_id);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_CODE + "=?";
        String whereArgs[] = {code};
        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, whereClause, whereArgs);
        return result;
    }


    // Update Scanned Def Code List -Defective return send code list- For Dismiss-all delete
    public long updateScannedDefCode(String flag, String offline_flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_CODE_FLAG, flag);
        objContentValues.put(DEF_CODE_SENDER_ID, "");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_CODE_OFFLINE_FLAG + "=?";
        String whereArgs[] = {offline_flag};
//        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, DEF_CODE + "=? and " + DEF_CODE_OFFLINE_FLAG + "=?", new String[]{flag, offline_flag});
        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, whereClause, whereArgs);
        return result;
    }


    public long updateAllDefCodewithSenderZero(String sender, String rin) {
        Log.d(TAG, "updateAllDefCodewithSenderZero: " + sender);
        Log.d(TAG, "updateAllDefCodewithSenderZero: " + rin);
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_CODE_SENDER_ID, sender);
        objContentValues.put(DEF_CODE_RIN, rin);
        objContentValues.put(DEF_CODE_OFFLINE_FLAG, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_CODE_SENDER_ID + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, whereClause, whereArgs);
//        result = db.update(TABLE_DEF_CODE_LIST, objContentValues, DEF_CODE_SENDER_ID + "=? and " + DEF_CODE_OFFLINE_FLAG + "=?", new String[]{"0", "0"});
        return result;
    }


    // Store Force Code Details -Force In Code Details
    public long insertForceInCodeDetails(String product_code, String is_master,
                                         String product_name, String pkg_ratio, String defective,
                                         String force_def_flag, String token) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(FORCE_IS_MASTER, is_master);
        objContentValues.put(FORCE_PRODUCT_CODE, product_code);
        objContentValues.put(FORCE_PRODUCT_NAME, product_name);
        objContentValues.put(FORCE_PKG_RATIO, pkg_ratio);
        objContentValues.put(FORCE_DEFECTIVE, defective);
        objContentValues.put(FORCE_DEFECTIVE_FLAG, force_def_flag);
        objContentValues.put(TOKEN, token);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_FORCE_CODE_DETAILS, null, objContentValues);
        return result;
    }


    public void deleteScannedForcedCode(String flag, String token) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_DEFECTIVE_FLAG + " = '" + flag + "' AND " + TOKEN + " = '" + token + "'");
    }

    public void deleteScannedForcedCodeSingle(String barcode) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_PRODUCT_CODE + " = '" + barcode + "' ");
    }


    // Fetch Scanned Forced Code List-Force In Code Details
    public ArrayList<String> getScannedForceCodeList(String def_flag, String token) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_DEFECTIVE_FLAG + " = '" + def_flag + "' AND " + TOKEN + " ='" + token + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_PRODUCT_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }


    public ArrayList<NewShipmentDetailModel> getScannedForcedCodeList(String flag, String token) {
        //
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + FORCE_CODE_ID + "," + FORCE_DEFECTIVE_FLAG + "," + FORCE_PRODUCT_CODE + "," + FORCE_PRODUCT_NAME + ",SUM(" + FORCE_PKG_RATIO + ") as " + FORCE_PKG_RATIO + " FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_DEFECTIVE_FLAG + " = '" + flag + "' AND " + TOKEN + " = '" + token + "' GROUP BY " + FORCE_PRODUCT_NAME, null);

        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.FORCE_CODE_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_PRODUCT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_PRODUCT_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_DEFECTIVE_FLAG)), 0));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    public ArrayList<String> getScannedForcedCodeList1(String flag, String token) {
        ArrayList<String> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + FORCE_CODE_ID + "," + FORCE_DEFECTIVE_FLAG + "," + FORCE_PRODUCT_CODE + "," + FORCE_PRODUCT_NAME + ",SUM(" + FORCE_PKG_RATIO + ") as " + FORCE_PKG_RATIO + " FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_DEFECTIVE_FLAG + " = '" + flag + "' AND " + TOKEN + " = '" + token + "' GROUP BY " + FORCE_PRODUCT_NAME, null);

        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.FORCE_PRODUCT_CODE)));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    // Fetch Scanned Forced Code List boolean-Force In Code Details
    public boolean getScannedForceCodeListBoolean(String master_code, String flag, String token) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_FORCE_CODE_DETAILS + " WHERE " + FORCE_PRODUCT_CODE + " = '" + master_code + "' AND " + FORCE_DEFECTIVE_FLAG + " = '" + flag + "' AND " + TOKEN + " ='" + token + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }


    // Store Scanned Master carton Code List-New Shipment
    public long insertScannedMCCCode(String master_code, String flag) {
        Log.d(TAG, "insertScannedMCCCode master_code: " + master_code);
        Log.d(TAG, "insertScannedMCCCode flag: " + flag);
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_MCC_CODELIST, master_code);
        objContentValues.put(SCANNED_MCC_FLAG, flag);
        objContentValues.put(SCANNED_MCC_OFFLINE_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SCANNED_MCC_CODE, null, objContentValues);
        return result;
    }


    // Update Master carton Code List-New Shipment
    public long updateScannedMasterCartonCode(String agency, String agency_type, String invoice_no, String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_MCC_AGENCY, agency);
        objContentValues.put(SCANNED_MCC_AGENCY_TYPE, agency_type);
        objContentValues.put(SCANNED_MCC_INVOICE_NO, invoice_no);
        objContentValues.put(SCANNED_MCC_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_MCC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_SCANNED_MCC_CODE, objContentValues, whereClause, whereArgs);
        return result;

    }


    // Update Master carton Code List-New Shipment
    public long updateScannedMasterCartonCode(String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_OFFLINE_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SHIPMENT_UPLOAD_TO_SERVER + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_NAME_SHIPMENT, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Update Master carton Code List-New Shipment
    public long updateScannedMCCCode(String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_MCC_OFFLINE_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_MCC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_SCANNED_MCC_CODE, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Fetch Scanned Master carton Code List boolean-New Shipment
    public boolean getScannedMCCQRList(String master_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MCC_CODE + " WHERE " + SCANNED_MCC_CODELIST + " = '" + master_code + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }

    // Fetch Scanned Master carton Code List boolean-New Shipment
    public boolean checkInvoiceinDB(String invoice_number) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MCC_CODE + " WHERE " + SCANNED_MCC_INVOICE_NO + " = '" + invoice_number + "' AND " + SCANNED_MCC_OFFLINE_FLAG + " = '" + 1 + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }


    // Fetch Scanned Master carton Code List-New Shipment
    public ArrayList<String> getScannedMCCQRListforupload(String invoice_no, String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MCC_CODE + " WHERE " + SCANNED_MCC_INVOICE_NO + " = '" + invoice_no + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_MCC_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }


    // Fetch Scanned Master carton Code List-New Shipment
    public ArrayList<String> getScannedMCCQRList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MCC_CODE, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_MCC_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Delete Scanned Master carton Code List-New Shipment
    public void deleteScannedMasterCodeList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SCANNED_MCC_CODE);
    }


    //TODO NEW SHIPMENT OFFLINE START
    // Store New Shipment-Offline
    public void insertNewShipment(NewShipment object) {


        SQLiteDatabase db1 = this.getReadableDatabase();
        Cursor objCursor = db1.rawQuery("SELECT * FROM " + TABLE_NAME_NEWSHIPMENT + " WHERE " + NINVOICE_NUMBER + " = '" + object.getInvoice_number() + "'", null);

        if (objCursor.getCount() == 1) {
            Log.d(TAG, "insertNewShipment if: ");
        } else {
            Log.d(TAG, "insertNewShipment else: ");
            String flag = "0";
            SQLiteDatabase sqldb = getWritableDatabase();
            sqldb.execSQL("DELETE FROM " + TABLE_NAME_NEWSHIPMENT + " WHERE " + SHIPMENT_UPLOAD_SERVER + " = '" + flag + "' ");

            ContentValues objContentValues = new ContentValues();
            objContentValues.put(NAUTH_TOKEN, object.getAuth_token());
            objContentValues.put(QUANTITY, object.getQuantity());
            objContentValues.put(NAGENCY_ID, object.getAgency_id());
            objContentValues.put(NINVOICE_NUMBER, object.getInvoice_number());
            objContentValues.put(NINVOICE_DATE, object.getInvoice_date());
            objContentValues.put(NPO_NUMBER, object.getPo_number());
            objContentValues.put(NPO_DATE, object.getPo_date());
            objContentValues.put(SHIPMENT_TYPE, object.getShipment_type());
            objContentValues.put(MASTER_CODE, String.valueOf(object.getMasterCartonCode()));
            objContentValues.put(SHIPMENT_UPLOAD_SERVER, object.getShipment_upload_to_server());
            SQLiteDatabase db = this.getWritableDatabase();
            db.insert(TABLE_NAME_NEWSHIPMENT, null, objContentValues);
        }

    }

    // Fetch New Shipment-Offline
    public ArrayList<NewShipment> getNotUploadedNewShipmentDetails() {
        ArrayList<NewShipment> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_NEWSHIPMENT + " WHERE " + DatabaseHelper.SHIPMENT_UPLOAD_SERVER + " = " + 0, null);
        if (objCursor != null && objCursor.moveToFirst()) {
            if (objCursor != null && objCursor.getCount() > 0 && objCursor.moveToFirst()) {
                do {
                    arrLogDetailModels.add(new NewShipment(
                            objCursor.getInt(objCursor.getColumnIndex(NEWSHIPMENTID)),
                            objCursor.getString(objCursor.getColumnIndex(NAUTH_TOKEN)),
                            objCursor.getString(objCursor.getColumnIndex(QUANTITY)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NAGENCY_ID)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NINVOICE_NUMBER)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NINVOICE_DATE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NPO_NUMBER)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NPO_DATE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.MASTER_CODE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_TYPE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_SERVER))
                    ));
                } while (objCursor.moveToNext());
            }

        }

        objCursor.close();
        return arrLogDetailModels;
    }


    public ArrayList<NewShipment> getNotUploadedNewShipmentDetail() {
        ArrayList<NewShipment> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_NEWSHIPMENT + " WHERE " + DatabaseHelper.SHIPMENT_UPLOAD_SERVER + " = " + 1, null);
        if (objCursor != null && objCursor.moveToFirst()) {
            if (objCursor != null && objCursor.getCount() > 0 && objCursor.moveToFirst()) {
                do {
                    arrLogDetailModels.add(new NewShipment(
                            objCursor.getInt(objCursor.getColumnIndex(NEWSHIPMENTID)),
                            objCursor.getString(objCursor.getColumnIndex(NAUTH_TOKEN)),
                            objCursor.getString(objCursor.getColumnIndex(QUANTITY)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NAGENCY_ID)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NINVOICE_NUMBER)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NINVOICE_DATE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NPO_NUMBER)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.NPO_DATE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.MASTER_CODE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_TYPE)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_SERVER))
                    ));
                } while (objCursor.moveToNext());
            }

        }

        objCursor.close();
        return arrLogDetailModels;
    }


    // Update Master carton Code List-New Shipment
    public long updateScannedOfllineShipment() {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DatabaseHelper.SHIPMENT_UPLOAD_SERVER, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SHIPMENT_UPLOAD_SERVER + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_NAME_NEWSHIPMENT, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Delete New Shipment-Offline
    public void deleteOfflineNewShipment(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (tag.equals("all")) {
            db.delete(TABLE_NAME_NEWSHIPMENT, null, null);
        } else {
            String whereClause = SHIPMENT_UPLOAD_SERVER + "=?";
            String whereArgs[] = {"0"};
            db.delete(TABLE_NAME_NEWSHIPMENT, whereClause, whereArgs);
        }
    }
    //TODO NEW SHIPMENT OFFLINE END

    //TODO USER INFO START
    public void insertRegisteredUser(String userEmailValue, String userMobValue,
                                     String userNameValue, String auth_token, String user_id) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_COL_EMAIL, userEmailValue);
        contentValues.put(USER_COL_NUMBER, userMobValue);
        contentValues.put(USER_COL_NAME, userNameValue);
        contentValues.put(USER_COL_AUTH_TOKEN, auth_token);
        contentValues.put(USER_COL_ID, user_id);
        db.insert(TABLE_NAME_USER_INFO, null, contentValues);
    }

    public String getAuthToken() {
        String auth_token = "";
        String query = "SELECT * FROM " + TABLE_NAME_USER_INFO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(query, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            auth_token = objCursor.getString(objCursor.getColumnIndex(USER_COL_AUTH_TOKEN));
        }
        return auth_token;
    }

    public String getUserID() {
        String auth_token = "";
        String query = "SELECT * FROM " + TABLE_NAME_USER_INFO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(query, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            auth_token = objCursor.getString(objCursor.getColumnIndex(USER_COL_ID));
        }
        return auth_token;
    }

    public String getUsername() {
        String auth_token = "";
        String query = "SELECT * FROM " + TABLE_NAME_USER_INFO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(query, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            auth_token = objCursor.getString(objCursor.getColumnIndex(USER_COL_NAME));
        }
        return auth_token;
    }
    //TODO USER INFO END

    //TODO AGENCY LIST START
    // Store Agent Data
    public long insertAgentList(AgentListModel object) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(AGENT_ID, object.getId());
        objContentValues.put(AGENT_NAME, object.getName());
        objContentValues.put(AGENT_CODE, object.getCode());
        objContentValues.put(AGENT_EMAIL, object.getEmail());
        objContentValues.put(AGENT_ADDRESS, object.getAddress());
        objContentValues.put(AGENT_MOBILE, object.getMobile());
        objContentValues.put(AGENT_CP_PERSON, object.getCp_person());
        objContentValues.put(AGENT_TYPE, object.getType());
        objContentValues.put(AGENT_LOCATION, object.getLocation());
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.insert(TABLE_NAME_AGENT, null, objContentValues);
        return result;
    }

    // Fetch Agency list
    public ArrayList<AgentListModel> getAgentList(String object) {
        ArrayList<AgentListModel> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_AGENT + " WHERE " + AGENT_TYPE + " = '" + object + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(new AgentListModel(
                        objCursor.getString(objCursor.getColumnIndex(AGENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_EMAIL)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_ADDRESS)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_MOBILE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_CP_PERSON)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_TYPE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_LOCATION))
                ));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<AgentListModel> getAgentList() {
        ArrayList<AgentListModel> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_AGENT, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(new AgentListModel(
                        objCursor.getString(objCursor.getColumnIndex(AGENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_NAME)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_EMAIL)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_ADDRESS)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_MOBILE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_CP_PERSON)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_TYPE)),
                        objCursor.getString(objCursor.getColumnIndex(AGENT_LOCATION))
                ));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }
    //TODO AGENCY LIST END


    //TODO ITEMCODES-PACKAGING START
    // Store Scanned Item codes-packaging
    public long insertItemCode(String object, String flag) {
        long result;
        Log.d(TAG, "insertItemCode itemcode: " + object);
        Log.d(TAG, "insertItemCode flag: " + flag);
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(ITEMCODE_LIST, object);
        objContentValues.put(ITEMCODE_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_ITEMCODE, null, objContentValues);
        return result;
    }

    // Update scanned itemcodes main-packaging
    public long updateScannedItemCode(String item_code, int flag) {

        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(ITEMCODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = ITEMCODE_LIST + "=?";
        String whereArgs[] = {item_code};
        result = db.update(TABLE_ITEMCODE, objContentValues, whereClause, whereArgs);
        return result;

    }


    //  Fetch Scanned Item codes-packaging
    public ArrayList<String> getItemCodes(String flag) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        if (flag.equals("2")) {

            Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_ITEMCODE, null);

            if (objCursor.getCount() > 0) {
                objCursor.moveToFirst();
                do {
                    scanned.add(objCursor.getString(
                            objCursor.getColumnIndex(DatabaseHelper.ITEMCODE_LIST)));
                } while (objCursor.moveToNext());
            }
        } else {

            Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_ITEMCODE + " WHERE " + ITEMCODE_FLAG + " = '" + flag + "'", null);
            Log.d(TAG, "getItemCodes: " + objCursor.getCount());
            if (objCursor.getCount() > 0) {
                objCursor.moveToFirst();
                do {
                    scanned.add(objCursor.getString(
                            objCursor.getColumnIndex(DatabaseHelper.ITEMCODE_LIST)));
                } while (objCursor.moveToNext());
            }
        }

        return scanned;
    }
    //TODO ITEMCODES-PACKAGING END


    //TODO INDIVIDUAL PRODUCT CODES START
    // Store Individual product codes
    public long insertIPCodes(String object) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(IP_CODE_LIST, object);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_IP_CODES, null, objContentValues);
        return result;
    }

    // Fetch Individual product codes
    public ArrayList<String> getIPCode() {
        ArrayList<String> stringArrayList = new ArrayList<>();
        String strQuery = "SELECT * FROM " + TABLE_IP_CODES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);


        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                stringArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.IP_CODE_LIST)));
            } while (objCursor.moveToNext());
        }

        return stringArrayList;
    }


    // Store Scanned Individual product codes
    public void insertScannedIPCodes(String object, String type) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(IP_SCANNED_CODE_LIST, object);
        objContentValues.put(IP_SCANNED_CODE_TYPE, type);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_SCANNED_IP_CODES, null, objContentValues);

    }


    // Store Scanned Individual product codes
    public void insertIPCodeList(String shipment_id, String invoice_no, String invoice_date, String qty, String pending) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(IP_SHIPMENT_ID, shipment_id);
        objContentValues.put(IP_INVOICE_NO, invoice_no);
        objContentValues.put(IP_INVOICE_DATE, invoice_date);
        objContentValues.put(IP_QTY, qty);
        objContentValues.put(IP_PENDING, pending);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_IP_CODELIST, null, objContentValues);

    }

    // Fetch Invoice Scan - Master Carton code product name
    public String getIPCodeQty() {
        String strQuery = "SELECT * FROM " + TABLE_IP_CODELIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        String p_qty = null;

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                p_qty = objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.IP_QTY));

            } while (objCursor.moveToNext());
        }
        return p_qty;
    }


    // Fetch Scanned Individual product codes
    public boolean getIPScannedQRCodeList(String code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_IP_CODES + " WHERE " + IP_SCANNED_CODE_LIST + " = '" + code + "'", null);
        boolean isScan;
        Log.d(TAG, "getScannedInvoiceQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch Scanned Individual product codes
    public int getIPScannedQRCodeList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_IP_CODES, null);
        return objCursor.getCount();
    }

    //  Fetch Individual product codes
    public ArrayList<String> getIPScannedQRCodeListRegular_Breakage(String type) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_IP_CODES + " WHERE " + IP_SCANNED_CODE_TYPE + " = '" + type + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.IP_SCANNED_CODE_LIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public void deleteAllIPCode() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_IP_CODES);
    }

    public void deleteAllIPCodeList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_IP_CODELIST);
    }

    public void deleteAllScannedIPCode() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SCANNED_IP_CODES);
    }
    //TODO INDIVIDUAL PRODUCT CODES END

    //TODO INVOICE SCAN_MASTER CARTON CODES START
    // Store Invoice Scan - Master Carton code
    public long insertIS_MasterCartonCodes(String product_name, String invoice_number, String carton_code, String packing_ratio) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(product_name_MasterCarton, product_name);
        objContentValues.put(invoice_number_MasterCarton, invoice_number);
        objContentValues.put(Mastercode_MasterCarton, carton_code);
        objContentValues.put(Packing_ratio_MasterCarton, packing_ratio);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_IS_MasterCarton, null, objContentValues);
        return result;
    }

    // Fetch Invoice Scan - Master Carton code count
    public int getIS_MasterCartonCount() {
        String strQuery = "SELECT * FROM " + TABLE_IS_MasterCarton + " WHERE " + invoice_number_MasterCarton + " = '" + getRSScannedInvoiceQRList().get(0) + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(strQuery, null);
        return cursor.getCount();
    }


    // Fetch Invoice Scan - Master Carton code
    public ArrayList<master_carton_pojo> getIS_MasterCartonCodes() {
        ArrayList<master_carton_pojo> stringArrayList = new ArrayList<>();
        String strQuery = "SELECT * FROM " + TABLE_IS_MasterCarton + " WHERE " + invoice_number_MasterCarton + " = '" + getRSScannedInvoiceQRList().get(0) + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);


        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                master_carton_pojo masterCartonPojo = new master_carton_pojo(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.product_name_MasterCarton)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.Mastercode_MasterCarton)), objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.Packing_ratio_MasterCarton)));
                stringArrayList.add(masterCartonPojo);
            } while (objCursor.moveToNext());
        }
        return stringArrayList;
    }

    // Fetch Invoice Scan - Master Carton code product name
    public String getIS_MasterCartonCodeSingle(String carton_code) {
        String strQuery = "SELECT * FROM " + TABLE_IS_MasterCarton + " WHERE " + Mastercode_MasterCarton + " = '" + carton_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        String p_name = null;

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                p_name = objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.product_name_MasterCarton));

            } while (objCursor.moveToNext());
        }
        return p_name;
    }

    // Fetch Invoice Scan - Master Carton code packing ratio
    public String getIS_MasterCartonCodePkgRatioSingle(String carton_code) {
        String strQuery = "SELECT * FROM " + TABLE_IS_MasterCarton + " WHERE " + Mastercode_MasterCarton + " = '" + carton_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        String p_name = null;

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                p_name = objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.Packing_ratio_MasterCarton));

            } while (objCursor.moveToNext());
        }
        return p_name;
    }

    // Fetch Invoice Scan - Master Carton code count by product name
    public int getIS_MasterCartonCount(String product_name) {
        String selectQuery = "SELECT " + SCANNED_IS_MASTER_CARTON_PKG_RATIO_ + ",SUM(" + SCANNED_IS_MASTER_CARTON_PKG_RATIO_ + ") as " + SCANNED_IS_MASTER_CARTON_PKG_RATIO_ + " FROM " + TABLE_SCANNED_IS_MASTER_CARTON + " WHERE " + SCANNED_IS_MASTER_CARTON_PRODUCT_NAME + " = '" + product_name + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int total = 0;
        if (cursor.moveToFirst()) {

            total = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SCANNED_IS_MASTER_CARTON_PKG_RATIO_));// get final total
        }
        return total;
    }

    // Fetch Invoice Scan - Master Carton code count
    public int getIS_ScannedMasterCartonCount() {
        String selectQuery = "SELECT * FROM " + TABLE_SCANNED_IS_MASTER_CARTON;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor.getCount();
    }

    // Store Scanned Invoice Scan - Master Carton codes
    public long insertIS_ScannedMasterCartonCodes(String product_name, String carton_code, String type, String packing_ratio) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_IS_MASTER_CARTON_PRODUCT_NAME, product_name);
        objContentValues.put(SCANNED_IS_MASTER_CARTON_CODE_, carton_code);
        objContentValues.put(SCANNED_IS_MASTER_CARTON_TYPE_, type);
        objContentValues.put(SCANNED_IS_MASTER_CARTON_PKG_RATIO_, packing_ratio);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SCANNED_IS_MASTER_CARTON, null, objContentValues);
        return result;
    }

    //  Fetch Invoice Scan - Master Carton codes
    public boolean getIS_ScannedMasterCartonCodeList(String carton_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_IS_MASTER_CARTON + " WHERE " + SCANNED_IS_MASTER_CARTON_CODE_ + " = '" + carton_code + "'", null);
        boolean isScan;
        Log.d(TAG, "getScannedInvoiceQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    //  Fetch Invoice Scan - Master Carton codes
    public ArrayList<String> getIS_ScannedInvoiceQRList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_IS_MASTER_CARTON, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_IS_MASTER_CARTON_CODE_)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public void deleteAllReceiveShipmentISScannedMasterCartons() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SCANNED_IS_MASTER_CARTON);
    }

    public void deleteAllReceiveShipmentISMasterCartons() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_IS_MasterCarton);
    }

    //TODO INVOICE SCAN_MASTER CARTON CODES END


    //TODO PACKING RATIO_PACKAGING START
    //Store Packing ratio-packaging
    public void insertPackingRatio(String packing_ratio, String item_code) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PACKING_RATIO_ITEM_CODE, item_code);
        contentValues.put(PACKING_RATIO_CNT, packing_ratio);
        db.insert(TABLE_PACKING_RATIO, null, contentValues);
    }

    public void insertPackingRatio(String packing_ratio, String item_code, String flag) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PACKING_RATIO_ITEM_CODE, item_code);
        contentValues.put(PACKING_RATIO_CNT, packing_ratio);
        contentValues.put(PACKING_RATIO_FLAG, flag);
        db.insert(TABLE_PACKING_RATIO, null, contentValues);
    }

    public long updatePackingRatio() {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(PACKING_RATIO_FLAG, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = PACKING_RATIO_FLAG + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_PACKING_RATIO, objContentValues, whereClause, whereArgs);
        return result;
    }

    // Fetch packing ratio-packaging
    public int getPackingRatio(String item_code) {
        int cnt = 0;
        String selectQuery = "SELECT * FROM " + TABLE_PACKING_RATIO + " WHERE " + PACKING_RATIO_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                cnt = Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PACKING_RATIO_CNT)));
            } while (cursor.moveToNext());
        }
        return cnt;
    }

    public int getPackingRatioRepack(String item_code, String flag) {
        int cnt = 0;
        String selectQuery = "SELECT * FROM " + TABLE_PACKING_RATIO + " WHERE " + PACKING_RATIO_ITEM_CODE + " = '" + item_code + "' AND " + PACKING_RATIO_FLAG + " = '" + flag + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                cnt = Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PACKING_RATIO_CNT)));
            } while (cursor.moveToNext());
        }
        return cnt;
    }


    //TODO PACKING RATIO_PACKAGING END


    //TODO SCANNED QR CODE START
    // Store Product QR Codes-Packaging
    public long insertQRCode(SystemBarcodeModel object, String item_code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(QR_CODELIST, String.valueOf(object.getJsonArray()));
        objContentValues.put(QR_CODELIST_ITEM_CODE, item_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_QR_CODE, null, objContentValues);
        return result;
    }

    public long insertQRCodeRepack(String object, String item_code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(QR_CODELIST, object);
        objContentValues.put(QR_CODELIST_ITEM_CODE, item_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_QR_CODE, null, objContentValues);
        return result;
    }

    // Fetch Product QR Codes-Packaging
    public SystemBarcodeModel getQRCodeList(String item_code) {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_QR_CODE + " WHERE " + QR_CODELIST_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.QR_CODELIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return model;
    }

    public ArrayList<String> getQRCodeListRepack(String item_code) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        String strQuery = "SELECT * FROM " + TABLE_QR_CODE + " WHERE " + QR_CODELIST_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                stringArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.QR_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return stringArrayList;
    }

    //Store Scanned QR codes main-packaging
    public long insertScannedQRList(String qrcode, String item_code, int flag) {

        Log.d(TAG, "insertScannedQRList qrcode: " + qrcode);
        Log.d(TAG, "insertScannedQRList item_code: " + item_code);
        Log.d(TAG, "insertScannedQRList item_code: " + flag);
        long result = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_QRLIST + " WHERE " + SCANNED_PRODUCT_QR_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_QR_CODE + " = '" + qrcode + "'", null);

        Log.d(TAG, "getScannedQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results

        } else {
            // has NO results

            ContentValues objContentValues = new ContentValues();
            objContentValues.put(SCANNED_PRODUCT_QR_CODE_ITEMID, item_code);
            objContentValues.put(SCANNED_PRODUCT_QR_CODE, qrcode);
            objContentValues.put(SCANNED_PRODUCT_QR_CODE_FLAG, String.valueOf(flag));
            SQLiteDatabase db1 = this.getWritableDatabase();
            result = db1.insert(TABLE_SCANNED_PRODUCT_QRLIST, null, objContentValues);

        }

        return result;
    }

    //Store Scanned QR codes temp-packaging
    public long insertTempScannedQRList(String qrcode, String item_code, int flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(TEMP_SCANNED_PRODUCT_QR_CODE_ITEMID, item_code);
        objContentValues.put(TEMP_SCANNED_PRODUCT_QR_CODE, qrcode);
        objContentValues.put(TEMP_SCANNED_PRODUCT_QR_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_TEMP_SCANNED_PRODUCT_QRLIST, null, objContentValues);
        return result;
    }

    //Fetch Scanned QR codes main boolean-packaging
    public boolean getScannedQRList(String qrcode, String item_code) {
        boolean isScan;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_QRLIST + " WHERE " + SCANNED_PRODUCT_QR_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_QR_CODE + " = '" + qrcode + "'", null);

        Log.d(TAG, "getScannedQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    //Fetch Scanned QR codes temp-packaging
    public boolean getTempScannedQRList(String qrcode, String item_code) {
        boolean isScan;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_QR_CODE_ITEMID + " = '" + item_code + "' AND " + TEMP_SCANNED_PRODUCT_QR_CODE + " = '" + qrcode + "'", null);

        Log.d(TAG, "getScannedQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    //Fetch Scanned QR codes temp-packaging
    public ArrayList<String> getTempScannedQRList(String item_code) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_QR_CODE_ITEMID + " = '" + item_code + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_QR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }

    //Fetch Scanned QR codes main-packaging
    public ArrayList<String> getScannedQRList(String flag) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_QRLIST + " WHERE " + SCANNED_PRODUCT_QR_CODE_FLAG + " = '" + flag + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SCANNED_PRODUCT_QR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }

    // Update scanned qr codes main-packaging
    public long updateScannedQRList(String item_code, int flag) {

        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_PRODUCT_QR_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_PRODUCT_QR_CODE_ITEMID + "=?";
        String whereArgs[] = {item_code};
        result = db.update(TABLE_SCANNED_PRODUCT_QRLIST, objContentValues, whereClause, whereArgs);
        return result;
    }

    //Fetch Scanned QR codes temp-packaging
    public ArrayList<String> getTempScannedQRList() {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST, null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_QR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }
    //TODO SCANNED QR CODE END


    //TODO SCANNED BLADE CODE START
    // Store Blade QR Codes-Packaging
    public long insertBladeCode(SystemBarcodeModel object, String item_code) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(BLADE_CODELIST, String.valueOf(object.getJsonArray()));
        objContentValues.put(BLADE_CODELIST_ITEM_CODE, item_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_BLADE_CODE, null, objContentValues);
        return result;
    }

    public long insertBladeCodeRepack(String object, String item_code) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(BLADE_CODELIST, object);
        objContentValues.put(BLADE_CODELIST_ITEM_CODE, item_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_BLADE_CODE, null, objContentValues);
        return result;
    }

    // Fetch Blade QR Codes-Packaging
    public SystemBarcodeModel getBladeCodeList(String item_code) {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_BLADE_CODE + " WHERE " + BLADE_CODELIST_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.BLADE_CODELIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return model;
    }

    public ArrayList<String> getBladeCodeListRepack(String item_code) {
        ArrayList<String> stringArrayList = new ArrayList<>();
        String strQuery = "SELECT * FROM " + TABLE_BLADE_CODE + " WHERE " + BLADE_CODELIST_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                stringArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.BLADE_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return stringArrayList;
    }

    // Store scanned blade qr codes main-packaging
    public long insertScannedBladeQRList(String qrcode, String item_code, int flag) {
        long result = 0;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_BLADELIST + " WHERE " + SCANNED_PRODUCT_BLADE_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_BLADE_CODE + " = '" + qrcode + "'", null);


        if (objCursor != null && objCursor.getCount() > 0) {
            // has results

        } else {
            // has NO results

            ContentValues objContentValues = new ContentValues();
            objContentValues.put(SCANNED_PRODUCT_BLADE_CODE_ITEMID, item_code);
            objContentValues.put(SCANNED_PRODUCT_BLADE_CODE, qrcode);
            objContentValues.put(SCANNED_PRODUCT_BLADE_CODE_FLAG, String.valueOf(flag));
            SQLiteDatabase db1 = this.getWritableDatabase();
            result = db1.insert(TABLE_SCANNED_PRODUCT_BLADELIST, null, objContentValues);
        }


        return result;
    }

    // Fetch scanned blade qr codes temp-packaging
    public long insertTempScannedBladeQRList(String qrcode, String item_code, int flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(TEMP_SCANNED_PRODUCT_BLADE_CODE_ITEMID, item_code);
        objContentValues.put(TEMP_SCANNED_PRODUCT_BLADE_CODE, qrcode);
        objContentValues.put(TEMP_SCANNED_PRODUCT_BLADE_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_TEMP_SCANNED_PRODUCT_BLADELIST, null, objContentValues);
        return result;
    }


    // Fetch scanned blade qr codes main boolean-packaging
    public boolean getScannedBladeQRList(String qrcode, String item_code) {
        boolean isScan;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_BLADELIST + " WHERE " + SCANNED_PRODUCT_BLADE_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_BLADE_CODE + " = '" + qrcode + "'", null);

        Log.d(TAG, "getScannedBladeQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch scanned blade qr codes temp boolean-packaging
    public boolean getTempScannedBladeQRList(String qrcode, String item_code) {
        boolean isScan;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST + " WHERE " + TEMP_SCANNED_PRODUCT_BLADE_CODE_ITEMID + " = '" + item_code + "' AND " + TEMP_SCANNED_PRODUCT_BLADE_CODE + " = '" + qrcode + "'", null);

        Log.d(TAG, "getScannedBladeQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch scanned blade qr codes temp-packaging
    public ArrayList<String> getTempScannedBladeQRList(String item_code) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST + " WHERE " + TEMP_SCANNED_PRODUCT_BLADE_CODE_ITEMID + " = '" + item_code + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_BLADE_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }

    // Fetch scanned blade qr codes main-packaging
    public ArrayList<String> getScannedBladeQRList(String flag) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_BLADELIST + " WHERE " + SCANNED_PRODUCT_BLADE_CODE_FLAG + " = '" + flag + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SCANNED_PRODUCT_BLADE_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }


    // Update scanned qr codes main-packaging
    public long updateScannedBladeQRList(String item_code, int flag) {

        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_PRODUCT_BLADE_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_PRODUCT_BLADE_CODE_ITEMID + "=?";
        String whereArgs[] = {item_code};
        result = db.update(TABLE_SCANNED_PRODUCT_BLADELIST, objContentValues, whereClause, whereArgs);
        return result;
    }


    // Fetch scanned blade qr codes temp-packaging
    public ArrayList<String> getTempScannedBladeQRList() {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST, null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_BLADE_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }
    //TODO SCANNED BLADE CODE END


    //TODO SCANNED MASTER CODE START
    // Store Master QR Codes-Packaging
    public long insertMasterQrCode(SystemBarcodeModel object, String item_code) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(MASTER_QR_CODELIST, String.valueOf(object.getJsonArray()));
        objContentValues.put(MASTER_QR_CODELIS_ITEM_CODE, item_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_MASTER_QR_CODE, null, objContentValues);
        return result;
    }

    // Fetch Master QR Codes-Packaging
    public SystemBarcodeModel getMasterQRCodeList(String item_code) {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_MASTER_QR_CODE + " WHERE " + MASTER_QR_CODELIS_ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.MASTER_QR_CODELIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return model;
    }

    public SystemBarcodeModel getMasterQRCodeListRepack() {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_MASTER_QR_CODE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.MASTER_QR_CODELIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return model;
    }

    // Store scanned master qr codes main-packaging
    public long insertScannedMasterQRList(String qrcode, String item_code, String flag, String packing_ratio) {
        long result = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_MASTERQR_CODE + " = '" + qrcode + "'", null);
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results

        } else {
            // has NO results
            ContentValues objContentValues = new ContentValues();
            objContentValues.put(SCANNED_PRODUCT_MASTERQR_CODE_ITEMID, item_code);
            objContentValues.put(SCANNED_PRODUCT_MASTERQR_CODE, qrcode);
            objContentValues.put(SCANNED_PRODUCT_MASTERQR_CODE_FLAG, flag);
            objContentValues.put(SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO, packing_ratio);
            SQLiteDatabase db1 = this.getWritableDatabase();
            result = db1.insert(TABLE_SCANNED_PRODUCT_MASTERQRLIST, null, objContentValues);
        }


        return result;
    }

    // Store scanned master qr codes temp-packaging
    public long insertTempScannedMasterQRList(String qrcode, String item_code, int flag, String packing_ratio) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID, item_code);
        objContentValues.put(TEMP_SCANNED_PRODUCT_MASTERQR_CODE, qrcode);
        objContentValues.put(TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG, String.valueOf(flag));
        objContentValues.put(TEMP_SCANNED_PRODUCT_MASTERQR_CODE_PKG_RATIO, packing_ratio);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST, null, objContentValues);
        return result;
    }

    // Update scanned master qr codes temp-packaging
    public long updateTempScannedMasterQRList(String item_code, int flag) {

        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + "=?";
        String whereArgs[] = {item_code};
        result = db.update(TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Update scanned master qr codes main-packaging
    public long updateScannedMasterQRList(String item_code, int flag) {

        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_PRODUCT_MASTERQR_CODE_FLAG, String.valueOf(flag));
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + "=?";
        String whereArgs[] = {item_code};
        result = db.update(TABLE_SCANNED_PRODUCT_MASTERQRLIST, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Fetch scanned master qr codes main boolean-packaging
    public boolean getScannedMasterQRList(String qrcode, String item_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + " = '" + item_code + "' AND " + SCANNED_PRODUCT_MASTERQR_CODE + " = '" + qrcode + "'", null);
        boolean isScan;
        Log.d(TAG, "getScannedMasterQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch scanned master qr codes temp boolean-packaging
    public boolean getTempScannedMasterQRList(String qrcode, String item_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + " = '" + item_code + "' AND " + TEMP_SCANNED_PRODUCT_MASTERQR_CODE + " = '" + qrcode + "'", null);
        boolean isScan;
        Log.d(TAG, "getScannedMasterQRList: " + objCursor.getCount());
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch scanned master qr codes temp-packaging
    public ArrayList<String> getTempScannedMasterQRList(String item_code) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_ITEMID + " = '" + item_code + "'", null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_MASTERQR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;


    }

    // Fetch scanned master qr codes main-packaging
    public ArrayList<String> getScannedMasterQRList(String flag) {
        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + SCANNED_PRODUCT_MASTERQR_CODE_FLAG + " = '" + flag + "'", null);

        Log.d(TAG, "getScannedMasterQRList: " + "SELECT * FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + SCANNED_PRODUCT_MASTERQR_CODE_FLAG + " = '" + flag + "'");
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SCANNED_PRODUCT_MASTERQR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }

    // Fetch scanned master qr codes temp by flag-packaging
    public ArrayList<String> getTempScannedMasterQRListbyFlag(String flag) {

        ArrayList<String> scanned = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG + " = '" + flag + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                scanned.add(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.TEMP_SCANNED_PRODUCT_MASTERQR_CODE)));
            } while (objCursor.moveToNext());
        }
        return scanned;
    }
    //TODO SCANNED MASTER CODE END

    //TODO BATCH LIST START
    // Store batch list data
    public long insertBatchList(String item_code, String model, String seprate_blade) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(ITEM_CODE, item_code);
        objContentValues.put(MODEL, model);
        objContentValues.put(SEPARATE_BLADE, seprate_blade);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_NAME_BATCH_LIST, null, objContentValues);
        return result;
    }

    // Fetch batch list data
    public ArrayList<BatchIdList> getBatchList() {
        ArrayList<BatchIdList> batchIdLists = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_NAME_BATCH_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                BatchIdList batchIdList = new BatchIdList();
                batchIdList.setItemCode(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.ITEM_CODE)));
                batchIdList.setModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.MODEL)));
                batchIdList.setSeprateBlade(Boolean.valueOf(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SEPARATE_BLADE))));

                batchIdLists.add(batchIdList);
            } while (objCursor.moveToNext());
        }
        return batchIdLists;
    }

    public ArrayList<BatchIdList> getBatchListForRepacking() {
        ArrayList<BatchIdList> batchIdLists = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + ITEM_CODE + "," + MODEL + "," + SEPARATE_BLADE + ",SUM(" + ITEM_CODE + ") as " + ITEM_CODE + " FROM " + TABLE_NAME_BATCH_LIST + " GROUP BY " + MODEL, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
//                if (Integer.parseInt(objCursor.getString(
//                        objCursor.getColumnIndex(DatabaseHelper.ITEM_CODE))) > 1) {
                BatchIdList batchIdList = new BatchIdList();

                batchIdList.setItemCode(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.ITEM_CODE)));
                batchIdList.setModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.MODEL)));
                boolean blade;
                if (objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SEPARATE_BLADE)).equals("0")) {
                    blade = false;
                } else {
                    blade = true;
                }
                batchIdList.setSeprateBlade(blade);
                batchIdLists.add(batchIdList);
//                }
            } while (objCursor.moveToNext());
        }
        return batchIdLists;
    }

    // Store batch list data by item code
    public boolean getBatchList(String item_code) {

        boolean blade = false;
        String selectQuery = "SELECT * FROM " + TABLE_NAME_BATCH_LIST + " WHERE " + ITEM_CODE + " = '" + item_code + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                blade = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SEPARATE_BLADE)));
            } while (cursor.moveToNext());
        }
        return blade;
    }
    //TODO BATCH LIST END


    //TODO INVOICE CODE LIST START
    // Fetch Invoice Code List-New Shipment
    public ArrayList<String> getInvoiceCodeList() {
        ArrayList<String> stringArrayList = new ArrayList<>();
        String strQuery = "SELECT * FROM " + TABLE_INVOICE_CODE + " WHERE " + INVOICE_CODELIST_FLAG + " = '" + 0 + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                stringArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.INVOICE_CODELIST)));
            } while (objCursor.moveToNext());

        }
        objCursor.close();
        return stringArrayList;
    }

    public int getInvoiceCodeListbyCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_INVOICE_CODE + " WHERE " + INVOICE_CODELIST_FLAG + " = '" + 0 + "'", null);
        return objCursor.getCount();
    }


    // Store Invoice Code List-New Shipment
    public long insertInvoiceCodeList(String invoice_code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(INVOICE_CODELIST, invoice_code);
        objContentValues.put(INVOICE_CODELIST_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_INVOICE_CODE, null, objContentValues);
        return result;
    }

    // Update Master carton Code List-New Shipment
    public long updateInvoiceCode(String code, String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(INVOICE_CODELIST_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = INVOICE_CODELIST + "=?";
        String whereArgs[] = {code};
        result = db.update(TABLE_INVOICE_CODE, objContentValues, whereClause, whereArgs);
        return result;
    }


    // Update Master carton Code List-New Shipment
    public long updateInvoiceCode(String flag) {
        Log.d(TAG, "updateInvoiceCode: " + flag);
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(INVOICE_CODELIST_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = INVOICE_CODELIST_FLAG + "=?";
        String whereArgs[] = {"1"};
        result = db.update(TABLE_INVOICE_CODE, objContentValues, whereClause, whereArgs);
        return result;
    }

    // Delete Invoice Code List-New Shipment
    public void deleteInvoiceCodeList() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_INVOICE_CODE);
    }

    // Store Scanned Invoice Code List-New Shipment
    public long insertScannedInvoiceCode(String invoice_code) {
        long result;
        Log.d(TAG, "insertScannedInvoiceCode: " + invoice_code);
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_INVOICE_CODELIST, invoice_code);
        objContentValues.put(SCANNED_INVOICE_CODELIST_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SCANNED_INVOICE_CODE, null, objContentValues);
        return result;
    }

    // Update Master carton Code List-New Shipment
    public long updateScanneInvoiceCode(String code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_INVOICE_CODELIST_FLAG, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SCANNED_INVOICE_CODELIST + "=?";
        String whereArgs[] = {code};
        result = db.update(TABLE_SCANNED_INVOICE_CODE, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Fetch Scanned Invoice Code List boolean-New Shipment
    public boolean getScannedInvoiceQRList(String invoice_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_INVOICE_CODE + " WHERE " + SCANNED_INVOICE_CODELIST + " = '" + invoice_code + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch Scanned Invoice Code List-New Shipment
    public ArrayList<String> getScannedInvoiceQRList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_INVOICE_CODE + " WHERE " + SCANNED_INVOICE_CODELIST_FLAG + " = '" + 0 + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_INVOICE_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Fetch Scanned Invoice Code List-New Shipment
    public ArrayList<String> getScannedInvoiceQRListforUpload() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_INVOICE_CODE + " WHERE " + SCANNED_INVOICE_CODELIST_FLAG + " = '" + 1 + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_INVOICE_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Delete Scanned Invoice Code List-New Shipment
    public void deleteScannedInvoiceCodeList(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (tag.equals("all")) {
            db.execSQL("DELETE FROM " + TABLE_SCANNED_INVOICE_CODE);
        } else {
            String whereClause = SCANNED_INVOICE_CODELIST_FLAG + "=?";
            String whereArgs[] = {"0"};
            db.delete(TABLE_SCANNED_INVOICE_CODE, whereClause, whereArgs);
        }
    }
    //TODO INVOICE CODE LIST START


    //TODO RECEIVE NEW SHIPMENT START
    // Store Master Carton Code List-Receive New Shipment
    public void insertReceiveNewShipment(NewShipmentDetailModel object) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(RNS_ID, object.getNewShipmentID());
        objContentValues.put(RNS_CODE, object.getShipmentCode());
        objContentValues.put(RNS_PACKEDRATIO, object.getPackedRatio());
        objContentValues.put(RNS_PRODUCTNAME, object.getProductName());
        objContentValues.put(RNS_SWEEP, object.getSweep());
        objContentValues.put(RNS_COLOR, object.getColor());
        objContentValues.put(RNS_SHIPMENT_UPLOAD_TO_SERVER, object.getUploadToServer());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME_RECEIVENEWSHIPMENT, null, objContentValues);
    }

    // Fetch Master Carton Code List-Receive New Shipment
    public SystemBarcodeModel getReceiveShipmentBarcode1() {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_NAME_RECEIVENEWSHIPMENT;
        ArrayList<String> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {

            do {
                newShipmentDetailModelArrayList.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RNS_CODE)));
                model = new SystemBarcodeModel(new JSONArray(newShipmentDetailModelArrayList));
            } while (objCursor.moveToNext());


        }
        return model;
    }


    // Store Master Carton Codes-Receive New Shipment
    public long insertMasterCartoncode(SystemBarcodeModel object) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SYSTEM_BARCODE_LIST, String.valueOf(object.getJsonArray()));
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SYSTEM_BARCODE, null, objContentValues);
        return result;
    }

    // Fetch Master Carton Codes-Receive New Shipment
    public SystemBarcodeModel getMasterCartonCode() {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_SYSTEM_BARCODE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SYSTEM_BARCODE_LIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return model;
    }

    // Delete Master Carton Code List-Receive New Shipment
    public void deleteServerBarcodeDataTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SYSTEM_BARCODE);
    }

    // Update Master Carton Code List-Receive New Shipment
//    public void updateMasterCartonTable(LogDetailModel object) {
//        ContentValues objContentValues = new ContentValues();
//        objContentValues.put(SYSTEM_BARCODE_LIST, object.getScannedData());
//        int recordID = getRecordId1();
//        SQLiteDatabase db = this.getWritableDatabase();
//        String whereClause = SYSTEM_BARCODE_LIST_ID + "=?";
//        String whereArgs[] = {String.valueOf(recordID)};
//        db.update(TABLE_SYSTEM_BARCODE, objContentValues, whereClause, whereArgs);
//    }

    // Fetch Master Carton Code ID-Receive New Shipment
    private int getRecordId1() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SYSTEM_BARCODE, null);
        objCursor.moveToFirst();
        int id = objCursor.getInt(objCursor.getColumnIndex(SYSTEM_BARCODE_LIST_ID));
        objCursor.close();
        return id;
    }

    public boolean getScannedMasterCartonQRListBoolean(String barcode) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MASTER_CARTON_BARCODE + " WHERE " + SYSTEM_SCANNED_MASTER_CARTON_LIST + " = '" + barcode + "'", null);
        boolean isScan;

        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Store Scanned Master Carton Codes-Receive New Shipment
    public void insertScannedMasterCartonCode(String barcode, String flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SYSTEM_SCANNED_MASTER_CARTON_LIST, barcode);
        objContentValues.put(SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_SCANNED_MASTER_CARTON_BARCODE, null, objContentValues);
    }


    // Update Master carton Code List-New Shipment
    public long updateScannedReceiveMasterCartonCode(String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_SCANNED_MASTER_CARTON_BARCODE, objContentValues, whereClause, whereArgs);
        return result;

    }

    // Fetch Scanned Master Carton Codes-Receive New Shipment
    public ArrayList<String> getScannedMasterCartonQRList(String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_MASTER_CARTON_BARCODE + " WHERE " + SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG + " = '" + flag + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SYSTEM_SCANNED_MASTER_CARTON_LIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Store Invoice Codes-Receive New Shipment
    public long inserRSInvoicecode(SystemBarcodeModel object) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(RSI_CL_LIST, String.valueOf(object.getJsonArray()));
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST, null, objContentValues);
        return result;
    }

    // Fetch Invoice Codes-Receive New Shipment
    public SystemBarcodeModel getInvoiceScanCode() {
        SystemBarcodeModel model = null;
        String strQuery = "SELECT * FROM " + TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);

        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            try {
                model = new SystemBarcodeModel(new JSONArray(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RSI_CL_LIST))));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return model;
    }

    // Store Invoice Code-Receive New Shipment
    public long inserRSInvoicecodeData(NewShipmentDetailModel object) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(RECEIVE_INVOICE__ID, object.getShipmentCode());
        objContentValues.put(RECEIVE_INVOICE_MODEL, object.getProductName());
        objContentValues.put(RECEIVE_INVOICE_SWEEP, object.getSweep());
        objContentValues.put(RECEIVE_INVOICE_COLOR, object.getColor());
        objContentValues.put(RECEIVE_INVOICE_QTY, object.getQty());
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(RECEIVE_INVOICE_SHIPMENT, null, objContentValues);
        return result;
    }

    // Fetch Invoice Code boolean-Receive New Shipment
    public ArrayList<NewShipmentDetailModel> getRSInvoicecodeData() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + RECEIVE_INVOICE_SHIPMENT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE__ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_MODEL)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_SWEEP)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_COLOR)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_QTY)), ""));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    // Fetch One Invoice Code boolean-Receive New Shipment
    public ArrayList<NewShipmentDetailModel> getRSSingleInvoiceQR() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> newShipmentScannedArrayList = getRSScannedInvoiceQRList();
        Log.d(TAG, "getRSSingleInvoiceQR: " + newShipmentScannedArrayList.size());
        for (int i = 0; i < newShipmentScannedArrayList.size(); i++) {

            Cursor objCursor = db.rawQuery("SELECT " + RECEIVE_INVOICE__ID + "," + RECEIVE_INVOICE_SWEEP + "," + RECEIVE_INVOICE_COLOR + "," + RECEIVE_INVOICE_MODEL + ",SUM(" + RECEIVE_INVOICE_QTY + ") as " + RECEIVE_INVOICE_QTY + " FROM " + RECEIVE_INVOICE_SHIPMENT + " WHERE " + RECEIVE_INVOICE__ID + " = '" + newShipmentScannedArrayList.get(0) + "'" + " GROUP BY " + RECEIVE_INVOICE_MODEL, null);
            Log.d(TAG, "getRSSingleInvoiceQR: " + "SELECT " + RECEIVE_INVOICE__ID + "," + RECEIVE_INVOICE_SWEEP + "," + RECEIVE_INVOICE_COLOR + "," + RECEIVE_INVOICE_MODEL + ",SUM(" + RECEIVE_INVOICE_QTY + ") as " + RECEIVE_INVOICE_QTY + " FROM " + RECEIVE_INVOICE_SHIPMENT + " WHERE " + RECEIVE_INVOICE__ID + " = '" + newShipmentScannedArrayList.get(0) + "'" + " GROUP BY " + RECEIVE_INVOICE_MODEL);
            objCursor.moveToFirst();
            if (objCursor.getCount() > 0) {
                do {
                    newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                            objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE__ID)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_MODEL)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_SWEEP)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_COLOR)),
                            objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RECEIVE_INVOICE_QTY)), ""));
                } while (objCursor.moveToNext());
            }
        }


        return newShipmentDetailModelArrayList;
    }

    // Delete Receive New Shipment
    public void deleteAllReceiveShipmentCodeData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + RECEIVE_INVOICE_SHIPMENT);
    }


    // Store Scanned Invoice Code-Receive New Shipment
    public long insertRSScannedInvoiceCode(String invoice_code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(RS_SCANNED_INVOICE_CODELIST, invoice_code);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_RS_SCANNED_INVOICE_CODE, null, objContentValues);
        return result;
    }

    // Fetch Scanned Invoice Code boolean-Receive New Shipment
    public boolean getRSScannedInvoiceQRList(String invoice_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_RS_SCANNED_INVOICE_CODE + " WHERE " + RS_SCANNED_INVOICE_CODELIST + " = '" + invoice_code + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;

        }
        return isScan;
    }

    // Fetch Scanned Invoice Code-Receive New Shipment
    public ArrayList<String> getRSScannedInvoiceQRList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_RS_SCANNED_INVOICE_CODE, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.RS_SCANNED_INVOICE_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Delete Invoice Code-Receive New Shipment
    public void deleteAllReceiveShipmentScannedCodeData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_RS_SCANNED_INVOICE_CODE);
    }
    //TODO RECEIVE NEW SHIPMENT END

    //TODO NEW SHIPMENT DATA START
    // Store new shipment data-Packed Carton
    public void insertShipment(NewShipmentDetailModel object) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_ID, object.getNewShipmentID());
        objContentValues.put(SHIPMENT_PACKEDRATIO, object.getPackedRatio());
        objContentValues.put(SHIPMENT_CODE, object.getShipmentCode());
        objContentValues.put(SHIPMENT_PRODUCTNAME, object.getProductName());
        objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, object.getUploadToServer());
        objContentValues.put(SHIPMENT_OFFLINE_FLAG, "0");
        objContentValues.put(SHIPMENT_SENDER_ID, object.getSender_id());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME_SHIPMENT, null, objContentValues);
    }

    //Packed Carton
    public ArrayList<NewShipmentDetailModel> getShipmentSystemBarcode() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_OFFLINE_FLAG + " = '" + 0 + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    public ArrayList<NewShipmentDetailModel> getShipmentSystemBarcode(String barcode) {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_CODE + " = '" + barcode + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }


    //Packed Carton For Breakage Scan
    public ArrayList<NewShipmentDetailModel> getShipmentSystemBarcodeForBreakageScan() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = '" + 1 + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    //Packed Carton
    public ArrayList<NewShipmentDetailModel> getShipmentSystem() {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();

        String strQuery = "SELECT * FROM " + TABLE_NAME_SHIPMENT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery(strQuery, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }


    // Update new shipment data-Packed Carton
    public void updateShipment(String qrcode, String object) {

        Log.d(TAG, "updateShipment qrcode: " + qrcode);
        Log.d(TAG, "updateShipment flag: " + object);
//        long result;
//        SQLiteDatabase database = this.getWritableDatabase();
//        database.beginTransaction();
//        ContentValues values = new ContentValues();
//        values.put(SHIPMENT_UPLOAD_TO_SERVER, object);
//        // Updating Row
//        result = database.update(TABLE_NAME_SHIPMENT, values, SHIPMENT_CODE + " = ?", new String[]{qrcode});
//        database.setTransactionSuccessful();
//        database.endTransaction();
//        return result;

        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, object);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SHIPMENT_CODE + "=?";
        String whereArgs[] = {qrcode};
        db.update(TABLE_NAME_SHIPMENT, objContentValues, whereClause, whereArgs);
    }


    // Update new shipment data-Packed Carton
    public void updateShipmentFromSaleReturn(String qrcode, String object, String ship_flag) {

        Log.d(TAG, "updateShipment qrcode: " + qrcode);
        Log.d(TAG, "updateShipment flag: " + object);

        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_OFFLINE_FLAG, object);
        objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, ship_flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SHIPMENT_CODE + "=?";
        String whereArgs[] = {qrcode};
        db.update(TABLE_NAME_SHIPMENT, objContentValues, whereClause, whereArgs);
    }

    // Fetch new shipment data boolean-Packed Carton
    public boolean getCurrentShipmentDetails(String object) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_CODE + " = '" + object + "'", null);

        boolean isScan;
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            isScan = objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)).equalsIgnoreCase("0");
        } else {
            isScan = false;
        }
        return isScan;
    }

//    //Fetch not scanned new shipment data-Packed Carton
//    public ArrayList<NewShipmentDetailModel> getNotScanShipmentDetails() {
//        ArrayList<NewShipmentDetailModel> arrLogDetailModels = new ArrayList<>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER + " = " + 1, null);
//        if (objCursor.getCount() > 0) {
//            objCursor.moveToFirst();
//            do {
//                arrLogDetailModels.add(new NewShipmentDetailModel(
//                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
//                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
//                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
//                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
//                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER))
//                ));
//            } while (objCursor.moveToNext());
//        }
//        objCursor.close();
//        return arrLogDetailModels;
//    }


    //Update shipment data-Packed Carton
    public void updateShipmentData() {
        Log.d(TAG, "updateShipmentData: ");
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, "1");
        objContentValues.put(SHIPMENT_OFFLINE_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
//        String whereClause = SHIPMENT_OFFLINE_FLAG + "=?";
//        String whereArgs[] = {"0"};
        db.update(TABLE_NAME_SHIPMENT, objContentValues, null, null);
    }

    public void updateShipmentDataOffline() {
        Log.d(TAG, "updateShipmentData: ");
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SHIPMENT_OFFLINE_FLAG + "=?";
        String whereArgs[] = {"0"};
        db.update(TABLE_NAME_SHIPMENT, objContentValues, whereClause, whereArgs);
    }


    //Fetch scanned new shipment data-Packed Carton
    public ArrayList<NewShipmentDetailModel> getScanShipmentDetails() {
        ArrayList<NewShipmentDetailModel> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = " + 0 + " AND " + SHIPMENT_OFFLINE_FLAG + " = 0", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(new NewShipmentDetailModel(
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))
                ));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }


    //Fetch scanned new shipment data-Packed Carton
    public ArrayList<String> getScanShipmentBarcode() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = " + 0, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)));
            } while (objCursor.moveToNext());
        }

        return arrLogDetailModels;
    }

    //Fetch scanned new shipment data-Packed Carton
    public ArrayList<NewShipmentDetailModel> getListScanedShipment() {
        ArrayList<NewShipmentDetailModel> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + SHIPMENT_ID + "," + SHIPMENT_SENDER_ID + "," + SHIPMENT_UPLOAD_TO_SERVER + "," + SHIPMENT_CODE + "," + SHIPMENT_PRODUCTNAME + ",SUM(" + SHIPMENT_PACKEDRATIO + ") as " + SHIPMENT_PACKEDRATIO + " FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = 0 " + " AND " + SHIPMENT_OFFLINE_FLAG + " = 0 " + " GROUP BY " + SHIPMENT_PRODUCTNAME, null);

        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(new NewShipmentDetailModel(
                        objCursor.getString(objCursor.getColumnIndex(SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(SHIPMENT_SENDER_ID)))
                ));
            } while (objCursor.moveToNext());
        }

        return arrLogDetailModels;
    }

    //Fetch scanned new shipment data count-Packed Carton
    public long getTotalShipmentCount() {
        long ScanCount = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT SUM(" + SHIPMENT_PACKEDRATIO + ")as " + SHIPMENT_PACKEDRATIO + " FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = '0'", null);
        if (objCursor != null && objCursor.moveToFirst()) {
            do {
                ScanCount = objCursor.getLong(objCursor.getColumnIndex(SHIPMENT_PACKEDRATIO));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return ScanCount;
    }

    public void deleteAllPackedCartonCode() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME_SHIPMENT);
    }

    //TODO NEW SHIPMENT DATA END


    //TODO STOCK START
    // Fetch packed data-Stock
    public ArrayList<NewShipmentDetailModel> getPackedCartonDataForStock() {
        ArrayList<NewShipmentDetailModel> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor objCursor = db.rawQuery("SELECT " + SHIPMENT_ID + "," + SHIPMENT_SENDER_ID + "," + SHIPMENT_UPLOAD_TO_SERVER + "," + SHIPMENT_CODE + "," + SHIPMENT_PRODUCTNAME + ",SUM(" + SHIPMENT_PACKEDRATIO + ") as " + SHIPMENT_PACKEDRATIO + " FROM " + TABLE_NAME_SHIPMENT + " GROUP BY " + SHIPMENT_PRODUCTNAME, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(new NewShipmentDetailModel(
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PACKEDRATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_PRODUCTNAME)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_UPLOAD_TO_SERVER)),
                        Integer.parseInt(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SHIPMENT_SENDER_ID)))
                ));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }
    //TODO STOCK END


    //TODO RECEIVE NEW SHIPMENT-MASTER CARTONS START
    // Store Carton data-Receive New shipment by Master Carton
    public long insertLog(LogDetailModel object) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(LOG_NUMBER_OF_CARTONS, object.getNumberOfCartons());
        objContentValues.put(LOG_COMPLETED, object.getLogCompleted());
        objContentValues.put(LOG_UPLOAD_TO_SERVER, object.getLogUploadToServerStatus());
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.insert(TABLE_NAME_LOGS, null, objContentValues);
        return result;
    }

    // Update Carton data-Receive New shipment by Master Carton
    public long updateLog(LogDetailModel object) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(LOG_NUMBER_OF_CARTONS, object.getNumberOfCartons());
        objContentValues.put(LOG_COMPLETED, object.getLogCompleted());
        objContentValues.put(LOG_UPLOAD_TO_SERVER, object.getLogUploadToServerStatus());
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = LOG_UPLOAD_TO_SERVER + "=?";
        String whereArgs[] = {"0"};
        result = db.update(TABLE_NAME_LOGS, objContentValues, whereClause, whereArgs);
        return result;
    }

    // Fetch current Carton data-Receive New shipment by Master Carton
    public ArrayList<LogDetailModel> getCurrentLogDetails() {
        ArrayList<LogDetailModel> logDetailModelArrayList = new ArrayList<>();
        LogDetailModel logDetails;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_LOGS + " WHERE " + DatabaseHelper.LOG_UPLOAD_TO_SERVER + " = '" + 0 + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                logDetails = new LogDetailModel(
                        objCursor.getString(objCursor.getColumnIndex(LOG_NUMBER_OF_CARTONS)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.LOG_COMPLETED)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.LOG_UPLOAD_TO_SERVER))
                );
                logDetailModelArrayList.add(logDetails);
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return logDetailModelArrayList;
    }

    // Fetch all Carton data-Receive New shipment by Master Carton
    public ArrayList<LogDetailModel> getLogDetails() {
        ArrayList<LogDetailModel> logDetailModelArrayList = new ArrayList<>();
        LogDetailModel logDetails;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_LOGS + " WHERE " + DatabaseHelper.LOG_UPLOAD_TO_SERVER + " = '" + 1 + "' AND " + DatabaseHelper.LOG_COMPLETED + " = '" + 1 + "'", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                logDetails = new LogDetailModel(
                        objCursor.getString(objCursor.getColumnIndex(LOG_NUMBER_OF_CARTONS)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.LOG_COMPLETED)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.LOG_UPLOAD_TO_SERVER))
                );
                logDetailModelArrayList.add(logDetails);
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return logDetailModelArrayList;
    }

    // Delete current Carton data-Receive New shipment by Master Carton
    public void discardEntry() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = LOG_UPLOAD_TO_SERVER + "=?";
            String whereArgs[] = {"0"};
            db.delete(TABLE_NAME_LOGS, whereClause, whereArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //TODO RECEIVE NEW SHIPMENT-MASTER CARTONS END

    //Delete all packaging data-Pack products
    public void deleteAllpackingdata() {
        SQLiteDatabase db = this.getWritableDatabase();

        //Packing tables from server
        db.execSQL("DELETE FROM " + TABLE_NAME_BATCH_LIST);
        db.execSQL("DELETE FROM " + TABLE_QR_CODE);
        db.execSQL("DELETE FROM " + TABLE_BLADE_CODE);
        db.execSQL("DELETE FROM " + TABLE_MASTER_QR_CODE);
        db.execSQL("DELETE FROM " + TABLE_ITEMCODE);
        db.execSQL("DELETE FROM " + TABLE_PACKING_RATIO);

    }

    //Delete all data-Sync Server Data
    public void deleteAllTableData() {
        SQLiteDatabase db = this.getWritableDatabase();

        //New Shipment from server
        db.execSQL("DELETE FROM " + TABLE_NAME_AGENT);
        db.execSQL("DELETE FROM " + TABLE_NAME_SHIPMENT);
        db.execSQL("DELETE FROM " + TABLE_INVOICE_CODE);


        //Packing tables from server
        db.execSQL("DELETE FROM " + TABLE_NAME_BATCH_LIST);
        db.execSQL("DELETE FROM " + TABLE_QR_CODE);
        db.execSQL("DELETE FROM " + TABLE_BLADE_CODE);
        db.execSQL("DELETE FROM " + TABLE_MASTER_QR_CODE);

        //Scanned Packing tables
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST);

        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST);

        //Offline Shipment
        db.execSQL("DELETE FROM " + TABLE_NAME_NEWSHIPMENT);

        db.execSQL("DELETE FROM " + TABLE_SCANNED_INVOICE_CODE);
        db.execSQL("DELETE FROM " + TABLE_PACKING_RATIO);

        db.execSQL("DELETE FROM " + TABLE_NAME_LOGS);
        db.execSQL("DELETE FROM " + TABLE_NAME_RECEIVENEWSHIPMENT);
        db.execSQL("DELETE FROM " + TABLE_SYSTEM_BARCODE);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_MASTER_CARTON_BARCODE);

        db.execSQL("DELETE FROM " + TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST);
        db.execSQL("DELETE FROM " + RECEIVE_INVOICE_SHIPMENT);
        db.execSQL("DELETE FROM " + TABLE_RS_SCANNED_INVOICE_CODE);
        db.execSQL("DELETE FROM " + TABLE_ITEMCODE);
        db.execSQL("DELETE FROM " + TABLE_IS_MasterCarton);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_IS_MASTER_CARTON);
        db.execSQL("DELETE FROM " + TABLE_IP_CODES);
        db.execSQL("DELETE FROM " + TABLE_IP_CODELIST);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_IP_CODES);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_MCC_CODE);

        db.execSQL("DELETE FROM " + TABLE_DEF_CODE_LIST);
        db.execSQL("DELETE FROM " + TABLE_OFFLINE_DEF_RET_SEND);

        db.execSQL("DELETE FROM " + TABLE_DEF_RETURN_RECEIVE_CODES);
        db.execSQL("DELETE FROM " + TABLE_SALE_RETURN_RECEIVE_CODES);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_UNPACK_CODE);
    }


    public void deleteAllScannedTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST);

        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST);
        db.execSQL("DELETE FROM " + TABLE_ITEMCODE);
    }


    public void deleteScannedPackagingData(String flag) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_QRLIST + " WHERE " + SCANNED_PRODUCT_QR_CODE_FLAG + " = " + flag + "");
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_BLADELIST + " WHERE " + SCANNED_PRODUCT_BLADE_CODE_FLAG + " = " + flag + "");
        db.execSQL("DELETE FROM " + TABLE_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + SCANNED_PRODUCT_MASTERQR_CODE_FLAG + " = " + flag + "");

        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_QR_CODE_FLAG + " = " + flag + "");
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST + " WHERE " + TEMP_SCANNED_PRODUCT_BLADE_CODE_FLAG + " = " + flag + "");
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST + " WHERE " + TEMP_SCANNED_PRODUCT_MASTERQR_CODE_FLAG + " = " + flag + "");
        db.execSQL("DELETE FROM " + TABLE_ITEMCODE + " WHERE " + ITEMCODE_FLAG + " = " + flag + "");

    }


    public void deleteAllTempScannedTableData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_QRLIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_BLADELIST);
        db.execSQL("DELETE FROM " + TABLE_TEMP_SCANNED_PRODUCT_MASTERQRLIST);
    }

    public void deleteAllScannedMasterCartonCode(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (tag.equals("all")) {
            db.execSQL("DELETE FROM " + TABLE_SCANNED_MASTER_CARTON_BARCODE);
        } else {
            String whereClause = SYSTEM_SCANNED_MASTER_CARTON_LIST_FLAG + "=?";
            String whereArgs[] = {"0"};
            db.delete(TABLE_SCANNED_MASTER_CARTON_BARCODE, whereClause, whereArgs);
        }
    }

    public void deleteAllReceiveShipmentCode() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME_RECEIVENEWSHIPMENT);
        db.execSQL("DELETE FROM " + TABLE_RECEIVE_SHIPMENT_INVOICE_CODE_LIST);
    }

    public void deleteAllLogs() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME_LOGS);
    }

    public void deleteApackingratio() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PACKING_RATIO + " WHERE " + PACKING_RATIO_FLAG + " = '" + 0 + "' ");
    }

    public void deletepackingratio() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PACKING_RATIO);
    }


    public void deleteScannedMCCCode(String barcode) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SCANNED_MCC_CODE + " WHERE " + SCANNED_MCC_CODELIST + " = '" + barcode + "' ");
    }

    public void deleteScannedMCCCodeDissmiss() {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SCANNED_MCC_CODE + " WHERE " + SCANNED_MCC_OFFLINE_FLAG + " = '" + 0 + "' ");
    }

    // Delete Scanned Breakage Code List-Breakage Scan
    public void deleteScannedBreakageCode(String barcode) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SCANNED_BREAKAGE_CODE + " WHERE " + SCANNED_BREAKAGE_CODELIST + " = '" + barcode + "' ");
    }

    public void deleteScannedBreakageCode() {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SCANNED_BREAKAGE_CODE);
    }


    // Fetch Scanned Breakage Code List-Breakage Scan
    public ArrayList<String> getScannedBreakageQRList() {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_BREAKAGE_CODE, null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SCANNED_BREAKAGE_CODELIST)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    // Fetch Scanned Breakage Code List boolean-Breakage Scan
    public boolean getScannedBreakageQRList(String master_code) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SCANNED_BREAKAGE_CODE + " WHERE " + SCANNED_BREAKAGE_CODELIST + " = '" + master_code + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }


    // Store Scanned Master carton Code List-New Shipment
    public long insertScannedBreakageCode(String master_code, String flag) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SCANNED_BREAKAGE_CODELIST, master_code);
        objContentValues.put(SCANNED_BREAKAGE_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SCANNED_BREAKAGE_CODE, null, objContentValues);
        return result;
    }


    //Fetch scanned new shipment data-Packed Carton
    public ArrayList<NewShipmentDetailModel> getListScannedShipmentForBreakageScan() {
        ArrayList<NewShipmentDetailModel> arrLogDetailModels = new ArrayList<>();
        ArrayList<String> stringArrayList = getScannedBreakageQRList();

        for (int i = 0; i < stringArrayList.size(); i++) {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor objCursor = db.rawQuery("SELECT " + SHIPMENT_ID + "," + SHIPMENT_UPLOAD_TO_SERVER + "," + SHIPMENT_CODE + "," + SHIPMENT_PRODUCTNAME + ",SUM(" + SHIPMENT_PACKEDRATIO + ") as " + SHIPMENT_PACKEDRATIO + " FROM " + TABLE_NAME_SHIPMENT + " WHERE " + SHIPMENT_UPLOAD_TO_SERVER + " = 0 " + " AND " + SHIPMENT_CODE + " = " + stringArrayList.get(i) + " GROUP BY " + SHIPMENT_PRODUCTNAME, null);

            if (objCursor.getCount() > 0) {
                objCursor.moveToFirst();
                do {
                    arrLogDetailModels.add(new NewShipmentDetailModel(
                            objCursor.getString(objCursor.getColumnIndex(SHIPMENT_ID)),
                            objCursor.getString(objCursor.getColumnIndex(SHIPMENT_PACKEDRATIO)),
                            objCursor.getString(objCursor.getColumnIndex(SHIPMENT_CODE)),
                            objCursor.getString(objCursor.getColumnIndex(SHIPMENT_PRODUCTNAME)),
                            objCursor.getString(objCursor.getColumnIndex(SHIPMENT_UPLOAD_TO_SERVER)),
                            ""
                    ));
                } while (objCursor.moveToNext());
            }
        }


        return arrLogDetailModels;
    }


    public void updateShipmentDataOfflineForBreakageScan() {
        Log.d(TAG, "updateShipmentData: ");
        ArrayList<String> stringArrayList = getScannedBreakageQRList();

        for (int i = 0; i < stringArrayList.size(); i++) {
            ContentValues objContentValues = new ContentValues();
            objContentValues.put(SHIPMENT_UPLOAD_TO_SERVER, "1");
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = SHIPMENT_CODE + "=?";
            String whereArgs[] = {stringArrayList.get(i)};
            db.update(TABLE_NAME_SHIPMENT, objContentValues, whereClause, whereArgs);
        }

    }

    //Defective return receive start
    public long insertDefRetList(String code, String model) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_RRC_CODE, code);
        objContentValues.put(DEF_RRC_MODEL, model);
        objContentValues.put(DEF_RRC_PKG_RATIO, "1");
        objContentValues.put(DEF_RRC_FLAG, "0");
        objContentValues.put(DEF_RRC_OFFLINE_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_DEF_RETURN_RECEIVE_CODES, null, objContentValues);
        return result;
    }

    public boolean getScannedDefRetRecCodeListBoolean(String code, String flag) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_RETURN_RECEIVE_CODES + " WHERE " + DEF_RRC_CODE + " = '" + code + "' AND " + DEF_RRC_FLAG + " = '" + flag + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }

    // Fetch Scanned Defective Return Receive Code List
    public ArrayList<String> getScannedDefRetRecCodeList(String offline_flag, String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_RETURN_RECEIVE_CODES + " WHERE " + DEF_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  OR " + DEF_RRC_FLAG + " = '" + flag + "' ", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<String> getScannedDefRetRecCodeList1(String offline_flag, String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_DEF_RETURN_RECEIVE_CODES + " WHERE " + DEF_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  AND " + DEF_RRC_FLAG + " = '" + flag + "' ", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<NewShipmentDetailModel> getScannedDefRecCodeList(String flag, String offline_flag) {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + DEF_RRC_OFFLINE_FLAG + "," + DEF_RRC_ID + "," + DEF_RRC_CODE + "," + DEF_RRC_MODEL + ",SUM(" + DEF_RRC_PKG_RATIO + ") as " + DEF_RRC_PKG_RATIO + " FROM " + TABLE_DEF_RETURN_RECEIVE_CODES + " WHERE " + DEF_RRC_OFFLINE_FLAG + " = '" + offline_flag + "' AND " + DEF_RRC_FLAG + " = '" + flag + "' GROUP BY " + DEF_RRC_MODEL, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_MODEL)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.DEF_RRC_OFFLINE_FLAG)), 0));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    public void deleteScannedDefRetRecCodeList(String flag, String offline_flag) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_DEF_RETURN_RECEIVE_CODES + " WHERE " + DEF_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  AND " + DEF_RRC_FLAG + " = '" + flag + "' ");
    }

    public void deleteScannedDefRetRecCodeList() {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_DEF_RETURN_RECEIVE_CODES);
    }

    public void updateScannedDefRetRecCodeList2(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_RRC_FLAG, "0");
        objContentValues.put(DEF_RRC_OFFLINE_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_RRC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_DEF_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public void updateScannedDefRetRecCodeList(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_RRC_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_RRC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_DEF_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public void updateScannedDefRetRecCodeList1(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_RRC_OFFLINE_FLAG, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_RRC_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_DEF_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public long updateScannedDefRetRecCode(String flag, String code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(DEF_RRC_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = DEF_RRC_CODE + "=?";
        String whereArgs[] = {code};
        result = db.update(TABLE_DEF_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
        return result;
    }
    //Defective return receive end

    //Sale return receive start
    public long insertSaleRetList(String code, String model, String pkg_ratio) {
        long result = 0;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SALE_RRC_CODE, code);
        objContentValues.put(SALE_RRC_MODEL, model);
        objContentValues.put(SALE_RRC_PKG_RATIO, pkg_ratio);
        objContentValues.put(SALE_RRC_FLAG, "0");
        objContentValues.put(SALE_RRC_OFFLINE_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.insert(TABLE_SALE_RETURN_RECEIVE_CODES, null, objContentValues);
        return result;
    }

    public boolean getScannedSaleRetRecCodeListBoolean(String code, String flag) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SALE_RETURN_RECEIVE_CODES + " WHERE " + SALE_RRC_CODE + " = '" + code + "' AND " + SALE_RRC_FLAG + " = '" + flag + "'", null);
        boolean isScan;
        if (objCursor != null && objCursor.getCount() > 0) {
            // has results
            isScan = true;
        } else {
            // has NO results
            isScan = false;
        }
        return isScan;
    }

    public ArrayList<String> getScannedSaleRetRecCodeList(String offline_flag, String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SALE_RETURN_RECEIVE_CODES + " WHERE " + SALE_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  OR " + SALE_RRC_FLAG + " = '" + flag + "' ", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<String> getScannedSaleRetRecCodeList1(String offline_flag, String flag) {
        ArrayList<String> arrLogDetailModels = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT * FROM " + TABLE_SALE_RETURN_RECEIVE_CODES + " WHERE " + SALE_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  AND " + SALE_RRC_FLAG + " = '" + flag + "' ", null);
        if (objCursor.getCount() > 0) {
            objCursor.moveToFirst();
            do {
                arrLogDetailModels.add(objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_CODE)));
            } while (objCursor.moveToNext());
        }
        objCursor.close();
        return arrLogDetailModels;
    }

    public ArrayList<NewShipmentDetailModel> getScannedSaleRecCodeList(String flag, String offline_flag) {
        ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor objCursor = db.rawQuery("SELECT " + SALE_RRC_OFFLINE_FLAG + "," + SALE_RRC_ID + "," + SALE_RRC_CODE + "," + SALE_RRC_MODEL + ",SUM(" + SALE_RRC_PKG_RATIO + ") as " + SALE_RRC_PKG_RATIO + " FROM " + TABLE_SALE_RETURN_RECEIVE_CODES + " WHERE " + SALE_RRC_OFFLINE_FLAG + " = '" + offline_flag + "' AND " + SALE_RRC_FLAG + " = '" + flag + "' GROUP BY " + SALE_RRC_MODEL, null);
        objCursor.moveToFirst();
        if (objCursor.getCount() > 0) {
            do {
                newShipmentDetailModelArrayList.add(new NewShipmentDetailModel(objCursor.getString(
                        objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_ID)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_PKG_RATIO)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_CODE)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_MODEL)),
                        objCursor.getString(objCursor.getColumnIndex(DatabaseHelper.SALE_RRC_OFFLINE_FLAG)), 0));
            } while (objCursor.moveToNext());
        }
        return newShipmentDetailModelArrayList;
    }

    public void deleteScannedSaleRetRecCodeList(String flag, String offline_flag) {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SALE_RETURN_RECEIVE_CODES + " WHERE " + SALE_RRC_OFFLINE_FLAG + " = '" + offline_flag + "'  AND " + SALE_RRC_FLAG + " = '" + flag + "' ");
    }

    public void deleteScannedSaleRetRecCodeList() {
        SQLiteDatabase sqldb = getWritableDatabase();
        sqldb.execSQL("DELETE FROM " + TABLE_SALE_RETURN_RECEIVE_CODES);
    }

    public void updateScannedSaleRetRecCodeList2(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SALE_RRC_FLAG, "0");
        objContentValues.put(SALE_RRC_OFFLINE_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SALE_RRC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_SALE_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public void updateScannedSaleRetRecCodeList(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SALE_RRC_FLAG, "0");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SALE_RRC_OFFLINE_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_SALE_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public void updateScannedSaleRetRecCodeList1(String offline_flag) {
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SALE_RRC_OFFLINE_FLAG, "1");
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SALE_RRC_FLAG + "=?";
        String whereArgs[] = {offline_flag};
        db.update(TABLE_SALE_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
    }

    public long updateScannedSaleRetRecCode(String flag, String code) {
        long result;
        ContentValues objContentValues = new ContentValues();
        objContentValues.put(SALE_RRC_FLAG, flag);
        SQLiteDatabase db = this.getWritableDatabase();
        String whereClause = SALE_RRC_CODE + "=?";
        String whereArgs[] = {code};
        result = db.update(TABLE_SALE_RETURN_RECEIVE_CODES, objContentValues, whereClause, whereArgs);
        return result;
    }
    //Sale return receive end

}