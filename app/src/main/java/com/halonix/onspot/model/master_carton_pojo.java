package com.halonix.onspot.model;

public class master_carton_pojo {
    String p_name;
    String code;
    String packing_ratio;

    public master_carton_pojo(String p_name, String code, String packing_ratio) {
        this.p_name = p_name;
        this.code = code;
        this.packing_ratio = packing_ratio;
    }

    public String getP_name() {
        return p_name;
    }

    public String getCode() {
        return code;
    }

    public String getPacking_ratio() {
        return packing_ratio;
    }
}
