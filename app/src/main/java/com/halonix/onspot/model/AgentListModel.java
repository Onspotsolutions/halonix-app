package com.halonix.onspot.model;

public class AgentListModel {


    String id, name, code, email, address, mobile, cp_person, type, location;

    public AgentListModel(String id, String name, String email, String mobile, String cp_person, String location, String type) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.cp_person = cp_person;
        this.location = location;
        this.type = type;
    }


    public AgentListModel(String id, String name, String code, String email, String address, String mobile, String cp_person, String type, String location) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.email = email;
        this.address = address;
        this.mobile = mobile;
        this.cp_person = cp_person;
        this.type = type;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCp_person() {
        return cp_person;
    }

    public void setCp_person(String cp_person) {
        this.cp_person = cp_person;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
