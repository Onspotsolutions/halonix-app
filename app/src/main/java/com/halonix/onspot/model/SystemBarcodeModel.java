package com.halonix.onspot.model;

import org.json.JSONArray;

import java.io.Serializable;


public class SystemBarcodeModel implements Serializable {

    private JSONArray jsonArray;

    public SystemBarcodeModel(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }
}
