package com.halonix.onspot.model;


public class NewShipment {

    private int NewShipmentId;
    private String Auth_token = "";
    private String quantity = "";
    private String Agency_id = "";
    private String Invoice_number = "";
    private String Invoice_date = "";
    private String Po_number = "";
    private String Po_date = "";
    private String MasterCartonCode;
    private String shipment_type = "";
    private String shipment_upload_to_server;


    private String sender_id;
    private String rin;
    private String rid;
    private String reason;
    private String flag;


    public NewShipment(String sender_id, String rin, String rid, String reason, String flag) {
        this.sender_id = sender_id;
        this.rin = rin;
        this.rid = rid;
        this.reason = reason;
        this.flag = flag;
    }

    public String getSender_id() {
        return sender_id;
    }

    public String getRin() {
        return rin;
    }

    public String getRid() {
        return rid;
    }

    public String getReason() {
        return reason;
    }

    public String getFlag() {
        return flag;
    }

    public NewShipment(int newShipmentId, String auth_token, String quantity, String agency_id, String invoice_number, String invoice_date, String po_number, String po_date, String masterCartonCode, String shipment_type, String flag) {
        NewShipmentId = newShipmentId;
        Auth_token = auth_token;
        this.quantity = quantity;
        Agency_id = agency_id;
        Invoice_number = invoice_number;
        Invoice_date = invoice_date;
        Po_number = po_number;
        Po_date = po_date;
        MasterCartonCode = masterCartonCode;
        this.shipment_type = shipment_type;
        shipment_upload_to_server = flag;
    }

    public int getNewShipmentId() {
        return NewShipmentId;
    }

    public void setNewShipmentId(int newShipmentId) {
        NewShipmentId = newShipmentId;
    }

    public String getAuth_token() {
        return Auth_token;
    }

    public void setAuth_token(String auth_token) {
        Auth_token = auth_token;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAgency_id() {
        return Agency_id;
    }

    public void setAgency_id(String agency_id) {
        Agency_id = agency_id;
    }

    public String getInvoice_number() {
        return Invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        Invoice_number = invoice_number;
    }

    public String getInvoice_date() {
        return Invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        Invoice_date = invoice_date;
    }

    public String getPo_number() {
        return Po_number;
    }

    public void setPo_number(String po_number) {
        Po_number = po_number;
    }

    public String getPo_date() {
        return Po_date;
    }

    public void setPo_date(String po_date) {
        Po_date = po_date;
    }

    public String getMasterCartonCode() {
        return MasterCartonCode;
    }


    public String getShipment_type() {
        return shipment_type;
    }


    public String getShipment_upload_to_server() {
        return shipment_upload_to_server;
    }


}
