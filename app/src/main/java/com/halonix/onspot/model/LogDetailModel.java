package com.halonix.onspot.model;

import java.io.Serializable;


public class LogDetailModel implements Serializable {

    private String NumberOfCartons;
    private String LogCompleted;
    private String LogUploadToServerStatus;

    public LogDetailModel(String numberOfCartons,String logCompleted, String logUploadToServerStatus) {
        NumberOfCartons = numberOfCartons;
        LogCompleted = logCompleted;
        LogUploadToServerStatus = logUploadToServerStatus;
    }

    public String getNumberOfCartons() {
        return NumberOfCartons;
    }

    public void setNumberOfCartons(String numberOfCartons) {
        NumberOfCartons = numberOfCartons;
    }

    public String getLogCompleted() {
        return LogCompleted;
    }

    public void setLogCompleted(String logCompleted) {
        LogCompleted = logCompleted;
    }

    public String getLogUploadToServerStatus() {
        return LogUploadToServerStatus;
    }

    public void setLogUploadToServerStatus(String logUploadToServerStatus) {
        LogUploadToServerStatus = logUploadToServerStatus;
    }
}
