package com.halonix.onspot.model;

public class PendingShipmentListModel {

    String quantity;
    String Invoice_number;
    String Invoice_date;
    String pending;
    String shipment_id;

    public PendingShipmentListModel(String quantity, String invoice_number, String invoice_date, String pending, String shipment_id) {
        this.quantity = quantity;
        Invoice_number = invoice_number;
        Invoice_date = invoice_date;
        this.pending = pending;
        this.shipment_id = shipment_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getInvoice_number() {
        return Invoice_number;
    }

    public String getInvoice_date() {
        return Invoice_date;
    }

    public String getPending() {
        return pending;
    }

    public String getShipment_id() {
        return shipment_id;
    }
}
