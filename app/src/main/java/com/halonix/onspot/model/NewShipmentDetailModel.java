package com.halonix.onspot.model;

import java.io.Serializable;


public class NewShipmentDetailModel implements Serializable {

    private String NewShipmentID;
    private String PackedRatio;
    private String ShipmentCode;
    private String ProductName;
    private String Sweep;
    private String Color;
    private String UploadToServer;
    private String Qty;
    private int sender_id;


    public NewShipmentDetailModel(String newShipmentID, String packedRatio, String shipmentCode,
                                  String productName, String uploadToServer,int Sender_Id) {
        NewShipmentID = newShipmentID;
        PackedRatio = packedRatio;
        ShipmentCode = shipmentCode;
        ProductName = productName;
        UploadToServer = uploadToServer;
        sender_id = Sender_Id;
    }

    public NewShipmentDetailModel(String newShipmentID, String packedRatio, String shipmentCode, String productName, String sweep, String color, String uploadToServer) {
        NewShipmentID = newShipmentID;
        PackedRatio = packedRatio;
        ShipmentCode = shipmentCode;
        ProductName = productName;
        Sweep = sweep;
        Color = color;
        UploadToServer = uploadToServer;
    }

    public NewShipmentDetailModel(String shipmentCode, String productName, String sweep, String color,String qty, String uploadToServer) {
        ShipmentCode = shipmentCode;
        ProductName = productName;
        Sweep = sweep;
        Color = color;
        Qty = qty;
        UploadToServer = uploadToServer;
    }

    public String getSweep() {
        return Sweep;
    }

    public void setSweep(String sweep) {
        Sweep = sweep;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getNewShipmentID() {
        return NewShipmentID;
    }

    public void setNewShipmentID(String newShipmentID) {
        NewShipmentID = newShipmentID;
    }

    public String getPackedRatio() {
        return PackedRatio;
    }

    public void setPackedRatio(String packedRatio) {
        PackedRatio = packedRatio;
    }

    public String getShipmentCode() {
        return ShipmentCode;
    }

    public void setShipmentCode(String shipmentCode) {
        ShipmentCode = shipmentCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getUploadToServer() {
        return UploadToServer;
    }

    public void setUploadToServer(String uploadToServer) {
        UploadToServer = uploadToServer;
    }

    public String getQty() {
        return Qty;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }
}
