package com.halonix.onspot.retrofit;


import com.halonix.onspot.contributors.AgencyResponse;
import com.halonix.onspot.contributors.CodeDetails_respo;
import com.halonix.onspot.contributors.PendingShipments_respo;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.SenderDetails_respo;
import com.halonix.onspot.contributors.bladecode_list_respo;
import com.halonix.onspot.contributors.code_list_respo;
import com.halonix.onspot.contributors.codelist_respo;
import com.halonix.onspot.contributors.def_return_code_respo;
import com.halonix.onspot.contributors.force_in_codelist_respo;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.contributors.otp_verify;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_history_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.contributors.repacking_respo;
import com.halonix.onspot.contributors.sale_history_respo;
import com.halonix.onspot.contributors.shipment_history_respo;
import com.halonix.onspot.contributors.shortfall_Respo;
import com.halonix.onspot.contributors.signin;
import com.halonix.onspot.contributors.unpacked_batch_respo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {


    //-------------------------------Sign In--------------------------------------//
    @FormUrlEncoded
    @POST("/api/app_v1/sign_in")
    Call<signin> getSignin(
            @Field("mobile_number") String mobile
    );

    @FormUrlEncoded
    @POST("/api/app_v1/verify_otp")
    Call<otp_verify> getOTPverify(
            @Field("otp_code") String otp_code,
            @Field("user_id") String user_id
    );

    //-------------------------------Send New Shipment--------------------------------------//
    @FormUrlEncoded
    @POST("/api/v1/packed_carton")
    Call<packed_carton_respo> getPackedCarton(
            @Field("auth_token") String auth_token
    );

    @FormUrlEncoded
    @POST("/api/shipment/agencies_list")
    Call<AgencyResponse> getAgencyList(
            @Field("auth_token") String auth_token,
            @Field("shipment_type") String shipment_type
    );


    @FormUrlEncoded
    @POST("/api/v1/invoice_codes")
    Call<invoice_code_respo> getInvoiceCodes(
            @Field("auth_token") String auth_token
    );

    @FormUrlEncoded
    @POST("/api/shipment/agency_dispatch_mapping")
    Call<master_mapping_respo> agency_dispatch_mapping(
            @Field("auth_token") String auth_token,
            @Field("list") String list
    );

    //-------------------------------Vendor Packaging--------------------------------------//
    @FormUrlEncoded
    @POST("/api/v1/unpacked_batches")
    Call<unpacked_batch_respo> getUnpackedBatches(
            @Field("auth_token") String auth_token
    );


    @FormUrlEncoded
    @POST("/api/v1/code_list")
    Call<code_list_respo> getcode_list(
            @Field("auth_token") String auth_token,
            @Field("item_code") String item_code
    );


    @FormUrlEncoded
    @POST("/api/v1/blade_code_list")
    Call<bladecode_list_respo> getbladecode_list(
            @Field("auth_token") String auth_token,
            @Field("item_code") String item_code
    );

    //-------------------------------Shipment History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/shipment_history")
    Call<shipment_history_respo> getShipmentHistory(
            @Field("auth_token") String auth_token,
            @Field("completed") boolean completed
    );

    //-------------------------------Vendor Master Carton Mapping----------------------------//
    @FormUrlEncoded
    @POST("/api/v1/master_mapping")
    Call<master_mapping_respo> master_mapping(
            @Field("auth_token") String auth_token,
            @Field("Mapping") String Mapping
    );


    //-------------------------------History with Date--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/history_with_date")
    Call<master_mapping_respo> getHistoryWithDate(
            @Field("auth_token") String auth_token,
            @Field("type") String type,
            @Field("s_date") String s_date,
            @Field("e_date") String e_date
    );


    //-------------------------------Receive New Shipmensplasht Master Carton Codes--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/get_list_receive_shipment")
    Call<get_receive_shipment_respo> getReceiveNewShipmentCodes(
            @Field("auth_token") String auth_token
    );

    //-------------------------------Receive New Shipment by Master Carton--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/received_shipments")
    Call<received_shipment_master_carton_respo> getReceiveNewShipment(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list,
            @Field("receive_by") String receive_by,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("accuracy") String accuracy
    );


    //-------------------------------Receive New Shipment by Individual products defective--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/received_shipments")
    Call<received_shipment_master_carton_respo> getReceiveNewShipmentDefective(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list,
            @Field("receive_by") String receive_by,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("accuracy") String accuracy,
            @Field("defectiv") String defectiv
    );

    //-------------------------------Received History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/received_history")
    Call<received_history_respo> getReceivehistory(
            @Field("auth_token") String auth_token,
            @Field("completed") boolean completed
    );


    //-------------------------------Received History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/received_history")
    Call<received_history_respo> getShortfallHistory(
            @Field("auth_token") String auth_token,
            @Field("completed") String shortfall
    );

    //-------------------------------Shortfall Request--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/mark_shortfall")
    Call<shortfall_Respo> shortfall_request(
            @Field("shipment_id") String shipment_id,
            @Field("reason") String reason,
            @Field("auth_token") String auth_token
    );


    //-------------------------------Receive Shipment By Invoice--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/get_shipment_details")
    Call<ReceiveShipmentInvoice_respo> getReceiveShipmentByInvoice(
            @Field("auth_token") String auth_token,
            @Field("invoice_code") String invoice_code
    );

    //-------------------------------Receive Shipment By Individual Products List--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/get_pending_shipments")
    Call<PendingShipments_respo> getPendingShipmentList(
            @Field("auth_token") String auth_token
    );

    //-------------------------------Receive Shipment By Individual Products List Codes--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/get_pending_shipment_codes")
    Call<codelist_respo> getPendingShipmentListCodes(
            @Field("auth_token") String auth_token,
            @Field("shipment_id") String shipment_id
    );

    //-------------------------------Receive Defective Shipment Codes--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/receive_defective_codes")
    Call<ReceiveDefectiveShipment_respo> getDefectiveShipment(
            @Field("auth_token") String auth_token
    );


    //-------------------------------Receive Defective Shipment--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/receive_shipment")
    Call<ReceiveDefectiveShipment_respo> receiveDefectiveShipment(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list
    );

    //-------------------------------Defective Receive History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/defective_receive_history")
    Call<ReceiveDefectiveShipment_respo> DefectiveReceiveHistory(
            @Field("auth_token") String auth_token,
            @Field("completed") boolean completed
    );


    //-------------------------------Sale Return Codes--------------------------------------//
    @FormUrlEncoded
    @POST("/api/sale_return/receive_sale_return_codes")
    Call<ReceiveDefectiveShipment_respo> getSaleReturnCodes(
            @Field("auth_token") String auth_token
    );

    //-------------------------------Receive Sale Return--------------------------------------//
    @FormUrlEncoded
    @POST("/api/sale_return/receive_shipment")
    Call<ReceiveDefectiveShipment_respo> receiveSaleReturn(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list
    );

    //-------------------------------Sale Return History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/sale_return/sale_return_shipment_history")
    Call<ReceiveDefectiveShipment_respo> SaleReturnHistory(
            @Field("auth_token") String auth_token,
            @Field("completed") boolean completed
    );


    //-------------------------------code_details-retailer sale--------------------------------------//
    @FormUrlEncoded
    @POST("/api/v1/code_details")
    Call<CodeDetails_respo> code_details(
            @Field("auth_token") String auth_token,
            @Field("code") String code
    );

    //-------------------------------Activate Warranty--------------------------------------//
    @FormUrlEncoded
    @POST("/api/app_v1/start_warranty")
    Call<ReceiveDefectiveShipment_respo> activateWarranty(
            @Field("auth_token") String auth_token,
            @Field("qr_code") String qr_code
    );

    //-------------------------------Activate Warranty With Bill--------------------------------------//
    @FormUrlEncoded
    @POST("/api/app_v1/start_warranty")
    Call<ReceiveDefectiveShipment_respo> activateWarrantyWithBill(
            @Field("auth_token") String auth_token,
            @Field("qr_code") String qr_code,
            @Field("sale_date") String sale_date,
            @Field("bill_no") String bill_no
    );


    //-------------------------------Replacement-Start Warranty--------------------------------------//
    @FormUrlEncoded
    @POST("/api/replacement/warranty_replacement")
    Call<ReceiveDefectiveShipment_respo> replacement_start_warranty(
            @Field("auth_token") String auth_token,
            @Field("replacements") String replacements
    );

    //-------------------------------Sale History--------------------------------------//
    @FormUrlEncoded
    @POST("/api/app_v1/sale_history")
    Call<sale_history_respo> sale_history(
            @Field("auth_token") String auth_token,
            @Field("s_date") String s_date,
            @Field("e_date") String e_date
    );


    //-------------------------------Breakage Scan--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/mark_defected")
    Call<ReceiveDefectiveShipment_respo> breakageScan(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list
    );


    //-------------------------------Force IN Code Details--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/check_code")
    Call<force_in_codelist_respo> force_in_check_code(
            @Field("auth_token") String auth_token,
            @Field("code") String code
    );


    //-------------------------------Force IN Code Details--------------------------------------//
    @FormUrlEncoded
    @POST("/api/shipment/force_in")
    Call<ReceiveDefectiveShipment_respo> force_in_receive(
            @Field("auth_token") String auth_token,
            @Field("code_list") String code_list,
            @Field("comment") String comment,
            @Field("defectiv") String defectiv
    );

    //-------------------------------Sender Details--------------------------------------//
    @FormUrlEncoded
    @POST("/api/sale_return/get_sender_details")
    Call<SenderDetails_respo> sender_details(
            @Field("auth_token") String auth_token,
            @Field("sender_id") String sender_id
    );

    //-------------------------------Sale return send shipment--------------------------------------//
    @FormUrlEncoded
    @POST("/api/sale_return/agency_dispatch")
    Call<SenderDetails_respo> sale_return_send(
            @Field("auth_token") String auth_token,
            @Field("rv_no") String rv_no,
            @Field("reason") String reason,
            @Field("code_list") String code_list,
            @Field("agency_id") int agency_id
    );

    //-------------------------------Branch Unpack--------------------------------------//
    @FormUrlEncoded
    @POST("/api/repackaging/unpacked_carton")
    Call<SenderDetails_respo> unpack(
            @Field("auth_token") String auth_token,
            @Field("master_carton_codes") String master_carton_codes
    );

    //-------------------------------Branch Repacking Code List----------------------------//
    @FormUrlEncoded
    @POST("/api/repackaging/code_for_repackaging")
    Call<repacking_respo> repacking_code_list(
            @Field("auth_token") String auth_token
    );

    //-------------------------------Branch Repacking----------------------------//
    @FormUrlEncoded
    @POST("/api/repackaging/repack_carton")
    Call<master_mapping_respo> repacking(
            @Field("auth_token") String auth_token,
            @Field("list") String list
    );

    //-------------------------------Breakage Scan--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/mark_defected")
    Call<SenderDetails_respo> breakage_scan_send(
            @Field("auth_token") String auth_token,
            @Field("reason") String reason,
            @Field("code_list") String code_list
    );

    //-------------------------------Defective return send codelist--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/defective_code_list")
    Call<def_return_code_respo> defective_code_list(
            @Field("auth_token") String auth_token
    );


    //-------------------------------Defective return send--------------------------------------//
    @FormUrlEncoded
    @POST("/api/defective_shipments/agency_dispatch")
    Call<SenderDetails_respo> defective_return_send(
            @Field("auth_token") String auth_token,
            @Field("reason") String reason,
            @Field("code_list") String code_list,
            @Field("agency_id") int agency_id,
            @Field("return_invoice_no") String return_invoice_no,
            @Field("return_invoice_date") String return_invoice_date
    );


}
