package com.halonix.onspot.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    ApiInterface service;

    private static Retrofit retrofit = null;
    private static String BASE_URL = "https://myhalonix.com/";

    public RetrofitClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        service = retrofit.create(ApiInterface.class);

    }

    public ApiInterface getService() {
        return service;
    }

}
