package com.halonix.onspot.view.Webview_page;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AlertDialog;

import com.halonix.onspot.R;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import static com.halonix.onspot.utils.Utils.dismissprogressdialog;
import static com.halonix.onspot.utils.Utils.showprogressdialog;


public class CommonWebFragment extends BaseFragment {

    Context cntx;
    View view;
    private String page_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_common_web, container, false);
        cntx = getActivity();

        Bundle bundle = getArguments();
        if (bundle != null) {
            page_name = bundle.getString("page_name");
            String url = bundle.getString("url");

        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).getSupportActionBar().setTitle(page_name);

        WebView objWebView = view.findViewById(R.id.webView_content);
        objWebView.getSettings().setJavaScriptEnabled(true);
        objWebView.getSettings().setBuiltInZoomControls(true);
        showprogressdialog(getActivity(), "Loading page,Please wait...");
        objWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        final AlertDialog alertDialog = new AlertDialog.Builder(cntx).create();

        objWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                dismissprogressdialog();
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                dismissprogressdialog();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                alertDialog.setTitle("Error");
//                alertDialog.setMessage(description);
//                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (dialogInterface, i) -> {
//                    return;
//                });
//                alertDialog.show();
            }
        });

        objWebView.loadUrl("https://halonix.onspotsolutions.com/pages/au_en");
    }


}
