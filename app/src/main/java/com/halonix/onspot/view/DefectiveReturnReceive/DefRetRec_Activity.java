package com.halonix.onspot.view.DefectiveReturnReceive;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.ReceiveDefectiveShipmentList_respo;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefRetRec_Activity extends AppCompatActivity implements View.OnClickListener {


    private Activity activity = null;

    @BindView(R.id.scanShipmentList)
    RecyclerView scanShipmentList;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    @BindView(R.id.ly_scan_system)
    LinearLayout ly_scan_system;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.button_done)
    AppCompatButton button_done;

    private DatabaseHelper objDatabaseHelper = null;
    ArrayList<NewShipmentDetailModel> newShipmentScannedArrayList = new ArrayList<>();
    BatchListAdapter scanShipmentCodeAdapter;
    private long mLastClickTime = 0;
    RetrofitClient retrofitClient;
    ConnectionDetectorActivity cd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_new_shipment);
        ButterKnife.bind(this);
        activity = DefRetRec_Activity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);

        retrofitClient = new RetrofitClient();
        cd = new ConnectionDetectorActivity(DefRetRec_Activity.this);

        scanShipmentList.setHasFixedSize(true);
        scanShipmentList.setItemAnimator(new DefaultItemAnimator());
        scanShipmentList.setLayoutManager(new LinearLayoutManager(DefRetRec_Activity.this));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });
        toolbar.setTitle("Defective Return Receive");

        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_addmore).setOnClickListener(this);
        findViewById(R.id.button_done).setOnClickListener(this);
        button_done.setText("Receive");
        findViewById(R.id.button_discard_all).setOnClickListener(this);
    }


    private void remove_scan() {
        Intent intent = new Intent(DefRetRec_Activity.this, DefRetRec_ScannerActivity.class);
        intent.putExtra("token", "remove");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    private void start_scanning() {
        Intent intent = new Intent(DefRetRec_Activity.this, DefRetRec_ScannerActivity.class);
        intent.putExtra("token", "add");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }


    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
            case R.id.button_addmore:
                start_scanning();
                break;
            case R.id.button_done:
                if (cd.isConnectingToInternet()) {
                    if (newShipmentScannedArrayList.size() > 0) {
                        receive_codes();
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }
                } else {
                    objDatabaseHelper.updateScannedDefRetRecCodeList1("1");
                    Utils.showToast(DefRetRec_Activity.this, "Defective return receive shipment stored offline.");
                    Intent intent = new Intent(DefRetRec_Activity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    DefRetRec_Activity.this.finish();
                    overridePendingTransition(0, 0);
                }
                break;
            case R.id.button_discard_all:
                remove_scan();
                break;
        }
    }


    private void receive_codes() {

        Utils.showprogressdialog(DefRetRec_Activity.this, "Receiving...please wait");
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedDefRetRecCodeList("1", "1");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Utils.dismissprogressdialog();
        Log.d(Constants.TAG, "receive_codes Def: " + jsonArray.toString());

        retrofitClient.getService().receiveDefectiveShipment(objDatabaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteScannedDefRetRecCodeList();
                    getDefRetReceiveCodeList(response.body().getMessage());
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(DefRetRec_Activity.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    ArrayList<ReceiveDefectiveShipmentList_respo> receiveDefectiveShipmentList_respos;

    private void getDefRetReceiveCodeList(String message) {
        receiveDefectiveShipmentList_respos = new ArrayList<>();

        retrofitClient.getService().getDefectiveShipment(objDatabaseHelper.getAuthToken()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                Utils.dismissprogressdialog();
                if (response.body().getSuccess()) {
                    receiveDefectiveShipmentList_respos = response.body().getData();

                    if (receiveDefectiveShipmentList_respos.size() > 0) {
                        for (int i = 0; i < receiveDefectiveShipmentList_respos.size(); i++) {
                            objDatabaseHelper.insertDefRetList(receiveDefectiveShipmentList_respos.get(i).getCode(),
                                    receiveDefectiveShipmentList_respos.get(i).getModel());
                        }
                    }
                }
                Intent intent = new Intent(DefRetRec_Activity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                DefRetRec_Activity.this.finish();
                overridePendingTransition(0, 0);
                Utils.showToast(DefRetRec_Activity.this, message);
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    @Override
    protected void onResume() {
        Log.d(Constants.TAG, "onResume: " + objDatabaseHelper.getScannedDefRecCodeList("1", "0").size());
        findViewById(R.id.ly_scan_system).setVisibility(objDatabaseHelper.getScannedDefRecCodeList("1", "0").size() > 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.button_start_scanning).setVisibility(objDatabaseHelper.getScannedDefRecCodeList("1", "0").size() > 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.scanShipmentList).setVisibility(objDatabaseHelper.getScannedDefRecCodeList("1", "0").size() > 0 ? View.VISIBLE : View.GONE);

        if (objDatabaseHelper.getScannedDefRecCodeList("1", "0") != null) {
            newShipmentScannedArrayList = objDatabaseHelper.getScannedDefRecCodeList("1", "0");
        }

        if (newShipmentScannedArrayList.size() > 0) {
            scanShipmentList.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
            scanShipmentList.setAdapter(scanShipmentCodeAdapter);
            ly_scan_system.setVisibility(View.VISIBLE);
            findViewById(R.id.button_start_scanning).setVisibility(View.GONE);
        } else {
            linHeader.setVisibility(View.GONE);
        }
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    if (objDatabaseHelper.getScannedDefRecCodeList("1", "0") != null) {
                        newShipmentScannedArrayList = new ArrayList<>();
                        newShipmentScannedArrayList = objDatabaseHelper.getScannedDefRecCodeList("1", "0");
                    }
                    if (newShipmentScannedArrayList.size() > 0)
                        linHeader.setVisibility(View.VISIBLE);
                    else
                        linHeader.setVisibility(View.GONE);

                    scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
                    scanShipmentList.setAdapter(scanShipmentCodeAdapter);
                    onResume();
                    break;
            }
        }
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewId;
            private TextView textViewQuantity;
            private TextView textViewName;

            public MyViewHolder(View view) {
                super(view);
                textViewId = view.findViewById(R.id.textView_no);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewName = view.findViewById(R.id.textView_name);

            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_scan_shipment, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewId.setText(String.valueOf(position + 1));

            holder.textViewName.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getPackedRatio());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onBackPressed() {
        newShipmentScannedArrayList = objDatabaseHelper.getScannedDefRecCodeList("1", "0");
        if (newShipmentScannedArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            objDatabaseHelper.updateScannedDefRetRecCodeList("0");

                            Intent intent = new Intent(DefRetRec_Activity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            DefRetRec_Activity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
