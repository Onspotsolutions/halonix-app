package com.halonix.onspot.view.Login_OTP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.otp_verify;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Utils.dismissprogressdialog;
import static com.halonix.onspot.utils.Utils.showprogressdialog;


public class OTPVerifyActivity extends FragmentActivity {

    Context cntx;
    private String userMobValue;
    private String userId;
    private DatabaseHelper databaseHelper;
    private ConnectionDetectorActivity cd;
    private String user_exist;
    private Resources resources;

    RetrofitClient retrofitClient;

    @BindView(R.id.etOTPVerifaction)
    EditText etOTPVerifaction;

    @BindView(R.id.textInputVerify)
    TextInputLayout textInputVerify;

    @BindView(R.id.textViewMobile)
    TextView textViewMobile;

    @BindView(R.id.btnOTPVerify)
    Button btnOTPVerify;
    private long mLastClickTime = 0;

    private static final int REQ_USER_CONSENT = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        ButterKnife.bind(this);
        retrofitClient = new RetrofitClient();
        databaseHelper = DatabaseHelper.getInstance(OTPVerifyActivity.this);

        cd = new ConnectionDetectorActivity(OTPVerifyActivity.this);
        cntx = this;
        resources = getResources();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getString("id");
            userMobValue = bundle.getString("userMobValue");
            user_exist = bundle.getString("user_exist");
        }
        textViewMobile.setText(userMobValue);
        SmsBroadcastReceiver.bindListener(new SmsListener() {
            @Override
            public void onSuccess(Intent intent) {

                startActivityForResult(intent, REQ_USER_CONSENT);
            }

            @Override
            public void onFailure() {

            }
        });


        etOTPVerifaction.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!validateOTP()) {
                    return;
                }
            }
        });

        btnOTPVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!validateOTP()) {
                    return;
                }

                if (cd.isConnectingToInternet()) {
                    verifyOTP();
                } else {
                    showNetworkFailDialog(resources.getString(R.string.no_internet_connection));
                }
            }
        });
    }


    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {
        String[] res = message.split(":");
        etOTPVerifaction.setText(res[1].trim());
        if (cd.isConnectingToInternet()) {
            verifyOTP();
        } else {
            showNetworkFailDialog(resources.getString(R.string.no_internet_connection));
        }
    }


    private boolean validateOTP() {
        String mobile_no = etOTPVerifaction.getText().toString().trim();

        Pattern p_phone = Pattern.compile(Constants.OTP_REGEX);
        Matcher m_phone = p_phone.matcher(mobile_no);
        if (mobile_no.isEmpty()) {
            textInputVerify.setError(getString(R.string.hint_enter_otp));
            etOTPVerifaction.requestFocus();
            return false;
        } else if (!m_phone.find()) {
            textInputVerify.setError(getString(R.string.hint_valid_otp));
            etOTPVerifaction.requestFocus();
            return false;
        } else {
            textInputVerify.setErrorEnabled(false);
        }
        return true;
    }


    @SuppressLint("InflateParams")
    private void popUpDialog(String result1, final boolean newuser) {
        // TODO Auto-generated method stub
        AlertDialog.Builder builder = new AlertDialog.Builder(OTPVerifyActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = OTPVerifyActivity.this.getLayoutInflater();

        View content = inflater.inflate(R.layout.otp_verification_dialog, null);

        TextView tv = content.findViewById(R.id.otpAlertMessage1);
        tv.setText(result1);
        ((Button) content.findViewById(R.id.btnOTPAlertOK)).setText(resources.getString(R.string.action_text_ok));
        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        content.findViewById(R.id.btnOTPAlertOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();
                        if (newuser) {
                            Bundle bundle = new Bundle();
                            bundle.putString("defaultFrag", "newUserFrag");
                            Intent i = new Intent(OTPVerifyActivity.this, MainActivity.class);
                            i.putExtras(bundle);
                            startActivity(i);
                            setResult(10);
                            finish();

                        } else {
                            Toast.makeText(cntx, "Please enter valid OTP", Toast.LENGTH_LONG).show();
                            etOTPVerifaction.setText("");
                        }
                    }
                });
    }


    //TODO Verify OTPVerifyActivity API Call here
    private void verifyOTP() {
        EditText etOTPVerification = findViewById(R.id.etOTPVerifaction);
        Constants.OTP_VER_VALUE = etOTPVerification.getText().toString();
        Constants.OTP_MOB_VALUE = userMobValue;


        if (user_exist.compareTo("true") == 0)
            Constants.OTP_USER_EXIST_VALUE = "true";
        else
            Constants.OTP_USER_EXIST_VALUE = "false";
        showprogressdialog(OTPVerifyActivity.this, "Verifying entered OTP,Please wait...");

        retrofitClient.getService().getOTPverify(Constants.OTP_VER_VALUE, userId).enqueue(new Callback<otp_verify>() {
            @Override
            public void onResponse(Call<otp_verify> call, Response<otp_verify> response) {

                if (response.body().getSuccess()) {
                    dismissprogressdialog();
//                    ArrayList<String> shipmentTypes = response.body().getShipmentTypes();
//                    Utils.addPreference(cntx, PreferenceKeys.USER_SHIPMENT_TYPE, String.valueOf(shipmentTypes));

                    Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, response.body().getName());
                    Utils.addPreference(cntx, PreferenceKeys.USER_TYPE, response.body().getUserType());
                    Utils.addPreference(cntx, PreferenceKeys.USER_SHIPMENT_TYPE, String.valueOf(response.body().getShipmentTypes()));
                    Spannable wordtoSpan = new SpannableString("Hi "
                            + response.body().getName() + " , Welcome to Halonix");
                    wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 3,
                            3 + response.body().getName().length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    popUpDialogSuccess(wordtoSpan);

                    Log.d(Constants.TAG, "onResponse userId: "+userId);
                    databaseHelper.insertRegisteredUser(
                            response.body().getEmail(),
                            response.body().getMobileNumber(),
                            response.body().getName(),
                            response.body().getAuthToken(),userId);

                } else {
                    popUpDialog(response.body().getMessage(), false);
                }
                dismissprogressdialog();
            }

            @Override
            public void onFailure(Call<otp_verify> call, Throwable t) {
                dismissprogressdialog();
            }
        });

    }

    @SuppressLint("InflateParams")
    private void popUpDialogSuccess(Spannable result1) {
        // TODO Auto-generated method stub
        AlertDialog.Builder builder = new AlertDialog.Builder(OTPVerifyActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = OTPVerifyActivity.this.getLayoutInflater();

        View content = inflater.inflate(R.layout.otp_verification_dialog, null);

        TextView tv = content.findViewById(R.id.otpAlertMessage1);
        tv.setText(result1);

        ((Button) content.findViewById(R.id.btnOTPAlertOK)).setText(resources.getString(R.string.action_text_ok));

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        content.findViewById(R.id.btnOTPAlertOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();
                        startActivity(new Intent(OTPVerifyActivity.this, MainActivity.class));
                        setResult(10);
                        finish();
                    }
                });
    }


    @SuppressLint("InflateParams")
    private void showNetworkFailDialog(String msg) {
        // TODO Auto-generated method stub
        AlertDialog.Builder builder = new AlertDialog.Builder(OTPVerifyActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = OTPVerifyActivity.this.getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        alertDialog.dismiss();
                        setResult(10);
                        finish();
                    }

                });
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(OTPVerifyActivity.this, SignInActivity.class);
        startActivity(i);
        setResult(10);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        startSmsUserConsent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
//https://developers.google.com/identity/sms-retriever/request