package com.halonix.onspot.view.SaleReport;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.sale_history_list;
import com.halonix.onspot.contributors.sale_history_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Utils.getDate;


public class SaleReportFragment extends BaseFragment implements ConnectionLostCallback {

    @BindView(R.id.listView_searched_history)
    RecyclerView listView_searched_history;

    @BindView(R.id.textView_start_date)
    TextView textViewStartDate;

    @BindView(R.id.textView_end_date)
    TextView textViewEndDate;

    @BindView(R.id.textView_no_data_found_completed)
    TextView textViewNoDataFound;

    @BindView(R.id.button_go)
    Button buttonGo;

    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    @BindView(R.id.linbody)
    LinearLayout linbody;


    private Activity cntx = null;
    private DatabaseHelper objDatabaseHelper = null;
    private ConnectionDetectorActivity cd = null;
    private int mYear, mMonth, mDay;
    Calendar calendar;
    String myFormat = "yyyy-MM-dd";
    String expectedFormat = "dd-MMM-yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
    RetrofitClient retrofitClient;
    private long mLastClickTime = 0;

    private BroadcastReceiver myBroadcastReceiver;
    private boolean receiver = false;

    ArrayList<sale_history_list> sale_history_listArrayList;
    ArrayList<sale_history_list> sale_history_listArrayList1;

    protected void unregisterNetworkChanges() {
        try {
            getActivity().unregisterReceiver(myBroadcastReceiver);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sale_report, container, false);
        ButterKnife.bind(this, view);
        objDatabaseHelper = DatabaseHelper.getInstance(cntx);
        cd = new ConnectionDetectorActivity(getActivity());
        retrofitClient = new RetrofitClient();
        initViews();

        listView_searched_history.setHasFixedSize(true);
        listView_searched_history.setItemAnimator(new DefaultItemAnimator());
        listView_searched_history.setLayoutManager(new LinearLayoutManager(getActivity()));
        myBroadcastReceiver = new MyBroadcastReceiver(this);
        getActivity().registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        receiver = true;

        return view;
    }


    private void initViews() {
        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);


        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (textViewStartDate.getText().length() > 0 && textViewEndDate.getText().length() > 0) {
                    if (cd.isConnectingToInternet()) {
                        get_sale_history();
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }
                } else {
                    Utils.showToast(getActivity(), "Please select From or To Date");
                }
            }
        });


        textViewStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                try {
                    DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                            calendar.set(Calendar.YEAR, year);
                            calendar.set(Calendar.MONTH, (month));
                            calendar.set(Calendar.DAY_OF_MONTH, day_of_month);

                            textViewStartDate.setText(getDate(sdf.format(calendar.getTime()), myFormat, expectedFormat));
                            if (textViewEndDate.getText().toString().length() > 0) {
                                SimpleDateFormat sdf = new SimpleDateFormat(expectedFormat);
                                Date date = null;
                                Date date1 = null;
                                try {
                                    date = sdf.parse(textViewStartDate.getText().toString());
                                    date1 = sdf.parse(textViewEndDate.getText().toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (date1.before(date)) {
                                    textViewEndDate.setText(getDate(sdf.format(calendar.getTime()), expectedFormat, expectedFormat));

                                }

                            }

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());// TODO: used to hide future date,month and year
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        textViewEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                try {
                    if (textViewStartDate.getText().toString().trim().length() > 0) {
                        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, (month));
                                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                                textViewEndDate.setText(getDate(sdf.format(calendar.getTime()), myFormat, expectedFormat));
                            }
                        }, mYear, mMonth, mDay);
                        Calendar fromDateforTo = Calendar.getInstance();
                        if (!textViewStartDate.getText().toString().trim().isEmpty()) {
                            String start_date = getDate(textViewStartDate.getText().toString(), expectedFormat, myFormat);
                            String[] arrayDatefrom = start_date.split("-");
                            fromDateforTo.set(Calendar.YEAR, Integer.parseInt(arrayDatefrom[0]));
                            fromDateforTo.set(Calendar.MONTH, Integer.parseInt(arrayDatefrom[1]) - 1);
                            fromDateforTo.set(Calendar.DAY_OF_MONTH, Integer.parseInt(arrayDatefrom[2]));
                        }
                        dialog.getDatePicker().setMinDate(fromDateforTo.getTimeInMillis());// TODO: used to hide previous date,month and year
                        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        dialog.show();
                    } else {
                        Utils.showToast(getActivity(), "Please select from date first.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void get_sale_history() {
        Utils.showprogressdialog(getActivity(), "Requesting, please wait.");
        sale_history_listArrayList = new ArrayList<>();
        sale_history_listArrayList1 = new ArrayList<>();

        String start_date = getDate(textViewStartDate.getText().toString(), expectedFormat, myFormat);
        String end_date = getDate(textViewEndDate.getText().toString(), expectedFormat, myFormat);
        retrofitClient.getService().sale_history(objDatabaseHelper.getAuthToken(),start_date,
                end_date).enqueue(new Callback<sale_history_respo>() {
            @Override
            public void onResponse(Call<sale_history_respo> call, Response<sale_history_respo> response) {
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    sale_history_listArrayList = response.body().getList();

                    if (sale_history_listArrayList.size() > 0) {
                        textViewNoDataFound.setVisibility(View.GONE);
                        listView_searched_history.setVisibility(View.VISIBLE);
                        for (int i = 0; i < sale_history_listArrayList.size(); i++) {

                            sale_history_list saleHistoryList = new sale_history_list(sale_history_listArrayList.get(i).getCode(),
                                    sale_history_listArrayList.get(i).getModel(), sale_history_listArrayList.get(i).getSaleDate(),
                                    sale_history_listArrayList.get(i).getMfgDate());

                            sale_history_listArrayList1.add(saleHistoryList);
                        }

                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(sale_history_listArrayList1);
                        listView_searched_history.setAdapter(shipmentAdapter);
                    } else {
                        listView_searched_history.setVisibility(View.GONE);
                        textViewNoDataFound.setVisibility(View.VISIBLE);
                        textViewNoDataFound.setText("No sale history found.");
                    }

                } else {
                    listView_searched_history.setVisibility(View.GONE);
                    textViewNoDataFound.setVisibility(View.VISIBLE);
                    textViewNoDataFound.setText("No sale history found.");
                }
                Utils.dismissprogressdialog();
            }

            @Override
            public void onFailure(Call<sale_history_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    public void dialog(boolean value) {
        Log.d(Constants.TAG, "dialog value: " + value);
        if (value) {

            no_internet.setVisibility(View.GONE);
            linbody.setVisibility(View.VISIBLE);

        } else {

            linbody.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }


    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }

                });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_sale_report);
    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {

        private ArrayList<sale_history_list> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textView_sr_no;
            private AppCompatTextView textView_qr_code;
            private AppCompatTextView textView_model;
            private AppCompatTextView textView_mfg_date;
            private AppCompatTextView textView_sale_date;
            private LinearLayout ael;
            private AppCompatTextView tvMore;

            public MyViewHolder(View view) {
                super(view);
                textView_sr_no = view.findViewById(R.id.textView_sr_no);
                textView_qr_code = view.findViewById(R.id.textView_qr_code);
                textView_model = view.findViewById(R.id.textView_model);
                textView_mfg_date = view.findViewById(R.id.textView_mfg_date);
                textView_sale_date = view.findViewById(R.id.textView_sale_date);
                ael = view.findViewById(R.id.expandable_ans);
                tvMore = view.findViewById(R.id.tvMore);

            }
        }


        public ShipmentAdapter(ArrayList<sale_history_list> notesList) {
            this.notesList = notesList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_sale_history, parent, false);
            return new MyViewHolder(itemView);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final sale_history_list object = notesList.get(position);

            holder.textView_sr_no.setText(String.valueOf(position + 1));
            holder.textView_sale_date.setText(getDate(getDisplayDate(object.getSaleDate()), "yyyy-MM-dd", "dd-MM-yyyy"));
            holder.textView_model.setText(object.getModel());
            holder.textView_mfg_date.setText(getDate(getDisplayDate(object.getMfgDate()), "yyyy-MM-dd", "dd-MM-yyyy"));
            holder.textView_qr_code.setText(String.valueOf(object.getCode()));


            holder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.ael.setVisibility(holder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (holder.ael.getVisibility() == View.VISIBLE) {
                        holder.tvMore.setText("Less Details");
                    } else {
                        holder.tvMore.setText("More Details");
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

}
