package com.halonix.onspot.view.ReceiveNewShipment;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceCodeList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.get_receive_shipmentList;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.LogDetailModel;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.model.SystemBarcodeModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiveNewShipmentMainFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.button_scan_invoice)
    AppCompatButton button_scan_invoice;

    @BindView(R.id.button_scan_master)
    AppCompatButton button_scan_master;

    @BindView(R.id.button_scan_individual)
    AppCompatButton button_scan_individual;


    @BindView(R.id.textView_owner_name)
    AppCompatTextView textViewOwnerName;

    DatabaseHelper databaseHelper;
    ConnectionDetectorActivity cd;
    private RetrofitClient retrofitClient;
    private Location loca;
    private String strLatitude = "", strLongitude = "", strAccuracy = "";
    private long mLastClickTime = 0;

    //Receive New Shipment
    ArrayList<String> InvoiceCodeListArrayList;
    ArrayList<get_receive_shipmentList> receiveShipmentListArrayList;
    ArrayList<ReceiveShipmentInvoiceList> InvoiceCodeProductDataArrayList;
    ArrayList<PackedCartonList> packedcartonListArrayList;
    int k = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_receive_new_shipment_main, container, false);
        ButterKnife.bind(this, view);
        databaseHelper = DatabaseHelper.getInstance(getActivity());
        cd = new ConnectionDetectorActivity(getActivity());
        retrofitClient = new RetrofitClient();
        textViewOwnerName.setText(databaseHelper.getUsername());
        button_scan_invoice.setOnClickListener(this);
        button_scan_master.setOnClickListener(this);
        button_scan_individual.setOnClickListener(this);


        if (Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_RETAILER)) {
            button_scan_invoice.setVisibility(View.GONE);
        } else {
            button_scan_invoice.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public boolean backButtonPressed() {
        return true;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_scan_invoice:
                if (databaseHelper.getInvoiceScanCode() != null && databaseHelper.getRSInvoicecodeData().size() > 0) {
                    if (databaseHelper.getInvoiceScanCode().getJsonArray().length() > 0) {

                        if (databaseHelper.getIS_ScannedMasterCartonCount() > 0) {
                            Intent intent = new Intent(getActivity(), RSByInvoiceCheckCartonsActivity.class);
                            startActivity(intent);
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            Intent intent = new Intent(getActivity(), RSByInvoiceActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            getActivity().overridePendingTransition(0, 0);
                        }
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Please sync server data first to proceed further.")
                                .setPositiveButton("Ok", null)
                                .show();
                    }
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setCancelable(false)
                            .setMessage("Please sync server data first to proceed further.")
                            .setPositiveButton("Ok", null)
                            .show();
                }

                break;
            case R.id.button_scan_master:

                if (cd.isConnectingToInternet()) {
                    ArrayList<LogDetailModel> logDetails = databaseHelper.getLogDetails();
                    if (logDetails.size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline shipment (Receive by master cartons),Do you want to upload it to server.")
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.deleteAllLogs();
                                        databaseHelper.deleteAllScannedMasterCartonCode("all");
                                        gotonextforMasterCartonScan();
                                    }
                                })
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        LocationControl locationControl = new LocationControl();
                                        locationControl.execute();
                                    }
                                })
                                .show();
                    } else {
                        gotonextforMasterCartonScan();
                    }
                } else {
                    gotonextforMasterCartonScan();
                }


                break;
            case R.id.button_scan_individual:
                if (cd.isConnectingToInternet()) {
                    if (databaseHelper.getIPScannedQRCodeList() > 0) {
                        if (databaseHelper.getIPScannedQRCodeList() == databaseHelper.getIPCode().size()) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("You have individual scan shipment,Do you want to upload it to server.")
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            databaseHelper.deleteAllScannedIPCode();
                                            //   startActivity(new Intent(getActivity(), RSByIndividualProductActivity.class));

                                            Intent intent = new Intent(getActivity(), RSByIndividualProductActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                            startActivity(intent);
                                            getActivity().overridePendingTransition(0, 0);
                                        }
                                    })
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();

                                            if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                                                LocationControlRegular locationControl = new LocationControlRegular();
                                                locationControl.execute();
                                            } else {
                                                LocationControlBreakage locationControl = new LocationControlBreakage();
                                                locationControl.execute();
                                            }
                                        }
                                    })
                                    .show();

                        } else {
                            if (databaseHelper.getIPScannedQRCodeList() > 0) {
                                String qty = String.valueOf(databaseHelper.getIPCode().size());
                                Intent intent = new Intent(getActivity(), RSByIndividualScannerActivity.class);
                                intent.putExtra("qty", qty);
                                startActivity(intent);
                                getActivity().overridePendingTransition(0, 0);
                            } else {
                                Intent intent = new Intent(getActivity(), RSByIndividualProductActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                                getActivity().overridePendingTransition(0, 0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(getActivity(), RSByIndividualProductActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    }

                } else {
                    if (databaseHelper.getIPScannedQRCodeList() == databaseHelper.getIPCode().size()) {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    } else {
                        String qty = String.valueOf(databaseHelper.getIPCode().size());
                        Intent intent = new Intent(getActivity(), RSByIndividualScannerActivity.class);
                        intent.putExtra("qty", qty);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    }
                }
                break;

        }
    }

    private void gotonextforMasterCartonScan() {
//        Log.d(Constants.TAG, "gotonextforMasterCartonScan: " + databaseHelper.getMasterCartonCode().getJsonArray().length());
        if (databaseHelper.getMasterCartonCode() != null) {
            if (databaseHelper.getMasterCartonCode().getJsonArray().length() > 0) {
                Intent intent = new Intent(getActivity(), RSByMasterCartonsActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            } else {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Please sync server data first to proceed further.")
                        .setPositiveButton("Ok", null)
                        .show();
            }
        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please sync server data first to proceed further.")
                    .setPositiveButton("Ok", null)
                    .show();
        }
    }

    private class LocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading shipment(Receiving by master cartons,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServer();
        }

    }

    private void submitDetailsToServer() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(databaseHelper.getScannedMasterCartonQRList("1").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "submitDetailsToServer: " + jsonArray.toString());
        Log.d(Constants.TAG, "onResume strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "onResume strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "onResume strAccuracy: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipment(databaseHelper.getAuthToken(), jsonArray.toString(), "MasterCarton", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                    databaseHelper.deleteAllLogs();
                    databaseHelper.deleteAllScannedMasterCartonCode("all");
                }
                getpackedcarton();
            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(databaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }


                    }

                }
                String USER_TYPEs = Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_RETAILER) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else {
                    getinvoice_codes();
                }


            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {

        retrofitClient.getService().getInvoiceCodes(databaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(Constants.TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    databaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        databaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                    getReceiveNewShipmentList();
                }
            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Receive New Shipment API Call here
    private void getReceiveNewShipmentList() {
        k = 0;
        InvoiceCodeListArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveNewShipmentCodes(databaseHelper.getAuthToken()).enqueue(new Callback<get_receive_shipment_respo>() {
            @Override
            public void onResponse(Call<get_receive_shipment_respo> call, Response<get_receive_shipment_respo> response) {

                Log.d(Constants.TAG, "onResponse getReceiveNewShipmentList: " + response.toString());
                if (response.body().getSuccess()) {
                    InvoiceCodeListArrayList = response.body().getInvoiceCodes();
                    receiveShipmentListArrayList = response.body().getList();
                    databaseHelper.deleteAllReceiveShipmentCode();
                    databaseHelper.deleteServerBarcodeDataTableData();
                    databaseHelper.deleteAllReceiveShipmentCodeData();
                    databaseHelper.deleteAllReceiveShipmentISMasterCartons();

                    if (receiveShipmentListArrayList.size() > 0) {
                        for (int i = 0; i < receiveShipmentListArrayList.size(); i++) {
                            databaseHelper.insertReceiveNewShipment(new NewShipmentDetailModel(String.valueOf(receiveShipmentListArrayList.get(i).getId()),
                                    String.valueOf(receiveShipmentListArrayList.get(i).getPackingRatio()), receiveShipmentListArrayList.get(i).getCode(),
                                    receiveShipmentListArrayList.get(i).getProductName(), receiveShipmentListArrayList.get(i).getSweep(),
                                    receiveShipmentListArrayList.get(i).getColor(), "1"));
                        }


                        JSONArray jsonArray1 = databaseHelper.getReceiveShipmentBarcode1().getJsonArray();
                        databaseHelper.insertMasterCartoncode(new SystemBarcodeModel(jsonArray1));

                    }


                    if (InvoiceCodeListArrayList.size() > 0) {

                        databaseHelper.inserRSInvoicecode(new SystemBarcodeModel(new JSONArray(InvoiceCodeListArrayList)));


                        for (int i = 0; i < InvoiceCodeListArrayList.size(); i++) {
                            ReceiveShipmentByInvoiceData(InvoiceCodeListArrayList.get(i));
                        }

                    } else {
                        Utils.dismissprogressdialog();
                        Utils.showToast(getActivity(), "Shipment uploaded Successfully");
                        gotonextforMasterCartonScan();
                    }


                }
            }

            @Override
            public void onFailure(Call<get_receive_shipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Receive New Shipment Invoice API Call here
    private void ReceiveShipmentByInvoiceData(String invoice_code) {
        Log.d(Constants.TAG, "ReceiveShipmentByInvoiceData i: " + invoice_code);
        InvoiceCodeProductDataArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveShipmentByInvoice(databaseHelper.getAuthToken(), invoice_code).enqueue(new Callback<ReceiveShipmentInvoice_respo>() {
            @Override
            public void onResponse(Call<ReceiveShipmentInvoice_respo> call, Response<ReceiveShipmentInvoice_respo> response) {

                //Log.d(Constants.TAG, "onResponse ReceiveShipmentByInvoiceData: " + response.toString());
                if (response.body().getSuccess()) {
                    k++;
                    InvoiceCodeProductDataArrayList = response.body().getList();

                    if (InvoiceCodeProductDataArrayList.size() > 0) {
                        for (int i = 0; i < InvoiceCodeProductDataArrayList.size(); i++) {
                            if (InvoiceCodeProductDataArrayList.get(i).getQuantity() == null) {
                            } else {
                                databaseHelper.inserRSInvoicecodeData(new NewShipmentDetailModel(invoice_code, InvoiceCodeProductDataArrayList.get(i).getModel(),
                                        "", "",
                                        String.valueOf(InvoiceCodeProductDataArrayList.get(i).getQuantity()), ""));
                                ArrayList<ReceiveShipmentInvoiceCodeList> codeArrayList = InvoiceCodeProductDataArrayList.get(i).getCodes();

                                for (int j = 0; j < codeArrayList.size(); j++) {
                                    databaseHelper.insertIS_MasterCartonCodes(InvoiceCodeProductDataArrayList.get(i).getModel(), invoice_code, codeArrayList.get(j).getCode(), String.valueOf(codeArrayList.get(j).getPackingRatio()));
                                }
                            }
                        }
                    }


                    if (k == InvoiceCodeListArrayList.size()) {
                        Utils.dismissprogressdialog();
                        Utils.showToast(getActivity(), "Shipment uploaded Successfully");
                        gotonextforMasterCartonScan();
                    }

                }
            }

            @Override
            public void onFailure(Call<ReceiveShipmentInvoice_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    private class LocationControlRegular extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Receiving shipment by individual products,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerRegular();
        }
    }

    private void submitDetailsToServerRegular() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "submitDetailsToServerRegular: " + res);

        Log.d(Constants.TAG, "submitDetailsToServerRegular strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "submitDetailsToServerRegular strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "submitDetailsToServerRegular strAccuracy: " + strAccuracy);


        retrofitClient.getService().getReceiveNewShipment(databaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {


                if (response.body().getSuccess()) {
                    if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                        LocationControlBreakage locationControl = new LocationControlBreakage();
                        locationControl.execute();
                    } else {
                        databaseHelper.deleteAllScannedIPCode();
                        Utils.dismissprogressdialog();
                        startActivity(new Intent(getActivity(), RSByIndividualProductActivity.class));
                        getActivity().overridePendingTransition(0, 0);
                        Utils.showToast(getActivity(), "Shipment received Successfully");
                    }

                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    private class LocationControlBreakage extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Receiving shipment by individual products,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerBreakage();
        }
    }

    private void submitDetailsToServerBreakage() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "submitDetailsToServerBreakage: " + res);

        Log.d(Constants.TAG, "submitDetailsToServerBreakage strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "submitDetailsToServerBreakage strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "submitDetailsToServerBreakage strAccuracy: " + strAccuracy);
        Utils.dismissprogressdialog();

        retrofitClient.getService().getReceiveNewShipmentDefective(databaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy, "1").enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {


                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllScannedIPCode();
                    startActivity(new Intent(getActivity(), RSByIndividualProductActivity.class));
                    getActivity().overridePendingTransition(0, 0);
                    Utils.showToast(getActivity(), "Shipment received Successfully");
                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Receive New Shipment");
        enableNavigationDrawer(false);
    }
}
