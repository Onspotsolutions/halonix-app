package com.halonix.onspot.view.base;

import android.os.Bundle;

import androidx.fragment.app.Fragment;


public interface OnFragmentListner {
    void handleChangeFragment(String constantFragment, boolean addToBackstack, Bundle bundle);

    void handleRemoveFragment(Fragment fragment);

    void enableNavigationDrawer(boolean enable);

}

