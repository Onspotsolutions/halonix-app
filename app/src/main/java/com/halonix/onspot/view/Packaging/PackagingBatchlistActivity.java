package com.halonix.onspot.view.Packaging;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.BatchIdList;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class PackagingBatchlistActivity extends AppCompatActivity {

    @BindView(R.id.rvBatchlist)
    RecyclerView rvBatchlist;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    ArrayList<BatchIdList> batchIdListArrayList;
    DatabaseHelper databaseHelper;
    private long mLastClickTime = 0;

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(PackagingBatchlistActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_batchlist);
        ButterKnife.bind(this);
        databaseHelper = DatabaseHelper.getInstance(PackagingBatchlistActivity.this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent intent = new Intent(PackagingBatchlistActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        toolbar.setTitle(getResources().getString(R.string.pack_products));

        rvBatchlist.setHasFixedSize(true);
        rvBatchlist.setItemAnimator(new DefaultItemAnimator());
        rvBatchlist.setLayoutManager(new LinearLayoutManager(PackagingBatchlistActivity.this));
        batchIdListArrayList = databaseHelper.getBatchList();

        BatchListAdapter batchListAdapter = new BatchListAdapter(batchIdListArrayList);
        rvBatchlist.setAdapter(batchListAdapter);
    }

    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<BatchIdList> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView tvBatch;
            private AppCompatTextView tvProcess;

            public MyViewHolder(View view) {
                super(view);
                tvBatch = view.findViewById(R.id.tvBatch);
                tvProcess = view.findViewById(R.id.tvProcess);
            }
        }


        public BatchListAdapter(ArrayList<BatchIdList> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_batch_row, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final BatchIdList object = notesList.get(position);

            holder.tvBatch.setText(object.getModel());

            holder.tvProcess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utils.removePreference(PackagingBatchlistActivity.this, "item_code");
                    Utils.removePreference(PackagingBatchlistActivity.this, "blade");
                    Utils.removePreference(PackagingBatchlistActivity.this, "model");

                    Utils.addPreference(PackagingBatchlistActivity.this, "item_code", object.getItemCode());
                    Utils.addPreferenceBoolean(PackagingBatchlistActivity.this, "blade", object.getSeprateBlade());
                    Utils.addPreference(PackagingBatchlistActivity.this, "model", object.getModel());

                    Intent intent = new Intent(PackagingBatchlistActivity.this, PackagingProductListActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                }
            });

        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
