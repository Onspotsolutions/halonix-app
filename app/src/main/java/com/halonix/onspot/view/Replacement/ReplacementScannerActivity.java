package com.halonix.onspot.view.Replacement;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.CodeDetails_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReplacementScannerActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener, ConnectionLostCallback {


    private Activity activity = null;
    private CaptureManager capture;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.textInput)
    TextInputLayout textInput;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.relBody)
    RelativeLayout relBody;

    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;

    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnFinish)
    AppCompatButton btnFinish;

    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private BeepManager beepManager = null;
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;
    private AlertDialog.Builder alert = null;
    private long mLastClickTime = 0;
    private String tag = "";
    RetrofitClient retrofitClient;
    ConnectionDetectorActivity cd;
    boolean reg = false;
    private AlertDialog builder;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replacement_scanner);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(ReplacementScannerActivity.this);
        if (getIntent().getExtras() != null) {
            tag = getIntent().getStringExtra("tag");
        }
        retrofitClient = new RetrofitClient();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });
        if (tag.equals("old")) {
            toolbar.setTitle("Scan Old Product QR Code");
            textInput.setHint("Old Product QR Code");
        } else {
            toolbar.setTitle("Scan New Product QR Code");
            textInput.setHint("New Product QR Code");
        }

        activity = ReplacementScannerActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);

        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);

        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    capture.onPause();
                    return;
                } else {
                    if (cd.isConnectingToInternet()) {
                        String[] final_barcode;
                        if (result.getText().contains("=")) {
                            final_barcode = result.getText().split("=");
                            String barCode1 = final_barcode[1].replaceAll("\u0000", "");
                            checkBarcode(barCode1, "camera");
                        } else {
                            checkBarcode(result.getText(), "camera");
                        }
                    }
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //capture.onResume();
                    }
                }, 3000);
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });


        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().trim().equals("")) {
                    checkBarcode(edScanCode.getText().toString().trim(), "manual");
                } else {
                    Toast.makeText(ReplacementScannerActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });

    }

    private void checkBarcode(String barcode, String tags) {

        String old_qr = Utils.getPreference(ReplacementScannerActivity.this, "old_qr", "");

        if (tag.equals("new")) {
            if (old_qr.equals(barcode)) {
                showAlert("Alert", "This is old qr code, please scan new qr code.", tags, true);
            } else {
                if (cd.isConnectingToInternet()) {
                    verify_code(barcode, tags);
                } else {
                    showNetworkFailDialog();
                }
            }
        } else {
            if (cd.isConnectingToInternet()) {
                verify_code(barcode, tags);
            } else {
                showNetworkFailDialog();
            }
        }


    }

    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(ReplacementScannerActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    private void verify_code(String barcode, String tags) {
        Utils.showprogressdialog(ReplacementScannerActivity.this, "Verifying your scan, please wait.");
        retrofitClient.getService().code_details(objDatabaseHelper.getAuthToken(), barcode).enqueue(new Callback<CodeDetails_respo>() {
            @Override
            public void onResponse(Call<CodeDetails_respo> call, Response<CodeDetails_respo> response) {

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    if (tag.equals("old")) {
                        if (response.body().getData().getWarranty().equals("In Warranty")) {
                            Utils.addPreference(ReplacementScannerActivity.this, "old_qr", barcode);
                            Intent intent = new Intent(ReplacementScannerActivity.this, ReplacementOldActivity.class);
                            intent.putExtra("model", response.body().getData().getProductName());
                            intent.putExtra("sweep", response.body().getData().getSweep());
                            intent.putExtra("voltage", response.body().getData().getVoltage());
                            intent.putExtra("warranty", response.body().getData().getWarranty());
                            intent.putExtra("qr_code", barcode);
                            intent.putExtra("tag", tag);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        } else if (response.body().getData().getWarranty().equals("Out Of Warranty")) {
                            showAlert("Alert", "Warranty of this product is expired.", tags, true);
                        } else {
                            showAlert("Alert", "This is not an old product,please scan old product.", tags, true);
                        }
                    } else {
                        if (response.body().getData().getWarranty().equals("Not Activated")) {
                            Utils.addPreference(ReplacementScannerActivity.this, "new_qr", barcode);
                            Intent intent = new Intent(ReplacementScannerActivity.this, ReplacementNewActivity.class);
                            intent.putExtra("qr_code", barcode);
                            intent.putExtra("tag", tag);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        } else if (response.body().getData().getWarranty().equals("Out Of Warranty")) {
                            showAlert("Alert", "Warranty of this product is expired.", tags, true);
                        } else {
                            showAlert("Alert", "This product is already sold out.", tags, true);
                        }

                    }
                } else {
                    Utils.dismissprogressdialog();
                    showAlert("Alert", "This Qr-code is invalid.", tags, true);
                }
            }

            @Override
            public void onFailure(Call<CodeDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    private void showAlert(String Title, String Message, String tag, boolean valid) {

        if (tag.equals("camera")) {
            capture.onPause();
        }

        alert = new AlertDialog.Builder(ReplacementScannerActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert = null;
            }
        });
        alert.show();
    }


    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }

    @Override
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(0, 0);
    }

    public void stopScanning(View view) {
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null) {
            capture.onResume();
        }

        if (!reg) {
            myBroadcastReceiver = new MyBroadcastReceiver(this);
            registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            reg = true;
        }

        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);
            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);

        } else {
            focusable(true);
//            btnGo.setEnabled(true);

            relOTG.setVisibility(View.GONE);
            radio_otg.setChecked(false);

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setChecked(true);
            radio_camera.setEnabled(true);


        }


//        if (btnGo.isEnabled())
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));
//        else
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));
    }

    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

//            btnGo.setEnabled(false);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(ReplacementScannerActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            focusable(true);

//            btnGo.setEnabled(true);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);
            relCamera.setVisibility(View.VISIBLE);
            capture.onResume();

            hideKeyboard(ReplacementScannerActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            focusable(false);
            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void barcodeLookup(String barCode) {
        if (cd.isConnectingToInternet()) {
            String[] final_barcode;
            if (barCode.contains("=")) {
                final_barcode = barCode.split("=");
                barCode = final_barcode[1].replaceAll("\u0000", "");
                edScanCode.setText(barCode);
                checkBarcode(barCode, "OTG");
            } else {
                barCode = barCode.replaceAll("\u0000", "");
                edScanCode.setText(barCode);
                checkBarcode(barCode, "OTG");
            }
        }
    }
    //OTG Code End

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    public void dialog(boolean value) {
        Log.d(Constants.TAG, "dialog value: " + value);
        if (value) {
            no_internet.setVisibility(View.GONE);
            relBody.setVisibility(View.VISIBLE);
        } else {
            relBody.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }

    private BroadcastReceiver myBroadcastReceiver;

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(myBroadcastReceiver);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


}
