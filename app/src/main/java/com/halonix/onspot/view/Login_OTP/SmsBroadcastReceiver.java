package com.halonix.onspot.view.Login_OTP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.halonix.onspot.utils.Constants;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == SmsRetriever.SMS_RETRIEVED_ACTION) {
            Bundle extras = intent.getExtras();
            Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
            switch (smsRetrieverStatus.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    //Pass the message text to interface
                    Log.d(Constants.TAG, "onReceive msgs dsfsdf: " + SmsRetriever.EXTRA_CONSENT_INTENT);
                    Intent messageIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                    if (mListener != null) {
                        mListener.onSuccess(messageIntent);
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                   // mListener.onFailure();
                    break;
            }
        }
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}
