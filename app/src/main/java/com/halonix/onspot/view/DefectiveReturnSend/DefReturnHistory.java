package com.halonix.onspot.view.DefectiveReturnSend;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PagerSlidingTabStrip;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.util.Locale;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DefReturnHistory extends BaseFragment implements ConnectionLostCallback {


    @BindView(R.id.pager)
    ViewPager mViewPager;

    @BindView(R.id.linbody)
    RelativeLayout linbody;

    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    @BindView(R.id.tabs)
    PagerSlidingTabStrip tabs;

    Context ctx;
    ActionBar actionbar;
    SectionsPagerAdapter adapter;
    private BroadcastReceiver myBroadcastReceiver;
    private boolean receiver = false;

    protected void unregisterNetworkChanges() {
        try {
            getActivity().unregisterReceiver(myBroadcastReceiver);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(Constants.TAG, "onDestroy: " + receiver);
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        ctx = getActivity();
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    public void dialog(boolean value) {
        Log.d(Constants.TAG, "dialog value: " + value);
        if (value) {

            no_internet.setVisibility(View.GONE);
            linbody.setVisibility(View.VISIBLE);
            adapter = new SectionsPagerAdapter(getChildFragmentManager());
            mViewPager.setOffscreenPageLimit(0);
            mViewPager.setAdapter(adapter);
            tabs.setViewPager(mViewPager);
        } else {
            linbody.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }

    @Override
    public void onResume() {
        super.onResume();
        enableNavigationDrawer(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Defective Return History");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shipment_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        actionbar = getActivity().getActionBar();
        tabs.setTextColorResource(R.color.white);
        tabs.setTypeface(null, Typeface.BOLD);
        tabs.setShouldExpand(true);
        adapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager.setOffscreenPageLimit(0);
        mViewPager.setAdapter(adapter);
        tabs.setViewPager(mViewPager);

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                switch (position) {
                    case 0:
                        DefReturnCompleted fragmentCompleted = (DefReturnCompleted) adapter.instantiateItem(mViewPager, 0);
                        if (fragmentCompleted != null) {
                            fragmentCompleted.fragmentBecameVisible();
                        }
                        break;
                    case 1:
                        DefReturnPending fragmentPending = (DefReturnPending) adapter.instantiateItem(mViewPager, 1);
                        if (fragmentPending != null) {
                            fragmentPending.fragmentBecameVisible();
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        myBroadcastReceiver = new MyBroadcastReceiver(this);
        getActivity().registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        receiver = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            FragmentManager manager = ((Fragment) object).getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove((Fragment) object);
            trans.commit();
            super.destroyItem(container, position, object);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    fragment = new DefReturnCompleted();
                    return fragment;
                case 1:
                    fragment = new DefReturnPending();
                    return fragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            Resources resources = getActivity().getResources();
            return resources.getStringArray(R.array.shipment_tabs).length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            Resources resources = getActivity().getResources();
            return resources.getStringArray(R.array.shipment_tabs)[position].toUpperCase(l);
        }
    }
}
