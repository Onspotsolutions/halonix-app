package com.halonix.onspot.view.Unpack;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.SenderDetails_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class UnpackActivity extends AppCompatActivity implements View.OnClickListener {


    private Activity activity = null;

    @BindView(R.id.scanShipmentList)
    RecyclerView scanShipmentList;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ly_scan_system)
    LinearLayout ly_scan_system;


    private DatabaseHelper objDatabaseHelper = null;
    ArrayList<NewShipmentDetailModel> newShipmentScannedArrayList = new ArrayList<>();
    private ConnectionDetectorActivity cd;
    BatchListAdapter scanShipmentCodeAdapter;
    private long mLastClickTime = 0;
    RetrofitClient retrofitClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpack);
        ButterKnife.bind(this);
        retrofitClient = new RetrofitClient();

        activity = UnpackActivity.this;

        objDatabaseHelper = DatabaseHelper.getInstance(activity);
        cd = new ConnectionDetectorActivity(activity);

        scanShipmentList.setHasFixedSize(true);
        scanShipmentList.setItemAnimator(new DefaultItemAnimator());
        scanShipmentList.setLayoutManager(new LinearLayoutManager(UnpackActivity.this));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            onBackPressed();
            overridePendingTransition(0, 0);
        });
        toolbar.setTitle("Unpack Master Cartons");

        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_addmore).setOnClickListener(this);
        findViewById(R.id.button_submit).setOnClickListener(this);
        findViewById(R.id.button_discard_all).setOnClickListener(this);

    }


    private void start_scanning() {
        Intent intent = new Intent(UnpackActivity.this, UnpackScannerActivity.class);
        intent.putExtra("token", "add");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    private void remove_scan() {
        Intent intent = new Intent(UnpackActivity.this, UnpackScannerActivity.class);
        intent.putExtra("token", "remove");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
            case R.id.button_addmore:
                start_scanning();
                break;
            case R.id.button_submit:
                if (cd.isConnectingToInternet()) {
                    if (newShipmentScannedArrayList.size() > 0) {
                        new AlertDialog.Builder(activity)
                                .setTitle("Alert")
                                .setMessage("Are you sure you want to proceed further ?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dispatchShipment();
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }
                } else {
                    Utils.showToast(UnpackActivity.this, "Unpacked data stored offline.");
                    Intent intent = new Intent(UnpackActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    UnpackActivity.this.finish();
                    overridePendingTransition(0, 0);
                }


                break;
            case R.id.button_discard_all:
                remove_scan();
                break;
        }
    }


    private void dispatchShipment() {
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedForceCodeList("0", "unpack");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "dispatchShipment: " + jsonArray.toString());

        Utils.showprogressdialog(UnpackActivity.this, "Requesting for unpacking,please wait.");

        retrofitClient.getService().unpack(objDatabaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<SenderDetails_respo>() {
            @Override
            public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    objDatabaseHelper.deleteScannedForcedCode("0", "unpack");
                    getpackedcarton();
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(UnpackActivity.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    ArrayList<PackedCartonList> packedcartonListArrayList;

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {

                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }
                    }

                }
                Intent intent = new Intent(UnpackActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                UnpackActivity.this.finish();
                overridePendingTransition(0, 0);
                Utils.showToast(UnpackActivity.this, "Master cartons unpacked successfully");
            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    @Override
    protected void onResume() {
        findViewById(R.id.ly_scan_system).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "unpack").size() > 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.button_start_scanning).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "unpack").size() > 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.scanShipmentList).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "unpack").size() > 0 ? View.VISIBLE : View.GONE);

        if (objDatabaseHelper.getScannedForcedCodeList("0", "unpack") != null) {
            newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "unpack");
        }

        if (newShipmentScannedArrayList.size() > 0) {
            scanShipmentList.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
            scanShipmentList.setAdapter(scanShipmentCodeAdapter);
            ly_scan_system.setVisibility(View.VISIBLE);
            findViewById(R.id.button_start_scanning).setVisibility(View.GONE);
        } else {
            linHeader.setVisibility(View.GONE);
        }
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    if (objDatabaseHelper.getScannedForcedCodeList("0", "unpack") != null) {
                        newShipmentScannedArrayList = new ArrayList<>();
                        newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "unpack");
                    }
                    if (newShipmentScannedArrayList.size() > 0)
                        linHeader.setVisibility(View.VISIBLE);
                    else
                        linHeader.setVisibility(View.GONE);

                    scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
                    scanShipmentList.setAdapter(scanShipmentCodeAdapter);
                    onResume();
                    break;
            }
        }
    }

    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewId;
            private TextView textViewQuantity;
            private TextView textViewName;

            public MyViewHolder(View view) {
                super(view);
                textViewId = view.findViewById(R.id.textView_no);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewName = view.findViewById(R.id.textView_name);

            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_scan_shipment, parent, false);
            return new BatchListAdapter.MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(BatchListAdapter.MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewId.setText(String.valueOf(position + 1));

            holder.textViewName.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getPackedRatio());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onBackPressed() {
        newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "unpack");
        if (newShipmentScannedArrayList.size() > 0) {
            new androidx.appcompat.app.AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "unpack");
                            for (int i = 0; i < newShipmentScannedArrayList.size(); i++) {
                                objDatabaseHelper.updateShipmentFromSaleReturn(newShipmentScannedArrayList.get(i).getShipmentCode(), "0", "1");
                            }

                            objDatabaseHelper.deleteScannedForcedCode("0", "unpack");

                            Intent intent = new Intent(UnpackActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            UnpackActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
