package com.halonix.onspot.view.ForcefullyIn;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForcefullyInMainFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.button_sale_return)
    AppCompatButton button_sale_return;

    @BindView(R.id.button_defective_return)
    AppCompatButton button_defective_return;


    @BindView(R.id.textView_owner_name)
    AppCompatTextView textViewOwnerName;

    DatabaseHelper databaseHelper;
    ConnectionDetectorActivity cd;
    private RetrofitClient retrofitClient;
    private long mLastClickTime = 0;
    private AlertDialog builder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forcefully_in_main, container, false);
        ButterKnife.bind(this, view);

        databaseHelper = DatabaseHelper.getInstance(getActivity());
        cd = new ConnectionDetectorActivity(getActivity());
        retrofitClient = new RetrofitClient();
        textViewOwnerName.setText(databaseHelper.getUsername());
        button_sale_return.setOnClickListener(this);
        button_defective_return.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    private void showNetworkFailDialog() {
        builder = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_sale_return:
                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(getActivity(), SaleReturnFIActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    if (databaseHelper.getScannedForceCodeList("0", "force_in").size() > 0) {
                        Intent intent = new Intent(getActivity(), SaleReturnFIActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        showNetworkFailDialog();
                    }
                }

                break;
            case R.id.button_defective_return:
                if (cd.isConnectingToInternet()) {
                    Intent intent1 = new Intent(getActivity(), DefReturnFIActivity.class);
                    startActivity(intent1);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    if (databaseHelper.getScannedForceCodeList("1", "force_in").size() > 0) {
                        Intent intent = new Intent(getActivity(), DefReturnFIActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        showNetworkFailDialog();
                    }

                }

                break;

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_forcefully_in);
        enableNavigationDrawer(false);
    }
}
