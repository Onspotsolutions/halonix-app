package com.halonix.onspot.view.NewShipment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class NewShipmentActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity = null;

    @BindView(R.id.scanShipmentList)
    RecyclerView scanShipmentList;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    @BindView(R.id.ly_scan_system)
    LinearLayout ly_scan_system;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private DatabaseHelper objDatabaseHelper = null;
    ArrayList<NewShipmentDetailModel> newShipmentScannedArrayList = new ArrayList<>();
    BatchListAdapter scanShipmentCodeAdapter;
    private long mLastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_new_shipment);
        ButterKnife.bind(this);
        activity = NewShipmentActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);

        scanShipmentList.setHasFixedSize(true);
        scanShipmentList.setItemAnimator(new DefaultItemAnimator());
        scanShipmentList.setLayoutManager(new LinearLayoutManager(NewShipmentActivity.this));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });
        toolbar.setTitle("Send New Shipment");

        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_addmore).setOnClickListener(this);
        findViewById(R.id.button_done).setOnClickListener(this);
        findViewById(R.id.button_discard_all).setOnClickListener(this);

    }


    private void remove_scan() {
        Intent intent = new Intent(NewShipmentActivity.this, NewShipmentOTG_CameraScannerActivity.class);
        intent.putExtra("token", "remove");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    private void start_scanning() {
        Intent intent = new Intent(NewShipmentActivity.this, NewShipmentOTG_CameraScannerActivity.class);
        intent.putExtra("token", "add");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }


    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
            case R.id.button_addmore:
                start_scanning();
                break;
            case R.id.button_done:
                if (newShipmentScannedArrayList.size() > 0) {
                    new AlertDialog.Builder(activity)
                            .setTitle("Alert")
                            .setMessage("Are you sure you want to proceed further ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(NewShipmentActivity.this, NewShipmentDispatchActivity.class));
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                } else {
                    Utils.showToast(activity, "Please scan at least one QR code");
                }

                break;
            case R.id.button_discard_all:
                remove_scan();
                break;
        }
    }


    @Override
    protected void onResume() {
        findViewById(R.id.ly_scan_system).setVisibility(objDatabaseHelper.getListScanedShipment().size() > 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.button_start_scanning).setVisibility(objDatabaseHelper.getListScanedShipment().size() > 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.scanShipmentList).setVisibility(objDatabaseHelper.getListScanedShipment().size() > 0 ? View.VISIBLE : View.GONE);

        if (objDatabaseHelper.getShipmentSystemBarcode() != null) {
            newShipmentScannedArrayList = objDatabaseHelper.getListScanedShipment();
        }

        if (newShipmentScannedArrayList.size() > 0) {
            scanShipmentList.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
            scanShipmentList.setAdapter(scanShipmentCodeAdapter);
            ly_scan_system.setVisibility(View.VISIBLE);
            findViewById(R.id.button_start_scanning).setVisibility(View.GONE);
        } else {
            linHeader.setVisibility(View.GONE);
        }
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    if (objDatabaseHelper.getShipmentSystemBarcode() != null) {
                        newShipmentScannedArrayList = new ArrayList<>();
                        newShipmentScannedArrayList = objDatabaseHelper.getListScanedShipment();
                    }
                    if (newShipmentScannedArrayList.size() > 0)
                        linHeader.setVisibility(View.VISIBLE);
                    else
                        linHeader.setVisibility(View.GONE);

                    scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
                    scanShipmentList.setAdapter(scanShipmentCodeAdapter);
                    onResume();
                    break;
            }
        }
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewId;
            private TextView textViewQuantity;
            private TextView textViewName;

            public MyViewHolder(View view) {
                super(view);
                textViewId = view.findViewById(R.id.textView_no);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewName = view.findViewById(R.id.textView_name);

            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_scan_shipment, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewId.setText(String.valueOf(position + 1));

            holder.textViewName.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getPackedRatio());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onBackPressed() {
        newShipmentScannedArrayList = objDatabaseHelper.getScanShipmentDetails();
        if (newShipmentScannedArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.removePreference(NewShipmentActivity.this, "shipment_type");
                            objDatabaseHelper.updateShipmentDataOffline();
                            objDatabaseHelper.deleteScannedMCCCodeDissmiss();

                            Intent intent = new Intent(NewShipmentActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            NewShipmentActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
