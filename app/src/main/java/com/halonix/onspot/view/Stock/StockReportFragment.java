package com.halonix.onspot.view.Stock;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class StockReportFragment extends BaseFragment {

    @BindView(R.id.rv_report)
    RecyclerView rv_report;

    @BindView(R.id.tv_nostock)
    AppCompatTextView tv_nostock;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    ArrayList<NewShipmentDetailModel> arrLogDetailModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stock_report, container, false);
        ButterKnife.bind(this, view);

        DatabaseHelper objDatabaseHelper = DatabaseHelper.getInstance(getActivity());
        arrLogDetailModels = objDatabaseHelper.getPackedCartonDataForStock();

        rv_report.setHasFixedSize(true);
        rv_report.setItemAnimator(new DefaultItemAnimator());
        rv_report.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (arrLogDetailModels.size() > 0) {
            tv_nostock.setVisibility(View.GONE);
            rv_report.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            BatchListAdapter batchListAdapter = new BatchListAdapter(arrLogDetailModels);
            rv_report.setAdapter(batchListAdapter);
        } else {
            rv_report.setVisibility(View.GONE);
            linHeader.setVisibility(View.GONE);
            tv_nostock.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewId;
            private TextView textViewQuantity;
            private TextView textViewName;

            public MyViewHolder(View view) {
                super(view);
                textViewId = view.findViewById(R.id.textView_no);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewName = view.findViewById(R.id.textView_name);
            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_stock_report, parent, false);
            return new BatchListAdapter.MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(BatchListAdapter.MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewId.setText(String.valueOf(position + 1));
            holder.textViewName.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getPackedRatio());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.menu_stock_report));
    }
}
