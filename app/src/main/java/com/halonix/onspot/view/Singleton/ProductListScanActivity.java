package com.halonix.onspot.view.Singleton;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipment;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.NewShipment.NewShipmentDispatchActivity;
import com.halonix.onspot.view.Packaging.PackagingProductListActivity;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class ProductListScanActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;


    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnGo)
    Button btnGo;

    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private CaptureManager capture;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.button_stop_scanning)
    Button buttonStopScanning;

    @BindView(R.id.btnDone)
    Button btnDone;

    @BindView(R.id.btnFinish)
    Button btnFinish;

    @BindView(R.id.textInput)
    TextInputLayout textInput;

    private BeepManager beepManager = null;
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;

    private int packing_ratio = 0;

    ArrayList<String> qrcodeArrayList;
    private JSONArray QRjsonArray;
    private JSONArray BladejsonArray;
    private JSONArray MasterQRjsonArray;

    ArrayList<String> bladecodeArrayList;
    ArrayList<String> masterqrcodeArrayList;
    ArrayList<String> InvoicecodeArrayList;

    private ArrayList<String> tempScannedQrList = new ArrayList<>();
    private ArrayList<String> tempScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> tempScannedMasterQrList = new ArrayList<>();

    private String item_code;
    private boolean blade;
    private String tag;

    RetrofitClient retrofitClient;
    private String AgencyId;
    private String Invoice_number;
    private String Invoice_date;
    private String PO_number;
    private String PO_date;
    private String shipment_type;
    ConnectionDetectorActivity cd;
    private long mLastClickTime = 0;
    ArrayList<PackedCartonList> packedcartonListArrayList;
    private AlertDialog.Builder alert = null;
    private AlertDialog.Builder alert1 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_scan);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(ProductListScanActivity.this);
        objDatabaseHelper = DatabaseHelper.getInstance(ProductListScanActivity.this);
        retrofitClient = new RetrofitClient();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        if (getIntent().getExtras() != null) {

            tag = getIntent().getExtras().getString("tag");
            if (tag.equals("packing")) {
                item_code = Utils.getPreference(ProductListScanActivity.this, "item_code", "");
                blade = getIntent().getExtras().getBoolean("blade");
            } else {
                SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                AgencyId = sharedPreferences.getString("id", "");
                ArrayList<NewShipment> arrOfflineshipmentModels = objDatabaseHelper.getNotUploadedNewShipmentDetails();

                Invoice_number = arrOfflineshipmentModels.get(0).getInvoice_number();
                Invoice_date = arrOfflineshipmentModels.get(0).getInvoice_date();
                PO_number = arrOfflineshipmentModels.get(0).getPo_number();
                PO_date = arrOfflineshipmentModels.get(0).getPo_date();
                shipment_type = Utils.getPreference(ProductListScanActivity.this, "shipment_type", "");
            }
        }


        //   String inv_code = objDatabaseHelper.getScannedInvoiceQRList().get(0);

        Log.d(Constants.TAG, "onCreate: " + objDatabaseHelper.getScannedInvoiceQRList());

        if (tag.equals("packing")) {
            btnDone.setVisibility(View.VISIBLE);
            toolbar.setTitle(getResources().getString(R.string.pack_products));

            if (blade) {
                bladecodeArrayList = new ArrayList<>();
                if (objDatabaseHelper.getBladeCodeList(item_code).getJsonArray() != null) {
                    BladejsonArray = objDatabaseHelper.getBladeCodeList(item_code).getJsonArray();
                }

                for (int i = 0; i < BladejsonArray.length(); i++) {
                    try {
                        bladecodeArrayList.add(String.valueOf(BladejsonArray.get(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                tempScannedBladeQrList = objDatabaseHelper.getTempScannedBladeQRList(item_code);
                Log.d("suraj", "onCreate: " + bladecodeArrayList.toString());
                Log.d(Constants.TAG, "onCreate ScannedQrList: " + tempScannedBladeQrList.toString());
            }

            qrcodeArrayList = new ArrayList<>();
            masterqrcodeArrayList = new ArrayList<>();
            QRjsonArray = objDatabaseHelper.getQRCodeList(item_code).getJsonArray();
            MasterQRjsonArray = objDatabaseHelper.getMasterQRCodeList(item_code).getJsonArray();

            tempScannedQrList = objDatabaseHelper.getTempScannedQRList(item_code);
            Log.d(Constants.TAG, "onCreate tempScannedQrList: " + tempScannedQrList.toString());

            tempScannedMasterQrList = objDatabaseHelper.getTempScannedMasterQRList(item_code);
            Log.d(Constants.TAG, "onCreate tempScannedMasterQrList: " + tempScannedMasterQrList.toString());


            for (int i = 0; i < QRjsonArray.length(); i++) {
                try {
                    qrcodeArrayList.add(String.valueOf(QRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                try {
                    masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            Log.d(Constants.TAG, "onCreate qrcodeArrayList: " + qrcodeArrayList.toString());
            Log.d(Constants.TAG, "onCreate masterqrcodeArrayList: " + masterqrcodeArrayList.toString());

            packing_ratio = objDatabaseHelper.getPackingRatio(item_code);

        } else {
            buttonStopScanning.setText("Submit Invoice Scan");
            toolbar.setTitle("Scan Invoice Code");
            btnDone.setVisibility(View.GONE);
            btnGo.setVisibility(View.GONE);
            btnFinish.setVisibility(View.VISIBLE);
            InvoicecodeArrayList = new ArrayList<>();
            InvoicecodeArrayList = objDatabaseHelper.getInvoiceCodeList();

            Log.d(Constants.TAG, "onCreate InvoicecodeArrayList: " + InvoicecodeArrayList.toString());

        }

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().equals("")) {
                    if (isInvoiceCodeExistsInSystem(edScanCode.getText().toString().trim())) {
                       /* if (!objDatabaseHelper.getScannedInvoiceQRList().get(0).equals(edScanCode.getText().toString().trim())) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ProductListScanActivity.this);
                            alert.setTitle("Alert");
                            alert.setCancelable(false);
                            alert.setMessage("One Invoice Qr-code already scanned : " + objDatabaseHelper.getScannedInvoiceQRList().get(0));

                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
                                    edScanCode.setSelection(edScanCode.getText().toString().length());
                                }
                            });
                            alert.show();
                        } else */
                        if (!isInvoiceQrExists(edScanCode.getText().toString().trim())) {
                            objDatabaseHelper.insertScannedInvoiceCode(edScanCode.getText().toString().trim());
                            AgencyDispatchMapping();
                        } else {
                            AgencyDispatchMapping();
                        }

                    } else {


                        alert1 = new AlertDialog.Builder(ProductListScanActivity.this);
                        alert1.setTitle("Alert");
                        alert1.setCancelable(false);
                        alert1.setMessage("This Qr-code is invalid");
                        alert1.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                edScanCode.setText("");
                                edScanCode.requestFocus();
                                edScanCode.setCursorVisible(true);
                                edScanCode.setFocusable(true);
                                edScanCode.setFocusableInTouchMode(true);
                                if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                                    Log.d(Constants.TAG, "onClick getScannedInvoiceQRList: ");
                                    edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
                                    edScanCode.setSelection(edScanCode.getText().toString().length());
                                }
                                alert1 = null;

                            }
                        });
                        alert1.show();
                        //     showAlert1("Alert", "This Qr-code is invalid", true, "manual");
                    }

                } else {
                    Toast.makeText(ProductListScanActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }


            }
        });

        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });


        buttonStopScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (tag.equals("packing")) {
                    stopScanning(v);
                } else {
                    if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                        AgencyDispatchMapping();
                    } else {
                        showAlert("Alert", "Please scan invoice qr code.", true, "camera");
                    }

                }
            }
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().trim().equals("")) {

                    if (tag.equals("packing")) {
                        checkbarcode(edScanCode.getText().toString().trim(), "manual");
                    } else {
                        checkInvoiceCode(edScanCode.getText().toString().trim(), "manual");
                    }

                } else {
                    Toast.makeText(ProductListScanActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);


        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    capture.onPause();
                    return;
                } else {

                    if (tag.equals("packing")) {
                        String[] final_barcode;
                        if (result.getText().contains("=")) {
                            final_barcode = result.getText().split("=");
                            String barCode1 = final_barcode[1].replaceAll("\u0000", "");

                            checkbarcode(barCode1, "camera");
                        } else {
                            checkbarcode(result.getText(), "camera");
                        }

                    } else {

                        String[] final_barcode;
                        if (result.getText().contains("=")) {
                            final_barcode = result.getText().split("=");
                            String barCode1 = final_barcode[1].replaceAll("\u0000", "");

                            checkInvoiceCode(barCode1, "camera");
                        } else {
                            checkInvoiceCode(result.getText(), "camera");
                        }

                    }


                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //capture.onResume();
                    }
                }, 3000);
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });
    }

    //TODO Agency dispatch API Call here
    private void AgencyDispatchMapping() {
        if (cd.isConnectingToInternet()) {

            objDatabaseHelper.updateScannedOfllineShipment();
            objDatabaseHelper.updateScannedMasterCartonCode("1");
            objDatabaseHelper.updateScannedMCCCode("1");
            objDatabaseHelper.updateScanneInvoiceCode(edScanCode.getText().toString().trim());
            objDatabaseHelper.updateInvoiceCode(edScanCode.getText().toString().trim(), "1");

            ArrayList<String> SacnnedBarcodeList = objDatabaseHelper.getScanShipmentBarcode();
            String InvoiceCode = objDatabaseHelper.getScannedInvoiceQRListforUpload().get(0);

            JSONObject objNewShipmentDetails = new JSONObject();
            try {
                objNewShipmentDetails.put("AgencyId", AgencyId);
                objNewShipmentDetails.put("Invoice_number", Invoice_number);
                objNewShipmentDetails.put("Invoice_date", Invoice_date);
                objNewShipmentDetails.put("PO_number", PO_number);
                objNewShipmentDetails.put("PO_date", PO_date);
                objNewShipmentDetails.put("quantity", objDatabaseHelper.getTotalShipmentCount());
                objNewShipmentDetails.put("shipment_type", shipment_type);
                objNewShipmentDetails.put("MasterCartonCode", new JSONArray(SacnnedBarcodeList));
                objNewShipmentDetails.put("InvoiceCode", InvoiceCode);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(objNewShipmentDetails);

            Log.d(Constants.TAG, "AgencyDispatchMapping: " + jsonArray.toString());
            Utils.showprogressdialog(ProductListScanActivity.this, "Requesting for sending new shipment,please wait.");

            retrofitClient.getService().agency_dispatch_mapping(objDatabaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
                @Override
                public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {
                    Log.d(Constants.TAG, "onResponse: " + response.body().getSuccess());
                    if (response.body().getSuccess()) {
                    }
                    getpackedcarton();
                }

                @Override
                public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });

        } else {
            objDatabaseHelper.updateScannedOfllineShipment();
            objDatabaseHelper.updateScannedMasterCartonCode("1");
            objDatabaseHelper.updateScannedMCCCode("1");
            objDatabaseHelper.updateScanneInvoiceCode(edScanCode.getText().toString().trim());
            objDatabaseHelper.updateInvoiceCode(edScanCode.getText().toString().trim(), "1");
            Utils.removePreference(ProductListScanActivity.this, "shipment_type");

            Utils.showToast(ProductListScanActivity.this, "Send new shipment stored offline.");

            Intent intent = new Intent(ProductListScanActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            ProductListScanActivity.this.finish();
            overridePendingTransition(0, 0);
        }


    }


    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();
        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }


                    }

                }
                getinvoice_codes();

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {
        retrofitClient.getService().getInvoiceCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(Constants.TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    objDatabaseHelper.deleteInvoiceCodeList();
                    if (response.body().getList().size() == 0) {
                        Utils.removePreference(ProductListScanActivity.this, "shipment_type");
                        objDatabaseHelper.deleteOfflineNewShipment("all");
                        objDatabaseHelper.deleteScannedInvoiceCodeList("all");
                        objDatabaseHelper.deleteScannedMasterCodeList();

                        Intent intent = new Intent(ProductListScanActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        ProductListScanActivity.this.finish();
                        overridePendingTransition(0, 0);

                        Utils.showToast(ProductListScanActivity.this, "Shipment dispatched successfully");

                    } else {
                        Utils.removePreference(ProductListScanActivity.this, "shipment_type");
                        objDatabaseHelper.deleteOfflineNewShipment("all");
                        objDatabaseHelper.deleteScannedInvoiceCodeList("all");
                        objDatabaseHelper.deleteScannedMasterCodeList();
                        Utils.dismissprogressdialog();

                        for (int i = 0; i < response.body().getList().size(); i++) {
                            objDatabaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                        }
                        Intent intent = new Intent(ProductListScanActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        ProductListScanActivity.this.finish();
                        overridePendingTransition(0, 0);

                        Utils.showToast(ProductListScanActivity.this, "Shipment dispatched successfully");

                    }

                }
                Utils.dismissprogressdialog();


            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }


    private void checkInvoiceCode(String barcode, String tag) {
        if (isInvoiceCodeExistsInSystem(barcode)) {
            if (!isInvoiceQrExists(barcode)) {
                // NEW BARCODE DETECTED
                if (objDatabaseHelper.getScannedInvoiceQRList().size() == 1) {
                    showAlert1("Alert", "Max Invoice Qr-code limit reached.", false, tag);

                    if (relCamera.getVisibility() == View.VISIBLE) {
                        relCamera.setVisibility(View.GONE);
                    }
                    radio_scanner_Grp.setVisibility(View.GONE);
                    if (relOTG.getVisibility() == View.GONE || relOTG.getVisibility() == View.INVISIBLE) {
                        relOTG.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (!tag.equals("manual")) {
                        capture.onResume();
                        edScanCode.setText(barcode);
                        edScanCode.setSelection(edScanCode.getText().toString().trim().length());
                    } else {
                        clearEditScan();
                    }
                    objDatabaseHelper.insertScannedInvoiceCode(barcode);

                    if (relCamera.getVisibility() == View.VISIBLE) {
                        relCamera.setVisibility(View.GONE);
                    }
                    radio_scanner_Grp.setVisibility(View.GONE);
                    if (relOTG.getVisibility() == View.GONE || relOTG.getVisibility() == View.INVISIBLE) {
                        relOTG.setVisibility(View.VISIBLE);
                    }
                    edScanCode.setFocusable(false);
                    edScanCode.setFocusableInTouchMode(false);
                    edScanCode.setCursorVisible(false);
                }

            } else {
                // BARCODE ALREADY EXISTS
                showAlert1("Alert", "This Invoice Qr-code is already scanned.", false, tag);
                if (relCamera.getVisibility() == View.VISIBLE) {
                    relCamera.setVisibility(View.GONE);
                }
                radio_scanner_Grp.setVisibility(View.GONE);
                if (relOTG.getVisibility() == View.GONE || relOTG.getVisibility() == View.INVISIBLE) {
                    relOTG.setVisibility(View.VISIBLE);
                }
            }
        } else {
            showAlert1("Alert", "This Qr-code is invalid.", true, tag);
        }
    }

    private void showAlert1(String Title, String Message, boolean valid, String tag) {
        if (tag.equals("camera")) {
            capture.onPause();
        }

//
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog.setCancelable(false);
//        alertDialog.show();
//        LayoutInflater inflater = this.getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
//        alertDialog.getWindow().setContentView(dialogView);
//
//        TextView tvTilte = dialogView.findViewById(R.id.tvTilte);
//        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
//        Button button = dialogView.findViewById(R.id.btn_ok);
//
//        tvTilte.setText(Title);
//        tvMsg.setText(Message);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alertDialog.dismiss();
//                if (valid) {
//                    clearEditScan();
//                    if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
//                        edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
//                        edScanCode.setSelection(edScanCode.getText().toString().length());
//                    }
//                }
//                if (tag.equals("camera")) {
//                    capture.onResume();
//                }
//
//            }
//        });

        alert1 = new AlertDialog.Builder(ProductListScanActivity.this);
        alert1.setTitle(Title);
        alert1.setCancelable(false);
        alert1.setMessage(Message);
        alert1.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    Log.d(Constants.TAG, "onClick: " + valid);
                    clearEditScan();
                    if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                        edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
                        edScanCode.setSelection(edScanCode.getText().toString().length());
                    }
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }

                alert1 = null;
            }
        });
        alert1.show();
    }


    private void checkbarcode(String barcode, String tag) {
        if (isBarcodeMasterQrExistsInSystem(barcode) && tempScannedQrList.size() < packing_ratio && !blade) {
            showAlert("Alert", "Please scan Product Qr-code first before scan Master Qr-code.", true, tag);
        } else if (isBarcodeMasterQrExistsInSystem(barcode) && blade && tempScannedBladeQrList.size() < packing_ratio) {
            if (tempScannedQrList.size() < packing_ratio) {
                showAlert("Alert", "Please scan Product Qr-code first before scan Master Qr-code.", true, tag);
            } else {
                showAlert("Alert", "Please scan Blade Qr-code first before scan Master Qr-code.", true, tag);
            }
        } else {
            if (!blade) {
                if (isBarcodeExistsInSystem(barcode)) {
                    if (tempScannedMasterQrList.size() == 0) {
                        if (!isTempBarcodeExists(barcode, item_code)) {
                            if (tempScannedQrList.size() == packing_ratio) {
                                showAlert("Alert", "Product Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                            } else {
                                if (!isBarcodeExists(barcode, item_code)) {
                                    capture.onResume();
                                    tempScannedQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedQRList(barcode, item_code, 0);
                                    clearEditScan();

                                    if (tempScannedQrList.size() < packing_ratio) {
                                        Utils.showToast(ProductListScanActivity.this, "Now Scan Product Qr-code.");
                                        capture.onResume();

                                    } else if (tempScannedQrList.size() == packing_ratio) {
                                        showAlert("Alert", "Product Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                    }
                                } else {
                                    showAlert("Alert", "This Product Qr-code is already used.", true, tag);
                                }
                            }
                        } else {
                            showAlert("Alert", "This Product Qr-code is already scanned.", true, tag);
                        }
                    } else {
                        showAlert("Alert", "Product & Master Qr-code limit reached.", true, tag);
                    }

                } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                    if (!isMasterTempBarcodeExists(barcode, item_code)) {

                        if (isMasterBarcodeExists(barcode, item_code)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            if (tempScannedMasterQrList.size() == 1) {
                                showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                            } else {
                                capture.onResume();
                                tempScannedMasterQrList.add(barcode);
                                objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                clearEditScan();
                                gotoproductListpage();
                            }
                        }
                    } else {
                        if (isMasterBarcodeExists(barcode, item_code)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                        }
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true, tag);
                }
            } else if (blade) {
                Log.d(Constants.TAG, "checkbarcode blade: " + blade);
                if (tempScannedBladeQrList.size() < packing_ratio) {
                    if (isBarcodeExistsInSystem(barcode)) {
                        if (!isTempBarcodeExists(barcode, item_code)) {
                            if (tempScannedQrList.size() == packing_ratio) {
                                showAlert("Alert", "Product Qr-code limit reached,Now Scan Blade Qr-code.", true, tag);
                            } else {
                                if (!isBarcodeExists(barcode, item_code)) {
                                    capture.onResume();
                                    tempScannedQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedQRList(barcode, item_code, 0);
                                    clearEditScan();

                                    if (tempScannedQrList.size() < packing_ratio) {
                                        Utils.showToast(ProductListScanActivity.this, "Now Scan Product Qr-code.");
                                        capture.onResume();

                                    } else if (tempScannedQrList.size() == packing_ratio) {
                                        showAlert("Alert", "Product Qr-code limit reached,Now Scan Blade Qr-code.", true, tag);
                                    }
                                } else {
                                    showAlert("Alert", "This Product Qr-code is already used.", true, tag);
                                }
                            }
                        } else {
                            showAlert("Alert", "This Product Qr-code is already scanned.", true, tag);
                        }
                    } else if (isBarcodeBladeExistsInSystem(barcode)) {
                        if (tempScannedQrList.size() < packing_ratio) {
                            showAlert("Alert", "Please scan Product Qr-code first,before scan Blade Qr-code", true, tag);
                        } else {
                            if (!isTempBladeoBarcodeExists(barcode, item_code)) {
                                if (tempScannedBladeQrList.size() == packing_ratio) {
                                    showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master Qr-code.", true, tag);
                                } else {
                                    if (!isBladeoBarcodeExists(barcode, item_code)) {
                                        capture.onResume();
                                        tempScannedBladeQrList.add(barcode);
                                        objDatabaseHelper.insertTempScannedBladeQRList(barcode, item_code, 0);
                                        clearEditScan();

                                        if (tempScannedBladeQrList.size() < packing_ratio) {
                                            Utils.showToast(ProductListScanActivity.this, "Now Scan Blade Qr-code.");
                                            capture.onResume();

                                        } else if (tempScannedBladeQrList.size() == packing_ratio) {
                                            showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                        }
                                    } else {
                                        showAlert("Alert", "This Blade Qr-code is already used.", true, tag);
                                    }
                                }
                            } else {
                                showAlert("Alert", "This Blade Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                        if (!isMasterTempBarcodeExists(barcode, item_code)) {

                            if (isMasterBarcodeExists(barcode, item_code)) {
                                showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                            } else {
                                if (tempScannedMasterQrList.size() == 1) {
                                    showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                                } else {
                                    capture.onResume();
                                    tempScannedMasterQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                    clearEditScan();

                                    gotoproductListpage();
                                }
                            }
                        } else {
                            if (isMasterBarcodeExists(barcode, item_code)) {
                                showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                            } else {
                                showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else {
                        showAlert("Alert", "This Qr-code is invalid.", true, tag);
                    }


                } else if (isBarcodeBladeExistsInSystem(barcode)) {
                    if (tempScannedMasterQrList.size() == 0) {
                        if (tempScannedQrList.size() < packing_ratio) {
                            showAlert("Alert", "Please scan Product Qr-code first,before scan Blade Qr-code", true, tag);
                        } else {
                            if (!isTempBladeoBarcodeExists(barcode, item_code)) {
                                if (tempScannedBladeQrList.size() == packing_ratio) {
                                    showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master Qr-code.", true, tag);
                                } else {
                                    if (!isBladeoBarcodeExists(barcode, item_code)) {
                                        capture.onResume();
                                        tempScannedBladeQrList.add(barcode);
                                        objDatabaseHelper.insertTempScannedBladeQRList(barcode, item_code, 0);
                                        clearEditScan();

                                        if (tempScannedBladeQrList.size() < packing_ratio) {
                                            Utils.showToast(ProductListScanActivity.this, "Now Scan Blade Qr-code.");
                                            capture.onResume();

                                        } else if (tempScannedBladeQrList.size() == packing_ratio) {
                                            showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                        }
                                    } else {
                                        showAlert("Alert", "This Blade Qr-code is already used.", true, tag);
                                    }
                                }
                            } else {
                                showAlert("Alert", "This Blade Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else {
                        showAlert("Alert", "Product,Blade & Master Qr-code limit reached.", true, tag);
                    }

                } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                    if (!isMasterTempBarcodeExists(barcode, item_code)) {

                        if (isMasterBarcodeExists(barcode, item_code)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            if (tempScannedMasterQrList.size() == 1) {
                                showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                            } else {
                                capture.onResume();
                                tempScannedMasterQrList.add(barcode);
                                objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                clearEditScan();

                                gotoproductListpage();
                            }
                        }
                    } else {
                        if (isMasterBarcodeExists(barcode, item_code)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                        }
                    }
                } else {
                    if (!isBarcodeExistsInSystem(barcode) && !isBarcodeBladeExistsInSystem(barcode)
                            && !isBarcodeMasterQrExistsInSystem(barcode)) {
                        showAlert("Alert", "This Qr-code is invalid.", true, tag);
                    } else {
                        if (tempScannedMasterQrList.size() == 1) {
                            showAlert("Alert", "Product,Blade & Master Qr-code limit reached.", true, tag);
                        } else if (isBarcodeExistsInSystem(barcode) && tempScannedQrList.size() == packing_ratio && tempScannedMasterQrList.size() == 0) {
                            showAlert("Alert", "Product & Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                        }
                    }


                }
            } else {
                showAlert("Alert", "This Qr-code is invalid.", true, tag);
            }

        }
    }

    private void gotoproductListpage() {
        Intent intent = new Intent(ProductListScanActivity.this, PackagingProductListActivity.class);
        startActivity(intent);
        ProductListScanActivity.this.finish();
        overridePendingTransition(0, 0);
    }

    private void showAlert(String Title, String Message, boolean valid, String tag) {
        if (tag.equals("camera")) {
            capture.onPause();
        }

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        alertDialog.setCancelable(false);
//        alertDialog.show();
//        LayoutInflater inflater = this.getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
//        alertDialog.getWindow().setContentView(dialogView);
//
//        TextView tvTilte = dialogView.findViewById(R.id.tvTilte);
//        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
//        Button button = dialogView.findViewById(R.id.btn_ok);
//
//        tvTilte.setText(Title);
//        tvMsg.setText(Message);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alertDialog.dismiss();
//                if (valid) {
//                    clearEditScan();
//                }
//                if (tag.equals("camera")) {
//                    capture.onResume();
//                }
//
//            }
//        });


        alert = new AlertDialog.Builder(ProductListScanActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert = null;
            }
        });
        alert.show();
    }

    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }

    private boolean isBarcodeExistsInSystem(String strBarcode) {
        return qrcodeArrayList.contains(strBarcode);
    }

    private boolean isBarcodeBladeExistsInSystem(String strBarcode) {
        return bladecodeArrayList.contains(strBarcode);
    }

    private boolean isBarcodeMasterQrExistsInSystem(String strBarcode) {
        return masterqrcodeArrayList.contains(strBarcode);
    }

    private boolean isInvoiceCodeExistsInSystem(String strBarcode) {
        return InvoicecodeArrayList.contains(strBarcode);
    }


    private boolean isTempBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedQRList(barcode, item_code);
    }


    private boolean isBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getScannedQRList(barcode, item_code);
    }

    private boolean isTempBladeoBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedBladeQRList(barcode, item_code);
    }

    private boolean isBladeoBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getScannedBladeQRList(barcode, item_code);
    }

    private boolean isMasterTempBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedMasterQRList(barcode, item_code);
    }

    private boolean isMasterBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getScannedMasterQRList(barcode, item_code);
    }

    private boolean isInvoiceQrExists(String barcode) {
        return objDatabaseHelper.getScannedInvoiceQRList(barcode);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null && alert1 == null) {
            capture.onResume();
        }
        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            if (tag.equals("packing")) {
                focusable(false);
            }
            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);
        } else {

            if (tag.equals("packing")) {
                focusable(false);
            }


            if (tag.equals("packing")) {
//                btnGo.setEnabled(true);

                relOTG.setVisibility(View.GONE);
                radio_otg.setChecked(false);

                relCamera.setVisibility(View.VISIBLE);
                radio_camera.setChecked(true);
                radio_camera.setEnabled(true);

            } else {
                if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                    relCamera.setVisibility(View.GONE);
                    relOTG.setVisibility(View.VISIBLE);
                    radio_scanner_Grp.setVisibility(View.GONE);
                    edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
                    edScanCode.setFocusable(false);
                    edScanCode.setFocusableInTouchMode(false);
                    edScanCode.setCursorVisible(false);

                } else {

                    relOTG.setVisibility(View.GONE);
                    radio_otg.setChecked(false);

                    relCamera.setVisibility(View.VISIBLE);
                    radio_camera.setChecked(true);
                    radio_camera.setEnabled(true);
                }
            }
        }
//
//        if (btnGo.isEnabled())
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));
//        else
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));
        if (tag.equals("packing")) {
            textInput.setHint("QR Code");
        } else {
            textInput.setHint("Invoice QR Code");
        }
    }

    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            if (tag.equals("packing")) {
                focusable(false);
            }

//            btnGo.setEnabled(false);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(ProductListScanActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            if (tag.equals("packing")) {
                focusable(false);
            }

//            btnGo.setEnabled(true);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);
            capture.onResume();

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);


            hideKeyboard(ProductListScanActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            if (tag.equals("packing")) {
                focusable(false);
            }
            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {

            if (tag.equals("packing")) {
                onBackPressed();
            } else {
                if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                    capture.onPause();
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductListScanActivity.this);
                    if (builder != null)
                        builder.setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Please submit the data to move ahead.")
                                .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        objDatabaseHelper.updateInvoiceCode(edScanCode.getText().toString().trim(), "0");
                                        objDatabaseHelper.deleteScannedInvoiceCodeList("");
                                        Intent intent = new Intent(ProductListScanActivity.this, NewShipmentDispatchActivity.class);
                                        startActivity(intent);
                                        ProductListScanActivity.this.finish();
                                        overridePendingTransition(0, 0);
                                    }
                                })
                                .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        capture.onResume();
                                    }
                                })
                                .show();

                } else {
                    Intent intent = new Intent(ProductListScanActivity.this, NewShipmentDispatchActivity.class);
                    startActivity(intent);
                    ProductListScanActivity.this.finish();
                    overridePendingTransition(0, 0);
                }


            }


        }
        return false;
    }


    private void barcodeLookup(String barCode) {

        String[] final_barcode;
        if (barCode.contains("=")) {
            final_barcode = barCode.split("=");
            barCode = final_barcode[1].replaceAll("\u0000", "");
            edScanCode.setText(barCode);

            if (tag.equals("packing")) {
                checkbarcode(barCode, "OTG");
            } else {
                checkInvoiceCode(barCode, "OTG");
            }

        } else {
            barCode = barCode.replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            if (tag.equals("packing")) {
                checkbarcode(barCode, "OTG");
            } else {
                checkInvoiceCode(barCode, "OTG");
            }
        }
    }
    //OTG Code End

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }


    /**
     * Check if the device's camera has a Flashlight.
     *
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }

    @Override
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }


    public void stopScanning(View view) {
        onBackPressed();
    }


    public void onBackPressed() {
        AlertDialog myDialog = null;
        if (tag.equals("packing")) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                if (myDialog != null && myDialog.isShowing()) return;
                capture.onPause();
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductListScanActivity.this);
                builder.setTitle("Alert");
                builder.setMessage("Please submit the data to move ahead.");
                builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        objDatabaseHelper.updateInvoiceCode(edScanCode.getText().toString().trim(), "0");
                        objDatabaseHelper.deleteScannedInvoiceCodeList("");
                        Intent intent = new Intent(ProductListScanActivity.this, NewShipmentDispatchActivity.class);
                        startActivity(intent);
                        ProductListScanActivity.this.finish();
                        overridePendingTransition(0, 0);
                    }
                });
                builder.setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        capture.onResume();
                    }
                });
                builder.setCancelable(false);
                myDialog = builder.create();
                myDialog.show();
            } else {
                Intent intent = new Intent(ProductListScanActivity.this, NewShipmentDispatchActivity.class);
                startActivity(intent);
                ProductListScanActivity.this.finish();
                overridePendingTransition(0, 0);
            }

        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
