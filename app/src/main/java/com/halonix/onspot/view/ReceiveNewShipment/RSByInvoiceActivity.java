package com.halonix.onspot.view.ReceiveNewShipment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceCodeList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.get_receive_shipmentList;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.model.SystemBarcodeModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class RSByInvoiceActivity extends AppCompatActivity implements View.OnClickListener {


    private Activity activity = null;

    @BindView(R.id.scanShipmentList)
    RecyclerView scanShipmentList;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    @BindView(R.id.ly_scan_system)
    LinearLayout ly_scan_system;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private DatabaseHelper objDatabaseHelper = null;
    ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
    ArrayList<String> newShipmentScannedArrayList = new ArrayList<>();

    private ConnectionDetectorActivity cd;

    BatchListAdapter scanShipmentCodeAdapter;
    private long mLastClickTime = 0;

    private Location loca;
    private String strLatitude = "", strLongitude = "", strAccuracy = "";

    RetrofitClient retrofitClient;

    //Receive New Shipment
    ArrayList<String> InvoiceCodeListArrayList;
    ArrayList<get_receive_shipmentList> receiveShipmentListArrayList;
    ArrayList<ReceiveShipmentInvoiceList> InvoiceCodeProductDataArrayList;
    int k = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_invoice_shipment);
        ButterKnife.bind(this);
        retrofitClient = new RetrofitClient();

        activity = RSByInvoiceActivity.this;

        objDatabaseHelper = DatabaseHelper.getInstance(activity);
        cd = new ConnectionDetectorActivity(activity);

        scanShipmentList.setHasFixedSize(true);
        scanShipmentList.setItemAnimator(new DefaultItemAnimator());
        scanShipmentList.setLayoutManager(new LinearLayoutManager(RSByInvoiceActivity.this));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            onBackPressed();
            overridePendingTransition(0, 0);
        });
        toolbar.setTitle("Scan Invoice");

        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_material_in).setOnClickListener(this);
        findViewById(R.id.button_check_cartons).setOnClickListener(this);
    }


    private void start_scanning() {
        Intent intent = new Intent(RSByInvoiceActivity.this, RSByInvoiceScannerActivity.class);
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }


    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
                start_scanning();
                break;

            case R.id.button_material_in:
                if (newShipmentScannedArrayList.size() > 0) {
                    new AlertDialog.Builder(activity)
                            .setTitle("Alert")
                            .setMessage("Are you sure you want to proceed further ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (cd.isConnectingToInternet()) {
                                        LocationControl locationControlTask = new LocationControl();
                                        locationControlTask.execute();
                                    } else {
                                        Utils.showToast(RSByInvoiceActivity.this, "Shipment (Receive by Invoice) stored offline.");
                                        Intent intent = new Intent(RSByInvoiceActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        RSByInvoiceActivity.this.finish();
                                        overridePendingTransition(0, 0);
                                    }
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                } else {
                    Utils.showToast(activity, "Please scan at least one QR code");
                }

                break;
            case R.id.button_check_cartons:
                if (objDatabaseHelper.getIS_MasterCartonCount() > 0) {
                    Intent intent = new Intent(RSByInvoiceActivity.this, RSByInvoiceCheckCartonsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    RSByInvoiceActivity.this.finish();
                    overridePendingTransition(0, 0);
                } else {
                    Utils.showToast(RSByInvoiceActivity.this, "No master carton codes found against this invoice.");
                }
                break;
        }
    }

    private class LocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(RSByInvoiceActivity.this, "Receiving shipment by invoice,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(RSByInvoiceActivity.this);
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServer();
        }
    }


    private void submitDetailsToServer() {

        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(objDatabaseHelper.getRSScannedInvoiceQRList()));
            if (jsonArray.length() == 1) {
                res = jsonArray.get(0).toString();
            } else {
                res = jsonArray.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(Constants.TAG, "submitDetailsToServer: " + res);
        Log.d(Constants.TAG, "submitDetailsToServer: " + objDatabaseHelper.getRSScannedInvoiceQRList());
        Log.d(Constants.TAG, "submitDetailsToServer: " + strLatitude);
        Log.d(Constants.TAG, "submitDetailsToServer: " + strLongitude);
        Log.d(Constants.TAG, "submitDetailsToServer: " + strAccuracy);


        retrofitClient.getService().getReceiveNewShipment(objDatabaseHelper.getAuthToken(), res, "Invoice", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse: " + response.body().getSuccess());
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteAllReceiveShipmentScannedCodeData();

                }
                getpackedcarton();
            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    ArrayList<PackedCartonList> packedcartonListArrayList;

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {

                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }
                    }

                }
                String USER_TYPEs = Utils.getPreference(RSByInvoiceActivity.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else {
                    getinvoice_codes();
                }


            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {

        retrofitClient.getService().getInvoiceCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(Constants.TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        objDatabaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                    getReceiveNewShipmentList();
                }

            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Receive New Shipment API Call here
    private void getReceiveNewShipmentList() {
        k = 0;
        InvoiceCodeListArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveNewShipmentCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<get_receive_shipment_respo>() {
            @Override
            public void onResponse(Call<get_receive_shipment_respo> call, Response<get_receive_shipment_respo> response) {

                Log.d(Constants.TAG, "onResponse getReceiveNewShipmentList: " + response.toString());
                if (response.body().getSuccess()) {

                    InvoiceCodeListArrayList = response.body().getInvoiceCodes();
                    receiveShipmentListArrayList = response.body().getList();
                    objDatabaseHelper.deleteAllReceiveShipmentCode();
                    objDatabaseHelper.deleteServerBarcodeDataTableData();
                    objDatabaseHelper.deleteAllReceiveShipmentCodeData();
                    objDatabaseHelper.deleteAllReceiveShipmentISMasterCartons();

                    if (receiveShipmentListArrayList.size() > 0) {
                        for (int i = 0; i < receiveShipmentListArrayList.size(); i++) {
                            objDatabaseHelper.insertReceiveNewShipment(new NewShipmentDetailModel(String.valueOf(receiveShipmentListArrayList.get(i).getId()),
                                    String.valueOf(receiveShipmentListArrayList.get(i).getPackingRatio()), receiveShipmentListArrayList.get(i).getCode(),
                                    receiveShipmentListArrayList.get(i).getProductName(), receiveShipmentListArrayList.get(i).getSweep(),
                                    receiveShipmentListArrayList.get(i).getColor(), "1"));
                        }

                        JSONArray jsonArray1 = objDatabaseHelper.getReceiveShipmentBarcode1().getJsonArray();

                        objDatabaseHelper.insertMasterCartoncode(new SystemBarcodeModel(jsonArray1));

                    }


                    if (InvoiceCodeListArrayList.size() > 0) {

                        objDatabaseHelper.inserRSInvoicecode(new SystemBarcodeModel(new JSONArray(InvoiceCodeListArrayList)));


                        for (int i = 0; i < InvoiceCodeListArrayList.size(); i++) {
                            ReceiveShipmentByInvoiceData(InvoiceCodeListArrayList.get(i));
                        }

                    } else {
                        Utils.dismissprogressdialog();
                        popUpDialogSuccess("The items on the Invoice are checked in. They are part of the stock now.");
                    }


                }
            }

            @Override
            public void onFailure(Call<get_receive_shipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Receive New Shipment Invoice API Call here
    private void ReceiveShipmentByInvoiceData(String invoice_code) {
        Log.d(Constants.TAG, "ReceiveShipmentByInvoiceData i: " + invoice_code);
        InvoiceCodeProductDataArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveShipmentByInvoice(objDatabaseHelper.getAuthToken(), invoice_code).enqueue(new Callback<ReceiveShipmentInvoice_respo>() {
            @Override
            public void onResponse(Call<ReceiveShipmentInvoice_respo> call, Response<ReceiveShipmentInvoice_respo> response) {

                Log.d(Constants.TAG, "onResponse ReceiveShipmentByInvoiceData: " + response.toString());
                if (response.body().getSuccess()) {
                    k++;
                    InvoiceCodeProductDataArrayList = response.body().getList();

                    if (InvoiceCodeProductDataArrayList.size() > 0) {
                        for (int i = 0; i < InvoiceCodeProductDataArrayList.size(); i++) {
                            if (InvoiceCodeProductDataArrayList.get(i).getQuantity() == null) {
                            } else {
                                objDatabaseHelper.inserRSInvoicecodeData(new NewShipmentDetailModel(invoice_code, InvoiceCodeProductDataArrayList.get(i).getModel(),
                                        "", "",
                                        String.valueOf(InvoiceCodeProductDataArrayList.get(i).getQuantity()), ""));
                                ArrayList<ReceiveShipmentInvoiceCodeList> codeArrayList = InvoiceCodeProductDataArrayList.get(i).getCodes();

                                for (int j = 0; j < codeArrayList.size(); j++) {
                                    objDatabaseHelper.insertIS_MasterCartonCodes(InvoiceCodeProductDataArrayList.get(i).getModel(), invoice_code, codeArrayList.get(j).getCode(), String.valueOf(codeArrayList.get(j).getPackingRatio()));
                                }
                            }
                        }
                    }


                    if (k == InvoiceCodeListArrayList.size()) {
                        Utils.dismissprogressdialog();
                        popUpDialogSuccess("The items on the Invoice are checked in. They are part of the stock now.");
                    }

                }
            }

            @Override
            public void onFailure(Call<ReceiveShipmentInvoice_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    private void popUpDialogSuccess(String result1) {
        // TODO Auto-generated method stub
        AlertDialog.Builder builder = new AlertDialog.Builder(RSByInvoiceActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = RSByInvoiceActivity.this.getLayoutInflater();

        View content = inflater.inflate(R.layout.otp_verification_dialog, null);

        TextView tv = content.findViewById(R.id.otpAlertMessage1);
        tv.setText(result1);

        ((Button) content.findViewById(R.id.btnOTPAlertOK)).setText(getResources().getString(R.string.action_text_ok));

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        content.findViewById(R.id.btnOTPAlertOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        alertDialog.dismiss();
                        Intent intent = new Intent(RSByInvoiceActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RSByInvoiceActivity.this.finish();

                    }
                });
    }


    @Override
    protected void onResume() {

        newShipmentScannedArrayList = new ArrayList<>();
        if (objDatabaseHelper.getRSScannedInvoiceQRList() != null) {
            newShipmentScannedArrayList = objDatabaseHelper.getRSScannedInvoiceQRList();
        }

        newShipmentDetailModelArrayList = new ArrayList<>();
        newShipmentDetailModelArrayList = objDatabaseHelper.getRSSingleInvoiceQR();
        Log.d(Constants.TAG, "onResume: " + newShipmentDetailModelArrayList.size());
        Log.d(Constants.TAG, "onResume newShipmentScannedArrayList: " + newShipmentScannedArrayList.size());

        if (newShipmentDetailModelArrayList.size() > 0) {
            findViewById(R.id.ly_scan_system).setVisibility(objDatabaseHelper.getRSScannedInvoiceQRList().size() > 0 ? View.VISIBLE : View.GONE);
            findViewById(R.id.button_start_scanning).setVisibility(objDatabaseHelper.getRSScannedInvoiceQRList().size() > 0 ? View.GONE : View.VISIBLE);
            findViewById(R.id.scanShipmentList).setVisibility(objDatabaseHelper.getRSScannedInvoiceQRList().size() > 0 ? View.VISIBLE : View.GONE);
        }


        if (newShipmentDetailModelArrayList.size() > 0) {
            scanShipmentList.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            scanShipmentCodeAdapter = new BatchListAdapter(newShipmentDetailModelArrayList);
            scanShipmentList.setAdapter(scanShipmentCodeAdapter);
            ly_scan_system.setVisibility(View.VISIBLE);
            findViewById(R.id.button_start_scanning).setVisibility(View.GONE);
        } else {
            linHeader.setVisibility(View.GONE);
        }
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    if (objDatabaseHelper.getRSScannedInvoiceQRList() != null) {
                        newShipmentScannedArrayList = new ArrayList<>();
                        if (objDatabaseHelper.getRSScannedInvoiceQRList() != null) {
                            newShipmentScannedArrayList = objDatabaseHelper.getRSScannedInvoiceQRList();
                        }

                        newShipmentDetailModelArrayList = new ArrayList<>();
                        newShipmentDetailModelArrayList = objDatabaseHelper.getRSSingleInvoiceQR();
                    }
                    if (newShipmentDetailModelArrayList.size() > 0)
                        linHeader.setVisibility(View.VISIBLE);
                    else
                        linHeader.setVisibility(View.GONE);
                    scanShipmentCodeAdapter = new BatchListAdapter(newShipmentDetailModelArrayList);
                    scanShipmentList.setAdapter(scanShipmentCodeAdapter);
                    onResume();
                    break;
            }
        }
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewModel;
            private TextView textViewQuantity;

            public MyViewHolder(View view) {
                super(view);
                textViewModel = view.findViewById(R.id.textView_model);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_invoicescan_shipment, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewModel.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getQty());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onBackPressed() {

        if (objDatabaseHelper.getRSScannedInvoiceQRList().size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            objDatabaseHelper.deleteAllReceiveShipmentScannedCodeData();
                            Intent intent = new Intent(RSByInvoiceActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            RSByInvoiceActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
