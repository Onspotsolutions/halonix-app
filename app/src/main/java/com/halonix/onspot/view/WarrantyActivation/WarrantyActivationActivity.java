package com.halonix.onspot.view.WarrantyActivation;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class WarrantyActivationActivity extends AppCompatActivity {

    @BindView(R.id.InputBillNo)
    TextInputLayout InputBillNo;

    @BindView(R.id.InputSaleDate)
    TextInputLayout InputSaleDate;

    @BindView(R.id.edSaleDate)
    EditText edSaleDate;

    @BindView(R.id.edBillNo)
    EditText edBillNo;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvModel)
    AppCompatTextView tvModel;

    @BindView(R.id.tvSweep)
    AppCompatTextView tvSweep;

    @BindView(R.id.tvVoltage)
    AppCompatTextView tvVoltage;

    @BindView(R.id.linVoltage)
    LinearLayout linVoltage;

    @BindView(R.id.linWithBill)
    LinearLayout linWithBill;

    @BindView(R.id.btnFinish)
    AppCompatButton btnFinish;

    String myFormat = "dd-MM-yyyy";
    private long mLastClickTime = 0;
    private String tag = "";
    private String model;
    private String sweep;
    private String voltage;
    ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;
    private String qr_code;
    DatabaseHelper databaseHelper;
    private AlertDialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warranty_activation);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(WarrantyActivationActivity.this);
        databaseHelper = new DatabaseHelper(WarrantyActivationActivity.this);
        retrofitClient = new RetrofitClient();


        if (getIntent().getExtras() != null) {
            tag = getIntent().getStringExtra("tag");
            model = getIntent().getStringExtra("model");
            sweep = getIntent().getStringExtra("sweep");
            voltage = getIntent().getStringExtra("voltage");
            qr_code = getIntent().getStringExtra("qr_code");

            tvModel.setText(model);
            tvSweep.setText(sweep);
            if (voltage == null) {
                linVoltage.setVisibility(View.GONE);
            }
            tvVoltage.setText(voltage);
        }

        if (tag.equals("with_bill")) {
            linWithBill.setVisibility(View.VISIBLE);
            toolbar.setTitle(R.string.title_warranty_activation_withbill);
        } else {
            toolbar.setTitle(R.string.title_warranty_activation);
        }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        edSaleDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateSaleDate()) {
                    return;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });

        edBillNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateBillNo()) {
                    return;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });


        edSaleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                final Calendar c = Calendar.getInstance();

                int y = c.get(Calendar.YEAR);
                int m = c.get(Calendar.MONTH);
                int d = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dp = new DatePickerDialog(WarrantyActivationActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                String date = new SimpleDateFormat(myFormat).format(c.getTime());
                                edSaleDate.setText(date);

                            }

                        }, y, m, d);
                c.add(Calendar.DAY_OF_MONTH, -30);
                dp.getDatePicker().setMinDate(c.getTimeInMillis());
                dp.getDatePicker().setMaxDate(System.currentTimeMillis());
                dp.show();
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    activateWarranty(tag);
                } else {
                    showNetworkFailDialog();
                }
            }
        });
    }


    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(WarrantyActivationActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    private void activateWarranty(String tag) {

        if (tag.equals("with_bill")) {
            if (!validateBillNo()) {
                return;
            }

            if (!validateSaleDate()) {
                return;
            }

            Log.d(Constants.TAG, "activateWarranty with_bill: " + edSaleDate.getText().toString().trim());
            Log.d(Constants.TAG, "activateWarranty with_bill: " + qr_code);
            Utils.showprogressdialog(WarrantyActivationActivity.this, "Activating warranty with bill,please wait.");

            retrofitClient.getService().activateWarrantyWithBill(databaseHelper.getAuthToken(), qr_code, edSaleDate.getText().toString().trim(), edBillNo.getText().toString().trim()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
                @Override
                public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                    if (response.body().getSuccess()) {
                        getpackedcarton();
                    } else {
                        Utils.dismissprogressdialog();
                        Utils.showToast(WarrantyActivationActivity.this, "Failed to activate warranty with bill.");

                    }
                }

                @Override
                public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });
        } else {
            Log.d(Constants.TAG, "activateWarranty: " + qr_code);
            Utils.showprogressdialog(WarrantyActivationActivity.this, "Activating warranty,please wait.");

            retrofitClient.getService().activateWarranty(databaseHelper.getAuthToken(), qr_code).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
                @Override
                public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                    if (response.body().getSuccess()) {
                        getpackedcarton();
                    } else {
                        Utils.dismissprogressdialog();
                        Utils.showToast(WarrantyActivationActivity.this, "Failed to activate warranty.");
                    }
                }

                @Override
                public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });
        }

    }

    ArrayList<PackedCartonList> packedcartonListArrayList;

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(databaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {

                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {

                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }
                    }

                }

                Utils.dismissprogressdialog();
                Intent intent = new Intent(WarrantyActivationActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                WarrantyActivationActivity.this.finish();
                overridePendingTransition(0, 0);
                Utils.showToast(WarrantyActivationActivity.this, "Warranty activated successfully.");

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    private boolean validateSaleDate() {
        String po_date = edSaleDate.getText().toString().trim();

        if (po_date.isEmpty()) {
            InputSaleDate.setError(getString(R.string.hint_select_sale_date));
            edSaleDate.requestFocus();
            return false;
        } else {
            InputSaleDate.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateBillNo() {
        String po_date = edBillNo.getText().toString().trim();

        if (po_date.isEmpty()) {
            InputBillNo.setError(getString(R.string.hint_enter_bill_no));
            edBillNo.requestFocus();
            return false;
        } else {
            InputBillNo.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        if (Utils.getPreference(WarrantyActivationActivity.this, "sale_qr", "").length() > 0) {
            new AlertDialog.Builder(WarrantyActivationActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.removePreference(WarrantyActivationActivity.this, "sale_qr");
                            Intent intent = new Intent(WarrantyActivationActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            WarrantyActivationActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }


}
