package com.halonix.onspot.view.SaleReturnSend;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class SaleReturnMasterCartonScannerActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener {

    private Activity activity = null;
    private CaptureManager capture;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;

    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnGo)
    Button btnGo;

    @BindView(R.id.btnFinish)
    Button btnFinish;

    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private BeepManager beepManager = null;
    private ArrayList<String> BarcodeList = new ArrayList<>();
    private ArrayList<String> BarcodeList1 = new ArrayList<>();
    private ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList = new ArrayList<>();
    private ArrayList<String> RemoveBarcodeList = new ArrayList<>();
    private int numberOfCartons = 0;
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;
    private String token;
    private AlertDialog.Builder alert = null;
    private long mLastClickTime = 0;
    private AlertDialog.Builder alert1 = null;
    ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;

    @BindView(R.id.relBody)
    RelativeLayout relBody;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_return_scan);
        ButterKnife.bind(this);

        cd = new ConnectionDetectorActivity(SaleReturnMasterCartonScannerActivity.this);
        retrofitClient = new RetrofitClient();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        toolbar_title.setVisibility(View.VISIBLE);
        toolbar_title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        toolbar_title.setSingleLine(true);
        toolbar_title.setMarqueeRepeatLimit(-1);
        toolbar_title.setSelected(true);
        toolbar_title.setText("Scan Master Cartons/Individual Products");

        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        if (getIntent().getExtras() != null) {
            token = getIntent().getExtras().getString("token");
        }

        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        activity = SaleReturnMasterCartonScannerActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);

        newShipmentDetailModelArrayList = objDatabaseHelper.getShipmentSystemBarcode();

        for (int index = 0; index < newShipmentDetailModelArrayList.size(); index++) {
            BarcodeList.add(String.valueOf(newShipmentDetailModelArrayList.get(index).getShipmentCode()));
        }


        if (objDatabaseHelper.getScannedMCCQRList().size() > 0) {
            for (int i = 0; i < objDatabaseHelper.getScannedMCCQRList().size(); i++) {
                BarcodeList1.add(String.valueOf(objDatabaseHelper.getScannedMCCQRList().get(i)));
            }
        }

        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedForceCodeList("0", "unpack");
        BarcodeList.addAll(stringArrayList);

        Log.d(Constants.TAG, "onCreate BarcodeList: " + BarcodeList.toString());
        Log.d(Constants.TAG, "onCreate BarcodeList1: " + BarcodeList1.toString());

        ArrayList<String> union = new ArrayList<>(BarcodeList);
        union.addAll(BarcodeList1);
        ArrayList<String> intersection = new ArrayList<>(BarcodeList);
        intersection.retainAll(BarcodeList1);
        union.removeAll(intersection);

        BarcodeList = new ArrayList<>();
        BarcodeList.addAll(union);

        Log.d(Constants.TAG, "onCreate BarcodeList: " + BarcodeList.toString());


        numberOfCartons = newShipmentDetailModelArrayList.size();

        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                //capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    //capture.onPause();
                    return;
                } else {
                    if (token.equals("add")) {

                        String[] final_barcode;
                        if (result.getText().contains("=")) {
                            final_barcode = result.getText().split("=");
                            String barCode1 = final_barcode[1].replaceAll("\u0000", "");
                            checkBarcode(barCode1, "camera");
                        } else {
                            checkBarcode(result.getText(), "camera");
                        }
                    } else {

                        String[] final_barcode;
                        if (result.getText().contains("=")) {
                            final_barcode = result.getText().split("=");
                            String barCode1 = final_barcode[1].replaceAll("\u0000", "");
                            checkBarcode1(barCode1, "camera");
                        } else {
                            checkBarcode1(result.getText(), "camera");
                        }

                    }

                }

            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().equals("")) {

                    if (token.equals("add")) {
                        checkBarcode(edScanCode.getText().toString().trim(), "manual");
                    } else {
                        checkBarcode1(edScanCode.getText().toString().trim(), "manual");
                    }

                } else {
                    Toast.makeText(SaleReturnMasterCartonScannerActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopScanning(v);
            }
        });
    }


    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }

    private ArrayList<String> SacnnedBarcodeList = new ArrayList<String>();

    private void checkBarcode(String barcode, String tag) {
        if (isBarcodeExistsInSystem(barcode)) {
            ArrayList<NewShipmentDetailModel> newShipmentDetailModelArrayList2 = objDatabaseHelper.getShipmentSystemBarcode(barcode);
            int sender = Utils.getPreferenceInt(SaleReturnMasterCartonScannerActivity.this, "sender_id", 0);

            Log.d(Constants.TAG, "checkBarcode sender: " + sender);
            Log.d(Constants.TAG, "checkBarcode getProductName: " + newShipmentDetailModelArrayList2.get(0).getProductName());
            Log.d(Constants.TAG, "checkBarcode getPackedRatio: " + newShipmentDetailModelArrayList2.get(0).getPackedRatio());
            Log.d(Constants.TAG, "checkBarcode getSender_id: " + newShipmentDetailModelArrayList2.get(0).getSender_id());

            if (!isBarcodeExists(barcode)) {
                if (SacnnedBarcodeList.size() <= numberOfCartons) {
                    SacnnedBarcodeList.add(barcode);
                    int sender_Id = Utils.getPreferenceInt(SaleReturnMasterCartonScannerActivity.this, "sender_id", 0);
                    if (sender_Id == 0) {
                        Utils.addPreferenceInt(SaleReturnMasterCartonScannerActivity.this, "sender_id", newShipmentDetailModelArrayList2.get(0).getSender_id());
                    }
                    int sender_Id1 = Utils.getPreferenceInt(SaleReturnMasterCartonScannerActivity.this, "sender_id", 0);
                    if (sender_Id1 == newShipmentDetailModelArrayList2.get(0).getSender_id()) {
                        objDatabaseHelper.insertForceInCodeDetails(barcode, "",
                                newShipmentDetailModelArrayList2.get(0).getProductName(), String.valueOf(newShipmentDetailModelArrayList2.get(0).getPackedRatio()),
                                "0", "0", "sale");

                        objDatabaseHelper.updateShipmentFromSaleReturn(barcode, "1", "0");
                        edScanCode.setText("");
                        capture.onResume();
                    } else {
                        showAlert("Alert", "You can scan only Qr-Code from one sender at a time.", tag, true);
                    }


                } else {
                    showAlert("Alert", "Max scan limit reached.", tag, true);
                }
            } else {
                showAlert("Alert", "This Qr-code is already scanned.", tag, true);
            }

        } else {
            if (isBarcodeExists(barcode)) {
                showAlert("Alert", "This Qr-code is already scanned.", tag, true);
            } else {
                showAlert("Alert", "This Qr-code is not part of the stock.", tag, true);
            }
        }
    }

    private void checkBarcode1(String barcode, String tags) {
        Log.d(Constants.TAG, "checkBarcode1: " + barcode);
        if (isBarcodeExists(barcode)) {
            objDatabaseHelper.updateShipmentFromSaleReturn(barcode, "0", "1");
            objDatabaseHelper.deleteScannedForcedCodeSingle(barcode);
            RemoveBarcodeList.add(barcode);

            String def_flag = "0";

            if (objDatabaseHelper.getScannedForceCodeList(def_flag, "sale").size() == 0) {
                Utils.removePreference(SaleReturnMasterCartonScannerActivity.this, "sender_id");
                Intent intent = new Intent(SaleReturnMasterCartonScannerActivity.this, SaleReturnMasterCartonActivity1.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                SaleReturnMasterCartonScannerActivity.this.finish();
                overridePendingTransition(0, 0);
            } else {
                capture.onPause();
                alert1 = new AlertDialog.Builder(SaleReturnMasterCartonScannerActivity.this);
                alert1.setTitle("Removed");
                alert1.setMessage("You have successfully removed a Qr-Code.");
                alert1.setCancelable(false);
                alert1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        capture.onResume();
                        clearEditScan();
                        alert1 = null;
                    }
                });
                alert1.show();
            }
        } else if (RemoveBarcodeList.contains(barcode)) {
            showAlert("Alert", "This Qr-Code is already removed.", tags, true);
        } else {
            showAlert("Alert", "This Qr-Code is not part of the shipment.", tags, true);
        }

    }


    private void showAlert(String Title, String Message, String tag, boolean valid) {

        if (tag.equals("camera")) {
            capture.onPause();
        }

//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog .setCancelable(false);
//        alertDialog.show();
//        LayoutInflater inflater = this.getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
//        alertDialog.getWindow().setContentView(dialogView);
//
//        TextView tvTilte = dialogView.findViewById(R.id.tvTilte);
//        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
//        Button button = dialogView.findViewById(R.id.btn_ok);
//
//        tvTilte.setText(Title);
//        tvMsg.setText(Message);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alertDialog.dismiss();
//                if (valid) {
//                    clearEditScan();
//                }
//                if (tag.equals("camera")) {
//                    capture.onResume();
//                }
//
//            }
//        });

        alert = new AlertDialog.Builder(SaleReturnMasterCartonScannerActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert = null;
            }
        });
        alert.show();
    }


    private boolean isBarcodeExistsInSystem(String strBarcode) {
        return BarcodeList.contains(strBarcode);
    }

    private boolean isBarcodeExists(String barcode) {
        return objDatabaseHelper.getScannedForceCodeListBoolean(barcode, "0", "sale");
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null && alert1 == null) {
            capture.onResume();
        }

        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);
            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);

        } else {
            focusable(true);
//            btnGo.setEnabled(true);

            relOTG.setVisibility(View.GONE);
            radio_otg.setChecked(false);

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setChecked(true);
            radio_camera.setEnabled(true);

        }


//        if (btnGo.isEnabled())
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));
//        else
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));
    }

    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

            //btnGo.setEnabled(false);
            //btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(SaleReturnMasterCartonScannerActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            focusable(true);

            //btnGo.setEnabled(true);
            //btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);
            relCamera.setVisibility(View.VISIBLE);
            capture.onResume();

            hideKeyboard(SaleReturnMasterCartonScannerActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            focusable(false);
            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void barcodeLookup(String barCode) {
        String[] final_barcode;
        if (barCode.contains("=")) {
            final_barcode = barCode.split("=");
            barCode = final_barcode[1].replaceAll("\u0000", "");
            edScanCode.setText(barCode);

            if (token.equals("add")) {
                checkBarcode(barCode, "OTG");
            } else {
                checkBarcode1(barCode, "OTG");
            }

        } else {
            barCode = barCode.replaceAll("\u0000", "");
            edScanCode.setText(barCode);

            if (token.equals("add")) {
                checkBarcode(barCode, "OTG");
            } else {
                checkBarcode1(barCode, "OTG");
            }
        }
    }
    //OTG Code End

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    /**
     * Check if the device's camera has a Flashlight.
     *
     * @return true if there is Flashlight, otherwise false.
     */
    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }

    @Override
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }


    public void stopScanning(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}

