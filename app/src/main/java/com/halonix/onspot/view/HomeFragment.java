package com.halonix.onspot.view;

import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;

import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.Agency;
import com.halonix.onspot.contributors.AgencyResponse;
import com.halonix.onspot.contributors.BatchIdList;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveDefectiveShipmentList_respo;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceCodeList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.SenderDetails_respo;
import com.halonix.onspot.contributors.bladecode_list_respo;
import com.halonix.onspot.contributors.code_list_respo;
import com.halonix.onspot.contributors.def_codelist;
import com.halonix.onspot.contributors.def_return_code_respo;
import com.halonix.onspot.contributors.get_receive_shipmentList;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.contributors.repacking_respo;
import com.halonix.onspot.contributors.unpacked_batch_respo;
import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.model.AgentListModel;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.LogDetailModel;
import com.halonix.onspot.model.NewShipment;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.model.SystemBarcodeModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.BreakageScan.BreakageScanActivity;
import com.halonix.onspot.view.DefectiveReturnReceive.DefRetRec_Activity;
import com.halonix.onspot.view.DefectiveReturnSend.DefReturnHistory;
import com.halonix.onspot.view.DefectiveReturnSend.DefReturnIndividualActivity1;
import com.halonix.onspot.view.ForcefullyIn.ForcefullyInMainFragment;
import com.halonix.onspot.view.History.tab.FragmentReceivedHistory;
import com.halonix.onspot.view.History.tab.FragmentShipmentHistory;
import com.halonix.onspot.view.NewShipment.NewShipmentActivity;
import com.halonix.onspot.view.NewShipment.NewShipmentDispatchActivity;
import com.halonix.onspot.view.Packaging.PackagingBatchlistActivity;
import com.halonix.onspot.view.Packaging.PackagingProductListActivity;
import com.halonix.onspot.view.ReceiveNewShipment.ReceiveNewShipmentMainFragment;
import com.halonix.onspot.view.Repacking.RepackProductsMainActivity;
import com.halonix.onspot.view.Repacking.RepackagingModellistActivity;
import com.halonix.onspot.view.Replacement.ReplacementScannerActivity;
import com.halonix.onspot.view.SaleReturnReceive.SaleRetRec_Activity;
import com.halonix.onspot.view.SaleReturnSend.History.SaleReturnHistory;
import com.halonix.onspot.view.SaleReturnSend.SaleReturnMainFragment;
import com.halonix.onspot.view.Singleton.ProductListScanActivity;
import com.halonix.onspot.view.Unpack.UnpackActivity;
import com.halonix.onspot.view.WarrantyActivation.WarrantyActivationScannerActivity;
import com.halonix.onspot.view.base.BaseFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Constants.TAG;

public class HomeFragment extends BaseFragment implements View.OnClickListener, ConnectionLostCallback {


    @BindView(R.id.button_sync_server_data)
    AppCompatButton button_sync_server_data;

    @BindView(R.id.button_pack_product)
    AppCompatButton button_pack_product;

    @BindView(R.id.button_create_new_shipment)
    AppCompatButton button_create_new_shipment;

    @BindView(R.id.button_shipment_history)
    AppCompatButton button_shipment_history;

    @BindView(R.id.button_received_history)
    AppCompatButton button_received_history;

    @BindView(R.id.button_receive_new_shipment)
    AppCompatButton button_receive_new_shipment;

    @BindView(R.id.button_unpack)
    AppCompatButton button_unpack;

    @BindView(R.id.button_repacking)
    AppCompatButton button_repacking;

    @BindView(R.id.LinRepack)
    LinearLayout LinRepack;

    @BindView(R.id.button_sale_warrenty_activation)
    AppCompatButton button_sale_warrenty_activation;

    @BindView(R.id.button_sale_warrenty_withbill)
    AppCompatButton button_sale_warrenty_withbill;

    @BindView(R.id.button_replacement)
    AppCompatButton button_replacement;

    @BindView(R.id.button_sale_return)
    AppCompatButton button_sale_return;

    @BindView(R.id.button_defective_return)
    AppCompatButton button_defective_return;

    @BindView(R.id.button_defective_history)
    AppCompatButton button_defective_history;

    @BindView(R.id.button_defective_history_for_vendor)
    AppCompatButton button_defective_history_for_vendor;

    @BindView(R.id.button_sale_return_history)
    AppCompatButton button_sale_return_history;

    @BindView(R.id.button_sale_return_forBD)
    AppCompatButton button_sale_return_forBD;

    @BindView(R.id.button_defective_return_for_BD)
    AppCompatButton button_defective_return_for_BD;

    @BindView(R.id.button_defective_history_for_BD)
    AppCompatButton button_defective_history_for_BD;

    @BindView(R.id.button_breakage_scan_for_BD)
    AppCompatButton button_breakage_scan_for_BD;

    @BindView(R.id.button_breakage_scan)
    AppCompatButton button_breakage_scan;

    @BindView(R.id.button_forcefully_in)
    AppCompatButton button_forcefully_in;


    @BindView(R.id.button_defective_receive)
    AppCompatButton button_defective_receive;

    @BindView(R.id.button_sale_receive)
    AppCompatButton button_sale_receive;

    @BindView(R.id.textView_owner_name)
    TextView textViewOwnerName;

    @BindView(R.id.tv_check_connection)
    TextView tv_check_connection;

    @BindView(R.id.nav_host_fragment)
    LinearLayout nav_host_fragment;

    @BindView(R.id.linSaleDef)
    LinearLayout linSaleDef;

    @BindView(R.id.linBreakDef)
    LinearLayout linBreakDef;

    @BindView(R.id.linSaleDefForBD)
    LinearLayout linSaleDefForBD;

    @BindView(R.id.LinSaleDefRec)
    LinearLayout LinSaleDefRec;

    @BindView(R.id.LinBulk)
    LinearLayout LinBulk;

    @BindView(R.id.LinDefSaleHistory)
    LinearLayout LinDefSaleHistory;

    @BindView(R.id.Linrepl)
    LinearLayout Linrepl;

    @BindView(R.id.linBreakScan_ForBD)
    LinearLayout linBreakScan_ForBD;

    private Resources resources;
    ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;
    DatabaseHelper databaseHelper;

    //Receive New Shipment
    ArrayList<String> InvoiceCodeListArrayList;
    ArrayList<ReceiveShipmentInvoiceList> InvoiceCodeProductDataArrayList;
    ArrayList<get_receive_shipmentList> receiveShipmentListArrayList;

    ArrayList<BatchIdList> batchIdListArrayList;
    ArrayList<PackedCartonList> packedcartonListArrayList;
    ArrayList<BatchIdList> batchIdListArrayList1;
    ArrayList<Agency> agencyArrayList;

    ArrayList<String> QRcodeListArrayList;
    ArrayList<String> MasterQRcodeListArrayList;
    ArrayList<String> BladeListArrayList;
    ArrayList<NewShipment> arrOfflineshipmentModels = new ArrayList<>();

    JSONArray ScannedQrJson;
    JSONArray ScannedBladeQrJson;
    JSONArray ScannedMasterQrJson;

    private ArrayList<String> ScannedQrList = new ArrayList<>();
    private ArrayList<String> ScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> ScannedMasterQrList = new ArrayList<>();


    private long mLastClickTime = 0;
    int k = 0;

    private Location loca;
    private String strLatitude = "", strLongitude = "", strAccuracy = "";
    ArrayList<LogDetailModel> logDetails = new ArrayList<>();
    private AlertDialog builder;
    private View root;
    private BroadcastReceiver myBroadcastReceiver;
    boolean lost = false;
    boolean alert = false;
    private boolean receiver = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);


        ButterKnife.bind(this, root);
        retrofitClient = new RetrofitClient();
        databaseHelper = DatabaseHelper.getInstance(getActivity());

        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        Log.d(TAG, "onCreateView: " + USER_TYPEs + " " + databaseHelper.getAuthToken());


        if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
            button_pack_product.setVisibility(View.GONE);
            button_receive_new_shipment.setVisibility(View.VISIBLE);
            button_received_history.setVisibility(View.VISIBLE);
            if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                // button_unpack.setVisibility(View.VISIBLE);
                //  button_repacking.setVisibility(View.VISIBLE);
                //  button_forcefully_in.setVisibility(View.VISIBLE);
                linSaleDefForBD.setVisibility(View.VISIBLE);
                linBreakScan_ForBD.setVisibility(View.VISIBLE);
                button_sale_return_forBD.setVisibility(View.VISIBLE);
                LinRepack.setVisibility(View.VISIBLE);
                // LinSaleDefRec.setVisibility(View.VISIBLE);
                // LinBulk.setVisibility(View.VISIBLE);
            }
//            linSaleDefForBD.setVisibility(View.VISIBLE);
//            linBreakScan_ForBD.setVisibility(View.VISIBLE);
//            button_sale_return_forBD.setVisibility(View.VISIBLE);
//            LinSaleDefRec.setVisibility(View.VISIBLE);
//            LinBulk.setVisibility(View.VISIBLE);
        } else if (USER_TYPEs.equals(Constants.USER_TYPE_RETAILER)) {
            button_create_new_shipment.setVisibility(View.GONE);
            button_shipment_history.setVisibility(View.GONE);
            button_unpack.setVisibility(View.GONE);
            button_sale_return_forBD.setVisibility(View.GONE);
            button_received_history.setVisibility(View.VISIBLE);

            button_sale_warrenty_activation.setVisibility(View.VISIBLE);
            button_sale_warrenty_withbill.setVisibility(View.VISIBLE);
            button_replacement.setVisibility(View.VISIBLE);
            //linSaleDef.setVisibility(View.VISIBLE);
            // linBreakDef.setVisibility(View.VISIBLE);
        } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
            button_pack_product.setVisibility(View.VISIBLE);
            button_receive_new_shipment.setVisibility(View.GONE);
            Linrepl.setVisibility(View.GONE);
            LinSaleDefRec.setVisibility(View.VISIBLE);
            LinDefSaleHistory.setVisibility(View.VISIBLE);
        }

        cd = new ConnectionDetectorActivity(getActivity());
        button_sync_server_data.setOnClickListener(this);
        button_pack_product.setOnClickListener(this);
        button_create_new_shipment.setOnClickListener(this);
        button_shipment_history.setOnClickListener(this);
        button_received_history.setOnClickListener(this);
        button_receive_new_shipment.setOnClickListener(this);
        button_unpack.setOnClickListener(this);
        button_repacking.setOnClickListener(this);
        button_sale_warrenty_activation.setOnClickListener(this);
        button_sale_warrenty_withbill.setOnClickListener(this);
        button_replacement.setOnClickListener(this);
        button_sale_return.setOnClickListener(this);
        button_defective_return.setOnClickListener(this);
        button_defective_history.setOnClickListener(this);
        button_defective_history_for_vendor.setOnClickListener(this);
        button_sale_return_history.setOnClickListener(this);

        button_sale_return_forBD.setOnClickListener(this);
        button_defective_history_for_BD.setOnClickListener(this);
        button_defective_return_for_BD.setOnClickListener(this);
        button_breakage_scan_for_BD.setOnClickListener(this);
        button_breakage_scan.setOnClickListener(this);


        button_forcefully_in.setOnClickListener(this);
        button_defective_receive.setOnClickListener(this);
        button_sale_receive.setOnClickListener(this);


        textViewOwnerName.setText(databaseHelper.getUsername());
        return root;
    }


    protected void unregisterNetworkChanges() {
        try {
            getActivity().unregisterReceiver(myBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        resources = getActivity().getResources();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(resources.getString(R.string.menu_home));
        enableNavigationDrawer(true);
        sync_after_pack();
    }

    private void sync_after_pack() {
        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        if (Utils.getPreference(getActivity(), "tag", "").equals("pack")) {
            if (cd.isConnectingToInternet()) {
                arrOfflineshipmentModels = databaseHelper.getNotUploadedNewShipmentDetail();

                if (arrOfflineshipmentModels.size() > 0 && databaseHelper.getScannedInvoiceQRListforUpload().size() > 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setCancelable(false)
                            .setMessage("You have offline new shipment,Do you want to upload it to server.")
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    databaseHelper.deleteOfflineNewShipment("all");
                                    databaseHelper.deleteScannedInvoiceCodeList("all");

                                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                                        sync_server_data();
                                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                                        ScannedQrList = databaseHelper.getScannedQRList("1");
                                        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                                        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                                        if (ScannedQrList.size() == 0) {
                                            ScannedQrList = databaseHelper.getTempScannedQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedQRList() != null)
                                                ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                                        }
                                        if (ScannedBladeQrList.size() == 0) {
                                            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedBladeQRList() != null)
                                                ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                                        }
                                        if (ScannedMasterQrList.size() == 0) {
                                            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                                        } else {
                                            if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                                ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                                        }

                                        if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                            uploadOfflinePacking();
                                        } else if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                                            receive_defective_codes1();
                                        } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                                            receive_sale_codes1();
                                        } else {
                                            sync_server_data();
                                        }

                                    }

                                }
                            })
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    uploadOfflineNewShipmentData("all");
                                }
                            })
                            .show();
                } else {


                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        sync_server_data();
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {

                        ScannedQrList = databaseHelper.getScannedQRList("1");
                        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                        if (ScannedQrList.size() == 0) {
                            ScannedQrList = databaseHelper.getTempScannedQRList();
                        } else {
                            if (databaseHelper.getTempScannedQRList() != null)
                                ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                        }
                        if (ScannedBladeQrList.size() == 0) {
                            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                        } else {
                            if (databaseHelper.getTempScannedBladeQRList() != null)
                                ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                        }
                        if (ScannedMasterQrList.size() == 0) {
                            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                        } else {
                            if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                        }

                        if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                            uploadOfflinePacking();
                        } else if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                            receive_defective_codes1();
                        } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                            receive_sale_codes1();
                        } else {
                            sync_server_data();
                        }
                    }
                }

            } else {
                showNetworkFailDialog(Constants.NO_INTERNET_MSG);
            }

        }
    }

    public void dialog(boolean value) {
        Log.d(TAG, "dialog value: " + value);
        Log.d(TAG, "dialog alert: " + alert);

        Animation slide_down = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);

        Animation slide_up = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_up);

        if (value) {
            if (lost) {
                alert = false;
                lost = false;
                tv_check_connection.setText("You are back online.");
                tv_check_connection.setBackgroundResource(R.color.green);
                tv_check_connection.setTextColor(Color.WHITE);

                Handler handler = new Handler();
                Runnable delayrunnable = () -> {
                    tv_check_connection.setVisibility(View.GONE);
                    tv_check_connection.startAnimation(slide_down);
                };
                handler.postDelayed(delayrunnable, 3000);
            }
        } else {
            lost = true;

            if (alert) {
                tv_check_connection.startAnimation(slide_up);
                tv_check_connection.setVisibility(View.VISIBLE);
                tv_check_connection.setText("Internet connection unavailable.");
                tv_check_connection.setBackgroundResource(R.color.colorPrimary);
                tv_check_connection.setTextColor(Color.WHITE);
            }
        }
    }

    ArrayList<ReceiveDefectiveShipmentList_respo> receiveDefectiveShipmentList_respos;


    //TODO Defective Return Receive CodeList API Call here
    private void getDefRetReceiveCodeList() {
        receiveDefectiveShipmentList_respos = new ArrayList<>();

        retrofitClient.getService().getDefectiveShipment(databaseHelper.getAuthToken()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                Log.d(TAG, "onResponse getDefRetReceiveCodeList: " + response.toString());
                if (response.body().getSuccess()) {
                    receiveDefectiveShipmentList_respos = response.body().getData();

                    if (receiveDefectiveShipmentList_respos.size() > 0) {
                        for (int i = 0; i < receiveDefectiveShipmentList_respos.size(); i++) {
                            databaseHelper.insertDefRetList(receiveDefectiveShipmentList_respos.get(i).getCode(),
                                    receiveDefectiveShipmentList_respos.get(i).getModel());
                        }
                    }
                }
                getSaleRetReceiveCodeList();
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    ArrayList<ReceiveDefectiveShipmentList_respo> receiveSaleShipmentList_respos;

    //TODO Sale Return Receive CodeList API Call here
    private void getSaleRetReceiveCodeList() {
        receiveSaleShipmentList_respos = new ArrayList<>();

        retrofitClient.getService().getSaleReturnCodes(databaseHelper.getAuthToken()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                Log.d(TAG, "onResponse getSaleRetReceiveCodeList: " + response.toString());
                if (response.body().getSuccess()) {
                    receiveSaleShipmentList_respos = response.body().getCode_list();

                    if (receiveSaleShipmentList_respos.size() > 0) {
                        for (int i = 0; i < receiveSaleShipmentList_respos.size(); i++) {
                            databaseHelper.insertSaleRetList(receiveSaleShipmentList_respos.get(i).getCode(),
                                    receiveSaleShipmentList_respos.get(i).getProduct_name(),
                                    String.valueOf(receiveSaleShipmentList_respos.get(i).getPacking_ratio()));
                        }
                    }
                }
                getpackedcarton();
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_sync_server_data:
                if (cd.isConnectingToInternet()) {
                    myBroadcastReceiver = new MyBroadcastReceiver(this);
                    getActivity().registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    receiver = true;
                    alert = true;

                    String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");

                    if (USER_TYPEs.equals(Constants.USER_TYPE_RETAILER)) {
                        logDetails = databaseHelper.getLogDetails();
                        if (logDetails.size() > 0) {
                            uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                            uploadofflineMasterCartonScanLocationControl.execute();
                        } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                            LocationControlRegular locationControl = new LocationControlRegular();
                            locationControl.execute();
                        } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                            LocationControlBreakage locationControl = new LocationControlBreakage();
                            locationControl.execute();
                        } else {
                            sync_server_data();
                        }
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR) || USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                        arrOfflineshipmentModels = databaseHelper.getNotUploadedNewShipmentDetail();

                        if (arrOfflineshipmentModels.size() > 0 && databaseHelper.getScannedInvoiceQRListforUpload().size() > 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("You have offline send new shipment,Do you want to upload it to server.")
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            databaseHelper.deleteOfflineNewShipment("all");
                                            databaseHelper.deleteScannedInvoiceCodeList("all");
                                            databaseHelper.deleteScannedMasterCodeList();

                                            if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {

                                                logDetails = databaseHelper.getLogDetails();
                                                Log.d(TAG, "onClick logDetails.size(): " + logDetails.size());
                                                checkforRepacking();
                                                if (databaseHelper.getRSScannedInvoiceQRList().size() > 0 && databaseHelper.getIS_ScannedInvoiceQRList().size() > 0) {
                                                    uploadofflineCheckMasterCartonLocationControl checkMasterCartonLocationControl = new uploadofflineCheckMasterCartonLocationControl();
                                                    checkMasterCartonLocationControl.execute();
                                                } else if (databaseHelper.getRSScannedInvoiceQRList().size() > 0) {
                                                    uploadofflineInvoiceScanLocationControl uploadofflineInvoiceScanLocationControl = new uploadofflineInvoiceScanLocationControl();
                                                    uploadofflineInvoiceScanLocationControl.execute();
                                                } else if (logDetails.size() > 0) {
                                                    uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                                                    uploadofflineMasterCartonScanLocationControl.execute();
                                                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                                                    LocationControlRegular locationControl = new LocationControlRegular();
                                                    locationControl.execute();
                                                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                                                    LocationControlBreakage locationControl = new LocationControlBreakage();
                                                    locationControl.execute();
                                                } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                                                    unpack_cartons();
                                                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                                    uploadOfflineRePacking();
                                                } else {
                                                    sync_server_data();
                                                }
                                            } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                                                ScannedQrList = databaseHelper.getScannedQRList("1");
                                                ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                                                ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                                                if (ScannedQrList.size() == 0) {
                                                    ScannedQrList = databaseHelper.getTempScannedQRList();
                                                } else {
                                                    if (databaseHelper.getTempScannedQRList() != null)
                                                        ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                                                }
                                                if (ScannedBladeQrList.size() == 0) {
                                                    ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                                                } else {
                                                    if (databaseHelper.getTempScannedBladeQRList() != null)
                                                        ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                                                }
                                                if (ScannedMasterQrList.size() == 0) {
                                                    ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                                                } else {
                                                    if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                                        ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                                                }

                                                if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                                    uploadOfflinePacking();
                                                } else if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                                                    receive_defective_codes1();
                                                } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                                                    receive_sale_codes1();
                                                } else {
                                                    sync_server_data();
                                                }
                                            }
                                        }
                                    })
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            uploadOfflineNewShipmentData("all");
                                        }
                                    })
                                    .show();
                        } else {

                            if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {

                                logDetails = databaseHelper.getLogDetails();
                                checkforRepacking();
                                if (databaseHelper.getRSScannedInvoiceQRList().size() > 0 && databaseHelper.getIS_ScannedInvoiceQRList().size() > 0) {
                                    uploadofflineCheckMasterCartonLocationControl checkMasterCartonLocationControl = new uploadofflineCheckMasterCartonLocationControl();
                                    checkMasterCartonLocationControl.execute();
                                } else if (databaseHelper.getRSScannedInvoiceQRList().size() > 0) {
                                    uploadofflineInvoiceScanLocationControl uploadofflineInvoiceScanLocationControl = new uploadofflineInvoiceScanLocationControl();
                                    uploadofflineInvoiceScanLocationControl.execute();
                                } else if (logDetails.size() > 0) {
                                    uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                                    uploadofflineMasterCartonScanLocationControl.execute();
                                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                                    LocationControlRegular locationControl = new LocationControlRegular();
                                    locationControl.execute();
                                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                                    LocationControlBreakage locationControl = new LocationControlBreakage();
                                    locationControl.execute();
                                } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                                    unpack_cartons();
                                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                    uploadOfflineRePacking();
                                } else {
                                    sync_server_data();
                                }

                            } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {

                                ScannedQrList = databaseHelper.getScannedQRList("1");
                                ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                                ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                                if (ScannedQrList.size() == 0) {
                                    ScannedQrList = databaseHelper.getTempScannedQRList();
                                } else {
                                    if (databaseHelper.getTempScannedQRList() != null)
                                        ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                                }
                                if (ScannedBladeQrList.size() == 0) {
                                    ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                                } else {
                                    if (databaseHelper.getTempScannedBladeQRList() != null)
                                        ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                                }
                                if (ScannedMasterQrList.size() == 0) {
                                    ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                                } else {
                                    if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                        ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                                }

                                if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                    uploadOfflinePacking();
                                } else if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                                    receive_defective_codes1();
                                } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                                    receive_sale_codes1();
                                } else {
                                    sync_server_data();
                                }
                            }
                        }

                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {

                        arrOfflineshipmentModels = databaseHelper.getNotUploadedNewShipmentDetail();

                        if (arrOfflineshipmentModels.size() > 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("You have offline send new shipment,Do you want to upload it to server.")
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            databaseHelper.deleteOfflineNewShipment("all");
                                            logDetails = databaseHelper.getLogDetails();

                                            if (databaseHelper.getRSScannedInvoiceQRList().size() > 0 && databaseHelper.getIS_ScannedInvoiceQRList().size() > 0) {
                                                uploadofflineCheckMasterCartonLocationControl checkMasterCartonLocationControl = new uploadofflineCheckMasterCartonLocationControl();
                                                checkMasterCartonLocationControl.execute();
                                            } else if (databaseHelper.getRSScannedInvoiceQRList().size() > 0) {
                                                uploadofflineInvoiceScanLocationControl uploadofflineInvoiceScanLocationControl = new uploadofflineInvoiceScanLocationControl();
                                                uploadofflineInvoiceScanLocationControl.execute();
                                            } else if (logDetails.size() > 0) {
                                                uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                                                uploadofflineMasterCartonScanLocationControl.execute();
                                            } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                                                LocationControlRegular locationControl = new LocationControlRegular();
                                                locationControl.execute();
                                            } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                                                LocationControlBreakage locationControl = new LocationControlBreakage();
                                                locationControl.execute();
                                            } else {
                                                sync_server_data();
                                            }
                                        }
                                    }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    uploadOfflineNewShipmentData(Constants.USER_TYPE_DISTRIBUTOR);
                                }
                            }).show();
                        } else {

                            logDetails = databaseHelper.getLogDetails();

                            if (databaseHelper.getRSScannedInvoiceQRList().size() > 0 && databaseHelper.getIS_ScannedInvoiceQRList().size() > 0) {
                                uploadofflineCheckMasterCartonLocationControl checkMasterCartonLocationControl = new uploadofflineCheckMasterCartonLocationControl();
                                checkMasterCartonLocationControl.execute();
                            } else if (databaseHelper.getRSScannedInvoiceQRList().size() > 0) {
                                uploadofflineInvoiceScanLocationControl uploadofflineInvoiceScanLocationControl = new uploadofflineInvoiceScanLocationControl();
                                uploadofflineInvoiceScanLocationControl.execute();
                            } else if (logDetails.size() > 0) {
                                uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                                uploadofflineMasterCartonScanLocationControl.execute();
                            } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                                LocationControlRegular locationControl = new LocationControlRegular();
                                locationControl.execute();
                            } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                                LocationControlBreakage locationControl = new LocationControlBreakage();
                                locationControl.execute();
                            } else {
                                sync_server_data();
                            }
                        }

                    }

                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_pack_product:
                if (cd.isConnectingToInternet()) {
                    ScannedQrList = databaseHelper.getScannedQRList("1");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                    if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline packaging of products,Do you want to upload it to server.")
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.deleteAllScannedTableData();
                                        batchIdListArrayList1 = databaseHelper.getBatchList();

                                        if (batchIdListArrayList1.size() > 0) {
                                            Utils.removePreference(getActivity(), "item_code");
                                            Utils.removePreference(getActivity(), "blade");
                                            Utils.removePreference(getActivity(), "model");
                                            startActivity(new Intent(getActivity(), PackagingBatchlistActivity.class));
                                            getActivity().overridePendingTransition(0, 0);
                                        } else {
                                            new AlertDialog.Builder(getActivity())
                                                    .setTitle("Alert")
                                                    .setCancelable(false)
                                                    .setMessage("Please sync server data first to proceed further.")
                                                    .setPositiveButton("Ok", null)
                                                    .show();
                                        }
                                    }
                                })
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ScannedQrList = databaseHelper.getScannedQRList("1");
                                        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                                        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                                        if (ScannedQrList.size() == 0) {
                                            ScannedQrList = databaseHelper.getTempScannedQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedQRList() != null)
                                                ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                                        }
                                        if (ScannedBladeQrList.size() == 0) {
                                            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedBladeQRList() != null)
                                                ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                                        }
                                        if (ScannedMasterQrList.size() == 0) {
                                            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                                        } else {
                                            if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                                ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                                        }

                                        if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                            uploadOfflinePacking();
                                        }
                                    }
                                })
                                .show();

                    } else {
                        batchIdListArrayList1 = databaseHelper.getBatchList();

                        if (batchIdListArrayList1.size() > 0) {
                            Utils.removePreference(getActivity(), "item_code");
                            Utils.removePreference(getActivity(), "blade");
                            Utils.removePreference(getActivity(), "model");
                            startActivity(new Intent(getActivity(), PackagingBatchlistActivity.class));
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }


                } else {

                    ScannedQrList = databaseHelper.getScannedQRList("0");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");

                    if (ScannedQrList.size() == 0) {
                        ScannedQrList = databaseHelper.getTempScannedQRList();
                    }
                    if (ScannedBladeQrList.size() == 0) {
                        ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                    }

                    if (ScannedMasterQrList.size() == 0) {
                        ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
                    }

                    if (ScannedQrList.size() > 0 || ScannedBladeQrList.size() > 0 || ScannedMasterQrList.size() > 0) {
                        Intent intent = new Intent(getActivity(), PackagingProductListActivity.class);
                        intent.putExtra("item_code", Utils.getPreference(getActivity(), "item_code", ""));
                        intent.putExtra("blade", Utils.getPreferenceBoolean(getActivity(), "blade", false));
                        intent.putExtra("model", Utils.getPreference(getActivity(), "model", ""));
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else {

                        batchIdListArrayList1 = databaseHelper.getBatchList();

                        if (batchIdListArrayList1.size() > 0) {
                            Utils.removePreference(getActivity(), "item_code");
                            Utils.removePreference(getActivity(), "blade");
                            Utils.removePreference(getActivity(), "model");
                            startActivity(new Intent(getActivity(), PackagingBatchlistActivity.class));
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }
                }


                break;

            case R.id.button_create_new_shipment:
                if (cd.isConnectingToInternet()) {
                    String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
                    arrOfflineshipmentModels = databaseHelper.getNotUploadedNewShipmentDetail();
                    if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        if (arrOfflineshipmentModels.size() > 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("You have offline send new shipment,Do you want to upload it to server.")
                                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            databaseHelper.updateShipmentData();
                                            databaseHelper.deleteScannedMasterCodeList();
                                            databaseHelper.deleteOfflineNewShipment("all");
                                            Utils.removePreference(getActivity(), "shipment_type");

                                            gotocreatenewshipment();
                                        }
                                    })
                                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            uploadMultipleOfflineNewShipmentData(Constants.USER_TYPE_DISTRIBUTOR);
                                        }
                                    })
                                    .show();
                        } else {
                            gotocreatenewshipment();
                        }
                    } else {
                        Log.d(TAG, "onClick databaseHelper.getScannedInvoiceQRListforUpload().size(): " + databaseHelper.getScannedInvoiceQRListforUpload().size());
                        if (arrOfflineshipmentModels.size() > 0 && databaseHelper.getScannedInvoiceQRListforUpload().size() > 0) {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("You have offline send new shipment,Do you want to upload it to server.")
                                    .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            databaseHelper.updateShipmentData();
                                            databaseHelper.deleteScannedMasterCodeList();
                                            databaseHelper.deleteOfflineNewShipment("all");
                                            databaseHelper.deleteScannedInvoiceCodeList("all");
                                            databaseHelper.updateInvoiceCode("0");
                                            Utils.removePreference(getActivity(), "shipment_type");

                                            gotocreatenewshipment();
                                        }
                                    })
                                    .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            uploadMultipleOfflineNewShipmentData("all");
                                        }
                                    })
                                    .show();
                        } else {
                            gotocreatenewshipment();
                        }
                    }
                } else {
                    gotocreatenewshipment();
                }

                break;

            case R.id.button_shipment_history:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(FragmentShipmentHistory.class.getSimpleName(), true, null);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_receive_new_shipment:
                changeFragmentTo(ReceiveNewShipmentMainFragment.class.getSimpleName(), true, null);
                break;

            case R.id.button_received_history:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(FragmentReceivedHistory.class.getSimpleName(), true, null);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_unpack:
                if (databaseHelper.getShipmentSystemBarcode().size() > 0) {
                    startActivity(new Intent(getActivity(), UnpackActivity.class));
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setCancelable(false)
                            .setMessage("Please sync server data first to proceed further.")
                            .setPositiveButton("Ok", null)
                            .show();
                }

                break;
            case R.id.button_repacking:
                if (cd.isConnectingToInternet()) {
                    ScannedQrList = databaseHelper.getScannedQRList("1");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                    if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline repackaging of products,Do you want to upload it to server.")
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.deleteAllScannedTableData();
                                        batchIdListArrayList1 = databaseHelper.getBatchListForRepacking();

                                        if (batchIdListArrayList1.size() > 0) {
                                            Utils.removePreference(getActivity(), "item_code");
                                            Utils.removePreference(getActivity(), "blade");
                                            Utils.removePreference(getActivity(), "model");
                                            startActivity(new Intent(getActivity(), RepackagingModellistActivity.class));
                                            getActivity().overridePendingTransition(0, 0);
                                        } else {
                                            new AlertDialog.Builder(getActivity())
                                                    .setTitle("Alert")
                                                    .setCancelable(false)
                                                    .setMessage("Please sync server data first to proceed further.")
                                                    .setPositiveButton("Ok", null)
                                                    .show();
                                        }
                                    }
                                })
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        ScannedQrList = databaseHelper.getScannedQRList("1");
                                        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                                        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                                        if (ScannedQrList.size() == 0) {
                                            ScannedQrList = databaseHelper.getTempScannedQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedQRList() != null)
                                                ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                                        }
                                        if (ScannedBladeQrList.size() == 0) {
                                            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                                        } else {
                                            if (databaseHelper.getTempScannedBladeQRList() != null)
                                                ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                                        }
                                        if (ScannedMasterQrList.size() == 0) {
                                            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                                        } else {
                                            if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                                                ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                                        }

                                        if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                                            uploadOfflineRePacking();
                                        }
                                    }
                                })
                                .show();

                    } else {
                        ScannedQrList = databaseHelper.getScannedQRList("0");
                        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");
                        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");

                        if (ScannedQrList.size() == 0) {
                            ScannedQrList = databaseHelper.getTempScannedQRList();
                        }
                        if (ScannedBladeQrList.size() == 0) {
                            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                        }

                        if (ScannedMasterQrList.size() == 0) {
                            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
                        }

                        if (ScannedQrList.size() > 0 || ScannedBladeQrList.size() > 0 || ScannedMasterQrList.size() > 0) {
                            int packing_ratio = databaseHelper.getPackingRatioRepack(Utils.getPreference(getActivity(), "item_code", ""), "0");
                            Intent intent = new Intent(getActivity(), RepackProductsMainActivity.class);
                            intent.putExtra("item_code", Utils.getPreference(getActivity(), "item_code", ""));
                            intent.putExtra("blade", Utils.getPreferenceBoolean(getActivity(), "blade", false));
                            intent.putExtra("model", Utils.getPreference(getActivity(), "model", ""));
                            intent.putExtra("qty", packing_ratio);
                            intent.putExtra("qt", String.valueOf(packing_ratio));
                            startActivity(intent);
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            batchIdListArrayList1 = databaseHelper.getBatchListForRepacking();

                            if (batchIdListArrayList1.size() > 0) {
                                Utils.removePreference(getActivity(), "item_code");
                                Utils.removePreference(getActivity(), "blade");
                                Utils.removePreference(getActivity(), "model");
                                databaseHelper.deletepackingratio();
                                startActivity(new Intent(getActivity(), RepackagingModellistActivity.class));
                                getActivity().overridePendingTransition(0, 0);
                            } else {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Alert")
                                        .setCancelable(false)
                                        .setMessage("Please sync server data first to proceed further.")
                                        .setPositiveButton("Ok", null)
                                        .show();
                            }
                        }


                    }


                } else {
                    ScannedQrList = databaseHelper.getScannedQRList("0");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");

                    if (ScannedQrList.size() == 0) {
                        ScannedQrList = databaseHelper.getTempScannedQRList();
                    }
                    if (ScannedBladeQrList.size() == 0) {
                        ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                    }

                    if (ScannedMasterQrList.size() == 0) {
                        ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
                    }

                    if (ScannedQrList.size() > 0 || ScannedBladeQrList.size() > 0 || ScannedMasterQrList.size() > 0) {
                        int packing_ratio = databaseHelper.getPackingRatioRepack(Utils.getPreference(getActivity(), "item_code", ""), "0");
                        Intent intent = new Intent(getActivity(), RepackProductsMainActivity.class);
                        intent.putExtra("item_code", Utils.getPreference(getActivity(), "item_code", ""));
                        intent.putExtra("blade", Utils.getPreferenceBoolean(getActivity(), "blade", false));
                        intent.putExtra("model", Utils.getPreference(getActivity(), "model", ""));
                        intent.putExtra("qty", packing_ratio);
                        intent.putExtra("qt", String.valueOf(packing_ratio));
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else {

                        batchIdListArrayList1 = databaseHelper.getBatchListForRepacking();

                        if (batchIdListArrayList1.size() > 0) {
                            Utils.removePreference(getActivity(), "item_code");
                            Utils.removePreference(getActivity(), "blade");
                            Utils.removePreference(getActivity(), "model");
                            startActivity(new Intent(getActivity(), RepackagingModellistActivity.class));
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }
                }

                break;

            case R.id.button_sale_warrenty_activation:
                if (cd.isConnectingToInternet()) {
                    Utils.removePreference(getActivity(), "sale_qr");
                    Intent intent = new Intent(getActivity(), WarrantyActivationScannerActivity.class);
                    intent.putExtra("tag", "without_bill");
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }

                break;

            case R.id.button_sale_warrenty_withbill:
                if (cd.isConnectingToInternet()) {
                    Utils.removePreference(getActivity(), "sale_qr");
                    Intent intent1 = new Intent(getActivity(), WarrantyActivationScannerActivity.class);
                    intent1.putExtra("tag", "with_bill");
                    startActivity(intent1);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }

                break;

            case R.id.button_replacement:
                if (cd.isConnectingToInternet()) {
                    Utils.removePreference(getActivity(), "old_qr");
                    Utils.removePreference(getActivity(), "new_qr");
                    Intent intent1 = new Intent(getActivity(), ReplacementScannerActivity.class);
                    intent1.putExtra("tag", "old");
                    startActivity(intent1);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_sale_return:
            case R.id.button_sale_return_forBD:
                changeFragmentTo(SaleReturnMainFragment.class.getSimpleName(), true, null);
                break;

            case R.id.button_defective_return:
            case R.id.button_defective_return_for_BD:
                if (cd.isConnectingToInternet()) {
                    if (databaseHelper.getNotUploadedDefRetShipments().size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline defective return shipment,Do you want to upload it to server.")
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.updateScannedDefCodeWithNo();
                                        databaseHelper.deleteDefReturn();
                                        if (databaseHelper.getDefCodeList().size() > 0) {
                                            Intent intent1 = new Intent(getActivity(), DefReturnIndividualActivity1.class);
                                            startActivity(intent1);
                                            getActivity().overridePendingTransition(0, 0);
                                        } else {
                                            new AlertDialog.Builder(getActivity())
                                                    .setTitle("Alert")
                                                    .setCancelable(false)
                                                    .setMessage("Please sync server data first to proceed further.")
                                                    .setPositiveButton("Ok", null)
                                                    .show();
                                        }

                                    }
                                })
                                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        uploadDefRetOfflineShipmentLocationControl1 locationControl = new uploadDefRetOfflineShipmentLocationControl1();
                                        locationControl.execute();
                                    }
                                })
                                .show();
                    } else {

                        if (databaseHelper.getDefCodeList().size() > 0) {
                            Intent intent1 = new Intent(getActivity(), DefReturnIndividualActivity1.class);
                            startActivity(intent1);
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }
                } else {
                    Log.d(TAG, "onClick: " + databaseHelper.getDefCodeList().size());
                    if (databaseHelper.getDefCodeList().size() > 0) {
                        Intent intent1 = new Intent(getActivity(), DefReturnIndividualActivity1.class);
                        startActivity(intent1);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Please sync server data first to proceed further.")
                                .setPositiveButton("Ok", null)
                                .show();
                    }
                }

                break;

            case R.id.button_defective_receive:
                if (cd.isConnectingToInternet()) {
                    if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline defective returns receive shipments,Do you want to upload it to server.")
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.updateScannedDefRetRecCodeList2("1");
                                        Intent intent1 = new Intent(getActivity(), DefRetRec_Activity.class);
                                        startActivity(intent1);
                                        getActivity().overridePendingTransition(0, 0);

                                    }
                                })
                                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        receive_defective_codes();
                                    }
                                })
                                .show();
                    } else {

                        if (databaseHelper.getScannedDefRetRecCodeList("0", "0").size() > 0) {
                            Intent intent1 = new Intent(getActivity(), DefRetRec_Activity.class);
                            startActivity(intent1);
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }
                } else {
                    Log.d(TAG, "onClick: " + databaseHelper.getDefCodeList().size());
                    if (databaseHelper.getScannedDefRetRecCodeList("0", "0").size() > 0) {
                        Intent intent1 = new Intent(getActivity(), DefRetRec_Activity.class);
                        startActivity(intent1);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Please sync server data first to proceed further.")
                                .setPositiveButton("Ok", null)
                                .show();
                    }
                }

                break;

            case R.id.button_sale_receive:
                if (cd.isConnectingToInternet()) {
                    if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("You have offline sale returns receive shipments,Do you want to upload it to server.")
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        databaseHelper.updateScannedSaleRetRecCodeList2("1");
                                        Intent intent1 = new Intent(getActivity(), SaleRetRec_Activity.class);
                                        startActivity(intent1);
                                        getActivity().overridePendingTransition(0, 0);

                                    }
                                })
                                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        receive_sale_codes();
                                    }
                                })
                                .show();
                    } else {

                        if (databaseHelper.getScannedSaleRetRecCodeList("0", "0").size() > 0) {
                            Intent intent1 = new Intent(getActivity(), SaleRetRec_Activity.class);
                            startActivity(intent1);
                            getActivity().overridePendingTransition(0, 0);
                        } else {
                            new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                    .setTitle("Alert")
                                    .setCancelable(false)
                                    .setMessage("Please sync server data first to proceed further.")
                                    .setPositiveButton("Ok", null)
                                    .show();
                        }
                    }
                } else {
                    Log.d(TAG, "onClick: " + databaseHelper.getDefCodeList().size());
                    if (databaseHelper.getScannedSaleRetRecCodeList("0", "0").size() > 0) {
                        Intent intent1 = new Intent(getActivity(), SaleRetRec_Activity.class);
                        startActivity(intent1);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Please sync server data first to proceed further.")
                                .setPositiveButton("Ok", null)
                                .show();
                    }
                }

                break;


            case R.id.button_defective_history_for_BD:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(ForcefullyInMainFragment.class.getSimpleName(), true, null);
                } else {
                    if (databaseHelper.getScannedForceCodeList("0", "force_in").size() > 0 || databaseHelper.getScannedForceCodeList("0", "force_in").size() > 0) {
                        changeFragmentTo(ForcefullyInMainFragment.class.getSimpleName(), true, null);
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }

                }
                break;
            case R.id.button_defective_history:
            case R.id.button_defective_history_for_vendor:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(DefReturnHistory.class.getSimpleName(), true, null);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_sale_return_history:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(SaleReturnHistory.class.getSimpleName(), true, null);
                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

            case R.id.button_breakage_scan:
            case R.id.button_breakage_scan_for_BD:
                if (cd.isConnectingToInternet()) {
                    Intent intent2 = new Intent(getActivity(), BreakageScanActivity.class);
                    startActivity(intent2);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    if (databaseHelper.getScannedForceCodeList("1", "breakage").size() > 0) {
                        Intent intent2 = new Intent(getActivity(), BreakageScanActivity.class);
                        startActivity(intent2);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }
                }
                break;


            case R.id.button_forcefully_in:
                if (cd.isConnectingToInternet()) {
                    changeFragmentTo(ForcefullyInMainFragment.class.getSimpleName(), true, null);
                } else {
                    if (databaseHelper.getScannedForceCodeList("0", "force_in").size() > 0 || databaseHelper.getScannedForceCodeList("0", "force_in").size() > 0) {
                        changeFragmentTo(ForcefullyInMainFragment.class.getSimpleName(), true, null);
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }

                }
                break;

        }

    }

    private void checkforRepacking() {
        ScannedQrList = databaseHelper.getScannedQRList("1");
        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

        if (ScannedQrList.size() == 0) {
            ScannedQrList = databaseHelper.getTempScannedQRList();
        } else {
            if (databaseHelper.getTempScannedQRList() != null)
                ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
        }
        if (ScannedBladeQrList.size() == 0) {
            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
        } else {
            if (databaseHelper.getTempScannedBladeQRList() != null)
                ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
        }
        if (ScannedMasterQrList.size() == 0) {
            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
        } else {
            if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
        }

    }

    private void unpack_cartons() {
        ArrayList<String> stringArrayList = databaseHelper.getScannedForceCodeList("0", "unpack");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "dispatchShipment: " + jsonArray.toString());

        Utils.showprogressdialog(getActivity(), "Uploading offline unpacking data to server,please wait.");

        retrofitClient.getService().unpack(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<SenderDetails_respo>() {
            @Override
            public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {

                if (response.body().getSuccess()) {
                    databaseHelper.deleteScannedForcedCode("0", "unpack");
                    Utils.showToast(getActivity(), "Unpacking data uploaded successfully");
                }
                checkforRepacking();
                if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                    uploadOfflineRePacking();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }
            }

            @Override
            public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    private void receive_defective_codes1() {
        Utils.showprogressdialog(getActivity(), "Uploading offline defective return receive shipment...please wait");
        ArrayList<String> stringArrayList = databaseHelper.getScannedDefRetRecCodeList("1", "1");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "receive_codes Def: " + jsonArray.toString());

        retrofitClient.getService().receiveDefectiveShipment(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {
                    databaseHelper.deleteScannedDefRetRecCodeList();
                }

                if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                    receive_sale_codes1();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }


            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    private void receive_defective_codes() {
        Utils.showprogressdialog(getActivity(), "Receiving...please wait");
        ArrayList<String> stringArrayList = databaseHelper.getScannedDefRetRecCodeList("1", "1");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "receive_codes Def: " + jsonArray.toString());

//        Utils.dismissprogressdialog();

        retrofitClient.getService().receiveDefectiveShipment(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {
                    databaseHelper.deleteScannedDefRetRecCodeList();
                    getDefRetReceiveCodeList1(response.body().getMessage());
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    ArrayList<ReceiveDefectiveShipmentList_respo> receiveDefectiveShipmentList_respos1;

    private void getDefRetReceiveCodeList1(String message) {
        receiveDefectiveShipmentList_respos1 = new ArrayList<>();

        retrofitClient.getService().getDefectiveShipment(databaseHelper.getAuthToken()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                Utils.dismissprogressdialog();
                if (response.body().getSuccess()) {
                    receiveDefectiveShipmentList_respos1 = response.body().getData();

                    if (receiveDefectiveShipmentList_respos1.size() > 0) {
                        for (int i = 0; i < receiveDefectiveShipmentList_respos1.size(); i++) {
                            databaseHelper.insertDefRetList(receiveDefectiveShipmentList_respos1.get(i).getCode(),
                                    receiveDefectiveShipmentList_respos1.get(i).getModel());
                        }
                    }
                }

                Utils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    private void receive_sale_codes1() {
        Utils.showprogressdialog(getActivity(), "Uploading offline sale return receive shipment...please wait");
        ArrayList<String> stringArrayList = databaseHelper.getScannedSaleRetRecCodeList("1", "1");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "receive_codes Def: " + jsonArray.toString());

        retrofitClient.getService().receiveSaleReturn(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {

                }
                databaseHelper.deleteScannedSaleRetRecCodeList();
                Utils.dismissprogressdialog();
                sync_server_data();
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    private void receive_sale_codes() {
        Utils.showprogressdialog(getActivity(), "Receiving...please wait");
        ArrayList<String> stringArrayList = databaseHelper.getScannedSaleRetRecCodeList("1", "1");

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "receive_codes Def: " + jsonArray.toString());

//        Utils.dismissprogressdialog();

        retrofitClient.getService().receiveSaleReturn(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {
                    databaseHelper.deleteScannedSaleRetRecCodeList();
                    getSaleRetReceiveCodeList1(response.body().getMessage());
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    ArrayList<ReceiveDefectiveShipmentList_respo> receiveSaleShipmentList_respos1;

    private void getSaleRetReceiveCodeList1(String message) {
        receiveSaleShipmentList_respos1 = new ArrayList<>();

        retrofitClient.getService().getSaleReturnCodes(databaseHelper.getAuthToken()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                Utils.dismissprogressdialog();
                if (response.body().getSuccess()) {
                    receiveSaleShipmentList_respos1 = response.body().getCode_list();

                    if (receiveSaleShipmentList_respos1.size() > 0) {
                        for (int i = 0; i < receiveSaleShipmentList_respos1.size(); i++) {
                            databaseHelper.insertSaleRetList(receiveSaleShipmentList_respos1.get(i).getCode(),
                                    receiveSaleShipmentList_respos1.get(i).getProduct_name(),
                                    String.valueOf(receiveSaleShipmentList_respos1.get(i).getPacking_ratio()));
                        }
                    }
                }

                Utils.showToast(getActivity(), message);
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    private class uploadDefRetOfflineShipmentLocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Defective return),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            uploadDefRetOfflineShipment();
        }
    }

    private void uploadDefRetOfflineShipment() {
        ArrayList<NewShipment> newShipmentArrayList = databaseHelper.getNotUploadedDefRetShipments();
        JSONObject jsonObject;
        JSONArray jsonArray1 = new JSONArray();
        for (int i = 0; i < newShipmentArrayList.size(); i++) {
            ArrayList<String> stringArrayList = databaseHelper.getScannedDefCodeListForUpload("1", newShipmentArrayList.get(i).getSender_id(),
                    newShipmentArrayList.get(i).getRin());
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(stringArrayList.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObject = new JSONObject();
                jsonObject.put("reason", newShipmentArrayList.get(i).getReason());
                jsonObject.put("code_list", jsonArray.toString());
                jsonObject.put("agency_id", newShipmentArrayList.get(i).getSender_id());
                jsonObject.put("return_invoice_no", newShipmentArrayList.get(i).getRin());
                jsonObject.put("return_invoice_date", newShipmentArrayList.get(i).getRid());
                jsonArray1.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int finalI = i;
            databaseHelper.updateDEFRETShipment(newShipmentArrayList.get(finalI).getRin());

            retrofitClient.getService().defective_return_send(databaseHelper.getAuthToken(), newShipmentArrayList.get(i).getReason(), jsonArray.toString(), Integer.parseInt(newShipmentArrayList.get(i).getSender_id()), newShipmentArrayList.get(i).getRin(), newShipmentArrayList.get(i).getRid()).enqueue(new Callback<SenderDetails_respo>() {
                @Override
                public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {
                    if (response.body().getSuccess()) {
                        Log.d(TAG, "onResponse defective_return_send: " + response.body().getSuccess());
                        Log.d(TAG, "onResponse defective_return_send: " + newShipmentArrayList.get(finalI).getRin());
                        databaseHelper.updateDEFRETShipment(newShipmentArrayList.get(finalI).getRin());
                    }
                }

                @Override
                public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });

        }
        Utils.dismissprogressdialog();

        sync_server_data();

    }


    private class uploadDefRetOfflineShipmentLocationControl1 extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Defective return),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            uploadDefRetOfflineShipment1();
        }
    }

    private void uploadDefRetOfflineShipment1() {
        ArrayList<NewShipment> newShipmentArrayList = databaseHelper.getNotUploadedDefRetShipments();
        JSONObject jsonObject;
        JSONArray jsonArray1 = new JSONArray();
        for (int i = 0; i < newShipmentArrayList.size(); i++) {
            ArrayList<String> stringArrayList = databaseHelper.getScannedDefCodeListForUpload("1", newShipmentArrayList.get(i).getSender_id(),
                    newShipmentArrayList.get(i).getRin());
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(stringArrayList.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                jsonObject = new JSONObject();
                jsonObject.put("reason", newShipmentArrayList.get(i).getReason());
                jsonObject.put("code_list", jsonArray.toString());
                jsonObject.put("agency_id", newShipmentArrayList.get(i).getSender_id());
                jsonObject.put("return_invoice_no", newShipmentArrayList.get(i).getRin());
                jsonObject.put("return_invoice_date", newShipmentArrayList.get(i).getRid());
                jsonArray1.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int finalI = i;
            databaseHelper.updateDEFRETShipment(newShipmentArrayList.get(finalI).getRin());

            retrofitClient.getService().defective_return_send(databaseHelper.getAuthToken(), newShipmentArrayList.get(i).getReason(), jsonArray.toString(), Integer.parseInt(newShipmentArrayList.get(i).getSender_id()), newShipmentArrayList.get(i).getRin(), newShipmentArrayList.get(i).getRid()).enqueue(new Callback<SenderDetails_respo>() {
                @Override
                public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {
                    if (response.body().getSuccess()) {
                        databaseHelper.updateDEFRETShipment(newShipmentArrayList.get(finalI).getRin());
                        Utils.removePreference(getActivity(), "def_sender_id");
                        databaseHelper.deleteScannedDefCodes(newShipmentArrayList.get(finalI).getRin());
                    }
                }

                @Override
                public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });

        }
        Utils.dismissprogressdialog();
        Utils.showToast(getActivity(), "Offline defective return shipment uploaded successfully.");
    }

    private void gotocreatenewshipment() {
        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        if (databaseHelper.getShipmentSystemBarcode().size() > 0 && databaseHelper.getAgentList().size() > 0) {
            if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                if (databaseHelper.getNotUploadedNewShipmentDetails().size() > 0) {
                    Intent intent = new Intent(getActivity(), NewShipmentDispatchActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    if (databaseHelper.getNotUploadedNewShipmentDetail().size() == 10) {
                        Utils.AlertDialog(getActivity(), "", "You can create only 10 shipments in offline mode, please upload it server first.");
                    } else {
                        Intent intent = new Intent(getActivity(), NewShipmentActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    }
                }
            } else {
                Log.d(TAG, "gotocreatenewshipment: " + databaseHelper.getNotUploadedNewShipmentDetail().size());
                if (databaseHelper.getInvoiceCodeListbyCount() > 0) {
                    if (databaseHelper.getNotUploadedNewShipmentDetails().size() > 0) {
                        Intent intent = new Intent(getActivity(), ProductListScanActivity.class);
                        intent.putExtra("tag", "dispatch");
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else if (databaseHelper.getListScanedShipment().size() > 0) {
                        Intent intent = new Intent(getActivity(), NewShipmentDispatchActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                    } else {
                        if (databaseHelper.getNotUploadedNewShipmentDetail().size() == 10) {
                            Utils.AlertDialog(getActivity(), "", "You can create only 10 shipments in offline mode, please upload it to server first.");
                        } else {
                            Intent intent = new Intent(getActivity(), NewShipmentActivity.class);
                            startActivity(intent);
                            getActivity().overridePendingTransition(0, 0);
                        }
                    }
                } else {
                    Utils.AlertDialog(getActivity(), "", "No invoice codes available. please sync server data first.");
                }

            }

        } else {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please sync server data first to proceed further.")
                    .setPositiveButton("Ok", null)
                    .show();
        }
    }


    //TODO Sync Server Data Starts here
    private void sync_server_data() {
        Log.d(TAG, "sync_server_data: " + databaseHelper.getNotUploadedDefRetShipments().size());
        if (databaseHelper.getNotUploadedDefRetShipments().size() > 0) {
            uploadDefRetOfflineShipmentLocationControl locationControl = new uploadDefRetOfflineShipmentLocationControl();
            locationControl.execute();
        } else {
            databaseHelper.deleteAllTableData();
            try {
                String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");

                if (!USER_TYPEs.equals(Constants.USER_TYPE_RETAILER)) {
                    JSONArray jsonArray = new JSONArray(Utils.getPreference(getContext(), PreferenceKeys.USER_SHIPMENT_TYPE, ""));
                    Log.d(TAG, "sync_server_data: " + jsonArray.toString());
                    Utils.showprogressdialog(getActivity(), getResources().getString(R.string.hint_sync_server_data));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        getAgencyList(jsonArray.getString(i));
                        if (i == jsonArray.length() - 1) {
                            if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                                getDefRetReceiveCodeList();
                            } else {
                                getAgencyList();
                            }
                        }
                    }
                } else {
                    Utils.showprogressdialog(getActivity(), getResources().getString(R.string.hint_sync_server_data));
                    getpackedcarton();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getDefCodeList() {

        retrofitClient.getService().defective_code_list(databaseHelper.getAuthToken()).enqueue(new Callback<def_return_code_respo>() {
            @Override
            public void onResponse(Call<def_return_code_respo> call, Response<def_return_code_respo> response) {
                Log.d(TAG, "onResponse getDefCodeList: " + response.toString());
                if (response.body().getSuccess()) {
                    ArrayList<def_codelist> def_codelistArrayList = response.body().getData();

                    if (def_codelistArrayList.size() > 0) {
                        for (int i = 0; i < def_codelistArrayList.size(); i++) {

                            databaseHelper.insertDefCodeList(def_codelistArrayList.get(i).getCode(), def_codelistArrayList.get(i).getProductName(),
                                    "1", "", "0");

                        }

                    }
                }
                getpackedcarton();
            }

            @Override
            public void onFailure(Call<def_return_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    //For New Shipment API Call & Local DB Start
    //TODO AgencyList API Call here
    public void getAgencyList(final String shipment_type) {
        Log.d(TAG, "getAgencyList shipment_type: " + shipment_type);
        agencyArrayList = new ArrayList<>();

        retrofitClient.getService().getAgencyList(databaseHelper.getAuthToken(), shipment_type).enqueue(new Callback<AgencyResponse>() {
            @Override
            public void onResponse(Call<AgencyResponse> call, Response<AgencyResponse> response) {

                Log.d(TAG, "onResponse getAgencyList: " + response.toString());
                boolean success = response.body().getSuccess();
                if (success) {
                    agencyArrayList = response.body().getAgencies();

                    if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_DISTRIBUTOR) || Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        for (int index = 0; index < agencyArrayList.size(); index++) {
                            databaseHelper.insertAgentList(new AgentListModel(
                                    String.valueOf(agencyArrayList.get(index).getId()),
                                    agencyArrayList.get(index).getName(),
                                    agencyArrayList.get(index).getEmail(),
                                    agencyArrayList.get(index).getMobile(),
                                    agencyArrayList.get(index).getCpPerson(),
                                    agencyArrayList.get(index).getLocation(),
                                    shipment_type));
                        }

                    } else {
                        for (int index = 0; index < agencyArrayList.size(); index++) {
                            databaseHelper.insertAgentList(new AgentListModel(
                                    String.valueOf(agencyArrayList.get(index).getId()),
                                    agencyArrayList.get(index).getName(),
                                    agencyArrayList.get(index).getCode(),
                                    agencyArrayList.get(index).getEmail(),
                                    agencyArrayList.get(index).getAddress(),
                                    agencyArrayList.get(index).getMobile(),
                                    agencyArrayList.get(index).getCpPerson(),
                                    shipment_type,
                                    agencyArrayList.get(index).getLocation()));
                        }

                    }


                }
            }

            @Override
            public void onFailure(Call<AgencyResponse> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO AgencyList API Call here
    public void getAgencyList() {

        agencyArrayList = new ArrayList<>();

        retrofitClient.getService().getAgencyList(databaseHelper.getAuthToken(), "Vendor").enqueue(new Callback<AgencyResponse>() {
            @Override
            public void onResponse(Call<AgencyResponse> call, Response<AgencyResponse> response) {
                Log.d(TAG, "onResponse getAgencyList Vendor: " + response.toString());
                if (response.body().getSuccess()) {
                    agencyArrayList = response.body().getAgencies();

                    if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_BRANCH)) {
                        for (int index = 0; index < agencyArrayList.size(); index++) {
                            databaseHelper.insertAgentList(new AgentListModel(
                                    String.valueOf(agencyArrayList.get(index).getId()),
                                    agencyArrayList.get(index).getName(),
                                    agencyArrayList.get(index).getEmail(),
                                    agencyArrayList.get(index).getMobile(),
                                    agencyArrayList.get(index).getCpPerson(),
                                    agencyArrayList.get(index).getLocation(),
                                    "Vendor"));
                        }
                    }
                }
                getDefCodeList();
            }

            @Override
            public void onFailure(Call<AgencyResponse> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(databaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }

                    }

                }

                String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");

                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_RETAILER) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else {
                    getinvoice_codes();
                }

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {
        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");

        retrofitClient.getService().getInvoiceCodes(databaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    databaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        databaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                }


                if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                    getBatchList();
                }
            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //For Packing API Call & Local DB Start
    //TODO Batchlist API Call here
    private void getBatchList() {
        batchIdListArrayList = new ArrayList<>();

        retrofitClient.getService().getUnpackedBatches(databaseHelper.getAuthToken()).enqueue(new Callback<unpacked_batch_respo>() {
            @Override
            public void onResponse(Call<unpacked_batch_respo> call, Response<unpacked_batch_respo> response) {
                Log.d(TAG, "onResponse getBatchList: " + response);
                if (response.body().getSuccess()) {

                    batchIdListArrayList = response.body().getBatchIdList();

                    if (batchIdListArrayList.size() > 0) {
                        for (int i = 0; i < batchIdListArrayList.size(); i++) {
                            databaseHelper.insertBatchList(batchIdListArrayList.get(i).getItemCode(),
                                    batchIdListArrayList.get(i).getModel(),
                                    String.valueOf(batchIdListArrayList.get(i).getSeprateBlade()));
                        }
                    }


                }


                ArrayList<BatchIdList> batchIdListArrayList = databaseHelper.getBatchList();
                if (batchIdListArrayList.size() == 0) {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), "Data synced successfully");
                    alert = false;
                    lost = false;
                } else {

                    LocationControl locationControlTask = new LocationControl();
                    locationControlTask.execute();
                }
                Utils.addPreference(getActivity(), "tag", "");

            }

            @Override
            public void onFailure(Call<unpacked_batch_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }

    //TODO CodeList/BladeCodeList API Call here
    private class LocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
        }

        protected String doInBackground(String... params) {
            getdataofpack();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String unused) {
            Utils.dismissprogressdialog();
            Utils.showToast(getActivity(), "Data synced successfully");
            alert = false;
            lost = false;

        }
    }

    private void getdataofpack() {
        ArrayList<BatchIdList> batchIdListArrayList = databaseHelper.getBatchList();

        for (int i = 0; i < batchIdListArrayList.size(); i++) {
            getcodeList(batchIdListArrayList.get(i).getItemCode(), batchIdListArrayList.get(i).getSeprateBlade());
        }
    }


    //TODO CodeList API Call here
    private void getcodeList(String item_code, Boolean seprateBlade) {
        QRcodeListArrayList = new ArrayList<>();
        MasterQRcodeListArrayList = new ArrayList<>();

        retrofitClient.getService().getcode_list(databaseHelper.getAuthToken(), item_code).enqueue(new Callback<code_list_respo>() {
            @Override
            public void onResponse(Call<code_list_respo> call, Response<code_list_respo> response) {

                Log.d(TAG, "onResponse getcodeList: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.insertPackingRatio(String.valueOf(response.body().getList().getPackingRatio()),
                            response.body().getList().getItemCode());

                    QRcodeListArrayList = response.body().getList().getQrcode();
                    databaseHelper.insertQRCode(new SystemBarcodeModel(new JSONArray(QRcodeListArrayList)), response.body().getList().getItemCode());

                    MasterQRcodeListArrayList = response.body().getList().getMasterCartonCode();
                    databaseHelper.insertMasterQrCode(new SystemBarcodeModel(new JSONArray(MasterQRcodeListArrayList)), response.body().getList().getItemCode());
                }

                if (seprateBlade) {
                    getblade_codelist(item_code);
                }
            }

            @Override
            public void onFailure(Call<code_list_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO BladeCodeList API Call here
    private void getblade_codelist(String item_code) {
        BladeListArrayList = new ArrayList<>();

        retrofitClient.getService().getbladecode_list(databaseHelper.getAuthToken(), item_code).enqueue(new Callback<bladecode_list_respo>() {
            @Override
            public void onResponse(Call<bladecode_list_respo> call, Response<bladecode_list_respo> response) {
                Log.d(TAG, "onResponse getblade_codelist: " + response.toString());
                if (response.body().getSuccess()) {
                    BladeListArrayList = response.body().getList();
                    databaseHelper.insertBladeCode(new SystemBarcodeModel(new JSONArray(BladeListArrayList)), item_code);
                }
            }

            @Override
            public void onFailure(Call<bladecode_list_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    //For Receive New Shipment API Call & Local DB Start
    //TODO Receive New Shipment Invoice & Master Carton List API Call here
    private void getReceiveNewShipmentList() {
        InvoiceCodeListArrayList = new ArrayList<>();
        k = 0;
        retrofitClient.getService().getReceiveNewShipmentCodes(databaseHelper.getAuthToken()).enqueue(new Callback<get_receive_shipment_respo>() {
            @Override
            public void onResponse(Call<get_receive_shipment_respo> call, Response<get_receive_shipment_respo> response) {

                Log.d(TAG, "onResponse getReceiveNewShipmentList: " + response.toString());
                if (response.body().getSuccess()) {
                    InvoiceCodeListArrayList = response.body().getInvoiceCodes();
                    receiveShipmentListArrayList = response.body().getList();
                    databaseHelper.deleteAllReceiveShipmentCode();
                    databaseHelper.deleteServerBarcodeDataTableData();
                    databaseHelper.deleteAllReceiveShipmentCodeData();
                    databaseHelper.deleteAllReceiveShipmentISMasterCartons();

                    if (receiveShipmentListArrayList.size() > 0) {
                        for (int i = 0; i < receiveShipmentListArrayList.size(); i++) {
                            databaseHelper.insertReceiveNewShipment(new NewShipmentDetailModel(String.valueOf(receiveShipmentListArrayList.get(i).getId()),
                                    String.valueOf(receiveShipmentListArrayList.get(i).getPackingRatio()), receiveShipmentListArrayList.get(i).getCode(),
                                    receiveShipmentListArrayList.get(i).getProductName(), receiveShipmentListArrayList.get(i).getSweep(),
                                    receiveShipmentListArrayList.get(i).getColor(), "1"));
                        }


                        JSONArray jsonArray1 = databaseHelper.getReceiveShipmentBarcode1().getJsonArray();

                        databaseHelper.insertMasterCartoncode(new SystemBarcodeModel(jsonArray1));

                    }
                }


                if (InvoiceCodeListArrayList.size() > 0) {
                    databaseHelper.inserRSInvoicecode(new SystemBarcodeModel(new JSONArray(InvoiceCodeListArrayList)));


                    for (int i = 0; i < InvoiceCodeListArrayList.size(); i++) {
                        ReceiveShipmentByInvoiceData(InvoiceCodeListArrayList.get(i));
                    }

                } else {
//                    Utils.dismissprogressdialog();
//                    Utils.showToast(getActivity(), "Data synced successfully");
//                    alert = false;
//                    lost = false;
                    CodesForRepacking();
                }

            }

            @Override
            public void onFailure(Call<get_receive_shipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Receive New Shipment Invoice API Call here
    private void ReceiveShipmentByInvoiceData(String invoice_code) {
        InvoiceCodeProductDataArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveShipmentByInvoice(databaseHelper.getAuthToken(), invoice_code).enqueue(new Callback<ReceiveShipmentInvoice_respo>() {
            @Override
            public void onResponse(Call<ReceiveShipmentInvoice_respo> call, Response<ReceiveShipmentInvoice_respo> response) {
                Log.d(TAG, "onResponse ReceiveShipmentByInvoiceData: " + response.toString());
                if (response.body().getSuccess()) {
                    k++;
                    InvoiceCodeProductDataArrayList = response.body().getList();

                    if (InvoiceCodeProductDataArrayList.size() > 0) {
                        for (int i = 0; i < InvoiceCodeProductDataArrayList.size(); i++) {
                            if (InvoiceCodeProductDataArrayList.get(i).getQuantity() == null) {
                            } else {
                                databaseHelper.inserRSInvoicecodeData(new NewShipmentDetailModel(invoice_code, InvoiceCodeProductDataArrayList.get(i).getModel(),
                                        "", "",
                                        String.valueOf(InvoiceCodeProductDataArrayList.get(i).getQuantity()), ""));
                                ArrayList<ReceiveShipmentInvoiceCodeList> codeArrayList = InvoiceCodeProductDataArrayList.get(i).getCodes();

                                for (int j = 0; j < codeArrayList.size(); j++) {
                                    databaseHelper.insertIS_MasterCartonCodes(InvoiceCodeProductDataArrayList.get(i).getModel(), invoice_code, codeArrayList.get(j).getCode(), String.valueOf(codeArrayList.get(j).getPackingRatio()));
                                }
                            }
                        }
                    }
                    int size = InvoiceCodeListArrayList.size();
                    if (k == size) {
//                        Utils.dismissprogressdialog();
//                        Utils.showToast(getActivity(), "Data synced successfully");
//                        alert = false;
//                        lost = false;

                        CodesForRepacking();
                    }


                }

            }

            @Override
            public void onFailure(Call<ReceiveShipmentInvoice_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    //TODO Repacking codelist API Call here
    private void CodesForRepacking() {
        QRcodeListArrayList = new ArrayList<>();

        retrofitClient.getService().repacking_code_list(databaseHelper.getAuthToken()).enqueue(new Callback<repacking_respo>() {
            @Override
            public void onResponse(Call<repacking_respo> call, Response<repacking_respo> response) {
                Log.d(TAG, "onResponse CodesForRepacking: " + response.toString());
                if (response.body().getSuccess()) {
                    for (int i = 0; i < response.body().getCode_list().size(); i++) {
                        databaseHelper.insertQRCodeRepack(response.body().getCode_list().get(i).getCode(),
                                response.body().getCode_list().get(i).getProduct_name());

                        databaseHelper.insertBatchList("1",
                                response.body().getCode_list().get(i).getProduct_name(),
                                String.valueOf(response.body().getCode_list().get(i).getSeparate_blades()));
                    }

                    databaseHelper.insertMasterQrCode(new SystemBarcodeModel(new JSONArray(response.body().getMaster_codes())), "");

                    for (int i = 0; i < response.body().getBlade_codes().size(); i++) {
                        databaseHelper.insertBladeCodeRepack(response.body().getBlade_codes().get(i).getCode(), response.body().getBlade_codes().get(i).getProduct_name());
                    }
                }
                Utils.dismissprogressdialog();
                Utils.showToast(getActivity(), "Data synced successfully");
                alert = false;
                lost = false;
            }

            @Override
            public void onFailure(Call<repacking_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    //TODO Upload Offline Shipment API Call here
    private void uploadOfflineNewShipmentData(String tag) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < arrOfflineshipmentModels.size(); i++) {
            NewShipment newShipment = arrOfflineshipmentModels.get(i);

            JSONObject objNewShipmentDetailss = new JSONObject();
            try {
                ArrayList<String> SacnnedBarcodeList = databaseHelper.getScannedMCCQRListforupload(newShipment.getInvoice_number(), "");
                objNewShipmentDetailss.put("AgencyId", newShipment.getAgency_id());
                objNewShipmentDetailss.put("Invoice_number", newShipment.getInvoice_number());
                objNewShipmentDetailss.put("Invoice_date", newShipment.getInvoice_date());
                objNewShipmentDetailss.put("PO_number", newShipment.getPo_number());
                objNewShipmentDetailss.put("PO_date", newShipment.getPo_date());
                objNewShipmentDetailss.put("quantity", newShipment.getQuantity());
                objNewShipmentDetailss.put("shipment_type", newShipment.getShipment_type());
                objNewShipmentDetailss.put("MasterCartonCode", new JSONArray(SacnnedBarcodeList));
                if (tag.equals(Constants.USER_TYPE_DISTRIBUTOR)) {
                } else {
                    objNewShipmentDetailss.put("InvoiceCode", databaseHelper.getScannedInvoiceQRListforUpload().get(i));
                }
                jsonArray.put(objNewShipmentDetailss);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


        Log.d(TAG, "uploadOfflineNewShipmentData jsonArray: " + jsonArray.toString());


        Utils.showprogressdialog(getActivity(), "Uploading offline send new shipment to server,please wait.");

        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        retrofitClient.getService().agency_dispatch_mapping(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
            @Override
            public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {
                Utils.dismissprogressdialog();

                if (response.body().getSuccess()) {
                    databaseHelper.deleteOfflineNewShipment("all");
                    databaseHelper.deleteScannedInvoiceCodeList("all");
                    Utils.showToast(getActivity(), "Send new shipment offline uploaded successfully");
                }

                if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    logDetails = databaseHelper.getLogDetails();
                    Log.d(TAG, "onResponse: " + logDetails.size());
                    Log.d(TAG, "onResponse: " + logDetails);
                    if (databaseHelper.getRSScannedInvoiceQRList().size() > 0 && databaseHelper.getIS_ScannedInvoiceQRList().size() > 0) {
                        uploadofflineCheckMasterCartonLocationControl checkMasterCartonLocationControl = new uploadofflineCheckMasterCartonLocationControl();
                        checkMasterCartonLocationControl.execute();
                    } else if (databaseHelper.getRSScannedInvoiceQRList().size() > 0) {
                        uploadofflineInvoiceScanLocationControl uploadofflineInvoiceScanLocationControl = new uploadofflineInvoiceScanLocationControl();
                        uploadofflineInvoiceScanLocationControl.execute();
                    } else if (logDetails.size() > 0) {
                        uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                        uploadofflineMasterCartonScanLocationControl.execute();
                    } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                        LocationControlRegular locationControl = new LocationControlRegular();
                        locationControl.execute();
                    } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                        LocationControlBreakage locationControl = new LocationControlBreakage();
                        locationControl.execute();
                    } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                        unpack_cartons();
                    } else {
                        sync_server_data();
                    }

                } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                    ScannedQrList = databaseHelper.getScannedQRList("1");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");

                    if (ScannedQrList.size() == 0) {
                        ScannedQrList = databaseHelper.getTempScannedQRList();
                    } else {
                        if (databaseHelper.getTempScannedQRList() != null)
                            ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                    }
                    if (ScannedBladeQrList.size() == 0) {
                        ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                    } else {
                        if (databaseHelper.getTempScannedBladeQRList() != null)
                            ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                    }
                    if (ScannedMasterQrList.size() == 0) {
                        ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("1");
                    } else {
                        if (databaseHelper.getTempScannedMasterQRListbyFlag("1") != null)
                            ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
                    }


                    if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                        uploadOfflinePacking();
                    } else if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                        receive_defective_codes1();
                    } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                        receive_sale_codes1();
                    } else {
                        sync_server_data();
                    }
                }


            }

            @Override
            public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Upload Offline Shipment API Call here
    private void uploadMultipleOfflineNewShipmentData(String tag) {
        JSONArray jsonArray = new JSONArray();
        Log.d(TAG, "uploadMultipleOfflineNewShipmentData arrOfflineshipmentModels.size(): " + arrOfflineshipmentModels.size());
        for (int i = 0; i < arrOfflineshipmentModels.size(); i++) {
            NewShipment newShipment = arrOfflineshipmentModels.get(i);

            JSONObject objNewShipmentDetailss = new JSONObject();
            try {
                ArrayList<String> SacnnedBarcodeList = databaseHelper.getScannedMCCQRListforupload(newShipment.getInvoice_number(), "");
                objNewShipmentDetailss.put("AgencyId", newShipment.getAgency_id());
                objNewShipmentDetailss.put("Invoice_number", newShipment.getInvoice_number());
                objNewShipmentDetailss.put("Invoice_date", newShipment.getInvoice_date());
                objNewShipmentDetailss.put("PO_number", newShipment.getPo_number());
                objNewShipmentDetailss.put("PO_date", newShipment.getPo_date());
                objNewShipmentDetailss.put("quantity", newShipment.getQuantity());
                objNewShipmentDetailss.put("shipment_type", newShipment.getShipment_type());
                objNewShipmentDetailss.put("MasterCartonCode", new JSONArray(SacnnedBarcodeList));
                if (tag.equals(Constants.USER_TYPE_DISTRIBUTOR)) {
                } else {
                    objNewShipmentDetailss.put("InvoiceCode", databaseHelper.getScannedInvoiceQRListforUpload().get(i));
                }
                jsonArray.put(objNewShipmentDetailss);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        Log.d(TAG, "uploadOfflineNewShipmentData jsonArray: " + jsonArray.toString());

        Utils.showprogressdialog(getActivity(), "Uploading offline send new shipment to server,please wait.");

        retrofitClient.getService().agency_dispatch_mapping(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
            @Override
            public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {
                if (response.body().getSuccess()) {
                    databaseHelper.deleteOfflineNewShipment("all");
                    databaseHelper.deleteScannedInvoiceCodeList("all");
                    Utils.showToast(getActivity(), "Send new shipment offline uploaded successfully");
                }
                getpackedcarton1();
            }

            @Override
            public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    //TODO Packed Carton API Call here
    private void getpackedcarton1() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(databaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }
                    }

                }
                String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");

                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    Utils.dismissprogressdialog();
                    gotocreatenewshipment();
                } else {
                    getinvoice_codes1();
                }

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes1() {
        retrofitClient.getService().getInvoiceCodes(databaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(TAG, "onResponse getinvoice_codes: " + response.toString());
                Utils.dismissprogressdialog();
                if (response.body().getSuccess()) {
                    databaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        databaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                }

                gotocreatenewshipment();

            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }
    //For New Shipment API Call & Local DB End

    //TODO Upload Offline Packing API Call here
    private void uploadOfflinePacking() {
        ScannedMasterQrJson = new JSONArray();
        for (int i = 0; i < ScannedMasterQrList.size(); i++) {
            ScannedMasterQrJson.put(ScannedMasterQrList.get(i));
        }

        JSONArray jsonArray = new JSONArray();
        try {

            for (int k = 0; k < ScannedMasterQrJson.length(); k++) {
                int res = 0;
                int res1 = 0;
                int packing_ratio = databaseHelper.getPackingRatio(databaseHelper.getItemCodes("1").get(k));
                boolean blade = databaseHelper.getBatchList(databaseHelper.getItemCodes("1").get(k));
                JSONObject jsonObject = new JSONObject();
                ScannedQrJson = new JSONArray();
                ScannedBladeQrJson = new JSONArray();
                jsonObject.put("MasterCartonCode", ScannedMasterQrJson.get(k));

                for (int j = res; j < (packing_ratio + res); j++) {
                    ScannedQrJson.put(ScannedQrList.get(j));

                }
                jsonObject.put("Qrcode", ScannedQrJson);
                for (int l = 0; l < ScannedQrJson.length(); l++) {
                    try {
                        ScannedQrList.remove(ScannedQrJson.get(l));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (blade) {
                    for (int j = res1; j < (packing_ratio + res1); j++) {
                        ScannedBladeQrJson.put(ScannedBladeQrList.get(j));
                    }
                    jsonObject.put("blade_codes", ScannedBladeQrJson);

                    for (int l = 0; l < ScannedBladeQrJson.length(); l++) {
                        try {
                            ScannedBladeQrList.remove(ScannedBladeQrJson.get(l));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    ScannedBladeQrJson = new JSONArray();
                    jsonObject.put("blade_codes", ScannedBladeQrJson);
                }
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "uploadOfflinePacking: " + jsonArray.toString());

        Utils.showprogressdialog(getActivity(), "Uploading offline packaging of products, please wait.");

        retrofitClient.getService().master_mapping(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
            @Override
            public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllScannedTableData();
                }

                if (databaseHelper.getScannedDefRetRecCodeList1("1", "1").size() > 0) {
                    receive_defective_codes1();
                } else if (databaseHelper.getScannedSaleRetRecCodeList1("1", "1").size() > 0) {
                    receive_sale_codes1();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }
            }

            @Override
            public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }
//For Packing API Call & Local DB End/

    //TODO Upload Receive shipment by invoice API Call here
    private class uploadofflineInvoiceScanLocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Receiving by invoice),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            uploadofflineInvoiceScan();
        }
    }

    private void uploadofflineInvoiceScan() {

        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getRSScannedInvoiceQRList()));
            if (jsonArray.length() == 1) {
                res = jsonArray.get(0).toString();
            } else {

                res = jsonArray.toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "submitDetailsToServer: " + res);
        Log.d(TAG, "submitDetailsToServer: " + strLatitude);
        Log.d(TAG, "submitDetailsToServer: " + strLongitude);
        Log.d(TAG, "submitDetailsToServer: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipment(databaseHelper.getAuthToken(), res, "Invoice", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {

                Log.d(TAG, "onResponse: " + response.body().getSuccess());
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllReceiveShipmentScannedCodeData();
                    Utils.showToast(getActivity(), "Shipment (Receiving by invoice) uploaded successfully");
                }

                logDetails = databaseHelper.getLogDetails();
                if (logDetails.size() > 0) {
                    uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                    uploadofflineMasterCartonScanLocationControl.execute();
                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                    LocationControlRegular locationControl = new LocationControlRegular();
                    locationControl.execute();
                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                    LocationControlBreakage locationControl = new LocationControlBreakage();
                    locationControl.execute();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Upload Receive shipment by invoice-check cartons API Call here
    private class uploadofflineCheckMasterCartonLocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Receiving by master cartons),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServer();
        }
    }

    private void submitDetailsToServer() {

        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getIS_ScannedInvoiceQRList()));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "submitDetailsToServer: " + res);
        Log.d(TAG, "submitDetailsToServer: " + strLatitude);
        Log.d(TAG, "submitDetailsToServer: " + strLongitude);
        Log.d(TAG, "submitDetailsToServer: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipment(databaseHelper.getAuthToken(), res, "MasterCarton", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {

                Log.d(TAG, "onResponse: " + response.body().getSuccess());
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllReceiveShipmentScannedCodeData();
                    databaseHelper.deleteAllReceiveShipmentISScannedMasterCartons();
                    Utils.showToast(getActivity(), "Shipment (Receiving by master cartons) uploaded successfully");
                }

                logDetails = databaseHelper.getLogDetails();
                checkforRepacking();
                if (logDetails.size() > 0) {
                    uploadofflineMasterCartonScanLocationControl uploadofflineMasterCartonScanLocationControl = new uploadofflineMasterCartonScanLocationControl();
                    uploadofflineMasterCartonScanLocationControl.execute();
                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                    LocationControlRegular locationControl = new LocationControlRegular();
                    locationControl.execute();
                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                    LocationControlBreakage locationControl = new LocationControlBreakage();
                    locationControl.execute();
                } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                    unpack_cartons();
                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                    uploadOfflineRePacking();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }
            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Upload Receive shipment by master cartons API Call here
    private class uploadofflineMasterCartonScanLocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Receiving by master cartons),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            uploadofflineMasterCartonScan();
        }
    }

    private void uploadofflineMasterCartonScan() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(databaseHelper.getScannedMasterCartonQRList("1").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "submitDetailsToServer: " + jsonArray.toString());
        Log.d(TAG, "onResume strLatitude: " + strLatitude);
        Log.d(TAG, "onResume strLongitude: " + strLongitude);
        Log.d(TAG, "onResume strAccuracy: " + strAccuracy);


        retrofitClient.getService().getReceiveNewShipment(databaseHelper.getAuthToken(), jsonArray.toString(), "MasterCarton", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllScannedMasterCartonCode("all");
                    databaseHelper.deleteAllLogs();
                    Utils.showToast(getActivity(), "Shipment (Receiving by master cartons) uploaded successfully");
                }
                checkforRepacking();
                if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                    LocationControlRegular locationControl = new LocationControlRegular();
                    locationControl.execute();
                } else if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                    LocationControlBreakage locationControl = new LocationControlBreakage();
                    locationControl.execute();
                } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                    unpack_cartons();
                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                    uploadOfflineRePacking();
                } else {
                    Utils.dismissprogressdialog();
                    sync_server_data();
                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Upload Receive shipment by individual products-regular API Call here
    private class LocationControlRegular extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment (Receiving by individual products-regular),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerRegular();
        }
    }

    private void submitDetailsToServerRegular() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getIPScannedQRCodeListRegular_Breakage("regular")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "submitDetailsToServerRegular: " + res);

        Log.d(TAG, "submitDetailsToServerRegular strLatitude: " + strLatitude);
        Log.d(TAG, "submitDetailsToServerRegular strLongitude: " + strLongitude);
        Log.d(TAG, "submitDetailsToServerRegular strAccuracy: " + strAccuracy);


        retrofitClient.getService().getReceiveNewShipmentDefective(databaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy, "0").enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                }
                checkforRepacking();
                if (databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                    LocationControlBreakage locationControl = new LocationControlBreakage();
                    locationControl.execute();
                } else if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                    unpack_cartons();
                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                    uploadOfflineRePacking();
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), "Shipment(Receiving by individual products) uploaded successfully");
                    databaseHelper.deleteAllScannedIPCode();
                    databaseHelper.deleteAllIPCode();
                    sync_server_data();

                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Upload Receive shipment by individual products-Breakage API Call here
    private class LocationControlBreakage extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(getActivity(), "Uploading offline shipment(Receiving by individual products-breakage),please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(getActivity());
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerBreakage();
        }

    }

    private void submitDetailsToServerBreakage() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(databaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "submitDetailsToServerBreakage: " + res);

        Log.d(TAG, "submitDetailsToServerBreakage strLatitude: " + strLatitude);
        Log.d(TAG, "submitDetailsToServerBreakage strLongitude: " + strLongitude);
        Log.d(TAG, "submitDetailsToServerBreakage strAccuracy: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipmentDefective(databaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy, "1").enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {


                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllScannedIPCode();
                    databaseHelper.deleteAllIPCode();
                    Utils.showToast(getActivity(), "Shipment (Receiving by individual products) uploaded successfully");
                }
                checkforRepacking();
                if (databaseHelper.getScannedForceCodeList("0", "unpack").size() > 0) {
                    unpack_cartons();
                } else if (ScannedQrList.size() > 0 && ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                    uploadOfflineRePacking();
                } else {
                    sync_server_data();
                }

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    private void showNetworkFailDialog(String msg) {

        builder = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    //TODO Upload Offline RePacking API Call here
    private void uploadOfflineRePacking() {


        ScannedMasterQrJson = new JSONArray();
        for (int i = 0; i < ScannedMasterQrList.size(); i++) {
            ScannedMasterQrJson.put(ScannedMasterQrList.get(i));
        }

        JSONArray jsonArray = new JSONArray();
        try {

            for (int k = 0; k < ScannedMasterQrJson.length(); k++) {
                int res = 0;
                int res1 = 0;
                int packing_ratio = databaseHelper.getPackingRatioRepack(databaseHelper.getItemCodes("1").get(k), "1");
                Log.d(TAG, "uploadOfflineRePacking packing_ratio : " + packing_ratio);
                boolean blade = databaseHelper.getBatchList(databaseHelper.getItemCodes("1").get(k));
                JSONObject jsonObject = new JSONObject();
                ScannedQrJson = new JSONArray();
                ScannedBladeQrJson = new JSONArray();
                jsonObject.put("master_carton", ScannedMasterQrJson.get(k));

                for (int j = res; j < (packing_ratio + res); j++) {
                    Log.d("suraj", "callMasterCartonAPI j loop: " + j);
                    ScannedQrJson.put(ScannedQrList.get(j));

                }
                jsonObject.put("qr_codes", ScannedQrJson);
                for (int l = 0; l < ScannedQrJson.length(); l++) {
                    try {
                        ScannedQrList.remove(ScannedQrJson.get(l));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (blade) {
                    for (int j = res1; j < (packing_ratio + res1); j++) {
                        ScannedBladeQrJson.put(ScannedBladeQrList.get(j));
                    }
                    jsonObject.put("blade_codes", ScannedBladeQrJson);

                    for (int l = 0; l < ScannedBladeQrJson.length(); l++) {
                        try {
                            ScannedBladeQrList.remove(ScannedBladeQrJson.get(l));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    ScannedBladeQrJson = new JSONArray();
                    jsonObject.put("blade_codes", ScannedBladeQrJson);
                }
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "uploadOfflinePacking: " + jsonArray.toString());

        Utils.showprogressdialog(getActivity(), "Uploading offline repackaging of products, please wait.");

        retrofitClient.getService().repacking(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
            @Override
            public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    databaseHelper.deleteAllScannedTableData();
                }
                Utils.dismissprogressdialog();
                sync_server_data();
            }

            @Override
            public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

}
