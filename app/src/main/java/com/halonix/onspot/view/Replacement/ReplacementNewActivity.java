package com.halonix.onspot.view.Replacement;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class ReplacementNewActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvOldCode)
    AppCompatTextView tvOldCode;

    @BindView(R.id.tvNewCode)
    AppCompatTextView tvNewCode;

    @BindView(R.id.btnFinish)
    AppCompatButton btnFinish;

    String myFormat = "dd-MM-yyyy";
    private long mLastClickTime = 0;
    private String tag = "";
    ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;
    DatabaseHelper databaseHelper;
    private AlertDialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_replacement);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(ReplacementNewActivity.this);
        databaseHelper = new DatabaseHelper(ReplacementNewActivity.this);
        if (getIntent().getExtras() != null) {
            tag = getIntent().getStringExtra("tag");
        }
        String old_qr = Utils.getPreference(ReplacementNewActivity.this, "old_qr", "");
        String new_qr = Utils.getPreference(ReplacementNewActivity.this, "new_qr", "");

        tvOldCode.setText(old_qr);
        tvNewCode.setText(new_qr);
        retrofitClient = new RetrofitClient();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });
        toolbar.setTitle("Replacement");

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (cd.isConnectingToInternet()) {
                    replacement();
                } else {
                    showNetworkFailDialog();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (Utils.getPreference(ReplacementNewActivity.this, "new_qr", "").length() > 0) {
            new AlertDialog.Builder(ReplacementNewActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.removePreference(ReplacementNewActivity.this, "old_qr");
                            Utils.removePreference(ReplacementNewActivity.this, "new_qr");
                            Intent intent = new Intent(ReplacementNewActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            ReplacementNewActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    private void replacement() {
        String old_qr = Utils.getPreference(ReplacementNewActivity.this, "old_qr", "");
        String new_qr = Utils.getPreference(ReplacementNewActivity.this, "new_qr", "");

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonObject.put("old_code", old_qr);
            jsonObject.put("new_code", new_qr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonArray.put(jsonObject);
        Log.d(Constants.TAG, "replacement: " + jsonArray.toString());

        Utils.showprogressdialog(ReplacementNewActivity.this, "Requesting for replacement,please wait.");

        retrofitClient.getService().replacement_start_warranty(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {

                if (response.body().getSuccess()) {
                    getpackedcarton();
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(ReplacementNewActivity.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    ArrayList<PackedCartonList> packedcartonListArrayList;

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(databaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    databaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                databaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }

                    }

                }

                Utils.dismissprogressdialog();
                Intent intent = new Intent(ReplacementNewActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                ReplacementNewActivity.this.finish();
                overridePendingTransition(0, 0);
                Utils.showToast(ReplacementNewActivity.this, "Replacement done successfully.");

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(ReplacementNewActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }


}
