package com.halonix.onspot.view.ReceiveNewShipment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.master_carton_pojo;
import com.halonix.onspot.utils.Constants;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class RSByInvoiceMasterCartonScannerActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener {


    private Activity activity = null;
    private CaptureManager capture;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;

    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnGo)
    Button btnGo;

    @BindView(R.id.textInput)
    TextInputLayout textInput;


    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private BeepManager beepManager = null;
    private ArrayList<String> systemBarcode = new ArrayList<>();
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;
    JSONArray systemBarcodeJsonArray;
    private AlertDialog.Builder alert = null;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_individual_shipment_scan);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });
        toolbar.setTitle("Scan Master Cartons");

        activity = RSByInvoiceMasterCartonScannerActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);
        textInput.setHint("Master Carton QR Code");

        systemBarcodeJsonArray = new JSONArray();
        ArrayList<master_carton_pojo> stringArrayList = objDatabaseHelper.getIS_MasterCartonCodes();
        for (int i = 0; i < stringArrayList.size(); i++) {
            systemBarcodeJsonArray.put(stringArrayList.get(i).getCode());
        }


        Log.d(Constants.TAG, "onCreate: " + systemBarcodeJsonArray.toString());

        for (int i = 0; i < systemBarcodeJsonArray.length(); i++) {
            try {
                systemBarcode.add(String.valueOf(systemBarcodeJsonArray.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);

        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    capture.onPause();
                    return;
                } else {
                    String[] final_barcode;
                    if (result.getText().contains("=")) {
                        final_barcode = result.getText().split("=");
                        String barCode1 = final_barcode[1].replaceAll("\u0000", "");

                        checkBarcode(barCode1, "camera");
                    } else {
                        checkBarcode(result.getText(), "camera");
                    }
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //capture.onResume();
                    }
                }, 3000);
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });


        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().equals("")) {
                    checkBarcode(edScanCode.getText().toString().trim(), "manual");
                } else {
                    Toast.makeText(RSByInvoiceMasterCartonScannerActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });


    }

    private void checkBarcode(String barcode, String tag) {
        if (isBarcodeExistsInSystem(barcode)) {
            if (!isInvoiceQrExists(barcode)) {
                if (!tag.equals("manual")) {
                    capture.onResume();
                } else {
                    clearEditScan();
                }
                String p_name = objDatabaseHelper.getIS_MasterCartonCodeSingle(barcode);
                String packing_ratio = objDatabaseHelper.getIS_MasterCartonCodePkgRatioSingle(barcode);
                objDatabaseHelper.insertIS_ScannedMasterCartonCodes(p_name, barcode, "", packing_ratio);

                if (systemBarcode.size() == objDatabaseHelper.getIS_ScannedMasterCartonCount()) {
                    Intent intent = new Intent(RSByInvoiceMasterCartonScannerActivity.this, RSByInvoiceCheckCartonsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    RSByInvoiceMasterCartonScannerActivity.this.finish();
                    overridePendingTransition(0, 0);
                }
            } else {
                showAlert("Alert", "This Master carton Qr-code is already scanned.", tag, false);
            }

        } else {

            showAlert("Alert", "This Qr-code is invalid.", tag, true);

        }
    }

    private void showAlert(String Title, String Message, String tag, boolean valid) {

        if (tag.equals("camera")) {
            capture.onPause();
        }

        alert = new AlertDialog.Builder(RSByInvoiceMasterCartonScannerActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert = null;
            }
        });
        alert.show();
    }


    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }

    private boolean isBarcodeExistsInSystem(String strBarcode) {
        return systemBarcode.contains(strBarcode);
    }


    private boolean isInvoiceQrExists(String barcode) {
        return objDatabaseHelper.getIS_ScannedMasterCartonCodeList(barcode);
    }


    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }

    @Override
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }

    public void stopScanning(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RSByInvoiceMasterCartonScannerActivity.this, RSByInvoiceCheckCartonsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        RSByInvoiceMasterCartonScannerActivity.this.finish();
        overridePendingTransition(0, 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null) {
            capture.onResume();
        }
        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);
            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);

        } else {
            focusable(true);
//          btnGo.setEnabled(true);

            relOTG.setVisibility(View.GONE);
            radio_otg.setChecked(false);

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setChecked(true);
            radio_camera.setEnabled(true);

        }


//        if (btnGo.isEnabled())
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));
//        else
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));
    }

    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

//            btnGo.setEnabled(false);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(RSByInvoiceMasterCartonScannerActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            focusable(true);

//            btnGo.setEnabled(true);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);
            relCamera.setVisibility(View.VISIBLE);
            capture.onResume();

            hideKeyboard(RSByInvoiceMasterCartonScannerActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            focusable(false);
            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void barcodeLookup(String barCode) {

        String[] final_barcode;
        if (barCode.contains("=")) {
            final_barcode = barCode.split("=");
            barCode = final_barcode[1].replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            checkBarcode(barCode, "OTG");
        } else {
            barCode = barCode.replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            checkBarcode(barCode, "OTG");
        }
    }
    //OTG Code End

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}

