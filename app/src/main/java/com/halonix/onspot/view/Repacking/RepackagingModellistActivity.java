package com.halonix.onspot.view.Repacking;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.BatchIdList;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class RepackagingModellistActivity extends AppCompatActivity {

    @BindView(R.id.rvBatchlist)
    RecyclerView rvBatchlist;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    ArrayList<BatchIdList> batchIdListArrayList;
    DatabaseHelper databaseHelper;
    private long mLastClickTime = 0;

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(RepackagingModellistActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_model_list);
        ButterKnife.bind(this);
        databaseHelper = DatabaseHelper.getInstance(RepackagingModellistActivity.this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                Intent intent = new Intent(RepackagingModellistActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        toolbar.setTitle("RePack Products");

        rvBatchlist.setHasFixedSize(true);
        rvBatchlist.setItemAnimator(new DefaultItemAnimator());
        rvBatchlist.setLayoutManager(new LinearLayoutManager(RepackagingModellistActivity.this));
        batchIdListArrayList = databaseHelper.getBatchListForRepacking();
        if (batchIdListArrayList.size() > 0) {
            linHeader.setVisibility(View.VISIBLE);
        } else {
            linHeader.setVisibility(View.GONE);
        }

        BatchListAdapter batchListAdapter = new BatchListAdapter(batchIdListArrayList);
        rvBatchlist.setAdapter(batchListAdapter);
    }

    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<BatchIdList> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView tvBatch;
            private AppCompatTextView tvQty;
            private AppCompatTextView tvProcess;

            public MyViewHolder(View view) {
                super(view);
                tvBatch = view.findViewById(R.id.tvBatch);
                tvQty = view.findViewById(R.id.tvQty);
                tvProcess = view.findViewById(R.id.tvProcess);
            }
        }


        public BatchListAdapter(ArrayList<BatchIdList> notesList) {
            this.notesList = notesList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_model_row, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final BatchIdList object = notesList.get(position);

            holder.tvBatch.setText(object.getModel());
            holder.tvQty.setText(object.getItemCode());

            holder.tvProcess.setOnClickListener(view -> {

                if (Integer.parseInt(object.getItemCode()) < 2) {
                    Utils.showToast(RepackagingModellistActivity.this, "Repacking of product requires at least quantity of 2.");
                } else {

                    ArrayList<String> qrcodeArrayList = new ArrayList<>();
                    ArrayList<String> bladecodeArrayList = new ArrayList<>();
                    ArrayList<String> masterqrcodeArrayList = new ArrayList<>();
                    try {
                        qrcodeArrayList = databaseHelper.getQRCodeListRepack(object.getModel());
                        bladecodeArrayList = databaseHelper.getBladeCodeListRepack(object.getModel());
                        JSONArray MasterQRjsonArray = databaseHelper.getMasterQRCodeListRepack().getJsonArray();
                        for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                            try {
                                masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    ArrayList<String> ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");

                    ArrayList<String> union = new ArrayList<>(masterqrcodeArrayList);
                    union.addAll(ScannedMasterQrList);
                    ArrayList<String> intersection = new ArrayList<>(masterqrcodeArrayList);
                    intersection.retainAll(ScannedMasterQrList);
                    union.removeAll(intersection);

                    masterqrcodeArrayList = new ArrayList<>();
                    masterqrcodeArrayList.addAll(union);

                    Log.d("suraj", "onBindViewHolder  object.getSeprateBlade(): " + object.getSeprateBlade());
                    if (object.getSeprateBlade()) {
                        if (qrcodeArrayList.size() == bladecodeArrayList.size()) {
                            if (masterqrcodeArrayList.size() > 0) {
                                Utils.removePreference(RepackagingModellistActivity.this, "item_code");
                                Utils.removePreference(RepackagingModellistActivity.this, "blade");
                                Utils.removePreference(RepackagingModellistActivity.this, "model");

                                Utils.addPreference(RepackagingModellistActivity.this, "item_code", object.getModel());
                                Utils.addPreferenceBoolean(RepackagingModellistActivity.this, "blade", object.getSeprateBlade());
                                Utils.addPreference(RepackagingModellistActivity.this, "model", object.getModel());

                                Intent intent = new Intent(RepackagingModellistActivity.this, RepackProductsMainActivity.class);
                                intent.putExtra("qty", Integer.parseInt(object.getItemCode()));
                                intent.putExtra("qt", object.getItemCode());
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            } else {
                                Utils.showToast(RepackagingModellistActivity.this, "Master carton codes are not available.");
                            }

                        } else {
                            Utils.showToast(RepackagingModellistActivity.this, "Blade codes are not available for this model.");
                        }
                    } else {
                        if (masterqrcodeArrayList.size() > 0) {
                            Utils.removePreference(RepackagingModellistActivity.this, "item_code");
                            Utils.removePreference(RepackagingModellistActivity.this, "blade");
                            Utils.removePreference(RepackagingModellistActivity.this, "model");

                            Utils.addPreference(RepackagingModellistActivity.this, "item_code", object.getModel());
                            Utils.addPreferenceBoolean(RepackagingModellistActivity.this, "blade", object.getSeprateBlade());
                            Utils.addPreference(RepackagingModellistActivity.this, "model", object.getModel());

                            Intent intent = new Intent(RepackagingModellistActivity.this, RepackProductsMainActivity.class);
                            intent.putExtra("qty", Integer.parseInt(object.getItemCode()));
                            intent.putExtra("qt", object.getItemCode());
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        } else {
                            Utils.showToast(RepackagingModellistActivity.this, "Master carton codes are not available.");
                        }

                    }

                }


            });

        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
