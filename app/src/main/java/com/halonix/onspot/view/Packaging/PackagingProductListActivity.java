package com.halonix.onspot.view.Packaging;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.Singleton.ProductListScanActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

;

public class PackagingProductListActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btnScan)
    Button btnScan;

    @BindView(R.id.button_done)
    Button button_done;

    @BindView(R.id.button_discard_all)
    Button button_discard_all;

    @BindView(R.id.button_packnext)
    Button button_packnext;

    @BindView(R.id.ly_scan_system)
    RelativeLayout ly_scan_system;

    @BindView(R.id.etMasterCarton)
    EditText etMasterCarton;

    @BindView(R.id.tvBlade)
    TextView tvBlade;

    @BindView(R.id.tvCartonCnt)
    TextView tvCartonCnt;

    @BindView(R.id.tvBatch)
    TextView tvBatch;

    @BindView(R.id.rvProductList)
    RecyclerView rvProductList;

    private boolean blade;
    private String item_code;
    RetrofitClient retrofitClient;
    DatabaseHelper databaseHelper;

    private int packing_ratio;
    private ArrayList<String> ScannedQrList = new ArrayList<>();
    private ArrayList<String> ScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> ScannedMasterQrList = new ArrayList<>();

    private ArrayList<String> tempScannedQrList = new ArrayList<>();
    private ArrayList<String> tempScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> tempScannedMasterQrList = new ArrayList<>();

    private ArrayList<String> ScannedMasterQrListCnt = new ArrayList<>();

    JSONArray ScannedMasterQrJson;

    private String model;
    ConnectionDetectorActivity cd;
    ArrayList<String> masterqrcodeArrayList;
    private long mLastClickTime = 0;
    private JSONArray ScannedQrJson;
    private JSONArray ScannedBladeQrJson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        retrofitClient = new RetrofitClient();
        toolbar.setTitle(getResources().getString(R.string.pack_products));
        databaseHelper = DatabaseHelper.getInstance(PackagingProductListActivity.this);
        cd = new ConnectionDetectorActivity(PackagingProductListActivity.this);

        rvProductList.setHasFixedSize(true);
        rvProductList.setItemAnimator(new DefaultItemAnimator());
        rvProductList.setLayoutManager(new LinearLayoutManager(PackagingProductListActivity.this));


        button_discard_all.setOnClickListener(this);
        button_done.setOnClickListener(this);
        button_packnext.setOnClickListener(this);
        btnScan.setOnClickListener(this);


        item_code = Utils.getPreference(PackagingProductListActivity.this, "item_code", "");
        blade = Utils.getPreferenceBoolean(PackagingProductListActivity.this, "blade", false);
        model = Utils.getPreference(PackagingProductListActivity.this, "model", "");
        Log.d(Constants.TAG, "onCreate: " + item_code);
        tvBatch.setText(model);

        packing_ratio = databaseHelper.getPackingRatio(item_code);

        Log.d("suraj", "onCreate packing_ratio: " + packing_ratio);
        masterqrcodeArrayList = new ArrayList<>();

        if (databaseHelper.getMasterQRCodeList(item_code) != null) {
            JSONArray MasterQRjsonArray = databaseHelper.getMasterQRCodeList(item_code).getJsonArray();
            for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                try {
                    masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        etMasterCarton.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etMasterCarton.setCursorVisible(true);
                etMasterCarton.setFocusable(true);
                etMasterCarton.setFocusableInTouchMode(true);
                if (s.toString().length() == 0) {
                    button_done.setVisibility(View.GONE);
                    button_packnext.setVisibility(View.GONE);
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                    ly_scan_system.setVisibility(View.VISIBLE);
                }

            }
        });



/*
        List views = new ArrayList();
        LayoutInflater layoutInflater = (LayoutInflater)
                this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < packing_ratio; i++) {
            View view = layoutInflater.inflate(R.layout.content_layout, null);
            TextView textView = view.findViewById(R.id.tvSrNo);
            TextView tvBlade = view.findViewById(R.id.tvBlade);
            textView.setText(String.valueOf(i + 1));
            if (blade) {
                tvBlade.setVisibility(View.VISIBLE);
            }
            // view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            views.add(view);
        }
        for (int i = 0; i < views.size(); i++)
            linRow.addView((View) views.get(i));*/

    }

    private void showAlert(String Title, String Message, boolean valid) {
        AlertDialog.Builder alert = new AlertDialog.Builder(PackagingProductListActivity.this);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
            }
        });
        alert.show();
    }

    private void clearEditScan() {
        if (etMasterCarton.getText().toString().trim().length() > 0) {
            etMasterCarton.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showPackedCnt();

        packing_ratio = databaseHelper.getPackingRatio(Utils.getPreference(PackagingProductListActivity.this, "item_code", ""));

        tempScannedQrList = databaseHelper.getTempScannedQRList();
        tempScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
        tempScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");

        Log.d(Constants.TAG, "onResume tempScannedQrList: " + tempScannedQrList.toString());
        Log.d(Constants.TAG, "onResume tempScannedBladeQrList: " + tempScannedBladeQrList.toString());
        Log.d(Constants.TAG, "onResume tempScannedMasterQrList: " + tempScannedMasterQrList.toString());

        if (tempScannedMasterQrList.size() > 0) {
            etMasterCarton.setText(tempScannedMasterQrList.get(tempScannedMasterQrList.size() - 1));
            etMasterCarton.setFocusable(false);
        }
        blade = Utils.getPreferenceBoolean(PackagingProductListActivity.this, "blade", false);


        if (blade) {
            tvBlade.setVisibility(View.VISIBLE);

            if (tempScannedQrList.size() > 0) {
                ly_scan_system.setVisibility(View.VISIBLE);
            }

            if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio) {


                if (tempScannedMasterQrList.size() > 0) {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                } else {
                    etMasterCarton.setFocusable(true);
                    etMasterCarton.setFocusableInTouchMode(true);
                    etMasterCarton.setCursorVisible(true);
                }

            } else {

                if (tempScannedQrList.size() == 0 || tempScannedBladeQrList.size() == 0) {
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);
                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(PackagingProductListActivity.this, "Please complete scan of ( product qr or blade qr code ) first.");
                        }
                    });
                }

            }
        } else {

            if (tempScannedQrList.size() > 0) {
                ly_scan_system.setVisibility(View.VISIBLE);
            }

            if (tempScannedQrList.size() == packing_ratio) {


                if (tempScannedMasterQrList.size() > 0) {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                } else {
                    etMasterCarton.setFocusable(true);
                    etMasterCarton.setFocusableInTouchMode(true);
                    etMasterCarton.setCursorVisible(true);
                }

            } else {
                if (tempScannedQrList.size() == 0) {
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);
                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(PackagingProductListActivity.this, "Please complete scan of product qr code first.");
                        }
                    });
                }

            }
        }
        BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
        rvProductList.setAdapter(batchListAdapter);

    }

    private void showPackedCnt() {
        ScannedMasterQrListCnt = databaseHelper.getScannedMasterQRList("0");
        if (ScannedMasterQrListCnt.size() > 0) {
            tvCartonCnt.setVisibility(View.VISIBLE);
            ly_scan_system.setVisibility(View.VISIBLE);
            button_packnext.setVisibility(View.VISIBLE);
            button_done.setVisibility(View.VISIBLE);
            if (ScannedMasterQrListCnt.size() == 1) {
                tvCartonCnt.setText("Total Packed Carton: " + ScannedMasterQrListCnt.size());
            } else {
                tvCartonCnt.setText("Total Packed Cartons: " + ScannedMasterQrListCnt.size());
            }
        } else {
            tvCartonCnt.setVisibility(View.GONE);

        }
    }

    @Override
    public void onBackPressed() {

        ScannedQrList = databaseHelper.getScannedQRList("0");
        if (ScannedQrList.size() == 0) {
            ScannedQrList = databaseHelper.getTempScannedQRList();
        }
        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");
        if (ScannedBladeQrList.size() == 0) {
            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
        }
        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");
        if (ScannedMasterQrList.size() == 0) {
            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
        }

        if (ScannedQrList.size() > 0 || ScannedBladeQrList.size() > 0 || ScannedMasterQrList.size() > 0) {
            new AlertDialog.Builder(PackagingProductListActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseHelper.deleteAllScannedTableData();
                            Intent intent = new Intent(PackagingProductListActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            PackagingProductListActivity.this.finish();
                            overridePendingTransition(0, 0);

                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    private boolean isBarcodeMasterQrExistsInSystem(String strBarcode) {
        masterqrcodeArrayList = new ArrayList<>();
        item_code = Utils.getPreference(PackagingProductListActivity.this, "item_code", "");
        if (databaseHelper.getMasterQRCodeList(item_code) != null) {
            JSONArray MasterQRjsonArray = databaseHelper.getMasterQRCodeList(item_code).getJsonArray();
            for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                try {
                    masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return masterqrcodeArrayList.contains(strBarcode);
    }

    private boolean isMasterBarcodeExists(String barcode, String item_code) {
        return databaseHelper.getTempScannedMasterQRList(barcode, item_code);
    }


    //TODO Master carton mapping API Call here
    private void setMapMasterCartonCode() {
        item_code = Utils.getPreference(PackagingProductListActivity.this, "item_code", "");

        if (tempScannedQrList.size() == 0) {
            databaseHelper.updateTempScannedMasterQRList(item_code, 1);
            databaseHelper.updateScannedMasterQRList(item_code, 1);
            databaseHelper.updateScannedQRList(item_code, 1);
            databaseHelper.updateScannedBladeQRList(item_code, 1);
            databaseHelper.updateScannedItemCode(item_code, 1);
            callMasterCartonAPI("1");
            return;
        } else if (tempScannedQrList.size() == packing_ratio && !blade) {

            if (!etMasterCarton.getText().toString().trim().equals("")) {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                        //ScannedMasterQrList.add(etMasterCarton.getText().toString());
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("2");
                        return;
                    } else {
                        databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("3");
                        return;
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            } else {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            }

        } else if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio && blade) {

            if (!etMasterCarton.getText().toString().trim().equals("")) {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("4");
                        return;
                    } else {
                        databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("5");
                        return;
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            } else {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            }
        } else {
            if (tempScannedQrList.size() > 0 && !blade) {
                Utils.showToast(PackagingProductListActivity.this, "Please scan product Qr-code first for next packaging.");
            } else if (tempScannedQrList.size() > 0 || tempScannedBladeQrList.size() > 0 && blade) {
                Utils.showToast(PackagingProductListActivity.this, "Please scan product or blade Qr-code first  for next packaging.");

            } else {
                if (!etMasterCarton.getText().toString().trim().equals("")) {
                    if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                        if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                            databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                            databaseHelper.updateScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedQRList(item_code, 1);
                            databaseHelper.updateScannedBladeQRList(item_code, 1);
                            databaseHelper.updateScannedItemCode(item_code, 1);
                            callMasterCartonAPI("6");
                            return;
                        } else {
                            databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedQRList(item_code, 1);
                            databaseHelper.updateScannedBladeQRList(item_code, 1);
                            databaseHelper.updateScannedItemCode(item_code, 1);
                            callMasterCartonAPI("7");
                            return;
                        }
                    } else {
                        showAlert("Alert", "This Qr-code is invalid.", true);
                    }
                } else {
                    showAlert("Alert", "Please enter Master carton Qr-code.", true);
                }
            }

        }


    }

    private void callMasterCartonAPI(String tag) {
        Log.d(Constants.TAG, "callMasterCartonAPI tag: " + tag);
        item_code = Utils.getPreference(PackagingProductListActivity.this, "item_code", "");

        if (databaseHelper.getTempScannedMasterQRListbyFlag("1").size() > 0) {
            ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList if: " + ScannedMasterQrList.toString());
            ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList if: " + ScannedMasterQrList.toString());
        } else {
            ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");

            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList else: " + ScannedMasterQrList.toString());

        }
        Log.d(Constants.TAG, "setMapMasterCartonCode databaseHelper.getItemCodes.size(): " + databaseHelper.getItemCodes("2").size());
        if (ScannedMasterQrList.size() == databaseHelper.getItemCodes("2").size()) {
            Log.d(Constants.TAG, "setMapMasterCartonCode getItemCodes if: ");
            if (databaseHelper.getItemCodes("2").size() == 0) {
                databaseHelper.insertItemCode(item_code, "1");
            }
        } else {
            Log.d(Constants.TAG, "setMapMasterCartonCode getItemCodes else: ");
            databaseHelper.insertItemCode(item_code, "1");
        }


        ScannedMasterQrJson = new JSONArray();
        for (int i = 0; i < ScannedMasterQrList.size(); i++) {
            ScannedMasterQrJson.put(ScannedMasterQrList.get(i));
        }


        if (databaseHelper.getTempScannedQRList().size() > 0) {
            ScannedQrList = databaseHelper.getScannedQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList if: " + ScannedQrList.toString());
            ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList if: " + ScannedQrList.toString());
        } else {
            ScannedQrList = databaseHelper.getScannedQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList else: " + ScannedQrList.toString());
        }


        if (databaseHelper.getTempScannedBladeQRList().size() > 0) {
            ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI  ScannedBladeQrList if: " + ScannedBladeQrList.toString());
            ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedBladeQrList if: " + ScannedBladeQrList.toString());
        } else {
            ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedBladeQrList else: " + ScannedBladeQrList.toString());
        }

        Log.d(Constants.TAG, "uploadOfflinePacking databaseHelper.getItemCodes(): " + databaseHelper.getItemCodes("1"));

        JSONArray jsonArray = new JSONArray();
        try {

            for (int k = 0; k < ScannedMasterQrJson.length(); k++) {
                int res = 0;
                int res1 = 0;
                int packing_ratio = databaseHelper.getPackingRatio(databaseHelper.getItemCodes("1").get(k));
                boolean blade = databaseHelper.getBatchList(databaseHelper.getItemCodes("1").get(k));
                JSONObject jsonObject = new JSONObject();
                ScannedQrJson = new JSONArray();
                ScannedBladeQrJson = new JSONArray();
                jsonObject.put("MasterCartonCode", ScannedMasterQrJson.get(k));

                for (int j = res; j < (packing_ratio + res); j++) {
                    ScannedQrJson.put(ScannedQrList.get(j));

                }

                jsonObject.put("Qrcode", ScannedQrJson);
                for (int l = 0; l < ScannedQrJson.length(); l++) {
                    try {
                        ScannedQrList.remove(ScannedQrJson.get(l));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (blade) {
                    for (int j = res1; j < (packing_ratio + res1); j++) {
                        ScannedBladeQrJson.put(ScannedBladeQrList.get(j));
                    }
                    jsonObject.put("blade_codes", ScannedBladeQrJson);

                    for (int l = 0; l < ScannedBladeQrJson.length(); l++) {
                        try {
                            ScannedBladeQrList.remove(ScannedBladeQrJson.get(l));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    ScannedBladeQrJson = new JSONArray();
                    jsonObject.put("blade_codes", ScannedBladeQrJson);
                }
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (cd.isConnectingToInternet()) {
            Utils.showprogressdialog(PackagingProductListActivity.this, "Requesting for packaging of products, please wait");

            retrofitClient.getService().master_mapping(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
                @Override
                public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {

                    if (response.body().getSuccess()) {
                        String msg;
                        Utils.dismissprogressdialog();
                        databaseHelper.deleteAllScannedTableData();
                        databaseHelper.deleteAllpackingdata();
                        if (ScannedMasterQrList.size() == 1) {
                            msg = "You have successfully packed " + ScannedMasterQrList.size() + " master carton.";
                        } else {
                            msg = "You have successfully packed " + ScannedMasterQrList.size() + " master cartons.";
                        }
                        new AlertDialog.Builder(PackagingProductListActivity.this)
                                .setTitle("Success")
                                .setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Utils.addPreference(PackagingProductListActivity.this, "tag", "pack");
                                        Intent intent = new Intent(PackagingProductListActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        PackagingProductListActivity.this.finish();

                                    }
                                }).show();
                    } else {
                        Utils.showToast(PackagingProductListActivity.this, response.body().getMessage());
                    }
                    Utils.dismissprogressdialog();
                }

                @Override
                public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });
        } else {
            Utils.showToast(PackagingProductListActivity.this, "Packaging of products stored offline.");

            for (int i = 0; i < tempScannedQrList.size(); i++) {
                databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 1);
            }

            if (tempScannedBladeQrList.size() > 0) {
                for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                    databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 1);
                }
            }

            for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "1", String.valueOf(packing_ratio));
            }

            databaseHelper.deleteAllTempScannedTableData();

            Intent intent = new Intent(PackagingProductListActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            PackagingProductListActivity.this.finish();
            overridePendingTransition(0, 0);

        }


    }


    private void start_scanning() {
        Intent intent = new Intent(PackagingProductListActivity.this, ProductListScanActivity.class);
        intent.putExtra("item_code", item_code);
        intent.putExtra("blade", blade);
        intent.putExtra("tag", "packing");
        startActivity(intent);
        overridePendingTransition(0, 0);
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_discard_all:
                new AlertDialog.Builder(PackagingProductListActivity.this)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Are you sure you want to discard?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearEditScan();
                                tvCartonCnt.setVisibility(View.GONE);
                                databaseHelper.deleteScannedPackagingData("0");
                                Intent intent = new Intent(PackagingProductListActivity.this, PackagingBatchlistActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                PackagingProductListActivity.this.finish();
                                overridePendingTransition(0, 0);
                                onResume();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                break;

            case R.id.btnScan:
                start_scanning();
                break;
            case R.id.button_done:
                new AlertDialog.Builder(PackagingProductListActivity.this)
                        .setTitle("Alert")
                        .setMessage("Are you sure you want to proceed further ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", (dialog, which) -> setMapMasterCartonCode())
                        .setNegativeButton("No", null)
                        .show();
                break;

            case R.id.button_packnext:
                packnext();
                break;
        }
    }

    private void packnext() {
        item_code = Utils.getPreference(PackagingProductListActivity.this, "item_code", "");

        if (tempScannedQrList.size() == packing_ratio && !blade) {

            if (etMasterCarton.getText().toString().trim().equals("")) {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            } else {
                Log.d(Constants.TAG, "onClick: " + etMasterCarton.getText().toString().trim());
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));

                    }
                    button_done.setVisibility(View.VISIBLE);

                    for (int i = 0; i < tempScannedQrList.size(); i++) {
                        databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 0);
                    }

                    if (tempScannedBladeQrList.size() > 0) {
                        for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                            databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 0);
                        }
                    }

                    if (tempScannedMasterQrList.size() == 0) {
                        databaseHelper.insertScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, "0", String.valueOf(packing_ratio));
                    } else {
                        for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                            databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "0", String.valueOf(packing_ratio));
                        }
                    }

                    databaseHelper.insertItemCode(item_code, "0");

                    tempScannedQrList = new ArrayList<>();
                    tempScannedBladeQrList = new ArrayList<>();
                    databaseHelper.deleteAllTempScannedTableData();

                    BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
                    rvProductList.setAdapter(batchListAdapter);
                    etMasterCarton.setText("");
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);


                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(PackagingProductListActivity.this, "Please complete scan of product qr code first.");
                        }
                    });

                    showPackedCnt();
                    return;
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            }

        } else if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio && blade) {

            if (etMasterCarton.getText().toString().equals("")) {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            } else {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));

                    }
                    button_done.setVisibility(View.VISIBLE);

                    for (int i = 0; i < tempScannedQrList.size(); i++) {
                        databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 0);
                    }

                    if (tempScannedBladeQrList.size() > 0) {
                        for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                            databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 0);
                        }
                    }

                    if (tempScannedMasterQrList.size() == 0) {
                        databaseHelper.insertScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, "0", String.valueOf(packing_ratio));
                    } else {
                        for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                            databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "0", String.valueOf(packing_ratio));
                        }
                    }

                    databaseHelper.insertItemCode(item_code, "0");

                    tempScannedQrList = new ArrayList<>();
                    tempScannedBladeQrList = new ArrayList<>();
                    databaseHelper.deleteAllTempScannedTableData();

                    BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
                    rvProductList.setAdapter(batchListAdapter);
                    etMasterCarton.setText("");
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);


                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(PackagingProductListActivity.this, "Please complete scan of product qr code first.");
                        }
                    });

                    showPackedCnt();
                    return;
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            }


        } else {
            if (tempScannedQrList.size() == 0 && !blade) {
                Utils.showToast(PackagingProductListActivity.this, "Please scan product Qr-code for next packaging.");
            } else if (tempScannedQrList.size() == 0 || tempScannedBladeQrList.size() == 0 && blade) {
                Utils.showToast(PackagingProductListActivity.this, "Please scan product or blade Qr-code for next packaging.");
            } else {

                if (tempScannedQrList.size() != packing_ratio && !blade) {
                    Utils.showToast(PackagingProductListActivity.this, "Please scan product Qr-code first for next packaging.");
                } else if (tempScannedQrList.size() != packing_ratio || tempScannedBladeQrList.size() != packing_ratio && blade) {
                    Utils.showToast(PackagingProductListActivity.this, "Please scan product or blade Qr-code first for next packaging.");
                } else {
                    if (etMasterCarton.getText().toString().trim().equals("")) {
                        showAlert("Alert", "Please enter Master carton Qr-code.", true);
                    } else {
                        if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                            if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                                databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));
                            }
                            button_done.setVisibility(View.VISIBLE);
                            showPackedCnt();
                            return;
                        } else {
                            button_done.setVisibility(View.VISIBLE);
                            showAlert("Alert", "This Qr-code is invalid.", true);
                        }
                    }
                }


            }


        }
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<String> ScannedQr;
        private ArrayList<String> ScannedBladeQr;
        int packing_ratio;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView tvSrNo;
            private AppCompatTextView tvMotor;
            private AppCompatTextView tvBlade;

            public MyViewHolder(View view) {
                super(view);
                tvSrNo = view.findViewById(R.id.tvSrNo);
                tvMotor = view.findViewById(R.id.tvMotor);
                tvBlade = view.findViewById(R.id.tvBlade);

            }
        }


        public BatchListAdapter(ArrayList<String> ScannedQr, ArrayList<String> scannedBladeQrList, int packing_ratio) {
            this.ScannedQr = ScannedQr;
            this.ScannedBladeQr = scannedBladeQrList;
            this.packing_ratio = packing_ratio;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_product_row, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.tvSrNo.setText(String.valueOf(position + 1));

            if (ScannedQr.size() > 0) {
                for (int i = 0; i < ScannedQr.size(); i++)
                    if (position == i)
                        holder.tvMotor.setText(ScannedQr.get(i));
            }

            if (blade) {
                holder.tvBlade.setVisibility(View.VISIBLE);
                if (ScannedBladeQr.size() > 0) {
                    for (int i = 0; i < ScannedBladeQr.size(); i++)
                        if (position == i)
                            holder.tvBlade.setText(ScannedBladeQr.get(position));
                }
            }

        }

        @Override
        public int getItemCount() {
            return packing_ratio;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


}
