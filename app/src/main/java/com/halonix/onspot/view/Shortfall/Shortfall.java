package com.halonix.onspot.view.Shortfall;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.shortfall_Respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Shortfall extends BaseFragment {

    @BindView(R.id.tv_billno)
    AppCompatTextView tv_billno;

    @BindView(R.id.tv_billdate)
    AppCompatTextView tv_billdate;

    @BindView(R.id.tv_qty)
    AppCompatTextView tv_qty;

    @BindView(R.id.tv_pending_qty)
    AppCompatTextView tv_pending_qty;

    @BindView(R.id.btn_submit)
    AppCompatButton btn_submit;

    @BindView(R.id.radio_rb_Grp)
    RadioGroup radio_rb_Grp;

    @BindView(R.id.radio_regular)
    RadioButton radio_regular;

    @BindView(R.id.radio_breakage)
    RadioButton radio_breakage;


    private String bill_no;
    private String bill_date;
    private int qty;
    private int pending_qty;

    ConnectionDetectorActivity cd;
    RetrofitClient retrofit;
    private AlertDialog builder;
    private String ship_id;
    private String reason;
    DatabaseHelper databaseHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shortfall, container, false);
        ButterKnife.bind(this, view);
        cd = new ConnectionDetectorActivity(getActivity());
        databaseHelper = DatabaseHelper.getInstance(getActivity());
        retrofit = new RetrofitClient();

        Bundle bundle = getArguments();
        bill_no = bundle.getString("bill_no");
        bill_date = bundle.getString("bill_date");
        ship_id = bundle.getString("ship_id");
        qty = bundle.getInt("qty");
        pending_qty = bundle.getInt("pending_qty");

        tv_billno.setText(bill_no);
        tv_billdate.setText(bill_date);
        tv_qty.setText(String.valueOf(qty));
        tv_pending_qty.setText(String.valueOf(pending_qty));

        radio_rb_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_regular) {
                reason = radio_regular.getText().toString();
            } else if (checkedId == R.id.radio_breakage) {
                reason = "Lost in Transport";
            }
        });
        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
            radio_regular.setText("Shortfall supply by Vendor");
        } else {
            radio_regular.setText("Shortfall supply by Branch");
        }


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radio_rb_Grp.getCheckedRadioButtonId() == -1) {
                    Utils.showToast(getActivity(), "Please select the reason to report shortfall.");
                } else {
                    if (cd.isConnectingToInternet()) {
                        shortfall_req();
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }
                }


            }
        });


        return view;
    }

    private void shortfall_req() {
        Utils.showprogressdialog(getActivity(), "Reporting as shortfall, Please wait");

        Log.d(Constants.TAG, "shortfall_req: " + ship_id);
        Log.d(Constants.TAG, "shortfall_req: " + reason);
        retrofit.getService().shortfall_request(ship_id, reason, databaseHelper.getAuthToken()).enqueue(new Callback<shortfall_Respo>() {
            @Override
            public void onResponse(Call<shortfall_Respo> call, Response<shortfall_Respo> response) {

                Log.d(Constants.TAG, " shortfall onResponse: " + response.toString());
                Log.d(Constants.TAG, " shortfall onResponse: " + response.body().getSuccess());
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), "Submitted successfully.");
                    removeSelf();
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(getActivity(), "Failed to submit, please try again.");
                }
            }

            @Override
            public void onFailure(Call<shortfall_Respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    private void showNetworkFailDialog(String msg) {

        builder = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    @Override
    public void onResume() {
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Shortfall");
        enableNavigationDrawer(false);
        super.onResume();
    }
}