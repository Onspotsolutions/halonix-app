package com.halonix.onspot.view.History;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.SearchHistoryModel;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Utils.getDate;

public class SearchHistoryFragment extends BaseFragment implements ConnectionLostCallback {

    @BindView(R.id.listView_searched_history)
    RecyclerView listView_searched_history;

    @BindView(R.id.textView_start_date)
    TextView textViewStartDate;

    @BindView(R.id.textView_end_date)
    TextView textViewEndDate;

    @BindView(R.id.textView_no_data_found_completed)
    TextView textViewNoDataFound;

    @BindView(R.id.spinner_mode)
    AppCompatSpinner spinnerMode;


    @BindView(R.id.button_go)
    Button buttonGo;

    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    @BindView(R.id.linbody)
    LinearLayout linbody;


    private Activity cntx = null;
    private ArrayList<SearchHistoryModel> arrSearchHistoryModel;
    private ArrayList<CompletedListModel> completedListModelArrayList;
    private DatabaseHelper objDatabaseHelper = null;
    private ConnectionDetectorActivity cd = null;
    private int mYear, mMonth, mDay;
    Calendar calendar;
    String myFormat = "yyyy-MM-dd";
    String expectedFormat = "dd-MMM-yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
    RetrofitClient retrofitClient;
    private long mLastClickTime = 0;

    private BroadcastReceiver myBroadcastReceiver;
    private boolean receiver = false;

    protected void unregisterNetworkChanges() {
        try {
            getActivity().unregisterReceiver(myBroadcastReceiver);
            receiver = false;

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(Constants.TAG, "onDestroy: " + receiver);
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_search_history, container, false);
        cntx = getActivity();
        ButterKnife.bind(this, view);
        objDatabaseHelper = DatabaseHelper.getInstance(cntx);
        cd = new ConnectionDetectorActivity(getActivity());
        retrofitClient = new RetrofitClient();
        initViews();

        listView_searched_history.setHasFixedSize(true);
        listView_searched_history.setItemAnimator(new DefaultItemAnimator());
        listView_searched_history.setLayoutManager(new LinearLayoutManager(getActivity()));

        registerReceiver();
        return view;
    }

    private void registerReceiver() {
        if (!receiver) {
            myBroadcastReceiver = new MyBroadcastReceiver(this);
            getActivity().registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            receiver = true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }


    private void initViews() {
        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);


        ArrayAdapter adapter = null;
        if (Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_VENDOR)) {
            adapter = new ArrayAdapter<>(cntx, android.R.layout.simple_spinner_item, getActivity().getResources().getStringArray(R.array.search_history_mode_array_vendor));
            adapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
            spinnerMode.setAdapter(adapter);
        } else if (Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_BRANCH)) {
            adapter = new ArrayAdapter<>(cntx, android.R.layout.simple_spinner_item, getActivity().getResources().getStringArray(R.array.search_history_mode_array));
            adapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
            spinnerMode.setAdapter(adapter);
        } else if (Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DISTRIBUTOR) || Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
            adapter = new ArrayAdapter<>(cntx, android.R.layout.simple_spinner_item, getActivity().getResources().getStringArray(R.array.search_history_mode_array));
            adapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
            spinnerMode.setAdapter(adapter);
        } else if (Utils.getPreference(getActivity(), PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_RETAILER)) {
            adapter = new ArrayAdapter<>(cntx, android.R.layout.simple_spinner_item, getActivity().getResources().getStringArray(R.array.search_history_mode_array_retailer));
            adapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
            spinnerMode.setAdapter(adapter);
        }

/*
        spinnerMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                arrSearchHistoryModel = new ArrayList<>();
//                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        buttonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (textViewStartDate.getText().length() > 0 && textViewEndDate.getText().length() > 0) {
                    if (cd.isConnectingToInternet()) {

                        callAPI();
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }
                } else {
                    Utils.showToast(getActivity(), "Please select From or To Date");
                }
            }
        });


        textViewStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                try {
                    DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                            calendar.set(Calendar.YEAR, year);
                            calendar.set(Calendar.MONTH, (month));
                            calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                            textViewStartDate.setText(getDate(sdf.format(calendar.getTime()), myFormat, expectedFormat));
                            if (textViewEndDate.getText().toString().length() > 0) {
                                SimpleDateFormat sdf = new SimpleDateFormat(expectedFormat);
                                Date date = null;
                                Date date1 = null;
                                try {
                                    date = sdf.parse(textViewStartDate.getText().toString());
                                    date1 = sdf.parse(textViewEndDate.getText().toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (date1.before(date)) {
                                    textViewEndDate.setText(getDate(sdf.format(calendar.getTime()), expectedFormat, expectedFormat));

                                }

                            }

                        }
                    }, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());// TODO: used to hide future date,month and year
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        textViewEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                try {
                    if (textViewStartDate.getText().toString().trim().length() > 0) {
                        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, (month));
                                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                                textViewEndDate.setText(getDate(sdf.format(calendar.getTime()), myFormat, expectedFormat));
                            }
                        }, mYear, mMonth, mDay);
                        Calendar fromDateforTo = Calendar.getInstance();
                        if (!textViewStartDate.getText().toString().trim().isEmpty()) {
                            String start_date = getDate(textViewStartDate.getText().toString(), expectedFormat, myFormat);
                            String[] arrayDatefrom = start_date.split("-");
                            fromDateforTo.set(Calendar.YEAR, Integer.parseInt(arrayDatefrom[0]));
                            fromDateforTo.set(Calendar.MONTH, Integer.parseInt(arrayDatefrom[1]) - 1);
                            fromDateforTo.set(Calendar.DAY_OF_MONTH, Integer.parseInt(arrayDatefrom[2]));
                        }
                        dialog.getDatePicker().setMinDate(fromDateforTo.getTimeInMillis());// TODO: used to hide previous date,month and year
                        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        dialog.show();
                    } else {
                        Utils.showToast(getActivity(), "Please select from date first.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    //TODO API Call here
    private void callAPI() {

        String mode;

        if (spinnerMode.getSelectedItem().toString().equals("Shipment Made")) {
            mode = "des";
        } else {
            mode = "rec";
        }
        completedListModelArrayList = new ArrayList<>();
        arrSearchHistoryModel = new ArrayList<>();

        String start_date = getDate(textViewStartDate.getText().toString(), expectedFormat, myFormat);
        String end_date = getDate(textViewEndDate.getText().toString(), expectedFormat, myFormat);

        Log.d(Constants.TAG, "callAPI: " + start_date);
        Log.d(Constants.TAG, "callAPI: " + end_date);
        Log.d(Constants.TAG, "callAPI: " + mode);
        Utils.showprogressdialog(getActivity(), "Requesting, please wait.");
        retrofitClient.getService().getHistoryWithDate(objDatabaseHelper.getAuthToken(), mode, start_date, end_date).enqueue(new Callback<master_mapping_respo>() {
            @Override
            public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {

                Log.d(Constants.TAG, "onResponse: " + response.body().getSuccess());

                if (response.body().getSuccess()) {
                    linbody.setVisibility(View.VISIBLE);
                    textViewNoDataFound.setVisibility(View.GONE);
                    listView_searched_history.setVisibility(View.VISIBLE);
                    Utils.dismissprogressdialog();

                    arrSearchHistoryModel = response.body().getList();
                    Log.d(Constants.TAG, "onResponse: " + arrSearchHistoryModel.size());
                    if (arrSearchHistoryModel.size() > 0) {
                        for (int i = 0; i < arrSearchHistoryModel.size(); i++) {
                            if (arrSearchHistoryModel.get(i).getId() == null) {
                                arrSearchHistoryModel.get(i).setId(0);
                            }
                            CompletedListModel completedListModel = new CompletedListModel(arrSearchHistoryModel.get(i).getId(),
                                    arrSearchHistoryModel.get(i).getQuantity(), arrSearchHistoryModel.get(i).getRecQuantity(),
                                    arrSearchHistoryModel.get(i).getCompleted(), arrSearchHistoryModel.get(i).getCreatedAt(),
                                    arrSearchHistoryModel.get(i).getPONumber(), arrSearchHistoryModel.get(i).getPODate(),
                                    arrSearchHistoryModel.get(i).getInvoiceNumber(), arrSearchHistoryModel.get(i).getInvoiceDate(),
                                    arrSearchHistoryModel.get(i).getClient(),
                                    String.valueOf(spinnerMode.getSelectedItem())
                            );
                            completedListModelArrayList.add(completedListModel);
                        }
                        Collections.sort(completedListModelArrayList, new Comparator<CompletedListModel>() {
                            public int compare(CompletedListModel o1, CompletedListModel o2) {
                                return o2.getInvoice_date().compareTo(o1.getInvoice_date());
                            }
                        });
                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(completedListModelArrayList);
                        listView_searched_history.setAdapter(shipmentAdapter);
                    } else {
                        listView_searched_history.setVisibility(View.GONE);
                        textViewNoDataFound.setVisibility(View.VISIBLE);
                        textViewNoDataFound.setText("No history found.");
                    }
                    Utils.dismissprogressdialog();
                } else {
                    listView_searched_history.setVisibility(View.GONE);
                    textViewNoDataFound.setVisibility(View.VISIBLE);
                    textViewNoDataFound.setText("No history found.");
                }
            }

            @Override
            public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    public void dialog(boolean value) {
        Log.d(Constants.TAG, "dialog value: " + value);
        if (value) {

            no_internet.setVisibility(View.GONE);
            linbody.setVisibility(View.VISIBLE);

        } else {

            linbody.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }


    private class CompletedListModel {
        int id;
        int quantity;
        int rec_quantity;
        boolean completed;
        String created_at;
        String PO_number;
        String PO_date;
        String Invoice_number;
        String Invoice_date;
        String client;
        String mode;


        public CompletedListModel(int id, int quantity, int rec_quantity, boolean completed,
                                  String created_at, String PO_number, String PO_date,
                                  String invoice_number, String invoice_date, String client,
                                  String mode) {
            this.id = id;
            this.quantity = quantity;
            this.rec_quantity = rec_quantity;
            this.completed = completed;
            this.created_at = created_at;
            this.PO_number = PO_number;
            this.PO_date = PO_date;
            this.Invoice_number = invoice_number;
            this.Invoice_date = invoice_date;
            this.client = client;
            this.mode = mode;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getRec_quantity() {
            return rec_quantity;
        }

        public void setRec_quantity(int rec_quantity) {
            this.rec_quantity = rec_quantity;
        }

        public boolean isCompleted() {
            return completed;
        }

        public void setCompleted(boolean completed) {
            this.completed = completed;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getPO_number() {
            return PO_number;
        }

        public void setPO_number(String PO_number) {
            this.PO_number = PO_number;
        }

        public String getPO_date() {
            return PO_date;
        }

        public void setPO_date(String PO_date) {
            this.PO_date = PO_date;
        }

        public String getInvoice_number() {
            return Invoice_number;
        }

        public void setInvoice_number(String invoice_number) {
            Invoice_number = invoice_number;
        }

        public String getInvoice_date() {
            return Invoice_date;
        }

        public void setInvoice_date(String invoice_date) {
            Invoice_date = invoice_date;
        }

        public String getClient() {
            return client;
        }

        public void setClient(String client) {
            this.client = client;
        }
    }


    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }

                });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Search History");
        registerReceiver();
    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {

        private ArrayList<CompletedListModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textViewInNo;
            private AppCompatTextView textViewPoNo;
            private AppCompatTextView textViewInDate;
            private AppCompatTextView textViewPoDate;
            private AppCompatTextView textViewQuantity;
            private AppCompatTextView textViewClientCell;
            private AppCompatTextView textView_client;
            private AppCompatTextView textViewPending;
            private LinearLayout ael;
            private AppCompatTextView tvMore;

            public MyViewHolder(View view) {
                super(view);
                textViewInNo = view.findViewById(R.id.textView_in_no);
                textViewInDate = view.findViewById(R.id.textView_in_date);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewPending = view.findViewById(R.id.textView_pending);
                textViewPoNo = view.findViewById(R.id.textView_po_no);
                textViewPoDate = view.findViewById(R.id.textView_po_date);
                ael = view.findViewById(R.id.expandable_ans);
                tvMore = view.findViewById(R.id.tvMore);
                textViewClientCell = view.findViewById(R.id.textViewClientCell);
                textView_client = view.findViewById(R.id.textView_client);
            }
        }


        public ShipmentAdapter(ArrayList<CompletedListModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_search_history_item, parent, false);
            return new MyViewHolder(itemView);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final CompletedListModel object = notesList.get(position);
            holder.textViewPoNo.setText(object.getPO_number());
            if (object.getPO_date() != null) {
                holder.textViewPoDate.setText(getDate(getDisplayDate(object.getPO_date()), "yyyy-MM-dd", "dd-MM-yyyy"));
            } else {
                holder.textViewPoDate.setText("");
            }
            holder.textViewInNo.setText(object.getInvoice_number());
            if (object.getInvoice_date() != null) {
                holder.textViewInDate.setText(getDate(getDisplayDate(object.getInvoice_date()), "yyyy-MM-dd", "dd-MM-yyyy"));
            } else {
                holder.textViewInDate.setText("");
            }
            holder.textViewQuantity.setText(String.valueOf(object.getQuantity()));
            holder.textViewPending.setText(String.valueOf(object.getRec_quantity()));
            holder.textViewClientCell.setText(object.getClient());

            holder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.ael.setVisibility(holder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (holder.ael.getVisibility() == View.VISIBLE) {
                        holder.tvMore.setText("Less Details");
                    } else {
                        holder.tvMore.setText("More Details");
                    }
                }
            });

            holder.textViewClientCell.setVisibility(object.getMode().equals("Shipment Received") ? TextView.GONE : TextView.VISIBLE);
            holder.textView_client.setVisibility(object.getMode().equals("Shipment Received") ? TextView.GONE : TextView.VISIBLE);
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }


}
