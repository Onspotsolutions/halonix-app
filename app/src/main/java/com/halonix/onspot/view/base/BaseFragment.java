package com.halonix.onspot.view.base;

import android.os.Bundle;

import androidx.fragment.app.Fragment;


public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    public abstract boolean backButtonPressed();

    protected void changeFragmentTo(String fragmentName, boolean addToBackstack,
                                    Bundle bundle) {
        if (getActivity() != null)
            ((OnFragmentListner) getActivity()).handleChangeFragment(
                    fragmentName, addToBackstack, bundle);
    }

    protected void removeFragment(Fragment fragment) {
        if (getActivity() != null)
            ((OnFragmentListner) getActivity()).handleRemoveFragment(fragment);
    }

    protected void removeSelf() {
        if (getActivity() != null) ((OnFragmentListner) getActivity()).handleRemoveFragment(this);

    }


    protected void enableNavigationDrawer(boolean enable) {
        if (getActivity() != null) {
            ((OnFragmentListner) getActivity()).enableNavigationDrawer(enable);
        }
    }


}


//    public void showShoppingCart(boolean enable) {
//        if(getActivity() != null)
//            ((OnFragmentListner) getActivity()).showShoppingCart(enable);
//    }