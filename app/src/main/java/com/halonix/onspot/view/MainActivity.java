package com.halonix.onspot.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.navigation.NavigationView;
import com.halonix.onspot.R;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.DefectiveReturnSend.DefReturnHistory;
import com.halonix.onspot.view.ForcefullyIn.ForcefullyInMainFragment;
import com.halonix.onspot.view.History.SearchHistoryFragment;
import com.halonix.onspot.view.History.tab.FragmentReceivedHistory;
import com.halonix.onspot.view.History.tab.FragmentShipmentHistory;
import com.halonix.onspot.view.ReceiveNewShipment.ReceiveNewShipmentMainFragment;
import com.halonix.onspot.view.SaleReport.SaleReportFragment;
import com.halonix.onspot.view.SaleReturnSend.History.SaleReturnHistory;
import com.halonix.onspot.view.SaleReturnSend.SaleReturnMainFragment;
import com.halonix.onspot.view.Shortfall.Shortfall;
import com.halonix.onspot.view.Stock.StockReportFragment;
import com.halonix.onspot.view.Webview_page.CommonWebFragment;
import com.halonix.onspot.view.base.OnFragmentListner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class MainActivity extends AppCompatActivity implements OnFragmentListner, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private boolean backPressedToExitOnce = false;
    Bundle bundle;

    ConnectionDetectorActivity cd;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    private GoogleApiClient googleApiClient = null;
    protected static final int REQUEST_CHECK_SETTINGS_START_ADDMORE = 0x1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private AlertDialog newAlertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        cd = new ConnectionDetectorActivity(MainActivity.this);
        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        setUpNavigationView();

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        if (savedInstanceState == null) {
            addFragment(HomeFragment.class.getSimpleName(), false, null); //HomeFragment
        }

        googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        String USER_TYPEs = Utils.getPreference(MainActivity.this, PreferenceKeys.USER_TYPE, "");

        Menu menuNav = navigationView.getMenu();
        MenuItem nav_sale_report = menuNav.findItem(R.id.nav_sale_report);
        if (USER_TYPEs.equals(Constants.USER_TYPE_RETAILER)) {
            nav_sale_report.setVisible(true);
        } else {
            nav_sale_report.setVisible(false);
        }

        location_alert();
    }


    public void location_alert() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
            }
        } else {
            showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
        }
    }


    private boolean checkAndRequestPermissions() {
        int GpsFindLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int CAMERA = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (GpsFindLocationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (CAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
            }
        } else {
            showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Log.d(Constants.TAG, "onRequestPermissionsResult if: ");
                    //Case 2
                    showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
                } else {
                    Log.d(Constants.TAG, "onRequestPermissionsResult else: ");

                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.CAMERA)) {
                        showDialogOK("Permissions (Camera & Location) are required for this app.");
                    } else {
                        explain("You need to Grant permissions to continue. Do you want to go to app settings?");
                    }
                }

            }
        }

    }


    private void explain(String msg) {

        if (null == newAlertDialog)
            newAlertDialog = new AlertDialog.Builder(MainActivity.this)
                    .setCancelable(false)
                    .setTitle("Permissions Required!")
                    .setMessage(msg)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null)));
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        newAlertDialog.show();
    }

    private void showDialogOK(String msg) {
        if (null == newAlertDialog)
            newAlertDialog = new AlertDialog.Builder(MainActivity.this)
                    .setCancelable(false)
                    .setTitle("Permissions Required!")
                    .setMessage(msg)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                                    || ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.CAMERA)) {
                                checkAndRequestPermissions();
                            } else {
                                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null)));
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        newAlertDialog.show();
    }


    private void showLocationEnableDialog(final int setting) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(1000);
        LocationSettingsRequest.Builder builder2 = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder2.setAlwaysShow(true); //this is the key ingredient


        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder2.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, setting);
                            Utils.out("IN RESOLUTION REQUIRED");
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Utils.out("IN SETTINGS_CHANGE_UNAVAILABLE");
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    // This is the menu option selected items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                finish();
                break;
        }
        return true;
    }

    long TIME = 1 * 100;

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                    }
                }, TIME);
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        addFragment(HomeFragment.class.getSimpleName(), false, null); //HomeFragment
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;
                    case R.id.nav_aboutus:
                        bundle = new Bundle();
                        bundle.putString("page_name", "About us");
                        bundle.putString("url", Constants.URL);
                        if (cd.isConnectingToInternet()) {
                            addFragment(CommonWebFragment.class.getSimpleName(), false, bundle);
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);

                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }

                        break;
                    case R.id.nav_appbasics:
                        bundle = new Bundle();
                        bundle.putString("page_name", "App Basics");
                        bundle.putString("url", Constants.URL);
                        if (cd.isConnectingToInternet()) {
                            addFragment(CommonWebFragment.class.getSimpleName(), false, bundle);
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);

                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }

                        break;
                    case R.id.nav_faq:
                        bundle = new Bundle();
                        bundle.putString("page_name", "FAQ");
                        bundle.putString("url", Constants.URL);
                        if (cd.isConnectingToInternet()) {
                            addFragment(CommonWebFragment.class.getSimpleName(), false, bundle);
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);

                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;
                    case R.id.nav_search_history:
                        if (cd.isConnectingToInternet()) {
                            addFragment(SearchHistoryFragment.class.getSimpleName(), false, null); //HomeFragment
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);

                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;

                    case R.id.nav_stock_report:
                        addFragment(StockReportFragment.class.getSimpleName(), false, null); //HomeFragment

                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;

                    case R.id.nav_sale_report:
                        if (cd.isConnectingToInternet()) {
                            addFragment(SaleReportFragment.class.getSimpleName(), false, null); //HomeFragment
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);

                        }

                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;

                    case R.id.nav_licenses:
                        bundle = new Bundle();
                        bundle.putString("page_name", "Licenses");
                        bundle.putString("url", Constants.URL);
                        if (cd.isConnectingToInternet()) {
                            addFragment(CommonWebFragment.class.getSimpleName(), false, bundle);
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;

                    case R.id.nav_tos:
                        bundle = new Bundle();
                        bundle.putString("page_name", "Terms of services");
                        bundle.putString("url", Constants.URL);
                        if (cd.isConnectingToInternet()) {
                            addFragment(CommonWebFragment.class.getSimpleName(), false, bundle);
                        } else {
                            showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                        }
                        if (drawer.isDrawerOpen(Gravity.LEFT)) {
                            drawer.closeDrawer(Gravity.LEFT);
                        }
                        break;
                }
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
    }


    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            // If there are back-stack entries, leave the FragmentActivity
            // implementation take care of them.
            fragmentManager.popBackStack();

        } else {

            if (backPressedToExitOnce) {
                finishAffinity();
            } else {
                if (drawer.isDrawerOpen(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    this.backPressedToExitOnce = true;
                    Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        backPressedToExitOnce = false;
                    }
                }, 3000);
            }
        }
    }


    @Override
    public void handleChangeFragment(String constantFragment, boolean addToBackstack, Bundle bundle) {
        addFragment(constantFragment, addToBackstack, bundle);
    }

    @Override
    public void handleRemoveFragment(Fragment fragment) {
        if (fragmentManager.getBackStackEntryCount() != 0) {
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
            fragmentManager.popBackStack();
        } else {
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            } else {
                if (fragmentManager.getBackStackEntryCount() == 0) {
                    this.finish();
                } else {
                    fragmentManager.popBackStackImmediate();
                }

            }
        }
    }

    @Override
    public void enableNavigationDrawer(boolean enable) {
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(enable ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if (enable) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);

            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.openDrawer(Gravity.LEFT);
                }
            });

            toggle.syncState();
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }


    private void addFragment(String fragment, boolean addToBackStack, Bundle bundle) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment);
        }

        //  HomeFragment
        if (fragment.equals(HomeFragment.class.getSimpleName())) {
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame, homeFragment, fragment);
            fragmentTransaction.commit();
        }

        //  SearchHistoryFragment
        if (fragment.equals(SearchHistoryFragment.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new SearchHistoryFragment(), fragment);
            fragmentTransaction.commit();
        }

        //  FragmentShipmentHistory
        if (fragment.equals(FragmentShipmentHistory.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new FragmentShipmentHistory(), fragment);
            fragmentTransaction.commit();
        }

        //  FragmentReceivedHistory
        if (fragment.equals(FragmentReceivedHistory.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new FragmentReceivedHistory(), fragment);
            fragmentTransaction.commit();
        }


        //  SaleReturnHistory
        if (fragment.equals(SaleReturnHistory.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new SaleReturnHistory(), fragment);
            fragmentTransaction.commit();
        }

        //  DefReturnHistory
        if (fragment.equals(DefReturnHistory.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new DefReturnHistory(), fragment);
            fragmentTransaction.commit();
        }

        //  StockReportFragment
        if (fragment.equals(StockReportFragment.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new StockReportFragment(), fragment);
            fragmentTransaction.commit();
        }

        //  SaleReportFragment
        if (fragment.equals(SaleReportFragment.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new SaleReportFragment(), fragment);
            fragmentTransaction.commit();
        }

        //  SaleReturnMainFragment
        if (fragment.equals(SaleReturnMainFragment.class.getSimpleName())) {
            fragmentTransaction.replace(R.id.frame, new SaleReturnMainFragment(), fragment);
            fragmentTransaction.commit();
        }

        //CommonWebFragment
        if (fragment.equals(CommonWebFragment.class.getSimpleName())) {
            CommonWebFragment commonWebFragment = new CommonWebFragment();
            commonWebFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame, commonWebFragment, fragment);
            fragmentTransaction.commit();
        }

        //ReceiveNewShipmentMainFragment
        if (fragment.equals(ReceiveNewShipmentMainFragment.class.getSimpleName())) {
            ReceiveNewShipmentMainFragment commonWebFragment = new ReceiveNewShipmentMainFragment();
            commonWebFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame, commonWebFragment, fragment);
            fragmentTransaction.commit();
        }

        //ForcefullyInMainFragment
        if (fragment.equals(ForcefullyInMainFragment.class.getSimpleName())) {
            ForcefullyInMainFragment commonWebFragment = new ForcefullyInMainFragment();
            commonWebFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame, commonWebFragment, fragment);
            fragmentTransaction.commit();
        }

        //Shortfall
        if (fragment.equals(Shortfall.class.getSimpleName())) {
            Shortfall commonWebFragment = new Shortfall();
            commonWebFragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame, commonWebFragment, fragment);
            fragmentTransaction.commit();
        }

    }

    /*  @Override
      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
          super.onActivityResult(requestCode, resultCode, data);
          Fragment frg = getSupportFragmentManager().findFragmentById(R.id.frame);
          if (frg != null) {
              frg.onActivityResult(requestCode, resultCode, data);
          }
      }
  */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_START_ADDMORE:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:
                        showLocationEnableDialog(REQUEST_CHECK_SETTINGS_START_ADDMORE);
                        break;
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


}
