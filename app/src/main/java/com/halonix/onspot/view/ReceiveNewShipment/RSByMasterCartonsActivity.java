package com.halonix.onspot.view.ReceiveNewShipment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceCodeList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.get_receive_shipmentList;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.LogDetailModel;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.model.SystemBarcodeModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class RSByMasterCartonsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.editText_number_of_cartons)
    AppCompatEditText editTextNumberOfCartons;

    @BindView(R.id.textView_scan_status)
    TextView textViewScanStatus;

    @BindView(R.id.textView_scan_status1)
    TextView textView_scan_status1;

    @BindView(R.id.button_resume_now)
    Button buttonResumeNow;

    @BindView(R.id.button_start_scanning)
    Button button_start_scanning;

    @BindView(R.id.button_stop_and_submit)
    Button buttonStopAndSubmit;

    @BindView(R.id.button_submit)
    Button button_submit;

    @BindView(R.id.button_discard_all)
    Button buttonDiscardAll;

    @BindView(R.id.button_discard)
    Button button_discard;


    @BindView(R.id.textInputNoOfCartons)
    TextInputLayout textInputNoOfCartons;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private Activity activity = null;


    private ArrayList<String> arrBarcodes = new ArrayList<>();
    private DatabaseHelper objDatabaseHelper = null;
    private JSONArray jsonArray = new JSONArray();
    private ArrayList<LogDetailModel> logDetailsArrayList = new ArrayList<>();
    LogDetailModel logDetails = null;
    private ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;
    private long mLastClickTime = 0;
    private Location loca;
    private String strLatitude = "", strLongitude = "", strAccuracy = "";

    //Receive New Shipment
    ArrayList<String> InvoiceCodeListArrayList;
    ArrayList<get_receive_shipmentList> receiveShipmentListArrayList;
    ArrayList<ReceiveShipmentInvoiceList> InvoiceCodeProductDataArrayList;
    ArrayList<PackedCartonList> packedcartonListArrayList;
    int k = 0;
    private ArrayList<String> scannedBarcodeHistory;
    private ArrayList<String> systemBarcode = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_master_carton_shipment);
        ButterKnife.bind(this);
        activity = RSByMasterCartonsActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);
        cd = new ConnectionDetectorActivity(activity);
        retrofitClient = new RetrofitClient();

        jsonArray = objDatabaseHelper.getMasterCartonCode().getJsonArray();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                systemBarcode.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        scannedBarcodeHistory = objDatabaseHelper.getScannedMasterCartonQRList("1");
        Log.d(Constants.TAG, "onCreate scannedBarcodeHistory: " + scannedBarcodeHistory.toString());
        Log.d(Constants.TAG, "onCreate systemBarcode: " + systemBarcode.toString());

        systemBarcode.addAll(scannedBarcodeHistory);
        List<String> intersection = new ArrayList<>(systemBarcode);
        intersection.retainAll(scannedBarcodeHistory);
        systemBarcode.removeAll(intersection);
        Log.d(Constants.TAG, "onCreate systemBarcode after: " + systemBarcode.toString());
        jsonArray = new JSONArray();
        for (int i = 0; i < systemBarcode.size(); i++) {
            jsonArray.put(systemBarcode.get(i));
        }
        Log.d(Constants.TAG, "onCreate: " + jsonArray.toString());

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        toolbar.setTitle("Scan Master Cartons");

        button_start_scanning.setOnClickListener(this);
        buttonResumeNow.setOnClickListener(this);
        buttonStopAndSubmit.setOnClickListener(this);
        button_submit.setOnClickListener(this);
        button_discard.setOnClickListener(this);
        buttonDiscardAll.setOnClickListener(this);

        arrBarcodes = new ArrayList<>();
        arrBarcodes = objDatabaseHelper.getScannedMasterCartonQRList("0");

        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
        if (logDetailsArrayList.size() > 0) {
            if (Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) > 1) {
                findViewById(R.id.linearLayout_start_scanning).setVisibility(LinearLayout.GONE);

                findViewById(R.id.linearLayout_resume).setVisibility(LinearLayout.VISIBLE);
                textViewScanStatus.setText(arrBarcodes.size() + " scan done till now " + (Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) - arrBarcodes.size()) + " pending.");

            } else {
                findViewById(R.id.linearLayout_start_scanning).setVisibility(LinearLayout.GONE);
                Log.d(Constants.TAG, "onCreate: " + logDetailsArrayList.get(0).getNumberOfCartons());
                Log.d(Constants.TAG, "onCreate: " + arrBarcodes.size());
                if ((Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) - arrBarcodes.size()) == 0) {
                    findViewById(R.id.button_resume_now).setVisibility(LinearLayout.GONE);
                    findViewById(R.id.lin).setVisibility(LinearLayout.VISIBLE);
                    textView_scan_status1.setText(arrBarcodes.size() + " scan done till now " + (Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) - arrBarcodes.size()) + " pending.");
                } else {
                    findViewById(R.id.linearLayout_resume).setVisibility(LinearLayout.VISIBLE);
                    textViewScanStatus.setText(arrBarcodes.size() + " scan done till now " + (Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) - arrBarcodes.size()) + " pending.");
                }

                if ((Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()) == 1)) {
                    buttonStopAndSubmit.setText("Submit");
                }


            }
        }


    }


    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
                if (Integer.parseInt(String.valueOf(editTextNumberOfCartons.getText().length() > 0 ? editTextNumberOfCartons.getText() : "0")) > 0) {
                    start_scanning();
                } else {
                    Utils.showToast(activity, "Please enter valid number of cartons.");
                }
                break;
            case R.id.button_resume_now:
                resume_scanning();
                break;
            case R.id.button_stop_and_submit:
                if (cd.isConnectingToInternet()) {
                    submit();
                } else {
                    ArrayList<String> stringArrayList = objDatabaseHelper.getScannedMasterCartonQRList("0");
                    Log.d(Constants.TAG, "onClick: " + stringArrayList.size());
                    if (stringArrayList.size() > 0) {
                        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                        logDetailsArrayList.get(0).setLogCompleted("1");
                        logDetailsArrayList.get(0).setLogUploadToServerStatus("1");
                        objDatabaseHelper.updateLog(logDetailsArrayList.get(0));
                        objDatabaseHelper.updateScannedReceiveMasterCartonCode("1");
                        Utils.showToast(RSByMasterCartonsActivity.this, "Shipment (Receive by Master Carton) stored offline.");
                        Intent intent = new Intent(RSByMasterCartonsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        RSByMasterCartonsActivity.this.finish();
                        overridePendingTransition(0, 0);
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }
                }

                break;
            case R.id.button_discard_all:
                discardEntry();
                break;
            case R.id.button_submit:
                if (cd.isConnectingToInternet()) {
                    logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                    ArrayList<String> stringArrayList = objDatabaseHelper.getScannedMasterCartonQRList("0");
                    Log.d(Constants.TAG, "submit: " + stringArrayList.size());
                    if (stringArrayList.size() > 0) {
                        new AlertDialog.Builder(activity)
                                .setTitle("Alert")
                                .setCancelable(false)
                                .setMessage("Are you sure you want to upload to server ?")
                                .setPositiveButton("Yes", (dialog, which) -> {
                                    logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                                    logDetailsArrayList.get(0).setNumberOfCartons(String.valueOf(arrBarcodes.size()));
                                    logDetailsArrayList.get(0).setLogCompleted("1");
                                    logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                                    logDetails = new LogDetailModel(logDetailsArrayList.get(0).getNumberOfCartons(), logDetailsArrayList.get(0).getLogCompleted(), logDetailsArrayList.get(0).getLogUploadToServerStatus());
                                    objDatabaseHelper.updateLog(logDetails);

                                    LocationControl locationControlTask = new LocationControl();
                                    locationControlTask.execute();
                                })
                                .setNegativeButton("No", null)
                                .show();
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }

                } else {
                    ArrayList<String> stringArrayList = objDatabaseHelper.getScannedMasterCartonQRList("0");
                    Log.d(Constants.TAG, "onClick: " + stringArrayList.size());
                    if (stringArrayList.size() > 0) {
                        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                        logDetailsArrayList.get(0).setLogCompleted("1");
                        logDetailsArrayList.get(0).setLogUploadToServerStatus("1");
                        objDatabaseHelper.updateLog(logDetailsArrayList.get(0));
                        objDatabaseHelper.updateScannedReceiveMasterCartonCode("1");

                        Utils.showToast(RSByMasterCartonsActivity.this, "Shipment (Receive by Master Carton) stored offline.");
                        Intent intent = new Intent(RSByMasterCartonsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        RSByMasterCartonsActivity.this.finish();
                        overridePendingTransition(0, 0);
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }

                }

                break;
            case R.id.button_discard:
                logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                if (logDetailsArrayList.size() > 0) {
                    new AlertDialog.Builder(activity)
                            .setTitle("Alert")
                            .setCancelable(false)
                            .setMessage("Are you sure you want to discard shipment ?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    objDatabaseHelper.discardEntry();
                                    objDatabaseHelper.deleteAllScannedMasterCartonCode("");
                                    finish();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();

                } else {
                    objDatabaseHelper.discardEntry();
                    finish();
                }
                break;
        }
    }

    private void submit() {
        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedMasterCartonQRList("0");
        Log.d(Constants.TAG, "submit: " + stringArrayList.size());
        if (stringArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Are you sure you want to stop scanning and upload to server ?")
                    .setPositiveButton("Yes", (dialog, which) -> {
                        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                        logDetailsArrayList.get(0).setNumberOfCartons(String.valueOf(arrBarcodes.size()));
                        logDetailsArrayList.get(0).setLogCompleted("1");
                        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                        logDetails = new LogDetailModel(logDetailsArrayList.get(0).getNumberOfCartons(), logDetailsArrayList.get(0).getLogCompleted(), logDetailsArrayList.get(0).getLogUploadToServerStatus());
                        objDatabaseHelper.updateLog(logDetails);

                        LocationControl locationControlTask = new LocationControl();
                        locationControlTask.execute();
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            Utils.showToast(activity, "Please scan at least one QR code");
        }
    }

    private void resume_scanning() {
        ArrayList<String> arrSystemBarcode = new ArrayList<>();
        for (int index = 0; index < jsonArray.length(); index++) {
            try {
                arrSystemBarcode.add(String.valueOf(jsonArray.get(index)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
        Intent intent = new Intent(activity, RSByMasterCartonsScannerActivity.class);
        intent.putExtra("NUMBER_OF_CARTONS", Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()));
        logDetails = new LogDetailModel(logDetailsArrayList.get(0).getNumberOfCartons(), logDetailsArrayList.get(0).getLogCompleted(), logDetailsArrayList.get(0).getLogUploadToServerStatus());
        intent.putExtra("LOG_MODEL", logDetails);
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
    }


    private void start_scanning() {
        if (Integer.parseInt(String.valueOf(editTextNumberOfCartons.getText().length() > 0 ? editTextNumberOfCartons.getText() : "0")) > 0) {
            if (Integer.parseInt(String.valueOf(editTextNumberOfCartons.getText().length() > 0 ? editTextNumberOfCartons.getText() : "0")) <= jsonArray.length()) {
                ArrayList<String> arrSystemBarcode = new ArrayList<>();
                for (int index = 0; index < jsonArray.length(); index++) {
                    try {
                        arrSystemBarcode.add(String.valueOf(jsonArray.get(index)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                /**
                 * INSERT LOG ENTRY AND PASS ID TO NEXT SCREEN
                 **/
                logDetails = new LogDetailModel(editTextNumberOfCartons.getText().toString().trim(), "0", "0");
                objDatabaseHelper.insertLog(logDetails);

                logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
                Intent intent = new Intent(activity, RSByMasterCartonsScannerActivity.class);
                intent.putExtra("NUMBER_OF_CARTONS", Integer.parseInt(logDetailsArrayList.get(0).getNumberOfCartons()));
                intent.putExtra("LOG_MODEL", logDetails);
                startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
                overridePendingTransition(0, 0);
            } else {
                Utils.showToast(activity, "Max number of cartons is: " + jsonArray.length());
            }
        } else {
            Utils.showToast(activity, "Please enter valid number of cartons.");
        }

    }

    private void discardEntry() {
        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
        if (logDetailsArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Are you sure you want to discard shipment ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            objDatabaseHelper.discardEntry();
                            objDatabaseHelper.deleteAllScannedMasterCartonCode("");
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        } else {
            objDatabaseHelper.discardEntry();
            finish();
        }

    }

    @Override
    public void onBackPressed() {

        logDetailsArrayList = objDatabaseHelper.getCurrentLogDetails();
        Log.d(Constants.TAG, "onBackPressed: " + logDetailsArrayList.size());
        if (logDetailsArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Are you sure you want to discard shipment ?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            objDatabaseHelper.discardEntry();
                            objDatabaseHelper.deleteAllScannedMasterCartonCode("");
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } else {
            objDatabaseHelper.discardEntry();
            finish();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    arrBarcodes = new ArrayList<>();
                    arrBarcodes = objDatabaseHelper.getScannedMasterCartonQRList("0");
                    logDetails = (LogDetailModel) data.getSerializableExtra("LOG_MODEL");

                    if (logDetails.getLogCompleted().equals("1")) {
                        findViewById(R.id.linearLayout_resume).setVisibility(LinearLayout.GONE);
                        findViewById(R.id.linearLayout_start_scanning).setVisibility(LinearLayout.GONE);
                        findViewById(R.id.lin).setVisibility(LinearLayout.VISIBLE);

                        textView_scan_status1.setText(arrBarcodes.size() + " scan done till now " + (Integer.parseInt(logDetails.getNumberOfCartons()) - arrBarcodes.size()) + " pending.");

                    } else {
                        findViewById(R.id.linearLayout_start_scanning).setVisibility(LinearLayout.GONE);
                        Log.d(Constants.TAG, "onActivityResult: " + logDetails.getNumberOfCartons());
                        Log.d(Constants.TAG, "onActivityResult: " + arrBarcodes.size());
                        if ((Integer.parseInt(logDetails.getNumberOfCartons()) - arrBarcodes.size()) == 0) {
                            findViewById(R.id.button_resume_now).setVisibility(LinearLayout.GONE);
                        }

                        if ((Integer.parseInt(logDetails.getNumberOfCartons()) == 1)) {
                            buttonStopAndSubmit.setText("Submit");
                        }
                        findViewById(R.id.linearLayout_resume).setVisibility(LinearLayout.VISIBLE);

                        textViewScanStatus.setText(arrBarcodes.size() + " scan done till now " + (Integer.parseInt(logDetails.getNumberOfCartons()) - arrBarcodes.size()) + " pending.");
                    }
                    break;
            }
        } else {

        }
    }

    private class LocationControl extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(RSByMasterCartonsActivity.this, "Receiving shipment by master cartons,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(RSByMasterCartonsActivity.this);
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            objDatabaseHelper.updateScannedReceiveMasterCartonCode("1");
            submitDetailsToServer();
        }

    }

    private void submitDetailsToServer() {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(objDatabaseHelper.getScannedMasterCartonQRList("1").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "submitDetailsToServer: " + jsonArray.toString());
        Log.d(Constants.TAG, "onResume strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "onResume strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "onResume strAccuracy: " + strAccuracy);


        retrofitClient.getService().getReceiveNewShipment(objDatabaseHelper.getAuthToken(), jsonArray.toString(), "MasterCarton", strLatitude, strLongitude, strAccuracy).enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override

            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteAllScannedMasterCartonCode("all");
                    objDatabaseHelper.deleteAllLogs();
                }
                getpackedcarton();
            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Receive New Shipment API Call here
    private void getReceiveNewShipmentList() {
        k = 0;
        InvoiceCodeListArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveNewShipmentCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<get_receive_shipment_respo>() {
            @Override
            public void onResponse(Call<get_receive_shipment_respo> call, Response<get_receive_shipment_respo> response) {

                Log.d(Constants.TAG, "onResponse getReceiveNewShipmentList: " + response.toString());
                if (response.body().getSuccess()) {
                    InvoiceCodeListArrayList = response.body().getInvoiceCodes();
                    receiveShipmentListArrayList = response.body().getList();
                    objDatabaseHelper.deleteAllReceiveShipmentCode();
                    objDatabaseHelper.deleteServerBarcodeDataTableData();
                    objDatabaseHelper.deleteAllReceiveShipmentCodeData();
                    objDatabaseHelper.deleteAllReceiveShipmentISMasterCartons();

                    if (receiveShipmentListArrayList.size() > 0) {
                        for (int i = 0; i < receiveShipmentListArrayList.size(); i++) {
                            objDatabaseHelper.insertReceiveNewShipment(new NewShipmentDetailModel(String.valueOf(receiveShipmentListArrayList.get(i).getId()),
                                    String.valueOf(receiveShipmentListArrayList.get(i).getPackingRatio()), receiveShipmentListArrayList.get(i).getCode(),
                                    receiveShipmentListArrayList.get(i).getProductName(), receiveShipmentListArrayList.get(i).getSweep(),
                                    receiveShipmentListArrayList.get(i).getColor(), "1"));
                        }

                        JSONArray jsonArray1 = objDatabaseHelper.getReceiveShipmentBarcode1().getJsonArray();
                        objDatabaseHelper.insertMasterCartoncode(new SystemBarcodeModel(jsonArray1));
                    }


                    if (InvoiceCodeListArrayList.size() > 0) {
                        objDatabaseHelper.inserRSInvoicecode(new SystemBarcodeModel(new JSONArray(InvoiceCodeListArrayList)));

                        for (int i = 0; i < InvoiceCodeListArrayList.size(); i++) {
                            ReceiveShipmentByInvoiceData(InvoiceCodeListArrayList.get(i));
                        }

                    } else {
                        Utils.dismissprogressdialog();
                        Intent intent = new Intent(RSByMasterCartonsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RSByMasterCartonsActivity.this.finish();
                        overridePendingTransition(0, 0);
                        Utils.showToast(activity, "Shipment received Successfully");
                    }


                }
            }

            @Override
            public void onFailure(Call<get_receive_shipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Receive New Shipment Invoice API Call here
    private void ReceiveShipmentByInvoiceData(String invoice_code) {
        Log.d(Constants.TAG, "ReceiveShipmentByInvoiceData i: " + invoice_code);
        InvoiceCodeProductDataArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveShipmentByInvoice(objDatabaseHelper.getAuthToken(), invoice_code).enqueue(new Callback<ReceiveShipmentInvoice_respo>() {
            @Override
            public void onResponse(Call<ReceiveShipmentInvoice_respo> call, Response<ReceiveShipmentInvoice_respo> response) {

                //Log.d(Constants.TAG, "onResponse ReceiveShipmentByInvoiceData: " + response.toString());
                if (response.body().getSuccess()) {
                    k++;
                    InvoiceCodeProductDataArrayList = response.body().getList();

                    if (InvoiceCodeProductDataArrayList.size() > 0) {
                        for (int i = 0; i < InvoiceCodeProductDataArrayList.size(); i++) {
                            if (InvoiceCodeProductDataArrayList.get(i).getQuantity() == null) {
                            } else {
                                objDatabaseHelper.inserRSInvoicecodeData(new NewShipmentDetailModel(invoice_code, InvoiceCodeProductDataArrayList.get(i).getModel(),
                                        "", "",
                                        String.valueOf(InvoiceCodeProductDataArrayList.get(i).getQuantity()), ""));
                                ArrayList<ReceiveShipmentInvoiceCodeList> codeArrayList = InvoiceCodeProductDataArrayList.get(i).getCodes();

                                for (int j = 0; j < codeArrayList.size(); j++) {
                                    objDatabaseHelper.insertIS_MasterCartonCodes(InvoiceCodeProductDataArrayList.get(i).getModel(), invoice_code, codeArrayList.get(j).getCode(), String.valueOf(codeArrayList.get(j).getPackingRatio()));
                                }
                            }
                        }
                    }


                    if (k == InvoiceCodeListArrayList.size()) {
                        Utils.dismissprogressdialog();
                        Intent intent = new Intent(RSByMasterCartonsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RSByMasterCartonsActivity.this.finish();
                        overridePendingTransition(0, 0);
                        Utils.showToast(activity, "Shipment received Successfully.");
                    }

                }
            }

            @Override
            public void onFailure(Call<ReceiveShipmentInvoice_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }

                        }

                    }
                }
                String USER_TYPEs = Utils.getPreference(RSByMasterCartonsActivity.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_RETAILER) || USER_TYPEs.equals(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else {
                    getinvoice_codes();
                }


            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {

        retrofitClient.getService().getInvoiceCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(Constants.TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        objDatabaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                    getReceiveNewShipmentList();
                }
            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
