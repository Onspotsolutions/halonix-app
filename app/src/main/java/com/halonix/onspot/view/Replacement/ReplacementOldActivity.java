package com.halonix.onspot.view.Replacement;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReplacementOldActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvModel)
    AppCompatTextView tvModel;

    @BindView(R.id.tvSweep)
    AppCompatTextView tvSweep;

    @BindView(R.id.tvVoltage)
    AppCompatTextView tvVoltage;

    @BindView(R.id.tvWarranty)
    AppCompatTextView tvWarranty;

    @BindView(R.id.linVoltage)
    LinearLayout linVoltage;

    @BindView(R.id.btnFinish)
    AppCompatButton btnFinish;

    private long mLastClickTime = 0;
    private String tag = "";
    private String model;
    private String sweep;
    private String voltage;
    ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;
    private String qr_code;
    DatabaseHelper databaseHelper;
    private AlertDialog builder;
    private String warranty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_replacement);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(ReplacementOldActivity.this);
        databaseHelper = new DatabaseHelper(ReplacementOldActivity.this);
        if (getIntent().getExtras() != null) {
            tag = getIntent().getStringExtra("tag");
            model = getIntent().getStringExtra("model");
            sweep = getIntent().getStringExtra("sweep");
            voltage = getIntent().getStringExtra("voltage");
            warranty = getIntent().getStringExtra("warranty");
            qr_code = getIntent().getStringExtra("qr_code");

            tvModel.setText(model);
            tvSweep.setText(sweep);
            if (voltage == null) {
                linVoltage.setVisibility(View.GONE);
            }
            tvVoltage.setText(voltage);
            tvWarranty.setText(warranty);
        }
        retrofitClient = new RetrofitClient();
        toolbar.setTitle("Replacement");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(ReplacementOldActivity.this, ReplacementScannerActivity.class);
                    intent.putExtra("tag", "new");
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                } else {
                    showNetworkFailDialog();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (Utils.getPreference(ReplacementOldActivity.this, "old_qr", "").length() > 0) {
            new AlertDialog.Builder(ReplacementOldActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utils.removePreference(ReplacementOldActivity.this, "old_qr");
                            Utils.removePreference(ReplacementOldActivity.this, "new_qr");
                            Intent intent = new Intent(ReplacementOldActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            ReplacementOldActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }


    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(ReplacementOldActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }


}
