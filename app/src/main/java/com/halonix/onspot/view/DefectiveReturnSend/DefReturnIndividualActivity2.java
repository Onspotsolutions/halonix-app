package com.halonix.onspot.view.DefectiveReturnSend;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.SenderDetails_respo;
import com.halonix.onspot.model.AgentListModel;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DefReturnIndividualActivity2 extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edINNumber)
    AppCompatEditText edINNumber;

    @BindView(R.id.edReason)
    AppCompatEditText edReason;

    @BindView(R.id.edInvoiceDate)
    AppCompatEditText edInvoiceDate;

    @BindView(R.id.InputINNumber)
    TextInputLayout InputINNumber;

    @BindView(R.id.InputInvoiceDate)
    TextInputLayout InputInvoiceDate;

    @BindView(R.id.btnDispatch)
    AppCompatButton btnDispatch;

    @BindView(R.id.spinnerAgentList)
    Spinner spinnerAgentList;

    @BindView(R.id.tvRece)
    TextView tvRece;

    @BindView(R.id.rel)
    RelativeLayout rel;

    private Context context;

    private ConnectionDetectorActivity cd;
    private DatabaseHelper objDatabaseHelper;

    private long mLastClickTime = 0;
    private RetrofitClient retrofitClient;
    String myFormat = "dd-MM-yyyy";
    private final Calendar myCalendar = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    Dialog dialog;
    private ListView listView;

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }

        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_def_return_2);
        context = DefReturnIndividualActivity2.this;
        cd = new ConnectionDetectorActivity(context);
        ButterKnife.bind(this);
        objDatabaseHelper = DatabaseHelper.getInstance(context);

        retrofitClient = new RetrofitClient();
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });

        toolbar.setTitle("Defective Return Send");


        edINNumber.setLongClickable(false);
        edInvoiceDate.setLongClickable(false);
        edReason.setLongClickable(false);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(myFormat);
        String formattedDate = df.format(c);
        edInvoiceDate.setText(formattedDate);

        setupAgencyList();

        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                dialog = new Dialog(DefReturnIndividualActivity2.this);
                LayoutInflater inflater = LayoutInflater.from(DefReturnIndividualActivity2.this);
                View view1 = inflater.inflate(R.layout.searchable_list_dialog, null);
                listView = view1.findViewById(R.id.listItems);
                SearchView searchView = view1.findViewById(R.id.search);

                Collections.sort(arrayAgent1, (text1, text2) -> text1.getName().compareToIgnoreCase(text2.getName()));
                StopArrayAdapter spinnerAgentAdapter = new StopArrayAdapter(DefReturnIndividualActivity2.this, arrayAgent1); //selected item will look like a spinner set from XML
                spinnerAgentAdapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
                listView.setAdapter(spinnerAgentAdapter);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String newText) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        spinnerAgentAdapter.filter(newText);
                        return false;
                    }
                });
                dialog.setContentView(view1);
                dialog.show();
            }
        });

        edINNumber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        edInvoiceDate.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        edReason.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        btnDispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!validateRVNumber()) {
                    return;
                }

                Utils.hideKeyboard(DefReturnIndividualActivity2.this);
                if (cd.isConnectingToInternet()) {

                    dispatchShipment();
                } else {
                    offlineshipment();
                    //showNetworkFailDialog();
                }


            }
        });

        edINNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateRVNumber()) {
                    return;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

        final DatePickerDialog.OnDateSetListener InDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                edInvoiceDate.setText(sdf.format(myCalendar.getTime()));
            }

        };

        edInvoiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, InDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

    }

    private void offlineshipment() {
        SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
        String def_sender_id = sharedPreferences.getString("def_sender_id", "");
        long res = objDatabaseHelper.updateAllDefCodewithSenderZero(def_sender_id, edINNumber.getText().toString());
        Log.d(Constants.TAG, "offlineshipment res: " + res);
        objDatabaseHelper.insertDefRetSendOffline(def_sender_id, edINNumber.getText().toString(),
                edInvoiceDate.getText().toString(), edReason.getText().toString(), "1");

        Utils.showToast(DefReturnIndividualActivity2.this, "Defective return send shipment stored offline.");
        Intent intent = new Intent(DefReturnIndividualActivity2.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        DefReturnIndividualActivity2.this.finish();
        overridePendingTransition(0, 0);
    }

    public class StopArrayAdapter extends ArrayAdapter<AgentListModel> {

        private ArrayList<AgentListModel> items;
        private ArrayList<AgentListModel> arraylist;
        LayoutInflater flater;

        public StopArrayAdapter(Context activity, ArrayList<AgentListModel> items) {
            super(activity, android.R.layout.simple_list_item_1, items);
            this.items = items;
            this.arraylist = new ArrayList<>();
            this.arraylist.addAll(items);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public AgentListModel getItem(int position) {
            return items.get(position);
        }

        private class viewHolder {
            TextView txtTitle;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            AgentListModel rowItem = getItem(position);

            StopArrayAdapter.viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {
                holder = new StopArrayAdapter.viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.my_simple_spinner_item, parent, false);
                holder.txtTitle = rowview.findViewById(R.id.spin_item);
                rowview.setTag(holder);
            } else {
                holder = (StopArrayAdapter.viewHolder) rowview.getTag();
            }
            String USER_TYPEs = Utils.getPreference(DefReturnIndividualActivity2.this, PreferenceKeys.USER_TYPE, "");
            if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getName() + " - " + rowItem.getLocation());
            } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
            } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
            }

            rowview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(activity, rowItem.getCp_person(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("def_sender_id", rowItem.getId());
                    editor.apply();
                    Log.d(Constants.TAG, "onClick: " + arraylist.size());
                    items.clear();
                    items.addAll(arraylist);
                    String USER_TYPEs = Utils.getPreference(DefReturnIndividualActivity2.this, PreferenceKeys.USER_TYPE, "");
                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getName() + " - " + rowItem.getLocation());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
                    }

                }
            });
            return rowview;
        }

        void filter(String charText) {
            charText = charText.toLowerCase();
            items.clear();
            if (charText.length() == 0) {
                items.addAll(arraylist);
            } else {
                for (AgentListModel s : arraylist) {
                    String USER_TYPEs = Utils.getPreference(DefReturnIndividualActivity2.this, PreferenceKeys.USER_TYPE, "");

                    if (s.getName().toLowerCase().contains(charText)) {
                        items.add(s);
                    } else if (s.getCp_person().toLowerCase().contains(charText)) {
                        items.add(s);
                    } else if (s.getLocation().toLowerCase().contains(charText)) {
                        items.add(s);
                    }
                }
/*
                if (items.size() == 0) {
                    txtNoResult.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                } else {
                    txtNoResult.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }*/
            }

            notifyDataSetChanged();
        }

    }


    private ArrayList<AgentListModel> arrayAgent = new ArrayList<>();
    ArrayList<AgentListModel> arrayAgent1 = new ArrayList<>();

    //TODO set recipient to spinner as per send
    public void setupAgencyList() {
        arrayAgent = new ArrayList<>();
        arrayAgent1 = new ArrayList<>();
        final ProgressDialog progressBar = new ProgressDialog(context);
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrayAgent = objDatabaseHelper.getAgentList("Vendor");
                Collections.sort(arrayAgent, (text1, text2) -> text1.getName().compareToIgnoreCase(text2.getName()));
                for (int i = 0; i < arrayAgent.size(); i++) {
                    if (arrayAgent.get(i).getName() == null) {
                        arrayAgent.get(i).setName("");
                    }
                    arrayAgent1.add(arrayAgent.get(i));
                }
                SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("def_sender_id", arrayAgent1.get(0).getId());
                editor.apply();
                String USER_TYPEs = Utils.getPreference(DefReturnIndividualActivity2.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getName() + " - " + arrayAgent1.get(0).getLocation());
                } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getLocation());
                } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getLocation());
                }
                progressBar.dismiss();
            }
        }, 1000);
    }

    private void dispatchShipment() {
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedDefCodeListForUpload("1");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
        String def_sender_id = sharedPreferences.getString("def_sender_id", "");
        int agency_id = Integer.parseInt(def_sender_id);
        Log.d(Constants.TAG, "dispatchShipment: " + jsonArray.toString());
        Log.d(Constants.TAG, "dispatchShipment: " + edReason.getText().toString());
        Log.d(Constants.TAG, "dispatchShipment: " + edINNumber.getText().toString());
        Log.d(Constants.TAG, "dispatchShipment: " + edInvoiceDate.getText().toString());
        Log.d(Constants.TAG, "dispatchShipment: " + agency_id);

        Utils.showprogressdialog(DefReturnIndividualActivity2.this, "Requesting for sending defective return shipment,please wait.");
        retrofitClient.getService().defective_return_send(objDatabaseHelper.getAuthToken(), edReason.getText().toString(), jsonArray.toString(), agency_id, edINNumber.getText().toString(), edInvoiceDate.getText().toString()).enqueue(new Callback<SenderDetails_respo>() {
            @Override
            public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {
                Utils.dismissprogressdialog();

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    Utils.removePreference(DefReturnIndividualActivity2.this, "def_sender_id");
                    objDatabaseHelper.deleteScannedDefCodes();

                    Intent intent = new Intent(DefReturnIndividualActivity2.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    DefReturnIndividualActivity2.this.finish();
                    overridePendingTransition(0, 0);
                    Utils.showToast(DefReturnIndividualActivity2.this, "Shipment dispatched successfully");
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(DefReturnIndividualActivity2.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    private AlertDialog builder;

    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(DefReturnIndividualActivity2.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }


    private boolean validateRVNumber() {
        String po_number = edINNumber.getText().toString().trim();
        boolean res = objDatabaseHelper.checkRINinDB(po_number);
        if (po_number.isEmpty()) {
            InputINNumber.setError("Enter return invoice number");
            edINNumber.requestFocus();
            return false;
        } else if (res) {
            InputINNumber.setError("This return invoice no. already used for other offline shipment,please use different return invoice number.");
            edINNumber.requestFocus();
            return false;
        } else {
            InputINNumber.setErrorEnabled(false);
        }
        return true;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
