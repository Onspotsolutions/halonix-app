package com.halonix.onspot.view;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.Login_OTP.SignInActivity;
import com.halonix.onspot.view.Packaging.PackagingProductListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Splash extends AppCompatActivity {

    Context cntx;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    int SPLASH_TIME_OUT = 2000;
    DatabaseHelper databaseHelper;
    private ArrayList<String> ScannedQrList = new ArrayList<>();
    private ArrayList<String> ScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> ScannedMasterQrList = new ArrayList<>();
    ConnectionDetectorActivity connectionDetectorActivity;
    private AlertDialog newAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        changeStatusBarColor();
        cntx = this;
        connectionDetectorActivity = new ConnectionDetectorActivity(cntx);
        databaseHelper = DatabaseHelper.getInstance(Splash.this);

//        login_direct("V");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                launchHomeScreen();
            }
        } else {
            launchHomeScreen();
        }
    }

    private void login_direct(String tag) {
        if (tag.equals("V")) {
            //------------------------------------Vendor------------------------------------//
            databaseHelper.insertRegisteredUser(
                    "",
                    "9049471792",
                    "halonix fan",
                    "k7gz3l5r", "11");

            Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, "halonix fan");
            Utils.addPreference(cntx, PreferenceKeys.USER_TYPE, Constants.USER_TYPE_VENDOR);
            ArrayList<String> shipmentTypes = new ArrayList<>();
            shipmentTypes.add("CWC");
            Utils.addPreference(cntx, PreferenceKeys.USER_SHIPMENT_TYPE, String.valueOf(shipmentTypes));
        } else if (tag.equals("B")) {
            //------------------------------------Branch------------------------------------//

            if (databaseHelper.getAuthToken().equals("")) {
                databaseHelper.insertRegisteredUser(
                        "",
                        "8433124563",
                        "branch1",
                        "d6l9qjgp", "74");

                Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, "branch1");
                Utils.addPreference(cntx, PreferenceKeys.USER_TYPE, Constants.USER_TYPE_BRANCH);
                ArrayList<String> shipmentTypes = new ArrayList<>();
                shipmentTypes.add(Constants.USER_TYPE_BRANCH);
                shipmentTypes.add(Constants.USER_TYPE_DISTRIBUTOR);
                shipmentTypes.add(Constants.USER_TYPE_CC);
                Utils.addPreference(cntx, PreferenceKeys.USER_SHIPMENT_TYPE, String.valueOf(shipmentTypes));
            }

        } else if (tag.equals("D")) {
            //------------------------------------Dealer------------------------------------//
            databaseHelper.insertRegisteredUser(
                    "",
                    "9420633853",
                    "Halonix1",
                    "6xl6olk7", "14");

            Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, "Halonix1");
            Utils.addPreference(cntx, PreferenceKeys.USER_TYPE, Constants.USER_TYPE_DEALER);
            ArrayList<String> shipmentTypes = new ArrayList<>();
            shipmentTypes.add(Constants.USER_TYPE_RETAILER);
            shipmentTypes.add(Constants.USER_TYPE_CC);
            Utils.addPreference(cntx, PreferenceKeys.USER_SHIPMENT_TYPE, String.valueOf(shipmentTypes));
        } else {
            //------------------------------------Retailer------------------------------------//
            databaseHelper.insertRegisteredUser(
                    "",
                    "1234567890",
                    "Retailer1",
                    "r2v26ngz", "85");

            Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, "Retailer1");
            Utils.addPreference(cntx, PreferenceKeys.USER_TYPE, Constants.USER_TYPE_RETAILER);
        }
        /*[{"MasterCartonCode":"ZL96ZRLJ","Qrcode":["OLNZM9LE","BVKRM3GZ"]}]
         * [{"MasterCartonCode":"WLEZ3PL1","Qrcode":["JV3O24GR","NV5ONXLJ"]}]*/
    }

    private boolean checkAndRequestPermissions() {
        int GpsFindLocationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int CAMERA = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (GpsFindLocationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (CAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                launchHomeScreen();
            }
        } else {
            launchHomeScreen();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Log.d(Constants.TAG, "onRequestPermissionsResult if: ");
                    //Case 2
                    launchHomeScreen();
//
                } else {
                    Log.d(Constants.TAG, "onRequestPermissionsResult else: ");

                    if (ActivityCompat.shouldShowRequestPermissionRationale(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            || ActivityCompat.shouldShowRequestPermissionRationale(Splash.this, Manifest.permission.CAMERA)) {
                        showDialogOK("Permissions (Camera & Location) are required for this app.");
                    } else {
                        explain("You need to Grant permissions to continue. Do you want to go to app settings?");
                    }
                }

            }
        }

    }

    private void explain(String msg) {

        if (null == newAlertDialog)
            newAlertDialog = new androidx.appcompat.app.AlertDialog.Builder(Splash.this, Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? android.R.style.Theme_Material_Light_Dialog_Alert : -1)
                    .setCancelable(false)
                    .setTitle("Permissions Required!")
                    .setMessage(msg)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null)));
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        newAlertDialog.show();
    }

    private void showDialogOK(String msg) {
        if (null == newAlertDialog)
            newAlertDialog = new AlertDialog.Builder(Splash.this, Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? android.R.style.Theme_Material_Light_Dialog_Alert : -1)
                    .setCancelable(false)
                    .setTitle("Permissions Required!")
                    .setMessage(msg)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(Splash.this, Manifest.permission.ACCESS_FINE_LOCATION)
                                    || ActivityCompat.shouldShowRequestPermissionRationale(Splash.this, Manifest.permission.CAMERA)) {
                                checkAndRequestPermissions();
                            } else {
                                startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.fromParts("package", getPackageName(), null)));
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        newAlertDialog.show();
    }

    private void launchHomeScreen() {
        new Handler().postDelayed(() -> {

            if (databaseHelper.getAuthToken().equals("")) {
                startActivity(new Intent(Splash.this, SignInActivity.class));
            } else {
                String USER_TYPEs = Utils.getPreference(Splash.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {

                    ScannedQrList = databaseHelper.getScannedQRList("0");
                    ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");
                    ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");

                    if (ScannedQrList.size() == 0) {
                        ScannedQrList = databaseHelper.getTempScannedQRList();
                    } else {
                        if (databaseHelper.getTempScannedQRList() != null)
                            ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
                    }
                    if (ScannedBladeQrList.size() == 0) {
                        ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
                    } else {
                        if (databaseHelper.getTempScannedBladeQRList() != null)
                            ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
                    }
                    if (ScannedMasterQrList.size() == 0) {
                        ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
                    } else {
                        if (databaseHelper.getTempScannedMasterQRListbyFlag("0") != null)
                            ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("0"));
                    }


                    if (ScannedQrList.size() > 0 || ScannedMasterQrList.size() > 0 || ScannedBladeQrList.size() > 0) {
                        Intent intent = new Intent(Splash.this, PackagingProductListActivity.class);
                        intent.putExtra("item_code", Utils.getPreference(Splash.this, "item_code", ""));
                        intent.putExtra("blade", Utils.getPreferenceBoolean(Splash.this, "blade", false));
                        intent.putExtra("model", Utils.getPreference(Splash.this, "model", ""));
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    } else {
                        startActivity(new Intent(Splash.this, MainActivity.class));
                    }
                } else {
                    startActivity(new Intent(Splash.this, MainActivity.class));
                }

            }
            Splash.this.finish();
            finish();
        }, SPLASH_TIME_OUT);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }
}
