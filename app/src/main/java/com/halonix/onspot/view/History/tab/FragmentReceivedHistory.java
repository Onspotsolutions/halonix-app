package com.halonix.onspot.view.History.tab;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.material.tabs.TabLayout;
import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentReceivedHistory extends BaseFragment implements ConnectionLostCallback {


    @BindView(R.id.pager)
    ViewPager mViewPager;

    @BindView(R.id.linbody)
    RelativeLayout linbody;

    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    ArrayList<String> tabTitles = new ArrayList<>();


    Context ctx;
    SectionsPagerAdapter adapter;
    private BroadcastReceiver myBroadcastReceiver;
    private boolean receiver = false;

    protected void unregisterNetworkChanges() {
        try {
            getActivity().unregisterReceiver(myBroadcastReceiver);
            receiver = false;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(Constants.TAG, "onDestroy: " + receiver);
        if (receiver) {
            unregisterNetworkChanges();

        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        ctx = getActivity();
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Resources resources = getActivity().getResources();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(resources.getString(R.string.received_history_page_title));
        enableNavigationDrawer(false);


        registerReceiver();

    }

    private void registerReceiver() {
        if (!receiver) {
            myBroadcastReceiver = new MyBroadcastReceiver(this);
            getActivity().registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            receiver = true;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_received_history, container, false);
        ButterKnife.bind(this, view);

        tabTitles = new ArrayList<>();
        tabTitles.add("COMPLETED");
        tabTitles.add("PENDING");
        tabTitles.add("SHORTFALL");

        adapter = new SectionsPagerAdapter(getChildFragmentManager(), tabTitles);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(mViewPager);

        String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
        if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_DEALER)) {

        } else {
            tabTitles.remove(2);
            mViewPager.getAdapter().notifyDataSetChanged();
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();

                switch (position) {
                    case 0:
                        FragmentReceiveCompleted fragmentCompleted = (FragmentReceiveCompleted) adapter.instantiateItem(mViewPager, 0);
                        if (fragmentCompleted != null) {
                            fragmentCompleted.fragmentBecameVisible();
                        }
                        break;
                    case 1:
                        FragmentReceivePending fragmentPending = (FragmentReceivePending) adapter.instantiateItem(mViewPager, 1);
                        if (fragmentPending != null) {
                            fragmentPending.fragmentBecameVisible();
                        }
                        break;
                    case 2:
                        FragmentReceiveShortfall fragmentReceiveShortfall = (FragmentReceiveShortfall) adapter.instantiateItem(mViewPager, 2);
                        if (fragmentReceiveShortfall != null) {
                            fragmentReceiveShortfall.fragmentBecameVisible();
                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        registerReceiver();
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }


    public void dialog(boolean value) {
        Log.d(Constants.TAG, "dialog value: " + value);
        if (value) {

            no_internet.setVisibility(View.GONE);
            linbody.setVisibility(View.VISIBLE);
            adapter = new SectionsPagerAdapter(getChildFragmentManager(), tabTitles);
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(mViewPager);
        } else {
            linbody.setVisibility(View.GONE);
            no_internet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }


    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<String> strings;

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<String> tabTitles) {
            super(fm);
            strings = tabTitles;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            FragmentManager manager = ((Fragment) object).getFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove((Fragment) object);
            trans.commit();
            super.destroyItem(container, position, object);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    fragment = new FragmentReceiveCompleted();
                    return fragment;
                case 1:
                    fragment = new FragmentReceivePending();
                    return fragment;

                case 2:
                    fragment = new FragmentReceiveShortfall();
                    return fragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return strings.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return strings.get(position).toUpperCase();
        }
    }
}
