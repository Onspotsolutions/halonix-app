package com.halonix.onspot.view.DefectiveReturnSend;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.ReceiveDefectiveShipmentList_respo;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Utils.getDate;

/**
 * A simple {@link Fragment} subclass.
 */
public class DefReturnCompleted extends Fragment {


    @BindView(R.id.rv_pending)
    RecyclerView rv_pending;

    @BindView(R.id.textView_no_data_found_pending)
    TextView textViewNoDataFound;

    private DatabaseHelper objDatabaseHelper = null;
    private ConnectionDetectorActivity cd = null;

    private ArrayList<ReceiveDefectiveShipmentList_respo> arrCompletedListModel;
    private ArrayList<CompletedListModel> completedListModelArrayList;

    RetrofitClient retrofitClient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetectorActivity(getActivity());
        objDatabaseHelper = DatabaseHelper.getInstance(getActivity());
    }

    public void fragmentBecameVisible() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_shipment_completed, container, false);
        ButterKnife.bind(this, mainView);
        retrofitClient = new RetrofitClient();
        rv_pending.setHasFixedSize(true);
        rv_pending.setItemAnimator(new DefaultItemAnimator());
        rv_pending.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (cd.isConnectingToInternet()) {
            getCompletedListFromServer();
        }

        return mainView;
    }


    private void getCompletedListFromServer() {

        arrCompletedListModel = new ArrayList<>();
        completedListModelArrayList = new ArrayList<>();
        retrofitClient.getService().DefectiveReceiveHistory(objDatabaseHelper.getAuthToken(), true).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {


                Log.d(Constants.TAG, "onResponse: " + response.toString());

                if (response.body().getSuccess()) {

                    textViewNoDataFound.setVisibility(TextView.GONE);
                    arrCompletedListModel = response.body().getData();

                    if (arrCompletedListModel.size() > 0) {
                        for (int i = 0; i < arrCompletedListModel.size(); i++) {
                            CompletedListModel completedListModel = new CompletedListModel(arrCompletedListModel.get(i).getQuantity(),
                                    arrCompletedListModel.get(i).getClient(), arrCompletedListModel.get(i).getReturn_invoice_no(),
                                    arrCompletedListModel.get(i).getReturn_invoice_date());
                            completedListModelArrayList.add(completedListModel);
                        }
                        Collections.sort(completedListModelArrayList, (o1, o2) -> o2.getReturn_invoice_date().compareTo(o1.getReturn_invoice_date()));
                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(completedListModelArrayList);
                        rv_pending.setAdapter(shipmentAdapter);
                    } else {
                        textViewNoDataFound.setVisibility(TextView.VISIBLE);
                    }
                } else {
                    textViewNoDataFound.setVisibility(TextView.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
            }
        });

    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {

        private List<CompletedListModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textViewInNo;
            private AppCompatTextView textView_Client;
            private AppCompatTextView textViewInDate;
            private AppCompatTextView textViewQuantity;
            private LinearLayout ael;
            private AppCompatTextView tvMore;
            private View v;

            public MyViewHolder(View view) {
                super(view);
                textViewInNo = view.findViewById(R.id.textView_in_no);
                textViewInDate = view.findViewById(R.id.textView_in_date);
                textViewQuantity = view.findViewById(R.id.textView_qty);
                textView_Client = view.findViewById(R.id.textView_Client);
                ael = view.findViewById(R.id.expandable_ans);
                tvMore = view.findViewById(R.id.tvMore);
                v = view.findViewById(R.id.view);
            }
        }


        public ShipmentAdapter(List<CompletedListModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public ShipmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_saledef_history, parent, false);
            return new ShipmentAdapter.MyViewHolder(itemView);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(ShipmentAdapter.MyViewHolder holder, final int position) {
            final CompletedListModel object = notesList.get(position);
            holder.v.setVisibility(View.INVISIBLE);
            holder.textViewInDate.setText(getDate(getDisplayDate(object.getReturn_invoice_date()), "yyyy-MM-dd", "dd-MM-yyyy"));
            holder.textViewInNo.setText(object.getReturn_invoice_no());
            holder.textViewQuantity.setText(String.valueOf(object.getQuantity()));
            holder.textView_Client.setText(object.getClient());

            holder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.ael.setVisibility(holder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (holder.ael.getVisibility() == View.VISIBLE) {
                        holder.tvMore.setText("Less Details");
                    } else {
                        holder.tvMore.setText("More Details");
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }


    private class CompletedListModel {
        int id;
        int quantity;
        String client;
        String return_invoice_no;
        String return_invoice_date;

        public CompletedListModel(int quantity, String client, String return_invoice_no, String return_invoice_date) {
            this.quantity = quantity;
            this.client = client;
            this.return_invoice_no = return_invoice_no;
            this.return_invoice_date = return_invoice_date;
        }

        public int getId() {
            return id;
        }

        public int getQuantity() {
            return quantity;
        }


        public String getClient() {
            return client;
        }

        public String getReturn_invoice_no() {
            return return_invoice_no;
        }

        public String getReturn_invoice_date() {
            return return_invoice_date;
        }
    }


}
