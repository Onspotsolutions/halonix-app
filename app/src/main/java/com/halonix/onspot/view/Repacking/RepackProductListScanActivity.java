package com.halonix.onspot.view.Repacking;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class RepackProductListScanActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;

    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnGo)
    Button btnGo;

    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private CaptureManager capture;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.button_stop_scanning)
    Button buttonStopScanning;

    @BindView(R.id.btnDone)
    Button btnDone;

    @BindView(R.id.btnFinish)
    Button btnFinish;

    @BindView(R.id.textInput)
    TextInputLayout textInput;

    private BeepManager beepManager = null;
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;

    private int packing_ratio;

    ArrayList<String> qrcodeArrayList;
    private JSONArray QRjsonArray;
    private JSONArray BladejsonArray;
    private JSONArray MasterQRjsonArray;

    ArrayList<String> bladecodeArrayList;
    ArrayList<String> masterqrcodeArrayList;

    private ArrayList<String> tempScannedQrList = new ArrayList<>();
    private ArrayList<String> tempScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> tempScannedMasterQrList = new ArrayList<>();

    private String item_code;
    private String item_code1;
    private boolean blade;

    RetrofitClient retrofitClient;
    ConnectionDetectorActivity cd;
    private long mLastClickTime = 0;
    private AlertDialog.Builder alert = null;
    private AlertDialog.Builder alert1 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_scan);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(RepackProductListScanActivity.this);
        objDatabaseHelper = DatabaseHelper.getInstance(RepackProductListScanActivity.this);
        retrofitClient = new RetrofitClient();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
            overridePendingTransition(0, 0);
        });

        if (getIntent().getExtras() != null) {
            item_code1 = Utils.getPreference(RepackProductListScanActivity.this, "item_code", "");
            String[] str = Utils.getPreference(RepackProductListScanActivity.this, "item_code", "").split("_");
            item_code = str[0];
            Log.d("suraj", "onCreate: " + item_code);
            Log.d("suraj", "onCreate: " + item_code1);
            blade = getIntent().getExtras().getBoolean("blade");
        }


        btnDone.setVisibility(View.VISIBLE);
        toolbar.setTitle(getResources().getString(R.string.repack_products));

        if (blade) {
            bladecodeArrayList = new ArrayList<>();
            bladecodeArrayList = objDatabaseHelper.getBladeCodeListRepack(item_code);
            tempScannedBladeQrList = objDatabaseHelper.getTempScannedBladeQRList(item_code);
            Log.d("suraj", "onCreate: " + bladecodeArrayList.toString());
            Log.d(Constants.TAG, "onCreate ScannedQrList: " + tempScannedBladeQrList.toString());
        }

        qrcodeArrayList = new ArrayList<>();
        masterqrcodeArrayList = new ArrayList<>();
        qrcodeArrayList = objDatabaseHelper.getQRCodeListRepack(item_code);
        MasterQRjsonArray = objDatabaseHelper.getMasterQRCodeListRepack().getJsonArray();

        tempScannedQrList = objDatabaseHelper.getTempScannedQRList(item_code);
        Log.d(Constants.TAG, "onCreate tempScannedQrList: " + tempScannedQrList.toString());

        tempScannedMasterQrList = objDatabaseHelper.getTempScannedMasterQRList(item_code);
        Log.d(Constants.TAG, "onCreate tempScannedMasterQrList: " + tempScannedMasterQrList.toString());


//        for (int i = 0; i < QRjsonArray.length(); i++) {
//            try {
//                qrcodeArrayList.add(String.valueOf(QRjsonArray.get(i)));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }

        for (int i = 0; i < MasterQRjsonArray.length(); i++) {
            try {
                masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayList<String> ScannedMasterQrList = objDatabaseHelper.getScannedMasterQRList("1");
        Log.d("suraj", "onCreate: " + ScannedMasterQrList);


        ArrayList<String> union = new ArrayList<>(masterqrcodeArrayList);
        union.addAll(ScannedMasterQrList);
        ArrayList<String> intersection = new ArrayList<>(masterqrcodeArrayList);
        intersection.retainAll(ScannedMasterQrList);
        union.removeAll(intersection);

        masterqrcodeArrayList = new ArrayList<>();
        masterqrcodeArrayList.addAll(union);


        Log.d(Constants.TAG, "onCreate qrcodeArrayList: " + qrcodeArrayList.toString());
        Log.d(Constants.TAG, "onCreate masterqrcodeArrayList: " + masterqrcodeArrayList.toString());

        packing_ratio = objDatabaseHelper.getPackingRatioRepack(Utils.getPreference(RepackProductListScanActivity.this, "item_code", ""), "0");
        Log.d(Constants.TAG, "packing_ratio masterqrcodeArrayList: " + packing_ratio);

        btnFinish.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            if (!edScanCode.getText().toString().equals("")) {
                alert1 = new AlertDialog.Builder(RepackProductListScanActivity.this);
                alert1.setTitle("Alert");
                alert1.setCancelable(false);
                alert1.setMessage("This Qr-code is invalid");
                alert1.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edScanCode.setText("");
                        edScanCode.requestFocus();
                        edScanCode.setCursorVisible(true);
                        edScanCode.setFocusable(true);
                        edScanCode.setFocusableInTouchMode(true);
                        if (objDatabaseHelper.getScannedInvoiceQRList().size() > 0) {
                            Log.d(Constants.TAG, "onClick getScannedInvoiceQRList: ");
                            edScanCode.setText(objDatabaseHelper.getScannedInvoiceQRList().get(0));
                            edScanCode.setSelection(edScanCode.getText().toString().length());
                        }
                        alert1 = null;

                    }
                });
                alert1.show();

            } else {
                Toast.makeText(RepackProductListScanActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
            }


        });

        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });


        buttonStopScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                stopScanning(v);
            }
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().trim().equals("")) {
                    checkbarcode(edScanCode.getText().toString().trim(), "manual");
                } else {
                    Toast.makeText(RepackProductListScanActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);


        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    capture.onPause();
                    return;
                } else {
                    String[] final_barcode;
                    if (result.getText().contains("=")) {
                        final_barcode = result.getText().split("=");
                        String barCode1 = final_barcode[1].replaceAll("\u0000", "");

                        checkbarcode(barCode1, "camera");
                    } else {
                        checkbarcode(result.getText(), "camera");
                    }
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //capture.onResume();
                    }
                }, 3000);
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });
    }


    private void checkbarcode(String barcode, String tag) {
        Log.d("suraj", "checkbarcode packing_ratio: " + packing_ratio);
        if (isBarcodeMasterQrExistsInSystem(barcode) && tempScannedQrList.size() < packing_ratio && !blade) {
            showAlert("Alert", "Please scan Product Qr-code first before scan Master Qr-code.", true, tag);
        } else if (isBarcodeMasterQrExistsInSystem(barcode) && blade && tempScannedBladeQrList.size() < packing_ratio) {
            if (tempScannedQrList.size() < packing_ratio) {
                showAlert("Alert", "Please scan Product Qr-code first before scan Master Qr-code.", true, tag);
            } else {
                showAlert("Alert", "Please scan Blade Qr-code first before scan Master Qr-code.", true, tag);
            }
        } else {
            if (!blade) {
                if (isBarcodeExistsInSystem(barcode)) {
                    if (tempScannedMasterQrList.size() == 0) {
                        if (!isTempBarcodeExists(barcode, item_code)) {
                            if (tempScannedQrList.size() == packing_ratio) {
                                showAlert("Alert", "Product Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                            } else {
                                if (!isBarcodeExists(barcode, item_code1)) {
                                    capture.onResume();
                                    tempScannedQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedQRList(barcode, item_code, 0);
                                    clearEditScan();

                                    if (tempScannedQrList.size() < packing_ratio) {
                                        Utils.showToast(RepackProductListScanActivity.this, "Now Scan Product Qr-code.");
                                        capture.onResume();

                                    } else if (tempScannedQrList.size() == packing_ratio) {
                                        showAlert("Alert", "Product Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                    }
                                } else {
                                    showAlert("Alert", "This Product Qr-code is already used.", true, tag);
                                }
                            }
                        } else {
                            showAlert("Alert", "This Product Qr-code is already scanned.", true, tag);
                        }
                    } else {
                        showAlert("Alert", "Product & Master Qr-code limit reached.", true, tag);
                    }

                } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                    if (!isMasterTempBarcodeExists(barcode, item_code)) {

                        if (isMasterBarcodeExists(barcode, item_code1)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            if (tempScannedMasterQrList.size() == 1) {
                                showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                            } else {
                                capture.onResume();
                                tempScannedMasterQrList.add(barcode);
                                objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                clearEditScan();
                                gotoproductListpage();
                            }
                        }
                    } else {
                        if (isMasterBarcodeExists(barcode, item_code1)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                        }
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true, tag);
                }
            } else if (blade) {
                Log.d(Constants.TAG, "checkbarcode blade: " + blade);
                if (tempScannedBladeQrList.size() < packing_ratio) {
                    if (isBarcodeExistsInSystem(barcode)) {
                        if (!isTempBarcodeExists(barcode, item_code)) {
                            if (tempScannedQrList.size() == packing_ratio) {
                                showAlert("Alert", "Product Qr-code limit reached,Now Scan Blade Qr-code.", true, tag);
                            } else {
                                if (!isBarcodeExists(barcode, item_code1)) {
                                    capture.onResume();
                                    tempScannedQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedQRList(barcode, item_code, 0);
                                    clearEditScan();

                                    if (tempScannedQrList.size() < packing_ratio) {
                                        Utils.showToast(RepackProductListScanActivity.this, "Now Scan Product Qr-code.");
                                        capture.onResume();

                                    } else if (tempScannedQrList.size() == packing_ratio) {
                                        showAlert("Alert", "Product Qr-code limit reached,Now Scan Blade Qr-code.", true, tag);
                                    }
                                } else {
                                    showAlert("Alert", "This Product Qr-code is already used.", true, tag);
                                }
                            }
                        } else {
                            showAlert("Alert", "This Product Qr-code is already scanned.", true, tag);
                        }
                    } else if (isBarcodeBladeExistsInSystem(barcode)) {
                        if (tempScannedQrList.size() < packing_ratio) {
                            showAlert("Alert", "Please scan Product Qr-code first,before scan Blade Qr-code", true, tag);
                        } else {
                            if (!isTempBladeoBarcodeExists(barcode, item_code)) {
                                if (tempScannedBladeQrList.size() == packing_ratio) {
                                    showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master Qr-code.", true, tag);
                                } else {
                                    if (!isBladeoBarcodeExists(barcode, item_code1)) {
                                        capture.onResume();
                                        tempScannedBladeQrList.add(barcode);
                                        objDatabaseHelper.insertTempScannedBladeQRList(barcode, item_code, 0);
                                        clearEditScan();

                                        if (tempScannedBladeQrList.size() < packing_ratio) {
                                            Utils.showToast(RepackProductListScanActivity.this, "Now Scan Blade Qr-code.");
                                            capture.onResume();

                                        } else if (tempScannedBladeQrList.size() == packing_ratio) {
                                            showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                        }
                                    } else {
                                        showAlert("Alert", "This Blade Qr-code is already used.", true, tag);
                                    }
                                }
                            } else {
                                showAlert("Alert", "This Blade Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                        if (!isMasterTempBarcodeExists(barcode, item_code)) {

                            if (isMasterBarcodeExists(barcode, item_code1)) {
                                showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                            } else {
                                if (tempScannedMasterQrList.size() == 1) {
                                    showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                                } else {
                                    capture.onResume();
                                    tempScannedMasterQrList.add(barcode);
                                    objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                    clearEditScan();

                                    gotoproductListpage();
                                }
                            }
                        } else {
                            if (isMasterBarcodeExists(barcode, item_code1)) {
                                showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                            } else {
                                showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else {
                        showAlert("Alert", "This Qr-code is invalid.", true, tag);
                    }


                } else if (isBarcodeBladeExistsInSystem(barcode)) {
                    if (tempScannedMasterQrList.size() == 0) {
                        if (tempScannedQrList.size() < packing_ratio) {
                            showAlert("Alert", "Please scan Product Qr-code first,before scan Blade Qr-code", true, tag);
                        } else {
                            if (!isTempBladeoBarcodeExists(barcode, item_code)) {
                                if (tempScannedBladeQrList.size() == packing_ratio) {
                                    showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master Qr-code.", true, tag);
                                } else {
                                    if (!isBladeoBarcodeExists(barcode, item_code1)) {
                                        capture.onResume();
                                        tempScannedBladeQrList.add(barcode);
                                        objDatabaseHelper.insertTempScannedBladeQRList(barcode, item_code, 0);
                                        clearEditScan();

                                        if (tempScannedBladeQrList.size() < packing_ratio) {
                                            Utils.showToast(RepackProductListScanActivity.this, "Now Scan Blade Qr-code.");
                                            capture.onResume();

                                        } else if (tempScannedBladeQrList.size() == packing_ratio) {
                                            showAlert("Alert", "Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                                        }
                                    } else {
                                        showAlert("Alert", "This Blade Qr-code is already used.", true, tag);
                                    }
                                }
                            } else {
                                showAlert("Alert", "This Blade Qr-code is already scanned.", true, tag);
                            }
                        }
                    } else {
                        showAlert("Alert", "Product,Blade & Master Qr-code limit reached.", true, tag);
                    }

                } else if (isBarcodeMasterQrExistsInSystem(barcode)) {
                    if (!isMasterTempBarcodeExists(barcode, item_code)) {

                        if (isMasterBarcodeExists(barcode, item_code1)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            if (tempScannedMasterQrList.size() == 1) {
                                showAlert("Alert", "Master carton Qr-code limit reached.", true, tag);
                            } else {
                                capture.onResume();
                                tempScannedMasterQrList.add(barcode);
                                objDatabaseHelper.insertTempScannedMasterQRList(barcode, item_code, 0, String.valueOf(packing_ratio));
                                clearEditScan();

                                gotoproductListpage();
                            }
                        }
                    } else {
                        if (isMasterBarcodeExists(barcode, item_code1)) {
                            showAlert("Alert", "This Master carton Qr-code is already used.", true, tag);
                        } else {
                            showAlert("Alert", "This Master carton Qr-code is already scanned.", true, tag);
                        }
                    }
                } else {
                    if (!isBarcodeExistsInSystem(barcode) && !isBarcodeBladeExistsInSystem(barcode)
                            && !isBarcodeMasterQrExistsInSystem(barcode)) {
                        showAlert("Alert", "This Qr-code is invalid.", true, tag);
                    } else {
                        if (tempScannedMasterQrList.size() == 1) {
                            showAlert("Alert", "Product,Blade & Master Qr-code limit reached.", true, tag);
                        } else if (isBarcodeExistsInSystem(barcode) && tempScannedQrList.size() == packing_ratio && tempScannedMasterQrList.size() == 0) {
                            showAlert("Alert", "Product & Blade Qr-code limit reached,Now Scan Master carton Qr-code.", true, tag);
                        }
                    }


                }
            } else {
                showAlert("Alert", "This Qr-code is invalid.", true, tag);
            }

        }
    }

    private void gotoproductListpage() {
        Intent intent = new Intent(RepackProductListScanActivity.this, RepackProductsMainActivity.class);
        startActivity(intent);
        RepackProductListScanActivity.this.finish();
        overridePendingTransition(0, 0);
    }

    private void showAlert(String Title, String Message, boolean valid, String tag) {
        if (tag.equals("camera")) {
            capture.onPause();
        }
        alert = new AlertDialog.Builder(RepackProductListScanActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert = null;
            }
        });
        alert.show();
    }

    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }

    private boolean isBarcodeExistsInSystem(String strBarcode) {
        return qrcodeArrayList.contains(strBarcode);
    }

    private boolean isBarcodeBladeExistsInSystem(String strBarcode) {
        return bladecodeArrayList.contains(strBarcode);
    }

    private boolean isBarcodeMasterQrExistsInSystem(String strBarcode) {
        return masterqrcodeArrayList.contains(strBarcode);
    }

    private boolean isTempBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedQRList(barcode, item_code);
    }

    private boolean isBarcodeExists(String barcode, String item_code) {
        Log.d("suraj", "isBarcodeExists: " + item_code);
        return objDatabaseHelper.getScannedQRList(barcode, item_code);
    }

    private boolean isTempBladeoBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedBladeQRList(barcode, item_code);
    }

    private boolean isBladeoBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getScannedBladeQRList(barcode, item_code);
    }

    private boolean isMasterTempBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getTempScannedMasterQRList(barcode, item_code);
    }

    private boolean isMasterBarcodeExists(String barcode, String item_code) {
        return objDatabaseHelper.getScannedMasterQRList(barcode, item_code);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null && alert1 == null) {
            capture.onResume();
        }
        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);
        } else {
            focusable(false);


            relOTG.setVisibility(View.GONE);
            radio_otg.setChecked(false);

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setChecked(true);
            radio_camera.setEnabled(true);

        }
        textInput.setHint("QR Code");

    }

    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(RepackProductListScanActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            focusable(false);
            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);
            capture.onResume();

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);


            hideKeyboard(RepackProductListScanActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            focusable(false);

            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }
        return false;
    }


    private void barcodeLookup(String barCode) {

        String[] final_barcode;
        if (barCode.contains("=")) {
            final_barcode = barCode.split("=");
            barCode = final_barcode[1].replaceAll("\u0000", "");
            edScanCode.setText(barCode);

            checkbarcode(barCode, "OTG");

        } else {
            barCode = barCode.replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            checkbarcode(barCode, "OTG");
        }
    }
    //OTG Code End

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }

    @Override
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }


    public void stopScanning(View view) {
        onBackPressed();
    }


    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
