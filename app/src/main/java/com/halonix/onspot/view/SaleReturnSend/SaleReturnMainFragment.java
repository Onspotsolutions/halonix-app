package com.halonix.onspot.view.SaleReturnSend;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.base.BaseFragment;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaleReturnMainFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.button_sale_history)
    AppCompatButton button_sale_history;

    @BindView(R.id.button_scan_master)
    AppCompatButton button_scan_master;

    @BindView(R.id.textView_owner_name)
    AppCompatTextView textViewOwnerName;

    DatabaseHelper databaseHelper;
    ConnectionDetectorActivity cd;
    private RetrofitClient retrofitClient;
    private long mLastClickTime = 0;
    private AlertDialog builder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sale_return_main, container, false);
        ButterKnife.bind(this, view);

        databaseHelper = DatabaseHelper.getInstance(getActivity());
        cd = new ConnectionDetectorActivity(getActivity());
        retrofitClient = new RetrofitClient();
        textViewOwnerName.setText(databaseHelper.getUsername());
        button_sale_history.setOnClickListener(this);
        button_scan_master.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    private void showNetworkFailDialog(String msg) {

        builder = new AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    private ArrayList<String> BarcodeList = new ArrayList<>();

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_sale_history:
//                if (cd.isConnectingToInternet()) {
//                    changeFragmentTo(SaleReturnHistory.class.getSimpleName(), true, null);
//                } else {
//                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
//                }
                break;

            case R.id.button_scan_master:
                for (int index = 0; index < databaseHelper.getShipmentSystemBarcode().size(); index++) {
                    BarcodeList.add(String.valueOf(databaseHelper.getShipmentSystemBarcode().get(index).getShipmentCode()));
                }
                if (databaseHelper.getScannedMCCQRList().size() > 0) {
                    for (int i = 0; i < databaseHelper.getScannedMCCQRList().size(); i++) {
                        if (BarcodeList.contains(databaseHelper.getScannedMCCQRList().get(i))) {
                            BarcodeList.remove(i);
                        }
                    }
                }
                if (BarcodeList.size() > 0) {
                    Intent intent = new Intent(getActivity(), SaleReturnMasterCartonActivity1.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setCancelable(false)
                            .setMessage("Please sync server data first to proceed further.")
                            .setPositiveButton("Ok", null)
                            .show();
                }
                break;

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_sale_return);
        enableNavigationDrawer(false);
    }
}
