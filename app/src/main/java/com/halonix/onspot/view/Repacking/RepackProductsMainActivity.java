package com.halonix.onspot.view.Repacking;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepackProductsMainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvProduct)
    TextView tvProduct;

    @BindView(R.id.tvBlade)
    TextView tvBlade;

    @BindView(R.id.tvCartonCnt)
    TextView tvCartonCnt;

    @BindView(R.id.etRepackRatio)
    EditText etRepackRatio;

    @BindView(R.id.etMasterCarton)
    EditText etMasterCarton;

    @BindView(R.id.btnScan)
    Button btnScan;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;


    @BindView(R.id.linMaster)
    LinearLayout linMaster;


    @BindView(R.id.ly_scan_system)
    RelativeLayout ly_scan_system;

    @BindView(R.id.rvProductList)
    RecyclerView rvProductList;

    @BindView(R.id.button_packnext)
    AppCompatButton button_packnext;

    @BindView(R.id.button_done)
    AppCompatButton button_done;

    @BindView(R.id.button_discard_all)
    AppCompatButton button_discard_all;

    private DatabaseHelper databaseHelper;
    private ConnectionDetectorActivity cd;
    private RetrofitClient retrofitClient;
    private boolean blade = false;

    private int packing_ratio;
    private ArrayList<String> ScannedQrList = new ArrayList<>();
    private ArrayList<String> ScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> ScannedMasterQrList = new ArrayList<>();

    private ArrayList<String> tempScannedQrList = new ArrayList<>();
    private ArrayList<String> tempScannedBladeQrList = new ArrayList<>();
    private ArrayList<String> tempScannedMasterQrList = new ArrayList<>();

    private ArrayList<String> ScannedMasterQrListCnt = new ArrayList<>();

    JSONArray ScannedMasterQrJson;
    private long mLastClickTime = 0;
    private String item_code;
    private String qt;
    private int qty;
    private String model;
    ArrayList<String> masterqrcodeArrayList;
    private JSONArray ScannedQrJson;
    private JSONArray ScannedBladeQrJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repack_product_main);
        ButterKnife.bind(this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle(getResources().getString(R.string.repack_products));

        databaseHelper = DatabaseHelper.getInstance(RepackProductsMainActivity.this);
        retrofitClient = new RetrofitClient();
        cd = new ConnectionDetectorActivity(RepackProductsMainActivity.this);

        rvProductList.setHasFixedSize(true);
        rvProductList.setItemAnimator(new DefaultItemAnimator());
        rvProductList.setNestedScrollingEnabled(true);
        rvProductList.setLayoutManager(new LinearLayoutManager(RepackProductsMainActivity.this));

        button_done.setOnClickListener(this);
        button_packnext.setOnClickListener(this);
        button_discard_all.setOnClickListener(this);
        btnScan.setOnClickListener(this);

        item_code = Utils.getPreference(RepackProductsMainActivity.this, "item_code", "");
        blade = Utils.getPreferenceBoolean(RepackProductsMainActivity.this, "blade", false);
        model = Utils.getPreference(RepackProductsMainActivity.this, "model", "");
        Log.d(Constants.TAG, "onCreate: " + item_code);
        Log.d(Constants.TAG, "onCreate: " + blade);
        Log.d(Constants.TAG, "onCreate: " + model);
        if (getIntent().getExtras() != null) {
            qty = getIntent().getIntExtra("qty", 0);
            qt = getIntent().getStringExtra("qt");
            Log.d(Constants.TAG, "onCreate: " + qt);
            if (qty == 0) {
                qty = databaseHelper.getPackingRatioRepack(item_code, "0");
            }
        }

        tvProduct.setText(model);

        //packing_ratio = databaseHelper.getPackingRatio(item_code);


        masterqrcodeArrayList = new ArrayList<>();

        if (databaseHelper.getMasterQRCodeListRepack() != null) {
            JSONArray MasterQRjsonArray = databaseHelper.getMasterQRCodeListRepack().getJsonArray();
            for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                try {
                    masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        int a = databaseHelper.getPackingRatioRepack(item_code, "0");
        Log.d(Constants.TAG, "onCreate a: " + a);
        if (a == 0) {
            etRepackRatio.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Log.d("suraj", "afterTextChanged: " + s.toString());
                    if (s.toString().length() == 0 || s.toString().equals("0")) {
                        linHeader.setVisibility(View.GONE);
                        etMasterCarton.setText("");
                        if (ly_scan_system.getVisibility() == View.VISIBLE) {
                            ly_scan_system.setVisibility(View.GONE);
                        }
                        linMaster.setVisibility(View.GONE);
                        rvProductList.setVisibility(View.GONE);
                    } else {
                        if (Integer.parseInt(s.toString()) < 2) {
                            Utils.showToast(RepackProductsMainActivity.this, "Please enter valid repacking ratio.");
                            linHeader.setVisibility(View.GONE);
                            etMasterCarton.setText("");
                            if (ly_scan_system.getVisibility() == View.VISIBLE) {
                                ly_scan_system.setVisibility(View.GONE);
                            }
                            linMaster.setVisibility(View.GONE);
                            rvProductList.setVisibility(View.GONE);
                        } else if (Integer.parseInt(s.toString()) > Integer.parseInt(qt)) {
                            Utils.showToast(RepackProductsMainActivity.this, "Entered repacking ratio is greater than available quantity.");
                            linHeader.setVisibility(View.GONE);
                            etMasterCarton.setText("");
                            if (ly_scan_system.getVisibility() == View.VISIBLE) {
                                ly_scan_system.setVisibility(View.GONE);
                            }
                            linMaster.setVisibility(View.GONE);
                            rvProductList.setVisibility(View.GONE);
                        } else {
                            linMaster.setVisibility(View.VISIBLE);
                            linHeader.setVisibility(View.VISIBLE);
                            rvProductList.setVisibility(View.VISIBLE);
                            tempScannedQrList = new ArrayList<>();
                            tempScannedBladeQrList = new ArrayList<>();
                            packing_ratio = Integer.parseInt(s.toString());
                            BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
                            rvProductList.setAdapter(batchListAdapter);
                        }


                    }


                }
            });
        }


        etMasterCarton.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                etMasterCarton.setCursorVisible(true);
                etMasterCarton.setFocusable(true);
                etMasterCarton.setFocusableInTouchMode(true);
                if (s.toString().length() == 0) {
                    button_done.setVisibility(View.GONE);
                    button_packnext.setVisibility(View.GONE);
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                    ly_scan_system.setVisibility(View.VISIBLE);
                }


            }
        });
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.button_packnext:
                packnext();
                break;

            case R.id.button_discard_all:
                new AlertDialog.Builder(RepackProductsMainActivity.this)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Are you sure you want to discard?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                clearEditScan();
                                tvCartonCnt.setVisibility(View.GONE);
                                databaseHelper.deleteScannedPackagingData("0");
                                Intent intent = new Intent(RepackProductsMainActivity.this, RepackagingModellistActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                RepackProductsMainActivity.this.finish();
                                overridePendingTransition(0, 0);
                                onResume();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                break;

            case R.id.button_done:
                new AlertDialog.Builder(RepackProductsMainActivity.this)
                        .setTitle("Alert")
                        .setMessage("Are you sure you want to proceed further ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", (dialog, which) -> RepackMasterCartonCode())
                        .setNegativeButton("No", null)
                        .show();
                break;

            case R.id.btnScan:
                start_scanning();
                break;


        }
    }

    private void showAlert(String Title, String Message, boolean valid) {
        AlertDialog.Builder alert = new AlertDialog.Builder(RepackProductsMainActivity.this);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
            }
        });
        alert.show();
    }

    private boolean isBarcodeMasterQrExistsInSystem(String strBarcode) {
        masterqrcodeArrayList = new ArrayList<>();
        item_code = Utils.getPreference(RepackProductsMainActivity.this, "item_code", "");
        if (databaseHelper.getMasterQRCodeListRepack() != null) {
            JSONArray MasterQRjsonArray = databaseHelper.getMasterQRCodeListRepack().getJsonArray();
            for (int i = 0; i < MasterQRjsonArray.length(); i++) {
                try {
                    masterqrcodeArrayList.add(String.valueOf(MasterQRjsonArray.get(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return masterqrcodeArrayList.contains(strBarcode);
    }

    private boolean isMasterBarcodeExists(String barcode, String item_code) {
        return databaseHelper.getTempScannedMasterQRList(barcode, item_code);
    }

    private void showPackedCnt() {
        ScannedMasterQrListCnt = databaseHelper.getScannedMasterQRList("0");
        if (ScannedMasterQrListCnt.size() > 0) {
            tvCartonCnt.setVisibility(View.VISIBLE);
            ly_scan_system.setVisibility(View.VISIBLE);
            button_packnext.setVisibility(View.VISIBLE);
            button_done.setVisibility(View.VISIBLE);
            if (ScannedMasterQrListCnt.size() == 1) {
                tvCartonCnt.setText("Total Packed Carton: " + ScannedMasterQrListCnt.size());
            } else {
                tvCartonCnt.setText("Total Packed Cartons: " + ScannedMasterQrListCnt.size());
            }
        } else {
            tvCartonCnt.setVisibility(View.GONE);

        }
    }

    private void packnext() {
        item_code = Utils.getPreference(RepackProductsMainActivity.this, "item_code", "");
        Log.d("suraj", "packnext: " + item_code);
        if (tempScannedQrList.size() == packing_ratio && !blade) {

            if (etMasterCarton.getText().toString().trim().equals("")) {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            } else {
                Log.d(Constants.TAG, "onClick: " + etMasterCarton.getText().toString().trim());
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));

                    }
                    button_done.setVisibility(View.VISIBLE);

                    for (int i = 0; i < tempScannedQrList.size(); i++) {
                        databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 0);
                    }

                    if (tempScannedBladeQrList.size() > 0) {
                        for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                            databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 0);
                        }
                    }

                    if (tempScannedMasterQrList.size() == 0) {
                        databaseHelper.insertScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, "0", String.valueOf(packing_ratio));
                    } else {
                        for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                            databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "0", String.valueOf(packing_ratio));
                        }
                    }

                    databaseHelper.insertItemCode(item_code, "0");

                    tempScannedQrList = new ArrayList<>();
                    tempScannedBladeQrList = new ArrayList<>();
                    databaseHelper.deleteAllTempScannedTableData();

                    BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
                    rvProductList.setAdapter(batchListAdapter);
                    etMasterCarton.setText("");
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);

                    etMasterCarton.setOnClickListener(v -> Utils.showToast(RepackProductsMainActivity.this, "Please complete scan of product qr code first."));

                    showPackedCnt();
                    return;
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            }

        } else if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio && blade) {

            if (etMasterCarton.getText().toString().equals("")) {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            } else {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));

                    }
                    button_done.setVisibility(View.VISIBLE);

                    for (int i = 0; i < tempScannedQrList.size(); i++) {
                        databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 0);
                    }

                    if (tempScannedBladeQrList.size() > 0) {
                        for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                            databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 0);
                        }
                    }

                    if (tempScannedMasterQrList.size() == 0) {
                        databaseHelper.insertScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, "0", String.valueOf(packing_ratio));
                    } else {
                        for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                            databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "0", String.valueOf(packing_ratio));
                        }
                    }

                    databaseHelper.insertItemCode(item_code, "0");

                    tempScannedQrList = new ArrayList<>();
                    tempScannedBladeQrList = new ArrayList<>();
                    databaseHelper.deleteAllTempScannedTableData();

                    BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
                    rvProductList.setAdapter(batchListAdapter);
                    etMasterCarton.setText("");
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);


                    etMasterCarton.setOnClickListener(v -> Utils.showToast(RepackProductsMainActivity.this, "Please complete scan of product qr code first."));

                    showPackedCnt();
                    return;
                } else {
                    button_done.setVisibility(View.VISIBLE);
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            }


        } else {
            if (tempScannedQrList.size() == 0 && !blade) {
                Utils.showToast(RepackProductsMainActivity.this, "Please scan product Qr-code for next packaging.");
            } else if (tempScannedQrList.size() == 0 || tempScannedBladeQrList.size() == 0 && blade) {
                Utils.showToast(RepackProductsMainActivity.this, "Please scan product or blade Qr-code for next packaging.");
            } else {

                if (tempScannedQrList.size() != packing_ratio && !blade) {
                    Utils.showToast(RepackProductsMainActivity.this, "Please scan product Qr-code first for next packaging.");
                } else if (tempScannedQrList.size() != packing_ratio || tempScannedBladeQrList.size() != packing_ratio && blade) {
                    Utils.showToast(RepackProductsMainActivity.this, "Please scan product or blade Qr-code first for next packaging.");
                } else {
                    if (etMasterCarton.getText().toString().trim().equals("")) {
                        showAlert("Alert", "Please enter Master carton Qr-code.", true);
                    } else {
                        if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString().trim())) {
                            if (!isMasterBarcodeExists(etMasterCarton.getText().toString().trim(), item_code)) {
                                databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString().trim(), item_code, 0, String.valueOf(packing_ratio));
                            }
                            button_done.setVisibility(View.VISIBLE);
                            showPackedCnt();
                            return;
                        } else {
                            button_done.setVisibility(View.VISIBLE);
                            showAlert("Alert", "This Qr-code is invalid.", true);
                        }
                    }
                }


            }


        }
    }


    private void clearEditScan() {
        if (etMasterCarton.getText().toString().trim().length() > 0) {
            etMasterCarton.setText("");
        }
    }

    private void start_scanning() {
        if (databaseHelper.getPackingRatioRepack(Utils.getPreference(RepackProductsMainActivity.this, "item_code", ""), "0") != 0) {
            qt = etRepackRatio.getText().toString();
        }
        if (etRepackRatio.getText().toString().equals("")) {
            Utils.showToast(RepackProductsMainActivity.this, "Please enter repacking ratio.");
        } else if (Integer.parseInt(etRepackRatio.getText().toString()) < 2) {
            Utils.showToast(RepackProductsMainActivity.this, "Please enter valid repacking ratio.");
        } else if (Integer.parseInt(etRepackRatio.getText().toString()) > Integer.parseInt(qt)) {
            Utils.showToast(RepackProductsMainActivity.this, "Entered repacking ratio is greater than available quantity.");
        } else {
            if (databaseHelper.getPackingRatioRepack(Utils.getPreference(RepackProductsMainActivity.this, "item_code", ""), "0") == 0) {
                etRepackRatio.setFocusable(false);
                databaseHelper.insertPackingRatio(etRepackRatio.getText().toString(), item_code + "_" + etRepackRatio.getText().toString(), "0");
                Utils.addPreference(RepackProductsMainActivity.this, "item_code", item_code + "_" + etRepackRatio.getText().toString());
            }
            Intent intent = new Intent(RepackProductsMainActivity.this, RepackProductListScanActivity.class);
            intent.putExtra("item_code", item_code);
            intent.putExtra("blade", blade);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }


    }

    @Override
    public void onBackPressed() {

        ScannedQrList = databaseHelper.getScannedQRList("0");
        if (ScannedQrList.size() == 0) {
            ScannedQrList = databaseHelper.getTempScannedQRList();
        }
        ScannedBladeQrList = databaseHelper.getScannedBladeQRList("0");
        if (ScannedBladeQrList.size() == 0) {
            ScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
        }
        ScannedMasterQrList = databaseHelper.getScannedMasterQRList("0");
        if (ScannedMasterQrList.size() == 0) {
            ScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");
        }

        if (ScannedQrList.size() > 0 || ScannedBladeQrList.size() > 0 || ScannedMasterQrList.size() > 0) {
            new AlertDialog.Builder(RepackProductsMainActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            databaseHelper.deleteAllScannedTableData();
                            Intent intent = new Intent(RepackProductsMainActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            RepackProductsMainActivity.this.finish();
                            overridePendingTransition(0, 0);

                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    //TODO Master carton mapping API Call here
    private void RepackMasterCartonCode() {
        item_code = Utils.getPreference(RepackProductsMainActivity.this, "item_code", "");

        if (tempScannedQrList.size() == 0) {
            databaseHelper.updateTempScannedMasterQRList(item_code, 1);
            databaseHelper.updateScannedMasterQRList(item_code, 1);
            databaseHelper.updateScannedQRList(item_code, 1);
            databaseHelper.updateScannedBladeQRList(item_code, 1);
            databaseHelper.updateScannedItemCode(item_code, 1);
            callMasterCartonAPI("1");
            return;
        } else if (tempScannedQrList.size() == packing_ratio && !blade) {

            if (!etMasterCarton.getText().toString().trim().equals("")) {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                        //ScannedMasterQrList.add(etMasterCarton.getText().toString());
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("2");
                        return;
                    } else {
                        databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("3");
                        return;
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            } else {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            }

        } else if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio && blade) {

            if (!etMasterCarton.getText().toString().trim().equals("")) {
                if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                    if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                        databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("4");
                        return;
                    } else {
                        databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedMasterQRList(item_code, 1);
                        databaseHelper.updateScannedQRList(item_code, 1);
                        databaseHelper.updateScannedBladeQRList(item_code, 1);
                        databaseHelper.updateScannedItemCode(item_code, 1);
                        callMasterCartonAPI("5");
                        return;
                    }
                } else {
                    showAlert("Alert", "This Qr-code is invalid.", true);
                }
            } else {
                showAlert("Alert", "Please enter Master carton Qr-code.", true);
            }
        } else {
            if (tempScannedQrList.size() > 0 && !blade) {
                Utils.showToast(RepackProductsMainActivity.this, "Please scan product Qr-code first for next packaging.");
            } else if (tempScannedQrList.size() > 0 || tempScannedBladeQrList.size() > 0 && blade) {
                Utils.showToast(RepackProductsMainActivity.this, "Please scan product or blade Qr-code first  for next packaging.");

            } else {
                if (!etMasterCarton.getText().toString().trim().equals("")) {
                    if (isBarcodeMasterQrExistsInSystem(etMasterCarton.getText().toString())) {
                        if (!isMasterBarcodeExists(etMasterCarton.getText().toString(), item_code)) {
                            databaseHelper.insertTempScannedMasterQRList(etMasterCarton.getText().toString(), item_code, 1, String.valueOf(packing_ratio));
                            databaseHelper.updateScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedQRList(item_code, 1);
                            databaseHelper.updateScannedBladeQRList(item_code, 1);
                            databaseHelper.updateScannedItemCode(item_code, 1);
                            callMasterCartonAPI("6");
                            return;
                        } else {
                            databaseHelper.updateTempScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedMasterQRList(item_code, 1);
                            databaseHelper.updateScannedQRList(item_code, 1);
                            databaseHelper.updateScannedBladeQRList(item_code, 1);
                            databaseHelper.updateScannedItemCode(item_code, 1);
                            callMasterCartonAPI("7");
                            return;
                        }
                    } else {
                        showAlert("Alert", "This Qr-code is invalid.", true);
                    }
                } else {
                    showAlert("Alert", "Please enter Master carton Qr-code.", true);
                }
            }

        }


    }

    private void callMasterCartonAPI(String tag) {
        Log.d(Constants.TAG, "callMasterCartonAPI tag: " + tag);
        item_code = Utils.getPreference(RepackProductsMainActivity.this, "item_code", "");
        databaseHelper.updatePackingRatio();
        if (databaseHelper.getTempScannedMasterQRListbyFlag("1").size() > 0) {
            ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");
            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList if: " + ScannedMasterQrList.toString());
            ScannedMasterQrList.addAll(databaseHelper.getTempScannedMasterQRListbyFlag("1"));
            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList if: " + ScannedMasterQrList.toString());
        } else {
            ScannedMasterQrList = databaseHelper.getScannedMasterQRList("1");

            Log.d(Constants.TAG, "setMapMasterCartonCode ScannedMasterQrList else: " + ScannedMasterQrList.toString());

        }
        Log.d(Constants.TAG, "setMapMasterCartonCode databaseHelper.getItemCodes.size(): " + databaseHelper.getItemCodes("2").size());
        if (ScannedMasterQrList.size() == databaseHelper.getItemCodes("2").size()) {
            Log.d(Constants.TAG, "setMapMasterCartonCode getItemCodes if: ");
            if (databaseHelper.getItemCodes("2").size() == 0) {
                databaseHelper.insertItemCode(item_code, "1");
            }
        } else {
            Log.d(Constants.TAG, "setMapMasterCartonCode getItemCodes else: ");
            databaseHelper.insertItemCode(item_code, "1");
        }


        ScannedMasterQrJson = new JSONArray();
        for (int i = 0; i < ScannedMasterQrList.size(); i++) {
            ScannedMasterQrJson.put(ScannedMasterQrList.get(i));
        }


        if (databaseHelper.getTempScannedQRList().size() > 0) {
            ScannedQrList = databaseHelper.getScannedQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList if: " + ScannedQrList.toString());
            ScannedQrList.addAll(databaseHelper.getTempScannedQRList());
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList if: " + ScannedQrList.toString());
        } else {
            ScannedQrList = databaseHelper.getScannedQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedQrList else: " + ScannedQrList.toString());
        }


        if (databaseHelper.getTempScannedBladeQRList().size() > 0) {
            ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI  ScannedBladeQrList if: " + ScannedBladeQrList.toString());
            ScannedBladeQrList.addAll(databaseHelper.getTempScannedBladeQRList());
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedBladeQrList if: " + ScannedBladeQrList.toString());
        } else {
            ScannedBladeQrList = databaseHelper.getScannedBladeQRList("1");
            Log.d(Constants.TAG, "callMasterCartonAPI ScannedBladeQrList else: " + ScannedBladeQrList.toString());
        }

        Log.d(Constants.TAG, "uploadOfflinePacking databaseHelper.getItemCodes(): " + databaseHelper.getItemCodes("1"));

        JSONArray jsonArray = new JSONArray();
        try {

            for (int k = 0; k < ScannedMasterQrJson.length(); k++) {
                int res = 0;
                int res1 = 0;
                int packing_ratio = databaseHelper.getPackingRatioRepack(databaseHelper.getItemCodes("1").get(k), "1");
                Log.d("suraj", "callMasterCartonAPI packing_ratio: " + packing_ratio);
                boolean blade = databaseHelper.getBatchList(databaseHelper.getItemCodes("1").get(k));
                JSONObject jsonObject = new JSONObject();
                ScannedQrJson = new JSONArray();
                ScannedBladeQrJson = new JSONArray();
                jsonObject.put("master_carton", ScannedMasterQrJson.get(k));

                for (int j = res; j < (packing_ratio + res); j++) {
                    Log.d("suraj", "callMasterCartonAPI j loop: " + j);
                    ScannedQrJson.put(ScannedQrList.get(j));
                }

                jsonObject.put("qr_codes", ScannedQrJson);
                for (int l = 0; l < ScannedQrJson.length(); l++) {
                    try {
                        ScannedQrList.remove(ScannedQrJson.get(l));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (blade) {
                    for (int j = res1; j < (packing_ratio + res1); j++) {
                        ScannedBladeQrJson.put(ScannedBladeQrList.get(j));
                    }
                    jsonObject.put("blade_codes", ScannedBladeQrJson);

                    for (int l = 0; l < ScannedBladeQrJson.length(); l++) {
                        try {
                            ScannedBladeQrList.remove(ScannedBladeQrJson.get(l));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    ScannedBladeQrJson = new JSONArray();
                    jsonObject.put("blade_codes", ScannedBladeQrJson);
                }
                jsonArray.put(jsonObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("suraj", "callMasterCartonAPI jsonArray.toString(): " + jsonArray.toString());

        if (cd.isConnectingToInternet()) {
            Utils.showprogressdialog(RepackProductsMainActivity.this, "Requesting for repackaging of products, please wait");

            retrofitClient.getService().repacking(databaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
                @Override
                public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {

                    if (response.body().getSuccess()) {
                        String msg;
                        Utils.dismissprogressdialog();
                        databaseHelper.deleteAllScannedTableData();
                        databaseHelper.deleteAllpackingdata();
                        if (ScannedMasterQrList.size() == 1) {
                            msg = "You have successfully repacked " + ScannedMasterQrList.size() + " master carton.";
                        } else {
                            msg = "You have successfully repacked " + ScannedMasterQrList.size() + " master cartons.";
                        }
                        new AlertDialog.Builder(RepackProductsMainActivity.this)
                                .setTitle("Success")
                                .setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Utils.addPreference(RepackProductsMainActivity.this, "tag", "pack");
                                        Intent intent = new Intent(RepackProductsMainActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        RepackProductsMainActivity.this.finish();

                                    }
                                }).show();
                    } else {
                        Utils.showToast(RepackProductsMainActivity.this, response.body().getMessage());
                    }
                    Utils.dismissprogressdialog();
                }

                @Override
                public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });
        } else {
            Utils.showToast(RepackProductsMainActivity.this, "Repackaging of products stored offline.");

            for (int i = 0; i < tempScannedQrList.size(); i++) {
                databaseHelper.insertScannedQRList(tempScannedQrList.get(i), item_code, 1);
            }

            if (tempScannedBladeQrList.size() > 0) {
                for (int i = 0; i < tempScannedBladeQrList.size(); i++) {
                    databaseHelper.insertScannedBladeQRList(tempScannedBladeQrList.get(i), item_code, 1);
                }
            }

            for (int i = 0; i < tempScannedMasterQrList.size(); i++) {
                databaseHelper.insertScannedMasterQRList(tempScannedMasterQrList.get(i), item_code, "1", String.valueOf(packing_ratio));
            }

            databaseHelper.deleteAllTempScannedTableData();
            databaseHelper.updatePackingRatio();

            Log.d("suraj", "callMasterCartonAPI item_code: " + Utils.getPreference(RepackProductsMainActivity.this, "item_code", ""));

            Intent intent = new Intent(RepackProductsMainActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            RepackProductsMainActivity.this.finish();
            overridePendingTransition(0, 0);

        }


    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<String> ScannedQr;
        private ArrayList<String> ScannedBladeQr;
        int packing_ratio;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView tvSrNo;
            private AppCompatTextView tvMotor;
            private AppCompatTextView tvBlade;

            public MyViewHolder(View view) {
                super(view);
                tvSrNo = view.findViewById(R.id.tvSrNo);
                tvMotor = view.findViewById(R.id.tvMotor);
                tvBlade = view.findViewById(R.id.tvBlade);
            }
        }


        public BatchListAdapter(ArrayList<String> ScannedQr, ArrayList<String> scannedBladeQrList, int packing_ratio) {
            this.ScannedQr = ScannedQr;
            this.ScannedBladeQr = scannedBladeQrList;
            this.packing_ratio = packing_ratio;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_product_row, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.tvSrNo.setText(String.valueOf(position + 1));

            if (ScannedQr.size() > 0) {
                for (int i = 0; i < ScannedQr.size(); i++)
                    if (position == i)
                        holder.tvMotor.setText(ScannedQr.get(i));
            }

            if (blade) {
                holder.tvBlade.setVisibility(View.VISIBLE);
                if (ScannedBladeQr.size() > 0) {
                    for (int i = 0; i < ScannedBladeQr.size(); i++)
                        if (position == i)
                            holder.tvBlade.setText(ScannedBladeQr.get(position));
                }
            }

        }

        @Override
        public int getItemCount() {
            return packing_ratio;
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        showPackedCnt();

        packing_ratio = databaseHelper.getPackingRatioRepack(Utils.getPreference(RepackProductsMainActivity.this, "item_code", ""), "0");
        Log.d("suraj", "onResume: " + packing_ratio);
        if (packing_ratio > 1) {
            etRepackRatio.setFocusable(false);
            etRepackRatio.setText(String.valueOf(packing_ratio));

            linMaster.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            rvProductList.setVisibility(View.VISIBLE);
            BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
            rvProductList.setAdapter(batchListAdapter);
        } else if (!etRepackRatio.getText().toString().equals("")) {
            etRepackRatio.setFocusable(false);
            if (Integer.parseInt(etRepackRatio.getText().toString()) > 1)
                packing_ratio = Integer.parseInt(etRepackRatio.getText().toString());
        }

        tempScannedQrList = databaseHelper.getTempScannedQRList();
        tempScannedBladeQrList = databaseHelper.getTempScannedBladeQRList();
        tempScannedMasterQrList = databaseHelper.getTempScannedMasterQRListbyFlag("0");

        Log.d(Constants.TAG, "onResume tempScannedQrList: " + tempScannedQrList.toString());
        Log.d(Constants.TAG, "onResume tempScannedBladeQrList: " + tempScannedBladeQrList.toString());
        Log.d(Constants.TAG, "onResume tempScannedMasterQrList: " + tempScannedMasterQrList.toString());

        if (tempScannedMasterQrList.size() > 0) {
            etMasterCarton.setText(tempScannedMasterQrList.get(tempScannedMasterQrList.size() - 1));
            etMasterCarton.setFocusable(false);
        }
        blade = Utils.getPreferenceBoolean(RepackProductsMainActivity.this, "blade", false);


        if (blade) {
            tvBlade.setVisibility(View.VISIBLE);

            if (tempScannedQrList.size() > 0) {
                ly_scan_system.setVisibility(View.VISIBLE);
            }

            if (tempScannedQrList.size() == packing_ratio && tempScannedBladeQrList.size() == packing_ratio) {
                if (tempScannedMasterQrList.size() > 0) {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                } else {
                    etMasterCarton.setFocusable(true);
                    etMasterCarton.setFocusableInTouchMode(true);
                    etMasterCarton.setCursorVisible(true);
                }

            } else {

                if (tempScannedQrList.size() == 0 || tempScannedBladeQrList.size() == 0) {
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);
                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(RepackProductsMainActivity.this, "Please complete scan of ( product qr or blade qr code ) first.");
                        }
                    });
                }

            }
        } else {

            if (tempScannedQrList.size() > 0) {
                ly_scan_system.setVisibility(View.VISIBLE);
            }

            if (tempScannedQrList.size() == packing_ratio) {
                if (tempScannedMasterQrList.size() > 0) {
                    button_done.setVisibility(View.VISIBLE);
                    button_packnext.setVisibility(View.VISIBLE);
                } else {
                    etMasterCarton.setFocusable(true);
                    etMasterCarton.setFocusableInTouchMode(true);
                    etMasterCarton.setCursorVisible(true);
                }

            } else {
                if (tempScannedQrList.size() == 0) {
                    etMasterCarton.setFocusable(false);
                    etMasterCarton.setFocusableInTouchMode(false);
                    etMasterCarton.setCursorVisible(false);
                    etMasterCarton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.showToast(RepackProductsMainActivity.this, "Please complete scan of product qr code first.");
                        }
                    });
                }

            }
        }
        BatchListAdapter batchListAdapter = new BatchListAdapter(tempScannedQrList, tempScannedBladeQrList, packing_ratio);
        rvProductList.setAdapter(batchListAdapter);

    }

}
