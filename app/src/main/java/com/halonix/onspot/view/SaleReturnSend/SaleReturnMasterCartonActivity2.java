package com.halonix.onspot.view.SaleReturnSend;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.SenderDetails_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleReturnMasterCartonActivity2 extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edRVNumber)
    AppCompatEditText edRVNumber;

    @BindView(R.id.edReason)
    AppCompatEditText edReason;

    @BindView(R.id.InputRVNumber)
    TextInputLayout InputRVNumber;

    @BindView(R.id.InputReason)
    TextInputLayout InputReason;

    @BindView(R.id.btnDispatch)
    AppCompatButton btnDispatch;

    @BindView(R.id.tvSender)
    TextView tvSender;


    @BindView(R.id.ly_send_to)
    LinearLayout ly_send_to;

    private Context context;

    private ConnectionDetectorActivity cd;
    private DatabaseHelper objDatabaseHelper;

    private long mLastClickTime = 0;
    private RetrofitClient retrofitClient;
    private int agency_id = 0;

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_return_master_carton2);
        context = SaleReturnMasterCartonActivity2.this;
        cd = new ConnectionDetectorActivity(context);
        ButterKnife.bind(this);
        objDatabaseHelper = DatabaseHelper.getInstance(context);

        retrofitClient = new RetrofitClient();
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
            }
        });


        toolbar.setTitle("Sale Return Send");
        edRVNumber.setLongClickable(false);
        edReason.setLongClickable(false);


        edRVNumber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        edReason.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        btnDispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (!validateRVNumber()) {
                    return;
                }

                if (cd.isConnectingToInternet()) {
                    if (agency_id == 0) {
                        getSenderDetails("dispatch");
                    } else {
                        dispatchShipment();
                    }
                } else {
                    showNetworkFailDialog();
                }


            }
        });

        edRVNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validateRVNumber()) {
                    return;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });


        if (cd.isConnectingToInternet()) {
            getSenderDetails("");
        } else {
            showNetworkFailDialog();
        }

    }

    private void dispatchShipment() {
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedForceCodeList("0", "sale");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "dispatchShipment: " + jsonArray.toString());

        Utils.showprogressdialog(SaleReturnMasterCartonActivity2.this, "Requesting for sending sale return shipment,please wait.");

        retrofitClient.getService().sale_return_send(objDatabaseHelper.getAuthToken(), edRVNumber.getText().toString(), edReason.getText().toString(),
                jsonArray.toString(), agency_id).enqueue(new Callback<SenderDetails_respo>() {
            @Override
            public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    Utils.removePreference(SaleReturnMasterCartonActivity2.this, "sender_id");
                    objDatabaseHelper.deleteScannedForcedCode("0", "sale");

                    Intent intent = new Intent(SaleReturnMasterCartonActivity2.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    SaleReturnMasterCartonActivity2.this.finish();
                    overridePendingTransition(0, 0);
                    Utils.showToast(SaleReturnMasterCartonActivity2.this, "Shipment dispatched successfully");

                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(SaleReturnMasterCartonActivity2.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    private AlertDialog builder;

    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(SaleReturnMasterCartonActivity2.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }

    private void getSenderDetails(String tag) {
        Utils.showprogressdialog(SaleReturnMasterCartonActivity2.this, "Getting sender details, please wait !");

        String sender_id = String.valueOf(Utils.getPreferenceInt(SaleReturnMasterCartonActivity2.this, "sender_id", 0));
        retrofitClient.getService().sender_details(objDatabaseHelper.getAuthToken(), sender_id).enqueue(new Callback<SenderDetails_respo>() {
            @Override
            public void onResponse(Call<SenderDetails_respo> call, Response<SenderDetails_respo> response) {
                Utils.dismissprogressdialog();

                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    ly_send_to.setVisibility(View.VISIBLE);
                    agency_id = response.body().getSender().getId();
                    tvSender.setText(response.body().getSender().getName() + ", " + response.body().getSender().getLocation());
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(SaleReturnMasterCartonActivity2.this, response.body().getMessage());
                }
                if (tag.equals("dispatch")) {
                    dispatchShipment();
                }
            }

            @Override
            public void onFailure(Call<SenderDetails_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }


    private boolean validateRVNumber() {
        String po_number = edRVNumber.getText().toString().trim();

        if (po_number.isEmpty()) {
            InputRVNumber.setError(getString(R.string.hint_enter_rv_number));
            edRVNumber.requestFocus();
            return false;
        } else {
            InputRVNumber.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateReason() {
        String po_date = edReason.getText().toString().trim();

        if (po_date.isEmpty()) {
            InputReason.setError(getString(R.string.hint_reason));
            edReason.requestFocus();
            return false;
        } else {
            InputReason.setErrorEnabled(false);
        }
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
