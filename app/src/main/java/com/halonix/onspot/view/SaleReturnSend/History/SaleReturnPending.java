package com.halonix.onspot.view.SaleReturnSend.History;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.ReceiveDefectiveShipmentList_respo;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Constants.TAG;
import static com.halonix.onspot.utils.Utils.getDate;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaleReturnPending extends Fragment {

    @BindView(R.id.rv_pending)
    RecyclerView rv_pending;

    @BindView(R.id.textView_no_data_found_pending)
    TextView textViewNoDataFound;

    private DatabaseHelper objDatabaseHelper = null;
    private ConnectionDetectorActivity cd = null;

    private ArrayList<ReceiveDefectiveShipmentList_respo> arrCompletedListModel;
    private ArrayList<CompletedListModel> completedListModelArrayList;

    RetrofitClient retrofitClient;
    private ProgressDialog pd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetectorActivity(getActivity());
        objDatabaseHelper = DatabaseHelper.getInstance(getActivity());
    }

    public void fragmentBecameVisible() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_shipment_pending, container, false);
        ButterKnife.bind(this, mainView);
        retrofitClient = new RetrofitClient();
        rv_pending.setHasFixedSize(true);
        rv_pending.setItemAnimator(new DefaultItemAnimator());
        rv_pending.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (cd.isConnectingToInternet()) {
            getCompletedListFromServer();
        }

        return mainView;
    }


    private void getCompletedListFromServer() {

        arrCompletedListModel = new ArrayList<>();
        completedListModelArrayList = new ArrayList<>();

        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setMessage("Getting history, please wait...");
        pd.show();

        retrofitClient.getService().SaleReturnHistory(objDatabaseHelper.getAuthToken(), false).enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {

                Log.d(TAG, "onResponse: " + response.body().getSuccess());

                if (response.body().getSuccess()) {
                    pd.dismiss();
                    textViewNoDataFound.setVisibility(TextView.GONE);
                    arrCompletedListModel = response.body().getData();

                    if (arrCompletedListModel.size() > 0) {
                        for (int i = 0; i < arrCompletedListModel.size(); i++) {

                            CompletedListModel completedListModel = new CompletedListModel(arrCompletedListModel.get(i).getQuantity(),
                                    arrCompletedListModel.get(i).getRec_quantity(), arrCompletedListModel.get(i).getRv_no(),
                                    arrCompletedListModel.get(i).getDate(), arrCompletedListModel.get(i).getClient());
                            completedListModelArrayList.add(completedListModel);
                        }

                        Collections.sort(completedListModelArrayList, (o1, o2) -> o2.getDate().compareTo(o1.getDate()));
                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(completedListModelArrayList);
                        rv_pending.setAdapter(shipmentAdapter);
                    } else {
                        pd.dismiss();
                        textViewNoDataFound.setVisibility(TextView.VISIBLE);
                    }
                } else {
                    pd.dismiss();
                    textViewNoDataFound.setVisibility(TextView.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                pd.dismiss();
            }
        });

    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {

        private List<CompletedListModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textViewInNo;
            private AppCompatTextView textView_Client;
            private AppCompatTextView textViewInDate;
            private AppCompatTextView textViewQuantity;
            private AppCompatTextView textView_pending;
            private LinearLayout linReceive;
            private LinearLayout ael;
            private AppCompatTextView tvMore;


            public MyViewHolder(View view) {
                super(view);
                textViewInNo = view.findViewById(R.id.textView_in_no);
                textViewInDate = view.findViewById(R.id.textView_in_date);
                textViewQuantity = view.findViewById(R.id.textView_qty);
                textView_Client = view.findViewById(R.id.textView_Client);
                textView_pending = view.findViewById(R.id.textView_pending);
                linReceive = view.findViewById(R.id.linReceive);
                ael = view.findViewById(R.id.expandable_ans);
                tvMore = view.findViewById(R.id.tvMore);
            }
        }


        public ShipmentAdapter(List<CompletedListModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewholder_saledef_history, parent, false);
            return new MyViewHolder(itemView);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final CompletedListModel object = notesList.get(position);
            holder.textViewInDate.setText(getDate(getDisplayDate(object.getDate()), "yyyy-MM-dd", "dd-MM-yyyy"));
            holder.textViewInNo.setText(object.getRv_no());
            holder.textViewQuantity.setText(String.valueOf(object.getQuantity()));
            holder.textView_Client.setText(object.getClient());
            holder.textView_pending.setText(String.valueOf(object.getRec_quantity()));
            holder.linReceive.setVisibility(View.VISIBLE);

            holder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.ael.setVisibility(holder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (holder.ael.getVisibility() == View.VISIBLE) {
                        holder.tvMore.setText("Less Details");
                    } else {
                        holder.tvMore.setText("More Details");
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }


    private class CompletedListModel {
        int quantity;
        int rec_quantity;
        String rv_no;
        String date;
        String client;

        public CompletedListModel(int quantity, int rec_quantity, String rv_no, String date, String client) {
            this.quantity = quantity;
            this.rec_quantity = rec_quantity;
            this.rv_no = rv_no;
            this.date = date;
            this.client = client;
        }

        public int getQuantity() {
            return quantity;
        }

        public int getRec_quantity() {
            return rec_quantity;
        }

        public String getRv_no() {
            return rv_no;
        }

        public String getDate() {
            return date;
        }

        public String getClient() {
            return client;
        }
    }
}
