package com.halonix.onspot.view.Login_OTP;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.signin;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


// sign In Page
public class SignInActivity extends AppCompatActivity implements OnClickListener {


    Context cntx;
    Resources resources;
    private ConnectionDetectorActivity cd;
    RetrofitClient retrofitClient;

    @BindView(R.id.tit_exsuserno)
    TextInputLayout tit_exsuserno;

    @BindView(R.id.etExistingUserNo)
    EditText etExistingUserNo;

    @BindView(R.id.signin)
    Button SignIn;
    private long mLastClickTime = 0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_page);
        ButterKnife.bind(this);
        cd = new ConnectionDetectorActivity(this);
        cntx = SignInActivity.this;
        resources = SignInActivity.this.getResources();
        retrofitClient = new RetrofitClient();


        SignIn.setOnClickListener(this);

        etExistingUserNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!validateMobile()) {
                    return;
                }
            }
        });


    }

    private boolean validateMobile() {
        String mobile_no = etExistingUserNo.getText().toString().trim();

        Pattern p_phone = Pattern.compile(Constants.MobileregEx);
        Matcher m_phone = p_phone.matcher(mobile_no);
        if (mobile_no.isEmpty()) {
            tit_exsuserno.setError(getString(R.string.hint_entermobile));
            etExistingUserNo.requestFocus();
            return false;
        } else if (!m_phone.find()) {
            tit_exsuserno.setError(getString(R.string.hint_invalid_mobile));
            etExistingUserNo.requestFocus();
            return false;
        } else {
            tit_exsuserno.setErrorEnabled(false);
        }
        return true;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.signin:
                if (cd.isConnectingToInternet()) {
                    hideKeyboard(SignInActivity.this);

                    if (!validateMobile()) {
                        return;
                    }

                    signinreq();


                } else {
                    showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                }
                break;

        }
    }


    @SuppressLint("InflateParams")
    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();

                    }
                });
    }


    //TODO Sign In API Call here
    private void signinreq() {

        Utils.showprogressdialog(SignInActivity.this, "Sign in progress, please wait");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_SIGN_IN_MOB_NUM, etExistingUserNo.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        retrofitClient.getService().getSignin(etExistingUserNo.getText().toString().trim()).enqueue(new Callback<signin>() {
            @Override
            public void onResponse(Call<signin> call, Response<signin> response) {

                if (response.isSuccessful()) {
                    String responseMsg = response.body().getMessage();
                    boolean userExist = response.body().getUserExist();
                    boolean success = response.body().getSuccess();

                    if (response.body().getUserId() != null) {
                        int userid = response.body().getUserId();
                        Utils.addPreference(cntx, PreferenceKeys.USERNAMEID, String.valueOf(userid));
                    }

                    if (userExist == false)
                        Utils.showToast(SignInActivity.this, responseMsg);
                    else
                        popUpDialog(resources.getString(R.string.verification_request_message_for_login), false, userExist);
                }

                Utils.dismissprogressdialog();
            }

            @Override
            public void onFailure(Call<signin> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    @SuppressLint("InflateParams")
    private void popUpDialog(String message, final boolean isNewUser, final boolean userExist) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = this.getLayoutInflater();

        View content = inflater.inflate(R.layout.otp_verification_dialog, null);

        TextView tv = content.findViewById(R.id.otpAlertMessage1);

        ((Button) content.findViewById(R.id.btnOTPAlertOK)).setText(resources.getString(R.string.action_text_ok));
        tv.setText(message);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        content.findViewById(R.id.btnOTPAlertOK).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        if (isNewUser) {
                          /*  Intent i = new Intent(SignInActivity.this,
                                    SignUpActivity.class);
                            startActivity(i);
                            setResult(10);
                            finish();*/
                        } else {
                            jumpToOTP(userExist);
                        }
                    }

                });

    }

    private void jumpToOTP(boolean userExist) {
        Bundle bundle = new Bundle();
        bundle.putString("userMobValue", etExistingUserNo.getText().toString());
        bundle.putString("id", Utils.getPreference(cntx, PreferenceKeys.USERNAMEID, ""));
        bundle.putString("user_exist", String.valueOf(userExist));
        Intent i = new Intent(SignInActivity.this, OTPVerifyActivity.class);
        i.putExtras(bundle);
        startActivity(i);
        setResult(10);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(10);
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}