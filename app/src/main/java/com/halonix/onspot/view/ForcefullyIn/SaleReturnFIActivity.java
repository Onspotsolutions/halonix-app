package com.halonix.onspot.view.ForcefullyIn;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.ReceiveDefectiveShipment_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleReturnFIActivity extends AppCompatActivity implements View.OnClickListener {

    private Activity activity = null;

    @BindView(R.id.scanShipmentList)
    RecyclerView scanShipmentList;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;

    @BindView(R.id.ly_scan_system)
    LinearLayout ly_scan_system;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.button_done)
    AppCompatButton button_done;

    private DatabaseHelper objDatabaseHelper = null;
    ArrayList<NewShipmentDetailModel> newShipmentScannedArrayList = new ArrayList<>();
    BatchListAdapter scanShipmentCodeAdapter;
    private long mLastClickTime = 0;
    RetrofitClient retrofitClient;
    ConnectionDetectorActivity cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_new_shipment);
        ButterKnife.bind(this);
        activity = SaleReturnFIActivity.this;
        objDatabaseHelper = DatabaseHelper.getInstance(activity);

        retrofitClient = new RetrofitClient();
        cd = new ConnectionDetectorActivity(SaleReturnFIActivity.this);

        scanShipmentList.setHasFixedSize(true);
        scanShipmentList.setItemAnimator(new DefaultItemAnimator());
        scanShipmentList.setLayoutManager(new LinearLayoutManager(SaleReturnFIActivity.this));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });
        toolbar.setTitle("Sale Return Force In");

        findViewById(R.id.button_start_scanning).setOnClickListener(this);
        findViewById(R.id.button_addmore).setOnClickListener(this);
        findViewById(R.id.button_done).setOnClickListener(this);
        button_done.setText("Receive");
        findViewById(R.id.button_discard_all).setOnClickListener(this);
    }


    private void remove_scan() {
        Intent intent = new Intent(SaleReturnFIActivity.this, FI_ScannerActivity.class);
        intent.putExtra("token", "remove");
        intent.putExtra("tag", "sale");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    private void start_scanning() {
        Intent intent = new Intent(SaleReturnFIActivity.this, FI_ScannerActivity.class);
        intent.putExtra("token", "add");
        intent.putExtra("tag", "sale");
        startActivityForResult(intent, Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN);
        overridePendingTransition(0, 0);
    }

    private AlertDialog builder;

    private void showNetworkFailDialog() {

        builder = new AlertDialog.Builder(SaleReturnFIActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.network_failure_dialog, null);
        builder.setView(content);
        builder.setCancelable(false);
        builder.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(Constants.NO_INTERNET_MSG);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                    }
                });

    }


    @Override
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (view.getId()) {
            case R.id.button_start_scanning:
            case R.id.button_addmore:
                if (cd.isConnectingToInternet()) {
                    start_scanning();
                } else {
                    showNetworkFailDialog();
                }

                break;
            case R.id.button_done:
                if (cd.isConnectingToInternet()) {
                    if (newShipmentScannedArrayList.size() > 0) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
                        AlertDialog alertDialog = dialogBuilder.create();
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        LayoutInflater inflater = this.getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_force_in, null);
                        alertDialog.getWindow().setContentView(dialogView);
                        alertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                        EditText edComment = dialogView.findViewById(R.id.edComment);
                        AppCompatButton btnReceive = dialogView.findViewById(R.id.btnReceive);

                        btnReceive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                                receive_codes(edComment.getText().toString());


                            }
                        });
                    } else {
                        Utils.showToast(activity, "Please scan at least one QR code");
                    }
                } else {
                    showNetworkFailDialog();
                }


                break;
            case R.id.button_discard_all:
                remove_scan();
                break;
        }
    }

    private void receive_codes(String s) {

        Utils.showprogressdialog(SaleReturnFIActivity.this, "Receiving...please wait");
        ArrayList<String> stringArrayList = objDatabaseHelper.getScannedForceCodeList("0", "force_in");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringArrayList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "receive_codes Sale: " + jsonArray.toString());
        retrofitClient.getService().force_in_receive(objDatabaseHelper.getAuthToken(), jsonArray.toString(), s, "0").enqueue(new Callback<ReceiveDefectiveShipment_respo>() {
            @Override
            public void onResponse(Call<ReceiveDefectiveShipment_respo> call, Response<ReceiveDefectiveShipment_respo> response) {
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    objDatabaseHelper.deleteScannedForcedCode("0", "force_in");

                    Intent intent = new Intent(SaleReturnFIActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    SaleReturnFIActivity.this.finish();
                    overridePendingTransition(0, 0);
                    Utils.showToast(SaleReturnFIActivity.this, "Received successfully");
                } else {
                    Utils.dismissprogressdialog();
                    Utils.showToast(SaleReturnFIActivity.this, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ReceiveDefectiveShipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    @Override
    protected void onResume() {
        findViewById(R.id.ly_scan_system).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "force_in").size() > 0 ? View.VISIBLE : View.GONE);
        findViewById(R.id.button_start_scanning).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "force_in").size() > 0 ? View.GONE : View.VISIBLE);
        findViewById(R.id.scanShipmentList).setVisibility(objDatabaseHelper.getScannedForcedCodeList("0", "force_in").size() > 0 ? View.VISIBLE : View.GONE);

        if (objDatabaseHelper.getScannedForcedCodeList("0", "force_in") != null) {
            newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "force_in");
        }

        if (newShipmentScannedArrayList.size() > 0) {
            scanShipmentList.setVisibility(View.VISIBLE);
            linHeader.setVisibility(View.VISIBLE);
            scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
            scanShipmentList.setAdapter(scanShipmentCodeAdapter);
            ly_scan_system.setVisibility(View.VISIBLE);
            findViewById(R.id.button_start_scanning).setVisibility(View.GONE);
        } else {
            linHeader.setVisibility(View.GONE);
        }
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN:
                    if (objDatabaseHelper.getScannedForcedCodeList("0", "force_in") != null) {
                        newShipmentScannedArrayList = new ArrayList<>();
                        newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "force_in");
                    }
                    if (newShipmentScannedArrayList.size() > 0)
                        linHeader.setVisibility(View.VISIBLE);
                    else
                        linHeader.setVisibility(View.GONE);

                    scanShipmentCodeAdapter = new BatchListAdapter(newShipmentScannedArrayList);
                    scanShipmentList.setAdapter(scanShipmentCodeAdapter);
                    onResume();
                    break;
            }
        }
    }


    public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.MyViewHolder> {

        private ArrayList<NewShipmentDetailModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView textViewId;
            private TextView textViewQuantity;
            private TextView textViewName;

            public MyViewHolder(View view) {
                super(view);
                textViewId = view.findViewById(R.id.textView_no);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewName = view.findViewById(R.id.textView_name);

            }
        }


        public BatchListAdapter(ArrayList<NewShipmentDetailModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public BatchListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_scan_shipment, parent, false);
            return new MyViewHolder(itemView);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            NewShipmentDetailModel object = notesList.get(position);

            holder.textViewId.setText(String.valueOf(position + 1));

            holder.textViewName.setText(object.getProductName());
            holder.textViewQuantity.setText(object.getPackedRatio());
        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }
    }

    @Override
    public void onBackPressed() {
        newShipmentScannedArrayList = objDatabaseHelper.getScannedForcedCodeList("0", "force_in");
        if (newShipmentScannedArrayList.size() > 0) {
            new AlertDialog.Builder(activity)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            objDatabaseHelper.deleteScannedForcedCode("0", "force_in");

                            Intent intent = new Intent(SaleReturnFIActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            SaleReturnFIActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
