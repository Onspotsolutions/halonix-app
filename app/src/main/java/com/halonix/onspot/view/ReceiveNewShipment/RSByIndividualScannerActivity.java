package com.halonix.onspot.view.ReceiveNewShipment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceCodeList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoiceList;
import com.halonix.onspot.contributors.ReceiveShipmentInvoice_respo;
import com.halonix.onspot.contributors.get_receive_shipmentList;
import com.halonix.onspot.contributors.get_receive_shipment_respo;
import com.halonix.onspot.contributors.invoice_code_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.contributors.received_shipment_master_carton_respo;
import com.halonix.onspot.locationprovider.GetCurrentLocation;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.model.SystemBarcodeModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class RSByIndividualScannerActivity extends AppCompatActivity implements DecoratedBarcodeView.TorchListener {


    private Activity activity = null;
    private CaptureManager capture;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.zxing_barcode_scanner)
    DecoratedBarcodeView barcodeScannerView;

    @BindView(R.id.switch_flashlight)
    ImageButton switchFlashlightButton;

    @BindView(R.id.relOTG)
    RelativeLayout relOTG;

    @BindView(R.id.relCamera)
    RelativeLayout relCamera;

    @BindView(R.id.radio_scanner_Grp)
    RadioGroup radio_scanner_Grp;

    @BindView(R.id.radio_otg)
    RadioButton radio_otg;

    @BindView(R.id.radio_camera)
    RadioButton radio_camera;


    @BindView(R.id.radio_rb_Grp)
    RadioGroup radio_rb_Grp;

    @BindView(R.id.radio_regular)
    RadioButton radio_regular;

    @BindView(R.id.radio_breakage)
    RadioButton radio_breakage;

    @BindView(R.id.edScanCode)
    EditText edScanCode;

    @BindView(R.id.btnGo)
    Button btnGo;

    @BindView(R.id.btnFinish)
    Button btnFinish;

    @BindView(R.id.textInput)
    TextInputLayout textInput;

    private boolean hardwareKeyboardPlugged = false;
    private String barCode = "";

    private BeepManager beepManager = null;
    private ArrayList<String> systemBarcode = new ArrayList<>();
    private ArrayList<String> stringArrayList = new ArrayList<>();
    private boolean isFlashOn = false;
    private DatabaseHelper objDatabaseHelper = null;

    private String product_type = "regular";
    private AlertDialog.Builder alert = null;
    private JSONArray systemBarcodeJsonArray;
    private String qty;
    private AlertDialog alertDialog;
    private Location loca;
    private String strLatitude = "", strLongitude = "", strAccuracy = "";
    private RetrofitClient retrofitClient;
    private long mLastClickTime = 0;
    ConnectionDetectorActivity cd;

    //Receive New Shipment
    ArrayList<String> InvoiceCodeListArrayList;
    ArrayList<get_receive_shipmentList> receiveShipmentListArrayList;
    ArrayList<ReceiveShipmentInvoiceList> InvoiceCodeProductDataArrayList;
    ArrayList<PackedCartonList> packedcartonListArrayList;
    int k = 0;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_individual_shipment_scan);
        ButterKnife.bind(this);
        retrofitClient = new RetrofitClient();
        objDatabaseHelper = DatabaseHelper.getInstance(activity);
        cd = new ConnectionDetectorActivity(RSByIndividualScannerActivity.this);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        toolbar.setTitle("Scan Product QR Codes");

        if (getIntent().getExtras() != null) {
            stringArrayList = objDatabaseHelper.getIPCode();
            qty = String.valueOf(stringArrayList.size());
        }

        activity = RSByIndividualScannerActivity.this;

        radio_rb_Grp.setVisibility(View.VISIBLE);
        stringArrayList = new ArrayList<>();
        stringArrayList = objDatabaseHelper.getIPCode();
        systemBarcodeJsonArray = new JSONArray();
        for (int i = 0; i < stringArrayList.size(); i++) {
            systemBarcodeJsonArray.put(stringArrayList.get(i));
        }
        textInput.setHint("Product QR Code");

        Log.d(Constants.TAG, "onCreate systemBarcodeJsonArray: " + systemBarcodeJsonArray.toString());

        for (int i = 0; i < systemBarcodeJsonArray.length(); i++) {
            try {
                systemBarcode.add(String.valueOf(systemBarcodeJsonArray.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        barcodeScannerView.setTorchListener(this);
        beepManager = new BeepManager(this);

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton.setVisibility(View.GONE);
        }
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);

        barcodeScannerView.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                capture.onPause();
                beepManager.playBeepSoundAndVibrate();

                if (result.getText() == null) {
                    capture.onPause();
                    return;
                } else {
                    String[] final_barcode;
                    if (result.getText().contains("=")) {
                        final_barcode = result.getText().split("=");
                        String barCode1 = final_barcode[1].replaceAll("\u0000", "");

                        checkBarcode(barCode1, "camera");
                    } else {
                        checkBarcode(result.getText(), "camera");
                    }
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //capture.onResume();
                    }
                }, 3000);
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });


        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!edScanCode.getText().toString().equals("")) {
                    checkBarcode(edScanCode.getText().toString().trim(), "manual");
                } else {
                    Toast.makeText(RSByIndividualScannerActivity.this, "Enter code manually or scan QR using device.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        radio_scanner_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_otg) {
                relOTG.setVisibility(View.VISIBLE);
                relCamera.setVisibility(View.GONE);
                capture.onPause();
            } else if (checkedId == R.id.radio_camera) {
                relOTG.setVisibility(View.GONE);
                relCamera.setVisibility(View.VISIBLE);
                capture.onResume();
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(group.getWindowToken(), 0);
            }
        });

        radio_rb_Grp.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_regular) {
                product_type = "regular";
                capture.onPause();
                showAlertradio("Alert", "Now you are scanning regular products");
            } else if (checkedId == R.id.radio_breakage) {
                product_type = "breakage";
                capture.onPause();
                showAlertradio("Alert", "Now you are scanning breakage products");
            }
        });


        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (objDatabaseHelper.getIPScannedQRCodeList() > 0) {
                    showPopup();
                } else {
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    setResult(Activity.RESULT_OK, intent);
                    RSByIndividualScannerActivity.this.finish();
                    overridePendingTransition(0, 0);
                }
            }
        });
    }

    private void showAlertradio(String Title, String Message) {
        alert = new AlertDialog.Builder(RSByIndividualScannerActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                capture.onResume();
                alert=null;
            }
        });
        alert.show();
    }

    private void checkBarcode(String barcode, String tag) {
        if (isBarcodeExistsInSystem(barcode)) {
            if (!isInvoiceQrExists(barcode)) {

                if (!tag.equals("manual")) {
                    capture.onResume();
                } else {
                    clearEditScan();
                }

                objDatabaseHelper.insertScannedIPCodes(barcode, product_type);
                int res = Integer.parseInt(qty) - objDatabaseHelper.getIPScannedQRCodeList();
                if (res == 0) {
                    showPopup();
                }
            } else {
                showAlert("Alert", "This Product Qr-code is already scanned.", tag, false);
            }
        } else {

            showAlert("Alert", "This Qr-code is invalid.", tag, true);

        }
    }

    private void showAlert(String Title, String Message, String tag, boolean valid) {

        if (tag.equals("camera")) {
            capture.onPause();
        }

        alert = new AlertDialog.Builder(RSByIndividualScannerActivity.this);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (valid) {
                    clearEditScan();
                }
                if (tag.equals("camera")) {
                    capture.onResume();
                }
                alert=null;
            }
        });
        alert.show();
    }


    private void clearEditScan() {
        if (edScanCode.getText().toString().trim().length() > 0) {
            edScanCode.setText("");
        }
    }

    private boolean isBarcodeExistsInSystem(String strBarcode) {
        return systemBarcode.contains(strBarcode);
    }


    private boolean isInvoiceQrExists(String barcode) {
        return objDatabaseHelper.getIPScannedQRCodeList(barcode);
    }


    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    private boolean hasFlash() {
        return getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void switchFlashlight(View view) {
        if (isFlashOn) {
            isFlashOn = false;
            barcodeScannerView.setTorchOff();
        } else {
            isFlashOn = true;
            barcodeScannerView.setTorchOn();
        }
    }

    @Override
    public void onTorchOn() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_on);
    }


    public void stopScanning(View view) {
        onBackPressed();
    }
    public void onTorchOff() {
        switchFlashlightButton.setImageResource(R.drawable.ic_flash_off);
    }



    private void showPopup() {
        capture.onPause();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
        alertDialog.getWindow().setContentView(dialogView);


        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        Button btn_ok = dialogView.findViewById(R.id.btn_ok);
        Button btncont = dialogView.findViewById(R.id.btncont);
        Button btn_discard = dialogView.findViewById(R.id.btn_discard);

        tvMsg.setText(objDatabaseHelper.getIPScannedQRCodeList() + " scan done till now " + (Integer.parseInt(qty) - objDatabaseHelper.getIPScannedQRCodeList() + " pending."));

        int res = Integer.parseInt(qty) - objDatabaseHelper.getIPScannedQRCodeList();
        if (res == 0) {
            btncont.setVisibility(View.INVISIBLE);
        } else {
            btncont.setVisibility(View.VISIBLE);
        }
        btncont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                capture.onResume();
                alertDialog.dismiss();
                alertDialog=null;
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                if (cd.isConnectingToInternet()) {
                    if (objDatabaseHelper.getIPScannedQRCodeListRegular_Breakage("regular").size() > 0) {
                        LocationControlRegular locationControl = new LocationControlRegular();
                        locationControl.execute();
                    } else {
                        LocationControlBreakage locationControl = new LocationControlBreakage();
                        locationControl.execute();
                    }
                } else {
                    Utils.showToast(RSByIndividualScannerActivity.this, "Shipment (Receive by Individual products) stored offline.");
                    Intent intent = new Intent(RSByIndividualScannerActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    RSByIndividualScannerActivity.this.finish();
                    overridePendingTransition(0, 0);
                }
            }
        });

        btn_discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                objDatabaseHelper.deleteAllScannedIPCode();
                objDatabaseHelper.deleteAllIPCode();

                finish();
            }
        });
    }

    private class LocationControlRegular extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(RSByIndividualScannerActivity.this, "Receiving shipment by individual products,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(RSByIndividualScannerActivity.this);
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());

                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerRegular();
        }
    }

    private void submitDetailsToServerRegular() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(objDatabaseHelper.getIPScannedQRCodeListRegular_Breakage("regular")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(Constants.TAG, "submitDetailsToServerRegular: " + res);
        Log.d(Constants.TAG, "submitDetailsToServerRegular strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "submitDetailsToServerRegular strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "submitDetailsToServerRegular strAccuracy: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipmentDefective(objDatabaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy,"0").enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                }

                if (objDatabaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage").size() > 0) {
                    LocationControlBreakage locationControl = new LocationControlBreakage();
                    locationControl.execute();
                } else {
                    objDatabaseHelper.deleteAllScannedIPCode();
                    objDatabaseHelper.deleteAllIPCode();
                    getpackedcarton();
                }
            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        packedcartonListArrayList = new ArrayList<>();

        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {

                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1",0));
                            }
                        }else
                        {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1",Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }


                    }

                }
                String USER_TYPEs = Utils.getPreference(RSByIndividualScannerActivity.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_RETAILER) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    getReceiveNewShipmentList();
                } else {
                    getinvoice_codes();
                }

            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }

    //TODO Invoice Code API Call here
    private void getinvoice_codes() {

        retrofitClient.getService().getInvoiceCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<invoice_code_respo>() {
            @Override
            public void onResponse(Call<invoice_code_respo> call, Response<invoice_code_respo> response) {
                Log.d(Constants.TAG, "onResponse getinvoice_codes: " + response.toString());
                if (response.body().getSuccess()) {
                    objDatabaseHelper.deleteInvoiceCodeList();
                    for (int i = 0; i < response.body().getList().size(); i++) {
                        objDatabaseHelper.insertInvoiceCodeList(response.body().getList().get(i));
                    }
                }
                getReceiveNewShipmentList();
            }

            @Override
            public void onFailure(Call<invoice_code_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }

    //TODO Receive New Shipment API Call here
    private void getReceiveNewShipmentList() {
        k = 0;
        InvoiceCodeListArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveNewShipmentCodes(objDatabaseHelper.getAuthToken()).enqueue(new Callback<get_receive_shipment_respo>() {
            @Override
            public void onResponse(Call<get_receive_shipment_respo> call, Response<get_receive_shipment_respo> response) {

                Log.d(Constants.TAG, "onResponse getReceiveNewShipmentList: " + response.toString());
                if (response.body().getSuccess()) {
                    InvoiceCodeListArrayList = response.body().getInvoiceCodes();
                    receiveShipmentListArrayList = response.body().getList();
                    objDatabaseHelper.deleteAllReceiveShipmentCode();
                    objDatabaseHelper.deleteServerBarcodeDataTableData();
                    objDatabaseHelper.deleteAllReceiveShipmentCodeData();
                    objDatabaseHelper.deleteAllReceiveShipmentISMasterCartons();

                    if (receiveShipmentListArrayList.size() > 0) {
                        for (int i = 0; i < receiveShipmentListArrayList.size(); i++) {
                            objDatabaseHelper.insertReceiveNewShipment(new NewShipmentDetailModel(String.valueOf(receiveShipmentListArrayList.get(i).getId()),
                                    String.valueOf(receiveShipmentListArrayList.get(i).getPackingRatio()), receiveShipmentListArrayList.get(i).getCode(),
                                    receiveShipmentListArrayList.get(i).getProductName(), receiveShipmentListArrayList.get(i).getSweep(),
                                    receiveShipmentListArrayList.get(i).getColor(), "1"));
                        }


                        JSONArray jsonArray1 = objDatabaseHelper.getReceiveShipmentBarcode1().getJsonArray();

                        objDatabaseHelper.insertMasterCartoncode(new SystemBarcodeModel(jsonArray1));

                    }


                    if (InvoiceCodeListArrayList.size() > 0) {

                        objDatabaseHelper.inserRSInvoicecode(new SystemBarcodeModel(new JSONArray(InvoiceCodeListArrayList)));


                        for (int i = 0; i < InvoiceCodeListArrayList.size(); i++) {
                            ReceiveShipmentByInvoiceData(InvoiceCodeListArrayList.get(i));
                        }

                    } else {
                        Utils.dismissprogressdialog();
                        Intent intent = new Intent(RSByIndividualScannerActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RSByIndividualScannerActivity.this.finish();
                        overridePendingTransition(0, 0);
                        Utils.showToast(activity, "Shipment received Successfully.");
                    }


                }
            }

            @Override
            public void onFailure(Call<get_receive_shipment_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }

    //TODO Receive New Shipment Invoice API Call here
    private void ReceiveShipmentByInvoiceData(String invoice_code) {
        Log.d(Constants.TAG, "ReceiveShipmentByInvoiceData i: " + invoice_code);
        InvoiceCodeProductDataArrayList = new ArrayList<>();

        retrofitClient.getService().getReceiveShipmentByInvoice(objDatabaseHelper.getAuthToken(), invoice_code).enqueue(new Callback<ReceiveShipmentInvoice_respo>() {
            @Override
            public void onResponse(Call<ReceiveShipmentInvoice_respo> call, Response<ReceiveShipmentInvoice_respo> response) {

                //Log.d(Constants.TAG, "onResponse ReceiveShipmentByInvoiceData: " + response.toString());
                if (response.body().getSuccess()) {
                    k++;
                    InvoiceCodeProductDataArrayList = response.body().getList();

                    if (InvoiceCodeProductDataArrayList.size() > 0) {
                        for (int i = 0; i < InvoiceCodeProductDataArrayList.size(); i++) {
                            if (InvoiceCodeProductDataArrayList.get(i).getQuantity() == null) {
                            } else {
                                objDatabaseHelper.inserRSInvoicecodeData(new NewShipmentDetailModel(invoice_code, InvoiceCodeProductDataArrayList.get(i).getModel(),
                                        "", "",
                                        String.valueOf(InvoiceCodeProductDataArrayList.get(i).getQuantity()), ""));
                                ArrayList<ReceiveShipmentInvoiceCodeList> codeArrayList = InvoiceCodeProductDataArrayList.get(i).getCodes();

                                for (int j = 0; j < codeArrayList.size(); j++) {
                                    objDatabaseHelper.insertIS_MasterCartonCodes(InvoiceCodeProductDataArrayList.get(i).getModel(), invoice_code, codeArrayList.get(j).getCode(), String.valueOf(codeArrayList.get(j).getPackingRatio()));
                                }
                            }
                        }
                    }


                    if (k == InvoiceCodeListArrayList.size()) {
                        Utils.dismissprogressdialog();
                        Intent intent = new Intent(RSByIndividualScannerActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        RSByIndividualScannerActivity.this.finish();
                        overridePendingTransition(0, 0);
                        Utils.showToast(activity, "Shipment received Successfully");
                    }

                }
            }

            @Override
            public void onFailure(Call<ReceiveShipmentInvoice_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });


    }


    private class LocationControlBreakage extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            Utils.showprogressdialog(RSByIndividualScannerActivity.this, "Receiving shipment by individual products,please wait.");
        }

        protected String doInBackground(String... params) {
            final GetCurrentLocation lListener = new GetCurrentLocation(RSByIndividualScannerActivity.this);
            lListener.startGettingLocation(new GetCurrentLocation.getLocation() {
                @Override
                public void onLocationChanged(Location location) {
                    if (location != null) {
                        loca = location;
                        lListener.stopGettingLocation();
                        strAccuracy = String.valueOf(loca.getAccuracy());
                        strLatitude = String.valueOf(loca.getLatitude());
                        strLongitude = String.valueOf(loca.getLongitude());
                    }
                }
            });
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return strAccuracy;
        }

        protected void onPostExecute(String unused) {
            submitDetailsToServerBreakage();
        }
    }

    private void submitDetailsToServerBreakage() {
        JSONArray jsonArray;
        String res = null;
        try {
            jsonArray = new JSONArray(String.valueOf(objDatabaseHelper.getIPScannedQRCodeListRegular_Breakage("breakage")));
            res = jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(Constants.TAG, "submitDetailsToServerBreakage: " + res);
        Log.d(Constants.TAG, "submitDetailsToServerBreakage strLatitude: " + strLatitude);
        Log.d(Constants.TAG, "submitDetailsToServerBreakage strLongitude: " + strLongitude);
        Log.d(Constants.TAG, "submitDetailsToServerBreakage strAccuracy: " + strAccuracy);

        retrofitClient.getService().getReceiveNewShipmentDefective(objDatabaseHelper.getAuthToken(), res, "IndividualProduct", strLatitude, strLongitude, strAccuracy, "1").enqueue(new Callback<received_shipment_master_carton_respo>() {
            @Override
            public void onResponse(Call<received_shipment_master_carton_respo> call, Response<received_shipment_master_carton_respo> response) {
                if (response.body().getSuccess()) {
                }
                objDatabaseHelper.deleteAllScannedIPCode();
                objDatabaseHelper.deleteAllIPCode();
                getpackedcarton();

            }

            @Override
            public void onFailure(Call<received_shipment_master_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (objDatabaseHelper.getIPScannedQRCodeList() > 0) {
            showPopup();
        } else {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            setResult(Activity.RESULT_OK, intent);
            RSByIndividualScannerActivity.this.finish();
            overridePendingTransition(0, 0);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (alert == null && alertDialog == null) {
            capture.onResume();
        }
        hardwareKeyboardPlugged = (getResources().getConfiguration().hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO);
        if (hardwareKeyboardPlugged) {
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);
            radio_camera.setChecked(false);
            radio_camera.setEnabled(false);
            relCamera.setVisibility(View.GONE);

            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);
//            btnGo.setEnabled(false);

        } else {
            focusable(true);
//            btnGo.setEnabled(true);

            relOTG.setVisibility(View.GONE);
            radio_otg.setChecked(false);

            relCamera.setVisibility(View.VISIBLE);
            radio_camera.setChecked(true);
            radio_camera.setEnabled(true);


        }


//        if (btnGo.isEnabled())
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));
//        else
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));
    }


    //OTG Code Start
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {

        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            Toast.makeText(this, "Barcode Scanner connected..", Toast.LENGTH_LONG).show();
            if (!edScanCode.getText().toString().equals("")) {
                edScanCode.setText("");
            }
            focusable(false);

//            btnGo.setEnabled(false);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_gray));

            radio_camera.setEnabled(false);
            radio_camera.setChecked(false);
            relCamera.setVisibility(View.GONE);
            capture.onPause();
            relOTG.setVisibility(View.VISIBLE);
            radio_otg.setChecked(true);

            hideKeyboard(RSByIndividualScannerActivity.this);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            Toast.makeText(this, "Barcode Scanner disconnected..", Toast.LENGTH_LONG).show();

            focusable(true);

//            btnGo.setEnabled(true);
//            btnGo.setBackground(getResources().getDrawable(R.drawable.btn_rounded_red));

            radio_camera.setEnabled(true);
            radio_camera.setChecked(true);

            radio_otg.setChecked(false);
            relOTG.setVisibility(View.GONE);
            relCamera.setVisibility(View.VISIBLE);
            capture.onResume();

            hideKeyboard(RSByIndividualScannerActivity.this);
        }
        super.onConfigurationChanged(newConfig);
    }

    public void focusable(boolean value) {
        edScanCode.setFocusable(true);
        edScanCode.setFocusableInTouchMode(true);
        edScanCode.setCursorVisible(true);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() != KeyEvent.KEYCODE_ENTER) {
            focusable(false);
            char pressedKey = (char) e.getUnicodeChar();
            barCode += pressedKey;
            if (barCode.contains("null")) {
                barCode = barCode.replace("null", "").trim();
            }
        }
        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            barcodeLookup(barCode);
            barCode = "";
        }

        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }


    private void barcodeLookup(String barCode) {

        String[] final_barcode;
        if (barCode.contains("=")) {
            final_barcode = barCode.split("=");
            barCode = final_barcode[1].replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            checkBarcode(barCode, "OTG");
        } else {
            barCode = barCode.replaceAll("\u0000", "");
            edScanCode.setText(barCode);
            checkBarcode(barCode, "OTG");
        }
    }
    //OTG Code End

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}

