package com.halonix.onspot.view.NewShipment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputLayout;
import com.halonix.onspot.ConnectionLostCallback;
import com.halonix.onspot.MyBroadcastReceiver;
import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PackedCartonList;
import com.halonix.onspot.contributors.master_mapping_respo;
import com.halonix.onspot.contributors.packed_carton_respo;
import com.halonix.onspot.model.AgentListModel;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.NewShipment;
import com.halonix.onspot.model.NewShipmentDetailModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.MainActivity;
import com.halonix.onspot.view.Singleton.ProductListScanActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.MyApplication.getContext;

public class NewShipmentDispatchActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ConnectionLostCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edInvoiceNumber)
    AppCompatEditText edInvoiceNumber;

    @BindView(R.id.edInvoiceDate)
    AppCompatEditText edInvoiceDate;

    @BindView(R.id.edPONumber)
    AppCompatEditText edPONumber;

    @BindView(R.id.edPODate)
    AppCompatEditText edPODate;

    @BindView(R.id.InputInvoiceNumber)
    TextInputLayout InputInvoiceNumber;

    @BindView(R.id.InputInvoiceDate)
    TextInputLayout InputInvoiceDate;

    @BindView(R.id.InputPONumber)
    TextInputLayout InputPONumber;

    @BindView(R.id.InputPODate)
    TextInputLayout InputPODate;

    @BindView(R.id.tvRece)
    TextView tvRece;

    @BindView(R.id.btnDispatch)
    AppCompatButton btnDispatch;


    @BindView(R.id.spinnerAgentList)
    Spinner spinnerAgentList;

    @BindView(R.id.spinnerSendList)
    Spinner spinnerSendList;

    @BindView(R.id.rel)
    RelativeLayout rel;

    @BindView(R.id.tv_check_connection)
    TextView tv_check_connection;

    private Context context;

    private ConnectionDetectorActivity cd;
    private String PO_date = "", Invoice_date = "", shipment_type = "", myFormat = "dd-MM-yyyy";

    private DatabaseHelper objDatabaseHelper;
    private ArrayList<AgentListModel> arrayAgent = new ArrayList<>();
    ArrayList<AgentListModel> arrayAgent1 = new ArrayList<>();
    private final Calendar myCalendar = Calendar.getInstance();
    private SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    private GoogleApiClient googleApiClient;
    String scanedList = "";

    NewShipment newShipment = null;

    ArrayList<NewShipment> arrOfflineshipmentModels;
    Dialog dialog;
    private long mLastClickTime = 0;
    private RetrofitClient retrofitClient;
    private ListView listView;
    private BroadcastReceiver myBroadcastReceiver;
    boolean lost = false;
    boolean alert = false;
    private boolean receiver = false;

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {


        if (e.getAction() == KeyEvent.ACTION_DOWN
                && e.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            showAlert();
        }

        return false;
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(myBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (receiver) {
            unregisterNetworkChanges();
        }
    }

    private void showAlert() {
        if (objDatabaseHelper.getNotUploadedNewShipmentDetails().size() > 0) {
            new AlertDialog.Builder(NewShipmentDispatchActivity.this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Please submit the data to move ahead.")
                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            objDatabaseHelper.deleteOfflineNewShipment("");
                            Utils.removePreference(NewShipmentDispatchActivity.this, "shipment_type");
                            objDatabaseHelper.updateScannedMasterCartonCode("", "", "", "0");

                            Intent intent = new Intent(NewShipmentDispatchActivity.this, NewShipmentActivity.class);
                            startActivity(intent);
                            NewShipmentDispatchActivity.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    })
                    .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else {
            Intent intent = new Intent(NewShipmentDispatchActivity.this, NewShipmentActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_shipment_dispatch);
        context = NewShipmentDispatchActivity.this;
        cd = new ConnectionDetectorActivity(context);
        ButterKnife.bind(this);
        objDatabaseHelper = DatabaseHelper.getInstance(context);

        retrofitClient = new RetrofitClient();
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                showAlert();
            }
        });
        toolbar.setTitle("Send New Shipment");


        arrOfflineshipmentModels = objDatabaseHelper.getNotUploadedNewShipmentDetails();

        try {
            JSONArray jsonArray = new JSONArray(Utils.getPreference(context, PreferenceKeys.USER_SHIPMENT_TYPE, ""));
            ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                list.add(jsonArray.getString(i));
            }
            ArrayAdapter<String> spinnerMenu = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, list);

            if (Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", "").equals("")) {
                spinnerSendList.setAdapter(spinnerMenu);
            } else {
                int spinnerPosition = spinnerMenu.getPosition(Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", ""));
                spinnerSendList.setAdapter(spinnerMenu);
                spinnerSendList.setSelection(spinnerPosition);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        edInvoiceNumber.setLongClickable(false);
        edInvoiceDate.setLongClickable(false);
        edPONumber.setLongClickable(false);
        edPODate.setLongClickable(false);

        edInvoiceNumber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        edInvoiceDate.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        edPONumber.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        edPODate.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        if (arrOfflineshipmentModels.size() > 0) {
            NewShipment newShipment = arrOfflineshipmentModels.get(0);
            edInvoiceNumber.setText(newShipment.getInvoice_number());
            edInvoiceDate.setText(newShipment.getInvoice_date());
            Invoice_date = newShipment.getInvoice_date();
            edPONumber.setText(newShipment.getPo_number());
            edPODate.setText(newShipment.getPo_date());
            PO_date = newShipment.getPo_date();

            if (!Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", "").equals("")) {
                shipment_type = Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", "");
            } else {
                shipment_type = spinnerSendList.getItemAtPosition(spinnerSendList.getSelectedItemPosition()).toString();
            }


            arrayAgent = objDatabaseHelper.getAgentList(shipment_type);
            for (int i = 0; i < arrayAgent.size(); i++) {
                if (arrayAgent.get(i).getName() == null) {
                    arrayAgent.get(i).setName("");
                }
                arrayAgent1.add(arrayAgent.get(i));

            }

            SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
            String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
            for (int j = 0; j < arrayAgent1.size(); j++) {
                if (arrayAgent1.get(j).getId().equals(sharedPreferences.getString("id", ""))) {
                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                        tvRece.setText(arrayAgent1.get(j).getCp_person() + " - " + arrayAgent1.get(j).getName());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                        tvRece.setText(arrayAgent1.get(j).getCp_person() + " - " + arrayAgent1.get(j).getLocation());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        tvRece.setText(arrayAgent1.get(j).getCp_person() + " - " + arrayAgent1.get(j).getLocation());
                    }
                    break;
                }
            }

            rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    dialog = new Dialog(NewShipmentDispatchActivity.this);
                    LayoutInflater inflater = LayoutInflater.from(NewShipmentDispatchActivity.this);
                    View view1 = inflater.inflate(R.layout.searchable_list_dialog, null);
                    listView = view1.findViewById(R.id.listItems);
                    SearchView searchView = view1.findViewById(R.id.search);

                    StopArrayAdapter spinnerAgentAdapter = new StopArrayAdapter(NewShipmentDispatchActivity.this, arrayAgent1); //selected item will look like a spinner set from XML
                    spinnerAgentAdapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
                    listView.setAdapter(spinnerAgentAdapter);

                    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String newText) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            spinnerAgentAdapter.filter(newText);
                            return false;
                        }
                    });
                    dialog.setContentView(view1);
                    dialog.show();
                }
            });


            spinnerSendList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String temp = Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", "");
                    Log.d(Constants.TAG, "onItemSelected temp: " + temp);
                    shipment_type = spinnerSendList.getItemAtPosition(spinnerSendList.getSelectedItemPosition()).toString();
                    Log.d(Constants.TAG, "onItemSelected shipment_type: " + shipment_type);
                    Utils.addPreference(NewShipmentDispatchActivity.this, "shipment_type", shipment_type);
                    Log.d(Constants.TAG, "onItemSelected addPreference: " + Utils.getPreference(NewShipmentDispatchActivity.this, "shipment_type", ""));
                    if (temp.equals(shipment_type)) {

                    } else {
                        setupAgencyList(shipment_type);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat(myFormat);
            String formattedDate = df.format(c);
            edInvoiceDate.setText(formattedDate);
            Invoice_date = formattedDate;

            spinnerSendList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    shipment_type = spinnerSendList.getItemAtPosition(spinnerSendList.getSelectedItemPosition()).toString();
                    Utils.addPreference(NewShipmentDispatchActivity.this, "shipment_type", shipment_type);

                    setupAgencyList(shipment_type);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


            rel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    dialog = new Dialog(NewShipmentDispatchActivity.this);
                    LayoutInflater inflater = LayoutInflater.from(NewShipmentDispatchActivity.this);
                    View view1 = inflater.inflate(R.layout.searchable_list_dialog, null);
                    ListView listView = view1.findViewById(R.id.listItems);
                    SearchView searchView = view1.findViewById(R.id.search);

                    Log.d(Constants.TAG, "onClick: " + arrayAgent1.size());
                    StopArrayAdapter spinnerAgentAdapter = new StopArrayAdapter(NewShipmentDispatchActivity.this, arrayAgent1); //selected item will look like a spinner set from XML
                    spinnerAgentAdapter.setDropDownViewResource(R.layout.my_simple_spinner_item);
                    listView.setAdapter(spinnerAgentAdapter);

                    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String newText) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            spinnerAgentAdapter.filter(newText);
                            return false;
                        }
                    });
                    dialog.setContentView(view1);
                    // dialog.setCancelable(false);
                    dialog.show();
                }
            });

        }

        if (Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_DISTRIBUTOR) || Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
            btnDispatch.setText("SUBMIT");
        }


        btnDispatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!validateInvoiceNumber()) {
                    return;
                }
                if (!validatePONumber()) {
                    return;
                }
                if (!validatePODate()) {
                    return;
                }
                SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                String id = sharedPreferences.getString("id", "");
                //  int position = spinnerAgentList.getSelectedItemPosition();
                newShipment = new NewShipment(0, objDatabaseHelper.getAuthToken(),
                        String.valueOf(objDatabaseHelper.getTotalShipmentCount()),
                        id, edInvoiceNumber.getText().toString().trim(),
                        Invoice_date, edPONumber.getText().toString().trim(),
                        PO_date, scanedList, shipment_type, "0");
                objDatabaseHelper.insertNewShipment(newShipment);
                objDatabaseHelper.updateScannedMasterCartonCode(id, shipment_type, edInvoiceNumber.getText().toString().trim(), "1");

                if (Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_DISTRIBUTOR) || Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    Utils.hideKeyboard(NewShipmentDispatchActivity.this);
                    myBroadcastReceiver = new MyBroadcastReceiver(NewShipmentDispatchActivity.this);
                    registerReceiver(myBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    receiver = true;
                    alert = true;

                    if (cd.isConnectingToInternet()) {
                        AgencyDispatchMapping(id);
                    } else {
                        if (receiver) {
                            unregisterNetworkChanges();
                            alert = false;
                            lost = false;
                        }

                        Utils.addPreference(NewShipmentDispatchActivity.this, "api", "");

                        objDatabaseHelper.updateScannedOfllineShipment();
                        objDatabaseHelper.updateScannedMasterCartonCode("1");

                        Utils.showToast(NewShipmentDispatchActivity.this, "Send New shipment stored offline.");
                        Intent intent = new Intent(NewShipmentDispatchActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        NewShipmentDispatchActivity.this.finish();
                        overridePendingTransition(0, 0);
                    }

                } else {

                    Utils.hideKeyboard(NewShipmentDispatchActivity.this);
                    Intent intent = new Intent(NewShipmentDispatchActivity.this, ProductListScanActivity.class);
                    intent.putExtra("tag", "dispatch");
                    startActivity(intent);
                    overridePendingTransition(0, 0);

//                    ArrayList<String> SacnnedBarcodeList = objDatabaseHelper.getScanShipmentBarcode();
//
//
//                    JSONObject objNewShipmentDetails = new JSONObject();
//                    try {
//                        objNewShipmentDetails.put("AgencyId", id);
//                        objNewShipmentDetails.put("Invoice_number", edInvoiceNumber.getText().toString().trim());
//                        objNewShipmentDetails.put("Invoice_date", Invoice_date);
//                        objNewShipmentDetails.put("PO_number", edPONumber.getText().toString().trim());
//                        objNewShipmentDetails.put("PO_date", PO_date);
//                        objNewShipmentDetails.put("quantity", objDatabaseHelper.getTotalShipmentCount());
//                        objNewShipmentDetails.put("shipment_type", shipment_type);
//                        objNewShipmentDetails.put("MasterCartonCode", new JSONArray(SacnnedBarcodeList));
//                        objNewShipmentDetails.put("InvoiceCode", "");
//                    } catch (JSONException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                    JSONArray jsonArray = new JSONArray();
//                    jsonArray.put(objNewShipmentDetails);
//
//                    Log.d(Constants.TAG, "AgencyDispatchMapping: " + jsonArray.toString());
                }


            }
        });


        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();


        edPONumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!validatePONumber()) {
                    return;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

        edPODate.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!validatePODate()) {
                    return;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

        edInvoiceNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!validateInvoiceNumber()) {
                    return;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });


        edPODate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                final Calendar c = Calendar.getInstance();

                int y = c.get(Calendar.YEAR);
                int m = c.get(Calendar.MONTH);
                int d = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dp = new DatePickerDialog(NewShipmentDispatchActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                String date = new SimpleDateFormat(myFormat).format(c.getTime());
                                edPODate.setText(date);
                                PO_date = edPODate.getText().toString();
                            }

                        }, y, m, d);
                dp.getDatePicker().setMaxDate(System.currentTimeMillis());
                dp.show();
            }
        });

        final DatePickerDialog.OnDateSetListener InDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                edInvoiceDate.setText(sdf.format(myCalendar.getTime()));
                Invoice_date = edInvoiceDate.getText().toString();
            }

        };

        edInvoiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, InDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


    }


    //TODO Agency dispatch API Call here
    private void AgencyDispatchMapping(String AgencyId) {
        if (cd.isConnectingToInternet()) {
            ArrayList<String> SacnnedBarcodeList;
            SacnnedBarcodeList = objDatabaseHelper.getScanShipmentBarcode();

            JSONObject objNewShipmentDetails = new JSONObject();
            try {
                objNewShipmentDetails.put("AgencyId", AgencyId);
                objNewShipmentDetails.put("Invoice_number", edInvoiceNumber.getText().toString().trim());
                objNewShipmentDetails.put("Invoice_date", edInvoiceDate.getText().toString().trim());
                objNewShipmentDetails.put("PO_number", edPONumber.getText().toString().trim());
                objNewShipmentDetails.put("PO_date", edPODate.getText().toString().trim());
                objNewShipmentDetails.put("quantity", objDatabaseHelper.getTotalShipmentCount());
                objNewShipmentDetails.put("shipment_type", shipment_type);
                objNewShipmentDetails.put("MasterCartonCode", new JSONArray(SacnnedBarcodeList));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(objNewShipmentDetails);

            Log.d(Constants.TAG, "AgencyDispatchMapping: " + jsonArray.toString());
            Utils.showprogressdialog(NewShipmentDispatchActivity.this, "Requesting for sending new shipment,please wait.");
            Utils.addPreference(NewShipmentDispatchActivity.this, "api", "mapping");
            retrofitClient.getService().agency_dispatch_mapping(objDatabaseHelper.getAuthToken(), jsonArray.toString()).enqueue(new Callback<master_mapping_respo>() {
                @Override
                public void onResponse(Call<master_mapping_respo> call, Response<master_mapping_respo> response) {
                    // Utils.dismissprogressdialog();

                    Log.d(Constants.TAG, "onResponse: " + response.body().getSuccess());
                    if (response.body().getSuccess()) {
                        Utils.addPreference(NewShipmentDispatchActivity.this, "api", "");
                        Utils.removePreference(NewShipmentDispatchActivity.this, "shipment_type");
                        objDatabaseHelper.deleteOfflineNewShipment("all");
                        objDatabaseHelper.deleteAllScannedMasterCartonCode("all");
                    }
                    getpackedcarton();
                }

                @Override
                public void onFailure(Call<master_mapping_respo> call, Throwable t) {
                    Utils.dismissprogressdialog();
                }
            });

        } else {
            if (receiver) {
                unregisterNetworkChanges();
                alert = false;
                lost = false;
            }
            Utils.addPreference(NewShipmentDispatchActivity.this, "api", "");

            Utils.showToast(NewShipmentDispatchActivity.this, "New shipment stored offline.");
            Intent intent = new Intent(NewShipmentDispatchActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            NewShipmentDispatchActivity.this.finish();
            overridePendingTransition(0, 0);
        }


    }

    ArrayList<PackedCartonList> packedcartonListArrayList;

    //TODO Packed Carton API Call here
    private void getpackedcarton() {
        Log.d(Constants.TAG, "getpackedcarton: ");
        packedcartonListArrayList = new ArrayList<>();
        Utils.addPreference(NewShipmentDispatchActivity.this, "api", "packed_carton");
        retrofitClient.getService().getPackedCarton(objDatabaseHelper.getAuthToken()).enqueue(new Callback<packed_carton_respo>() {
            @Override
            public void onResponse(Call<packed_carton_respo> call, Response<packed_carton_respo> response) {

                Log.d(Constants.TAG, "onResponse getpackedcarton: " + response.toString());
                if (response.body().getSuccess()) {
                    Utils.addPreference(NewShipmentDispatchActivity.this, "api", "");
                    objDatabaseHelper.deleteAllPackedCartonCode();
                    packedcartonListArrayList = response.body().getMasterCartonCodes();

                    if (packedcartonListArrayList.size() > 0) {
                        if (Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_VENDOR)) {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", 0));
                            }
                        } else {
                            for (int i = 0; i < packedcartonListArrayList.size(); i++) {
                                objDatabaseHelper.insertShipment(new NewShipmentDetailModel(
                                        String.valueOf(packedcartonListArrayList.get(i).getId()),
                                        String.valueOf(packedcartonListArrayList.get(i).getPackingRatio()),
                                        packedcartonListArrayList.get(i).getCode(),
                                        String.valueOf(packedcartonListArrayList.get(i).getProductName()),
                                        "1", Integer.parseInt(packedcartonListArrayList.get(i).getSender_id())));
                            }
                        }


                    }

                }

                String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    Utils.dismissprogressdialog();
                    Intent intent = new Intent(NewShipmentDispatchActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    NewShipmentDispatchActivity.this.finish();
                    overridePendingTransition(0, 0);
                    alert = false;
                    lost = false;

                    Utils.showToast(NewShipmentDispatchActivity.this, "Shipment dispatched successfully");

                }
            }

            @Override
            public void onFailure(Call<packed_carton_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });

    }


    private boolean validateInvoiceNumber() {
        String invoice_number = edInvoiceNumber.getText().toString().trim();
        boolean res = objDatabaseHelper.checkInvoiceinDB(invoice_number);
        if (invoice_number.isEmpty()) {
            InputInvoiceNumber.setError(getString(R.string.hint_enter_invoice_number));
            edInvoiceNumber.requestFocus();
            return false;
        } else if (res) {
            InputInvoiceNumber.setError("Invoice number already used for other offline shipment,please use different invoice number.");
            edInvoiceNumber.requestFocus();
            return false;
        } else {
            InputInvoiceNumber.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePONumber() {
        String po_number = edPONumber.getText().toString().trim();

        if (po_number.isEmpty()) {
            InputPONumber.setError(getString(R.string.hint_enter_po_number));
            edPONumber.requestFocus();
            return false;
        } else {
            InputPONumber.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePODate() {
        String po_date = edPODate.getText().toString().trim();

        if (po_date.isEmpty()) {
            InputPODate.setError(getString(R.string.hint_select_po_date));
            edPODate.requestFocus();
            return false;
        } else {
            InputPODate.setErrorEnabled(false);
        }
        return true;
    }

    //TODO set recipient to spinner as per send
    public void setupAgencyList(final String shipment_type) {
        arrayAgent = new ArrayList<>();
        arrayAgent1 = new ArrayList<>();
        final ProgressDialog progressBar = new ProgressDialog(context);
        progressBar.setMessage("Please wait...");
        progressBar.setCancelable(false);
        progressBar.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                arrayAgent = objDatabaseHelper.getAgentList(shipment_type);

                for (int i = 0; i < arrayAgent.size(); i++) {
                    if (arrayAgent.get(i).getName() == null) {
                        arrayAgent.get(i).setName("");
                    }
                    arrayAgent1.add(arrayAgent.get(i));

                }
                SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("id", arrayAgent1.get(0).getId());
                editor.apply();
                String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
                if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getName());
                } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getLocation());
                } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                    tvRece.setText(arrayAgent1.get(0).getCp_person() + " - " + arrayAgent1.get(0).getLocation());
                }
                progressBar.dismiss();
            }
        }, 1000);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void dialog(boolean value) {
        Animation slide_down = AnimationUtils.loadAnimation(NewShipmentDispatchActivity.this, R.anim.slide_down);
        Animation slide_up = AnimationUtils.loadAnimation(NewShipmentDispatchActivity.this, R.anim.slide_up);

        if (value) {
            if (lost) {
                Log.d(Constants.TAG, "dialog: ");
                alert = false;
                lost = false;
                tv_check_connection.setText("You are back online.");
                tv_check_connection.setBackgroundResource(R.color.green);
                tv_check_connection.setTextColor(Color.WHITE);
                Handler handler = new Handler();
                Runnable delayrunnable = () -> {
                    tv_check_connection.setVisibility(View.GONE);
                    tv_check_connection.startAnimation(slide_down);
                };
                handler.postDelayed(delayrunnable, 3000);

                if (Utils.getPreference(NewShipmentDispatchActivity.this, "api", "").equals("mapping")) {
                    SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                    String id = sharedPreferences.getString("id", "");
                    AgencyDispatchMapping(id);
                } else if (Utils.getPreference(NewShipmentDispatchActivity.this, "api", "").equals("packed_carton")) {
                    Utils.showprogressdialog(NewShipmentDispatchActivity.this, "Requesting for sending new shipment,please wait.");
                    getpackedcarton();
                }
            }
        } else {
            lost = true;

            if (alert) {
                tv_check_connection.startAnimation(slide_up);
                tv_check_connection.setVisibility(View.VISIBLE);
                tv_check_connection.setText("Internet connection unavailable.");
                tv_check_connection.setBackgroundColor(Color.RED);
                tv_check_connection.setTextColor(Color.WHITE);
            }
        }
    }

    @Override
    public void connectionLost(boolean b) {
        dialog(b);
    }


    public class StopArrayAdapter extends ArrayAdapter<AgentListModel> {

        private ArrayList<AgentListModel> items;
        private ArrayList<AgentListModel> arraylist;
        LayoutInflater flater;

        public StopArrayAdapter(Context activity, ArrayList<AgentListModel> items) {
            super(activity, android.R.layout.simple_list_item_1, items);
            this.items = items;
            this.arraylist = new ArrayList<>();
            this.arraylist.addAll(items);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @Override
        public AgentListModel getItem(int position) {
            return items.get(position);
        }

        private class viewHolder {
            TextView txtTitle;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            AgentListModel rowItem = getItem(position);

            viewHolder holder;
            View rowview = convertView;
            if (rowview == null) {
                holder = new viewHolder();
                flater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowview = flater.inflate(R.layout.my_simple_spinner_item, parent, false);
                holder.txtTitle = rowview.findViewById(R.id.spin_item);
                rowview.setTag(holder);
            } else {
                holder = (viewHolder) rowview.getTag();
            }
            String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
            if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getName());
            } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
            } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                holder.txtTitle.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
            }

            rowview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(activity, rowItem.getCp_person(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    SharedPreferences sharedPreferences = getSharedPreferences("agent_data", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id", rowItem.getId());
                    editor.apply();
                    Log.d(Constants.TAG, "onClick: " + arraylist.size());
                    items.clear();
                    items.addAll(arraylist);
                    String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getName());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_VENDOR)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
                    } else if (USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {
                        tvRece.setText(rowItem.getCp_person() + " - " + rowItem.getLocation());
                    }

                }
            });
            return rowview;
        }

        void filter(String charText) {
            charText = charText.toLowerCase();
            items.clear();
            if (charText.length() == 0) {
                items.addAll(arraylist);
            } else {
                for (AgentListModel s : arraylist) {
                    String USER_TYPEs = Utils.getPreference(NewShipmentDispatchActivity.this, PreferenceKeys.USER_TYPE, "");
                    if (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {

                        if (s.getCp_person().toLowerCase().contains(charText)) {
                            items.add(s);
                        } else if (s.getLocation().toLowerCase().contains(charText)) {
                            items.add(s);
                        }
                    } else {
                        if (s.getCp_person().toLowerCase().contains(charText)) {
                            items.add(s);
                        } else if (s.getName().toLowerCase().contains(charText)) {
                            items.add(s);
                        }
                    }
                }
/*
                if (items.size() == 0) {
                    txtNoResult.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                } else {
                    txtNoResult.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                }*/
            }

            notifyDataSetChanged();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
