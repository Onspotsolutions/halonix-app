package com.halonix.onspot.view.History.tab;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.received_historyList;
import com.halonix.onspot.contributors.received_history_respo;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;
import com.halonix.onspot.view.Shortfall.Shortfall;
import com.halonix.onspot.view.base.BaseFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.halonix.onspot.utils.Utils.getDate;

public class FragmentReceivePending extends BaseFragment {

    @BindView(R.id.rv_pending)
    RecyclerView rv_pending;

    @BindView(R.id.textView_no_data_found_pending)
    TextView textViewNoDataFound;

    private DatabaseHelper objDatabaseHelper = null;
    private ConnectionDetectorActivity cd = null;

    private ArrayList<received_historyList> arrCompletedListModel;
    private ArrayList<CompletedListModel> completedListModelArrayList;

    RetrofitClient retrofitClient;
    private ProgressDialog pd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetectorActivity(getActivity());
        objDatabaseHelper = DatabaseHelper.getInstance(getActivity());
    }

    @Override
    public boolean backButtonPressed() {
        return false;
    }

    public void fragmentBecameVisible() {
//        getCompletedListFromServer();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_shipment_pending, container, false);
        ButterKnife.bind(this, mainView);
        retrofitClient = new RetrofitClient();
        rv_pending.setHasFixedSize(true);
        rv_pending.setItemAnimator(new DefaultItemAnimator());
        rv_pending.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (cd.isConnectingToInternet()) {
            getCompletedListFromServer();
        }

        return mainView;
    }


    private void getCompletedListFromServer() {

        arrCompletedListModel = new ArrayList<>();
        completedListModelArrayList = new ArrayList<>();

        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setMessage("Getting history, please wait...");
        pd.show();
        retrofitClient.getService().getReceivehistory(objDatabaseHelper.getAuthToken(), false).enqueue(new Callback<received_history_respo>() {
            @Override
            public void onResponse(Call<received_history_respo> call, Response<received_history_respo> response) {

                if (response.body().getSuccess()) {
                    pd.dismiss();
                    textViewNoDataFound.setVisibility(TextView.GONE);
                    arrCompletedListModel = response.body().getList();

                    if (arrCompletedListModel.size() > 0) {
                        for (int i = 0; i < arrCompletedListModel.size(); i++) {
                            if (arrCompletedListModel.get(i).getId() == null) {
                                arrCompletedListModel.get(i).setId(0);
                            }
                            CompletedListModel completedListModel = new CompletedListModel(arrCompletedListModel.get(i).getId(),
                                    arrCompletedListModel.get(i).getQuantity(), arrCompletedListModel.get(i).getRecQuantity(),
                                    arrCompletedListModel.get(i).getCompleted(), arrCompletedListModel.get(i).getCreatedAt(),
                                    arrCompletedListModel.get(i).getPONumber(), arrCompletedListModel.get(i).getPODate(),
                                    arrCompletedListModel.get(i).getInvoiceNumber(), arrCompletedListModel.get(i).getInvoiceDate(),
                                    arrCompletedListModel.get(i).getUuid());
                            completedListModelArrayList.add(completedListModel);
                        }

                        Collections.sort(completedListModelArrayList, new Comparator<CompletedListModel>() {
                            public int compare(CompletedListModel o1, CompletedListModel o2) {
                                return o2.getInvoice_date().compareTo(o1.getInvoice_date());
                            }
                        });

                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(completedListModelArrayList);
                        rv_pending.setAdapter(shipmentAdapter);
                    } else {
                        pd.dismiss();
                        textViewNoDataFound.setVisibility(TextView.VISIBLE);
                    }
                } else {
                    pd.dismiss();
                    textViewNoDataFound.setVisibility(TextView.VISIBLE);
                    // Utils.showToast(getActivity(), response.body().getMessage());
                }
            }


            @Override
            public void onFailure(Call<received_history_respo> call, Throwable t) {
                Log.d(Constants.TAG, "onFailure: ");
                pd.dismiss();
            }
        });

    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {

        private List<CompletedListModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textViewInNo;
            private AppCompatTextView textViewPoNo;
            private AppCompatTextView textViewInDate;
            private AppCompatTextView textViewPoDate;
            private AppCompatTextView textViewQuantity;
            private AppCompatTextView textViewPending;
            private LinearLayout ael;
            private LinearLayout LinShortfall;
            private AppCompatTextView tvMore;
            private AppCompatButton btnShortfall;

            public MyViewHolder(View view) {
                super(view);
                textViewInNo = view.findViewById(R.id.textView_in_no);
                textViewInDate = view.findViewById(R.id.textView_in_date);
                textViewQuantity = view.findViewById(R.id.textView_quantity);
                textViewPending = view.findViewById(R.id.textView_pending);
                textViewPoNo = view.findViewById(R.id.textView_po_no);
                textViewPoDate = view.findViewById(R.id.textView_po_date);
                ael = view.findViewById(R.id.expandable_ans);
                LinShortfall = view.findViewById(R.id.LinShortfall);
                tvMore = view.findViewById(R.id.tvMore);
                btnShortfall = view.findViewById(R.id.btnShortfall);

            }
        }


        public ShipmentAdapter(List<CompletedListModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_received_history_pending, parent, false);
            return new MyViewHolder(itemView);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final CompletedListModel object = notesList.get(position);

            try {
                holder.textViewPoNo.setText(object.getPO_number());
                holder.textViewInNo.setText(object.getInvoice_number());
                holder.textViewPoDate.setText(getDate(getDisplayDate(object.getPO_date()), "yyyy-MM-dd", "dd-MM-yyyy"));
                holder.textViewInDate.setText(getDate(getDisplayDate(object.getInvoice_date()), "yyyy-MM-dd", "dd-MM-yyyy"));
                holder.textViewQuantity.setText(String.valueOf(object.getQuantity()));
                holder.textViewPending.setText(String.valueOf(object.getRec_quantity()));
                String USER_TYPEs = Utils.getPreference(getContext(), PreferenceKeys.USER_TYPE, "");
                if (object.getRec_quantity() != 0 && object.getRec_quantity() < object.getQuantity() && (USER_TYPEs.equals(Constants.USER_TYPE_BRANCH) || USER_TYPEs.equals(Constants.USER_TYPE_DISTRIBUTOR) || USER_TYPEs.equals(Constants.USER_TYPE_DEALER))) {
                    holder.LinShortfall.setVisibility(View.VISIBLE);

                    holder.btnShortfall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (cd.isConnectingToInternet()) {
                                Bundle bundle = new Bundle();
                                bundle.putString("bill_no", object.getInvoice_number());
                                bundle.putString("bill_date", object.getInvoice_number());
                                bundle.putInt("qty", object.getQuantity());
                                bundle.putInt("pending_qty", object.getRec_quantity());
                                bundle.putString("ship_id", object.getUuid());
                                changeFragmentTo(Shortfall.class.getSimpleName(), true, bundle);
                            }
                        }
                    });
                }

                holder.tvMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        holder.ael.setVisibility(holder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                        if (holder.ael.getVisibility() == View.VISIBLE) {
                            holder.tvMore.setText("Less Details");
                        } else {
                            holder.tvMore.setText("More Details");
                        }
                    }
                });


            } catch (NullPointerException e) {

            }


        }

        @Override
        public int getItemCount() {
            return notesList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }


    private class CompletedListModel {
        private int id;
        private int quantity;
        private int rec_quantity;
        private boolean completed;
        private String created_at = "";
        private String PO_number = "";
        private String PO_date = "";
        private String Invoice_number = "";
        private String Invoice_date;
        private String uuid;


        public CompletedListModel(int id, int quantity, int rec_quantity, boolean completed, String created_at, String PO_number, String PO_date, String invoice_number, String invoice_date,
                                  String UUID) {
            this.id = id;
            this.quantity = quantity;
            this.rec_quantity = rec_quantity;
            this.completed = completed;
            this.created_at = created_at;
            this.PO_number = PO_number;
            this.PO_date = PO_date;
            Invoice_number = invoice_number;
            Invoice_date = invoice_date;
            this.uuid = UUID;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getRec_quantity() {
            return rec_quantity;
        }

        public void setRec_quantity(int rec_quantity) {
            this.rec_quantity = rec_quantity;
        }

        public boolean isCompleted() {
            return completed;
        }

        public void setCompleted(boolean completed) {
            this.completed = completed;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getPO_number() {
            return PO_number;
        }

        public void setPO_number(String PO_number) {
            this.PO_number = PO_number;
        }

        public String getPO_date() {
            return PO_date;
        }

        public void setPO_date(String PO_date) {
            this.PO_date = PO_date;
        }

        public String getInvoice_number() {
            return Invoice_number;
        }

        public void setInvoice_number(String invoice_number) {
            Invoice_number = invoice_number;
        }

        public String getInvoice_date() {
            return Invoice_date;
        }

        public void setInvoice_date(String invoice_date) {
            Invoice_date = invoice_date;
        }

        public String getUuid() {
            return uuid;
        }
    }

}
