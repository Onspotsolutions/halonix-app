package com.halonix.onspot.view.ReceiveNewShipment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.halonix.onspot.R;
import com.halonix.onspot.contributors.PendingShipments_respo;
import com.halonix.onspot.contributors.codelist_respo;
import com.halonix.onspot.contributors.pending_shipmentsLIst;
import com.halonix.onspot.model.DatabaseHelper;
import com.halonix.onspot.model.PendingShipmentListModel;
import com.halonix.onspot.network.ConnectionDetectorActivity;
import com.halonix.onspot.retrofit.RetrofitClient;
import com.halonix.onspot.utils.Constants;
import com.halonix.onspot.utils.PreferenceKeys;
import com.halonix.onspot.utils.Utils;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RSByIndividualProductActivity extends AppCompatActivity {

    @BindView(R.id.rv_pending)
    RecyclerView rv_pending;

    @BindView(R.id.tv_nopending)
    AppCompatTextView tv_nopending;

    @BindView(R.id.tv_check_connection)
    TextView tv_check_connection;


    @BindView(R.id.textView_no)
    TextView textView_no1;

    @BindView(R.id.txt1)
    TextView txt1;

    @BindView(R.id.textView_date)
    TextView textView_date;

    @BindView(R.id.textView_quantity)
    TextView textView_quantity;


    @BindView(R.id.textView_pending)
    TextView textView_pending;

    @BindView(R.id.linHeader)
    LinearLayout linHeader;


    @BindView(R.id.no_internet)
    RelativeLayout no_internet;

    private RetrofitClient retrofitClient;
    ArrayList<pending_shipmentsLIst> pendingShipmentListModelArrayList;
    ArrayList<PendingShipmentListModel> pendingShipmentListModelArrayList1;
    ArrayList<String> stringArrayList;
    DatabaseHelper databaseHelper;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private long mLastClickTime = 0;
    ConnectionDetectorActivity cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_shipment_individual_product);
        ButterKnife.bind(this);
        databaseHelper = DatabaseHelper.getInstance(RSByIndividualProductActivity.this);

        retrofitClient = new RetrofitClient();
        rv_pending.setHasFixedSize(true);
        rv_pending.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_pending.setLayoutManager(llm);

        cd = new ConnectionDetectorActivity(RSByIndividualProductActivity.this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            onBackPressed();
            overridePendingTransition(0, 0);

        });

        toolbar.setTitle("Scan Individual Products");

        if (cd.isConnectingToInternet()) {
            getPendingShipmentList();
        } else {
            showNetworkFailDialog(Constants.NO_INTERNET_MSG);
        }

    }

    private void showNetworkFailDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RSByIndividualProductActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();

        View content = inflater.inflate(R.layout.network_failure_dialog, null);

        builder.setView(content);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView tvMsg = content.findViewById(R.id.networkFailMsg);
        tvMsg.setText(msg);
        content.findViewById(R.id.btnNetworkFailureOK).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
    }


    private void getPendingShipmentList() {
        pendingShipmentListModelArrayList = new ArrayList<>();
        pendingShipmentListModelArrayList1 = new ArrayList<>();
        Utils.showprogressdialog(RSByIndividualProductActivity.this, "Getting pending shipments,please wait.");
        retrofitClient.getService().getPendingShipmentList(databaseHelper.getAuthToken()).enqueue(new Callback<PendingShipments_respo>() {
            @Override
            public void onResponse(Call<PendingShipments_respo> call, Response<PendingShipments_respo> response) {
                Log.d(Constants.TAG, "onResponse getPendingShipmentList: " + response.toString());
                if (response.body().getSuccess()) {
                    Utils.dismissprogressdialog();
                    pendingShipmentListModelArrayList = response.body().getList();

                    if (pendingShipmentListModelArrayList.size() > 0) {


                        if (Utils.getPreference(RSByIndividualProductActivity.this, PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_DISTRIBUTOR) ||
                                Utils.getPreference(RSByIndividualProductActivity.this, PreferenceKeys.USER_TYPE, "").equals(Constants.USER_TYPE_RETAILER) ||
                                Utils.getPreference(RSByIndividualProductActivity.this, PreferenceKeys.USER_TYPE, "").equalsIgnoreCase(Constants.USER_TYPE_DEALER)) {

                            for (int i = 0; i < pendingShipmentListModelArrayList.size(); i++) {
                                PendingShipmentListModel shipmentListModel = new PendingShipmentListModel(String.valueOf(pendingShipmentListModelArrayList.get(i).getQuantity()),
                                        pendingShipmentListModelArrayList.get(i).getInvoiceNumber(), pendingShipmentListModelArrayList.get(i).getInvoiceDate(),
                                        String.valueOf(pendingShipmentListModelArrayList.get(i).getPending()), pendingShipmentListModelArrayList.get(i).getUuid());

                                pendingShipmentListModelArrayList1.add(shipmentListModel);
                            }
                        } else {
                            for (int i = 0; i < pendingShipmentListModelArrayList.size(); i++) {
                                PendingShipmentListModel shipmentListModel = new PendingShipmentListModel(String.valueOf(pendingShipmentListModelArrayList.get(i).getQuantity()),
                                        pendingShipmentListModelArrayList.get(i).getInvoiceNumber(), pendingShipmentListModelArrayList.get(i).getInvoiceDate(),
                                        String.valueOf(pendingShipmentListModelArrayList.get(i).getPending()), pendingShipmentListModelArrayList.get(i).getShipmentId());

                                pendingShipmentListModelArrayList1.add(shipmentListModel);
                            }
                        }

                        ShipmentAdapter shipmentAdapter = new ShipmentAdapter(pendingShipmentListModelArrayList1);
                        rv_pending.setAdapter(shipmentAdapter);
                    }

                    if (pendingShipmentListModelArrayList1.size() > 0) {
                        tv_nopending.setVisibility(View.GONE);
                        linHeader.setVisibility(View.GONE);
                    } else {
                        tv_nopending.setVisibility(View.VISIBLE);
                        linHeader.setVisibility(View.GONE);
                    }
                }


            }

            @Override
            public void onFailure(Call<PendingShipments_respo> call, Throwable t) {
                Utils.dismissprogressdialog();


            }
        });

    }


    public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.MyViewHolder> {
        private ArrayList<PendingShipmentListModel> notesList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textView_no;
            private AppCompatTextView textView_date;
            private AppCompatTextView textView_quantity;
            private AppCompatTextView textView_pending;
            private AppCompatImageView textView_scan;
            private AppCompatImageView tvMore;
            private LinearLayout ael;

            public MyViewHolder(View view) {
                super(view);
                textView_no = view.findViewById(R.id.textView_no);

                textView_date = view.findViewById(R.id.textView_date);
                textView_quantity = view.findViewById(R.id.textView_quantity);
                textView_pending = view.findViewById(R.id.textView_pending);
                textView_scan = view.findViewById(R.id.textView_scan);
                tvMore = view.findViewById(R.id.tvMore);
                ael = view.findViewById(R.id.expandable_ans);
            }
        }


        public ShipmentAdapter(ArrayList<PendingShipmentListModel> notesList) {
            this.notesList = notesList;
        }


        @Override
        public ShipmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            v = layoutInflater.inflate(R.layout.viewholder_pending_scan_item, parent, false);
            return new MyViewHolder(v);
        }


        private String getDisplayDate(String created_at) {
            try {
                return created_at.split("T")[0];
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        public void onBindViewHolder(ShipmentAdapter.MyViewHolder mHolder, final int position) {

            final PendingShipmentListModel object = notesList.get(position);


            mHolder.tvMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mHolder.ael.setVisibility(mHolder.ael.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                    if (mHolder.ael.getVisibility() == View.VISIBLE) {
                        mHolder.tvMore.setImageDrawable(getResources().getDrawable(R.drawable.ic_up_arrow));
                    } else {
                        mHolder.tvMore.setImageDrawable(getResources().getDrawable(R.drawable.ic_down_arrow));

                    }
                }
            });

            mHolder.textView_date.setText(getDisplayDate(object.getInvoice_date()));
//            mHolder.textView_date.measure(0, 0);
//            int textView_datewidth1 = mHolder.textView_date.getMeasuredWidth();
//            textView_date.measure(0, 0);
//            int textView_datewidth2 = textView_date.getMeasuredWidth();
//
//            if (textView_datewidth2 > textView_datewidth1) {
//                mHolder.textView_date.setWidth(textView_datewidth2);
//            } else {
//                textView_date.setWidth(textView_datewidth1);
//            }
//
//            int textView_nowidth1;
            mHolder.textView_no.setText(object.getInvoice_number());

//            mHolder.textView_no.measure(0, 0);
//             textView_nowidth1 = mHolder.textView_no.getMeasuredWidth();
//            Log.d(Constants.TAG, "onCreateViewHolder textView_nowidth1: " + textView_nowidth1);
//
//            textView_no1.measure(0, 0);
//            int textView_nowidth2 = textView_no1.getMeasuredWidth();
//            Log.d(Constants.TAG, "onCreateViewHolder textView_nowidth2: " + textView_nowidth2);
//
//
//            if (textView_nowidth1 > textView_nowidth2) {
//                mHolder.textView_no.setWidth(textView_nowidth1);
//                textView_no1.setWidth(textView_nowidth1);
//            } else {
//                mHolder.textView_no.setWidth(textView_nowidth2);
//                textView_no1.setWidth(131);
//            }

            mHolder.textView_quantity.setText(object.getQuantity());
//            mHolder.textView_quantity.measure(0, 0);
//            int textView_quantitywidth1 = mHolder.textView_quantity.getMeasuredWidth();
//            textView_quantity.measure(0, 0);
//            int textView_quantitywidth2 = textView_quantity.getMeasuredWidth();
//
//            if (textView_quantitywidth2 > textView_quantitywidth1) {
//                mHolder.textView_quantity.setWidth(textView_quantitywidth2);
//            } else {
//                textView_quantity.setWidth(textView_quantitywidth1);
//            }

            mHolder.textView_pending.setText(object.getPending());
//            mHolder.textView_pending.measure(0, 0);
//            int textView_pendingwidth1 = mHolder.textView_pending.getMeasuredWidth();
//            textView_quantity.measure(0, 0);
//            int textView_pendingwidth2 = textView_pending.getMeasuredWidth();
//
//            if (textView_pendingwidth2 > textView_pendingwidth1) {
//                mHolder.textView_pending.setWidth(textView_pendingwidth2);
//
//            } else {
//                textView_pending.setWidth(textView_pendingwidth1);
//            }
//

            mHolder.textView_scan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cd.isConnectingToInternet()) {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        databaseHelper.deleteAllIPCodeList();
                        getpendinglistcodes(object.getShipment_id(), object.getInvoice_number(), object.getInvoice_date(),
                                object.getQuantity(), object.getPending());
                    } else {
                        showNetworkFailDialog(Constants.NO_INTERNET_MSG);
                    }
                }
            });
            txt1.measure(0, 0);
            int txt1width2 = txt1.getMeasuredWidth();
            mHolder.textView_scan.getLayoutParams().width = txt1width2;
/*
            mHolder.textView_scan.measure(0, 0);
            int textView_pendingwidth1 = mHolder.textView_scan.getMeasuredWidth();
            textView_quantity.measure(0, 0);
            int textView_pendingwidth2 = txt1.getMeasuredWidth();

            if (textView_pendingwidth2 > textView_pendingwidth1) {
                mHolder.textView_scan.setWidth(textView_pendingwidth2);
            } else {
                txt1.setWidth(textView_pendingwidth1);
            }*/


        }


        @Override
        public int getItemCount() {
            return notesList.size();

        }


    }

    private void getpendinglistcodes(String shipmentId, String invoice_number, String invoice_date, String quantity, String pedning) {
        stringArrayList = new ArrayList<>();
        Utils.showprogressdialog(RSByIndividualProductActivity.this, "Getting pending shipment codes,please wait.");
        retrofitClient.getService().getPendingShipmentListCodes(databaseHelper.getAuthToken(), shipmentId).enqueue(new Callback<codelist_respo>() {
            @Override
            public void onResponse(Call<codelist_respo> call, Response<codelist_respo> response) {

                if (response.body().getSuccess()) {
                    databaseHelper.deleteAllIPCode();
                    Utils.dismissprogressdialog();
                    if (response.body().getList().size() > 0) {
                        databaseHelper.insertIPCodeList(shipmentId, invoice_number, invoice_date,
                                quantity, pedning);
                        stringArrayList = response.body().getList();

                        for (int i = 0; i < stringArrayList.size(); i++) {
                            databaseHelper.insertIPCodes(stringArrayList.get(i));
                        }
                        Intent intent = new Intent(RSByIndividualProductActivity.this, RSByIndividualScannerActivity.class);
                        intent.putExtra("qty", pedning);
                       // intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        RSByIndividualProductActivity.this.finish();
                        startActivity(intent);
                        overridePendingTransition(0,0);

                    } else {
                        Utils.showToast(RSByIndividualProductActivity.this, "No pending shipment codes available.");
                    }
                }

            }

            @Override
            public void onFailure(Call<codelist_respo> call, Throwable t) {
                Utils.dismissprogressdialog();
            }
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
