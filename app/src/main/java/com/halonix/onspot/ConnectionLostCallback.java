package com.halonix.onspot;

public interface ConnectionLostCallback {
    public void connectionLost(boolean b);
}
