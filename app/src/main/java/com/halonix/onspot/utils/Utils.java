package com.halonix.onspot.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;


public class Utils {

    public static SharedPreferences mAppPreferences;
    public static SharedPreferences.Editor mEditor;
    private static ProgressDialog pd;


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getDate(
            String date, String currentFormat, String expectedFormat)
    {

        if (date == null || currentFormat == null || expectedFormat == null) {
            return null;
        }
        SimpleDateFormat sourceDateFormat = new SimpleDateFormat(currentFormat);
        Date dateObj = null;
        try {
            dateObj = sourceDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat desiredDateFormat = new SimpleDateFormat(expectedFormat);
        return desiredDateFormat.format(dateObj);
    }



    public static void showAlert(Context ctx, final InterfaceAlertDissmiss mListener, String Message, String Title) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle(Title);
        alert.setCancelable(false);
        alert.setMessage(Message);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onPositiveButtonClick();
            }
        });
        alert.show();
    }

    public static void showprogressdialog(Context ctx, String Msg) {
        pd = new ProgressDialog(ctx);
        pd.setCancelable(false);
        pd.setMessage(Msg);
        pd.show();
    }


    public static void dismissprogressdialog() {
        if (pd.isShowing()) {
            pd.dismiss();
        }
    }

    public static void showToast(Context ctx, String Message) {
        Toast.makeText(ctx, Message, Toast.LENGTH_SHORT).show();
    }

    public static String addPreference(Context context, String pref_field, String pref_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.putString(pref_field, pref_value);
        mEditor.commit();
        return mAppPreferences.getString(pref_field, null);
    }


    public static String removePreference(Context context, String pref_field) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.remove(pref_field);
        mEditor.commit();
        return null;
    }


    public static int addPreferenceInt(Context context, String pref_field, int pref_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.putInt(pref_field, pref_value);
        mEditor.commit();
        return mAppPreferences.getInt(pref_field, 0);
    }

    public static boolean addPreferenceBoolean(Context context, String pref_field, Boolean pref_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.putBoolean(pref_field, pref_value);
        mEditor.commit();
        return mAppPreferences.getBoolean(pref_field, false);
    }

    public static String getPreference(Context context, String pref_field, String def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getString(pref_field, def_value);
    }

    public static int getPreferenceInt(Context context, String pref_field, int def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getInt(pref_field, def_value);
    }

    public static boolean getPreferenceBoolean(Context context, String pref_field, Boolean def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getBoolean(pref_field, false);
    }


    public static void AlertDialog(final Context context, String Title, String Message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }


    /**
     * @param message = Message to be displayed in logcat
     */
    public static void out(String message) {
        boolean printLogs = true;
        if (printLogs) {
            int maxLogSize = 4000;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                System.out.println(message.substring(start, end));
            }
        }
    }


    public static String getCurrentDate() {
//        return new SimpleDateFormat("dd/mm/yyyy hh:mm a").format(new Date()).toString();
        Utils.out("UTILS CURRENT DATE : " + new SimpleDateFormat("yyyy-MMM-dd").format(new Date()));
        return new SimpleDateFormat("yyyy-MM-dd hh:mm a").format(new Date());
    }


    public static String getCurrentTime() {
//        return new SimpleDateFormat("dd/mm/yyyy hh:mm a").format(new Date()).toString();
        Utils.out("UTILS CURRENT TIME : " + new SimpleDateFormat("hh:mm a").format(new Date()));
        return new SimpleDateFormat("hh:mm a").format(new Date());
    }


    public interface InterfaceAlertDissmiss {
        void onPositiveButtonClick();
    }
}
