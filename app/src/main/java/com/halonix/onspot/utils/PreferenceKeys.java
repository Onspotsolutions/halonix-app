package com.halonix.onspot.utils;

public class PreferenceKeys {

    public static final String USERNAMEID = "app_user_id";
    public static final String USER_TYPE = "app_user_type";
    public static final String USER_SHIPMENT_TYPE= "shipment_types";

}
