package com.halonix.onspot.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class custom_edittext extends AppCompatEditText {

    public custom_edittext(Context context) {
        super(context);
        setFont();
    }

    public custom_edittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public custom_edittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/MyriadProRegular.ttf");
        setTypeface(typeface);

    }

    @Override
    public int getSelectionStart() {
        for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
            if (element.getMethodName().equals("canPaste")) {
                return -1;
            }
        }
        return super.getSelectionStart();
    }


}
