package com.halonix.onspot.utils;


public class Constants {
    public static final String TAG = "suraj";
    public static final String URL = "https://halonix.onspotsolutions.com/";
    public static final int REQUEST_CODE_FOR_CONTINUES_BARCODE_SCAN = 1000;
    public static final String USER_TYPE_VENDOR = "Vendor";
    public static final String USER_TYPE_BRANCH = "Branch";
    public static final String USER_TYPE_DISTRIBUTOR = "Distributor";
    public static final String USER_TYPE_DEALER = "Dealer";
    public static final String USER_TYPE_RETAILER = "Retailer";
    public static final String USER_TYPE_CC = "CorporateClient";

    public static final String MobileregEx = "(^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$)";
    public static final String OTP_REGEX = "^[a-zA-Z0-9 ]{6}$";

    public static final String NO_INTERNET_MSG = "No internet connection.";
    public static String API_SIGN_IN_MOB_NUM = "mobile_number";
    public static String OTP_VER_VALUE = "verify_key";
    public static String OTP_MOB_VALUE = "mobile_number";
    public static String OTP_USER_EXIST_VALUE = "clicked_on_existing";
}
